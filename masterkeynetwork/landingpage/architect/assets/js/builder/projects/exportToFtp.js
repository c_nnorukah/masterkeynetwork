'use strict'

angular.module('builder.projects')

.directive('blOpenPublishModal', ['$http', '$state', 'project', function($http, $state, project) {
    return {
        restrict: 'A',
        link: function($scope, el, attrs) {
        	var modal = $('#publish-modal');

        	el.on('click', function(e) {
        		modal.data('id', attrs.blOpenPublishModal).modal('show');
        	});
        }
    };
}])


.directive('blExportToFtp', ['$http', '$state', 'project', function($http, $state, project) {
    return {
        restrict: 'A',
        link: function($scope, el) {
        	var	loader = el.find('.loader'),
        		error  = el.find('.error');

        	el.find('.publish').on('click', function(e) {
        		loader.show();

        		if ($state.current.name === 'dashboard') {
        			var id = el.data('id');
        		} else {
        			var id = project.active.id;
        		}

        		$http.post('export/project-ftp/'+id, $scope.publishCredentials).success(function(data) {
        			error.text('').hide();
        			el.modal('hide');
        			alertify.log('Published project to ftp successfully.', 'success', 2200);
        		}).error(function(data) {
        			error.text(data).show();
        		}).finally(function(data) {
        			loader.hide();
        		});

        	});

        	el.find('.close-modal').on('click', function(e) {
        		el.modal('hide');
        	});
        }
    };
}]);

