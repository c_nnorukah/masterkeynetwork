<?php namespace Builder\Install;

use Exception;
use Builder\Settings\SettingModel;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Capsule\Manager as Capsule;

class UpdateController
{	
	/**
	 * Silex application instance.
	 * 
	 * @var Silex\Application
	 */
	private $app;

	public function __construct($app)
	{
		$this->app = $app;
	}

	public function index()
	{
		if (Capsule::schema()->hasTable('settings')) {
			$updateVersion = SettingModel::where('name', 'update_version')->first();

			if ($updateVersion && $updateVersion->value == $this->app['version']) {
				return $this->app->redirect('/');
			}
		}

		return $this->app['twig']->render('install/update.html');
	}

	public function runUpdate()
	{
		if (Capsule::schema()->hasTable('settings')) {
			$updateVersion = SettingModel::where('name', 'update_version')->first();

			if ($updateVersion && $updateVersion->value == $this->app['version']) {
				return $this->app->redirect('/');
			}
		}

		try {
			ini_set('max_execution_time', 0);

			if ( ! Capsule::schema()->hasTable('settings')) {
				Capsule::schema()->create('settings', function($table)
		        {
		            $table->increments('id');
		            $table->string('name');
		            $table->text('value');
		            $table->timestamps();

		            $table->engine = 'InnoDB';
		            $table->unique('name');
		        });
			}

	        SettingModel::insert(array(
	        	array('name' => 'enable_registration', 'value' => 1),
	        	array('name' => 'update_version', 'value' => $this->app['version']),
	        	array('name' => 'permissions', 'value' => '{"templates.delete":1,"templates.update":1,"templates.create":1,"users.delete":0,"users.update":0,"export":1,"publish":1,"projects.create":1,"projects.update":1,"projects.delete":1,"themes.create":1,"themes.update":1,"themes.delete":1,"users.create":0}')
	        ));

		} catch (Exception $e) {
			//
		}
		
		return $this->app->redirect('/');
	}
}