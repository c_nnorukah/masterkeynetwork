<?php namespace Builder\Projects;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LandingPageModel extends Eloquent {

    protected $guarded = array('list_id');
    protected $fillable = array('name','project_id');
    
	protected $table = 'landingpage_list';

   	public function pageable()
    {
        return $this->morphTo();
    }
}