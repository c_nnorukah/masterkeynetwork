<?php namespace Builder\Projects;

use Exception;
use Illuminate\Database\Eloquent\Model as Eloquent;

class NameAlreadyExistsException extends Exception {}

class LandingpageListModel extends Eloquent {

	protected $table = 'landingpage_list';

	protected $morphClass = 'LandingpageList';
}