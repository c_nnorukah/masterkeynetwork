/*!
 * jQuery Main Plugin
 *
 */
(function( $ ) {

	$.fn.confirmDeletePopupOpen = function(url) {
		$("#confirmDelete").modal("show");
		$("#confirmDelete").find(".delete-button").prop("href", url);
	};

	$.fn.confirmDeletePopupClose = function() {
		$("#confirmDelete").modal("hide");
	};

	messageDialogBoxPopupOpen = function(msg) {
		$("#messageDialogBox").modal("show");
		$("#messageDialogBox").find(".modal-body").find("p").html(msg);
	};

}( jQuery ));