$(document).ready(function() {

    if($('#text-editor').length > 0) {
        $('#text-editor').redactor();

        /* Ace Editor no longer used
        var editor = ace.edit("editor");
        editor.setOptions({ maxLines: Infinity });
        editor.setTheme("ace/theme/twilight");
        editor.session.setMode("ace/mode/javascript");

        var editor1 = ace.edit("css-editor");
        editor1.setOptions({ maxLines: Infinity });
        editor1.setTheme("ace/theme/twilight");
        editor1.session.setMode("ace/mode/css");*/
    }

    if($('#landingPageTable').length > 0) {
        //$('#landingPageTable').orderTable();
    }

    if($("#landingPagePreview").length > 0) {

        $(".landing-page-preview").each(function() {
            //$(this).load(app_url+ "admin/landing-page/");
        });

        $(".btn-delete").off("click");
        $(".btn-delete").on("click", function(e) {
            var deleteUrl = $(this).data("row-url");
            $(this).confirmDeletePopupOpen(deleteUrl);
        });

        $(".btn-preview").off("click");
        $(".btn-preview").on("click", function() {
            var landingPageId = $(this).data("row-id");
            $.ajax({
                url     : app_url + "admin/landing-page/get-content/" + landingPageId,
                method  : 'GET',
                success : function(resp) {
                    $("#landingPagePreview").modal("show");
                    $("#htmlPreview").html(resp);
                }
            });
        });

        $("#search-landing-page").off("click");
        $("#search-landing-page").on("click", function() {
            var searchTag = $("#search-landing-page-value").val();
            if(searchTag!="") {
                window.location = app_url + "admin/landing-page/search/" + encodeURI(searchTag);
            }
        });
    }

    if($(".az-container").length > 0) {

    }


});