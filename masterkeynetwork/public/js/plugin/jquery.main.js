/*!
 * jQuery Main Plugin
 *
 */
(function( $ ) {

	$.fn.confirmDeletePopupOpen = function(url) {
		$("#confirmDelete").modal("show");
		$("#confirmDelete").find(".delete-button").prop("href", url);
	};

	$.fn.confirmDeletePopupClose = function() {
		$("#confirmDelete").modal("hide");
	};

}( jQuery ));