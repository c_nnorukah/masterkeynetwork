/* Webarch Admin Dashboard
 /* This JS is only for DEMO Purposes - Extract the code that you need
 -----------------------------------------------------------------*/
$(document).ready(function() {


    $('.select2', "#form_traditional_validation").change(function () {
        $('#form_traditional_validation').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    //Iconic form validation sample
    $('#form_iconic_validation').validate({
        errorElement: 'span',
        errorClass: 'error',
        focusInvalid: false,
        ignore: "",
        rules: {
            form1Name: {
                minlength: 2,
                required: true
            },
            form1Email: {
                required: true,
                email: true
            },
            form1Url: {
                required: true,
                url: true
            },
            gendericonic:{
                required: true
            }
        },

        invalidHandler: function (event, validator) {
            //display error alert on form submit
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-with-icon').children('i');
            var parent = $(element).parent('.input-with-icon');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
            parent.removeClass('success-control').addClass('error-control');
        },

        highlight: function (element) { // hightlight error inputs
            var parent = $(element).parent();
            parent.removeClass('success-control').addClass('error-control');
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            var icon = $(element).parent('.input-with-icon').children('i');
            var parent = $(element).parent('.input-with-icon');
            icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
            parent.removeClass('error-control').addClass('success-control');
        },

        submitHandler: function (form) {

        }

    });
    $('.select2', "#form_iconic_validation").change(function () {
        $('#form_iconic_validation').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    //Form Condensed Validation
    $('#form-condensed').validate({
        errorElement: 'span',
        errorClass: 'error',
        focusInvalid: false,
        ignore: "",
        rules: {
            form3FirstName: {
                minlength: 3,
                required: true
            },
            form3LastName: {
                minlength: 3,
                required: true
            },
            form3Gender: {
                required: true,
            },
            form3DateOfBirth: {
                required: true,
            },
            form3Occupation: {
                minlength: 3,
                required: true,
            },
            form3Email: {
                required: true,
                email: true
            },
            form3Address: {
                minlength: 10,
                required: true,
            },
            form3City: {
                minlength: 5,
                required: true,
            },
            form3State: {
                minlength: 3,
                required: true,
            },
            form3Country: {
                minlength: 3,
                required: true,
            },
            form3PostalCode: {
                number: true,
                maxlength: 4,
                required: true,
            },
            form3TeleCode: {
                minlength: 3,
                maxlength: 4,
                required: true,
            },
            form3TeleNo: {
                maxlength: 10,
                required: true,
            },
        },

        invalidHandler: function (event, validator) {
            //display error alert on form submit
        },

        errorPlacement: function (label, element) { // render error placement for each input type
            $('<span class="error"></span>').insertAfter(element).append(label)
        },

        highlight: function (element) { // hightlight error inputs

        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {

        },

        submitHandler: function (form) {

        }
    });

    //Form Wizard Validations
    var $validator = $("#registerform").validate({
        rules: {
            emailfield: {
                required: true,
                email: true,
                minlength: 3
            },
            txtFullName: {
                required: true,
                minlength: 3
            },
            txtFirstName: {
                required: true,
                minlength: 3
            },
            txtLastName: {
                required: true,
                minlength: 3
            },
            txtCountry: {
                required: true,
                minlength: 3
            },
            txtPostalCode: {
                required: true,
                minlength: 3
            },
            txtPhoneCode: {
                required: true,
                minlength: 3
            },
            txtPhoneNumber: {
                required: true,
                minlength: 3
            },
            urlfield: {
                required: true,
                minlength: 3,
                url: true
            }
        },
        errorPlacement: function(label, element) {
            $('<span class="arrow"></span>').insertBefore(element);
            $('<span class="error"></span>').insertAfter(element).append(label)
        }
    });

//reg-submit
    $('#rootwizard').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#rootwizard').bootstrapWizard({
        'tabClass': 'form-wizard',
        'onNext': function(tab, navigation, index) {
            var temp = $("#txtEmail:visible").length;
            var $valid = $("#registerform").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            else{

                if(index!=undefined && index==2){
                    $('li#next').hide();
                }else if(index>0){
                    $('li#next').show();
                    $('li#previous').show();
                }else{
                    $('li#next').show();
                }
                $('#rootwizard').find('.form-wizard').children('li').eq(index-1).addClass('complete');
                $('#rootwizard').find('.form-wizard').children('li').eq(index-1).find('.step').html('<i class="fa fa-check"></i>');

                if(index == 2) {
                    $('#rootwizard').find('.tab-content.transparent').find('.wizard-actions').children('li').eq(4).removeClass("hidden");
                    setTimeout(function (){
                        $('#rootwizard').find('.tab-content.transparent').find('.wizard-actions').children('li').eq(3).addClass("hidden");
                    },5);
                }
            }
        },
        onPrevious: function(tab, navigation, index) {
            if(index!=undefined && index==0){
                $('li#previous').hide();
            }else if(index>0){
                // alert(index);
                $('li#next').show();
                $('li#previous').show();
            }else{
                $('li#previous').show();
            }
            setTimeout(function (){
                $('#rootwizard').find('.tab-content.transparent').find('.wizard-actions').children('li').eq(3).addClass("hidden");
                $('#rootwizard').find('.tab-content.transparent').find('.wizard-actions').children('li').eq(2).removeClass("hidden");
            },5);
        },
        onTabClick: function(tab, navigation, index) {
            var tabClass = $('#rootwizard').find('.form-wizard').children('li').eq(index).attr('class');

            if(tabClass != 'complete') {
                return false;
            }
        },
        onTabShow: function(tab, navigation, index) {
            // Update the label of Next button when we are at the last tab
            var numTabs = $('#rootwizard').find('.tab-content.transparent').find('.tab-pane').length;
            if(index == 1)
            {
                $('#txtEmail').focus();
            }
            if(index == 2){
                $('#country_id').focus();
            }

            $('#registerform')
                .find('.next')
                .removeClass('disabled')    // Enable the Next button
                .find('a')
                .html(index === numTabs - 1 ? 'Submit' : 'Next');
        },
        onInit: function(tab, navigation, index){
            if(index!=undefined && index==0){
                $('li#previous').hide();
            }
        }
    });
    $(".search_cars").submit(function (e) {

        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            'z-index' :'99999999999'
        } });
    });


});
