$(document).ready(function () {
    $(document).on('click','.pager a', function (event) {
        event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
             $("html, body").animate({ scrollTop: 0 }, "fast");

            $("#modal").load($(this).attr('href'));

        }
    });
    $(".response_modal").click(function () {
        var id = $(this).attr('data-id');
        $.ajax({
            url: base_url + 'landing-page/response-details/' + id,
            type: 'GET',
            success: function (data) {
                $("#modal").html(data);
            },
        });
    });
    $(document).off("submit", "#lead-search");
    $(document).on("submit", "#lead-search", function (e) {
        e.preventDefault();
        if ($(".user_list").length > 0 && $(".user_list").val() == "") {
            alert("Please select the user first");
        } else {
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                success: function (data) {
                    $(".lead-status-data").html($(data).find(".lead-status-data").html())
                }
            })
        }
    })


});