$(document).ready(function () {
    form_validate("#user_search");
    manage_search_div();
    $(document).on("change", ".search_criteria", function () {
        manage_search_div();
    })
    $("#user_search").on("submit", function (e) {
        e.preventDefault();
        url = $(this).attr("action");
        var fd = new FormData(this);
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            data: fd,
            success: function (data) {
                $(".ajax-table").html($(data).find(".ajax-table").html());
            }
        });
    })
    $(document).off("click", ".user_delete");
    $(document).on("click", ".user_delete", function (e) {
        e.preventDefault();
        $this = $(this);
        action = confirm("Are you sure want to delete?");
        if (action) {
            var url = $this.attr("href");
            $.get(url, function (data) {
                url = redirect_url + "/" + $("#sort_by").val() + "/" + $("#sort_order").val() + "/" + $("#limit").val()
                $.get(url, function (data) {
                    $(".ajax-table").html($(data).find(".ajax-table").html());
                    $("#flash_msg .msg").html("User deleted successfully.");
                    $("#flash_msg").show();
                    scrollto("flash_msg")
                })
            })
        }
    })
});
function manage_search_div(){
    val = $(".search_criteria").find("option:selected").val();
    $(".search_criteria_div").hide();
    $(".search_container").find("label.error").remove();
    $("." + val + "_div").removeClass("hide").show();
}