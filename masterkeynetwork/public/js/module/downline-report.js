$(document).ready(function () {
    $(document).on("submit", "#downline-search", function (e) {
        e.preventDefault();
        if ($(".user_list").length > 0 && $(".user_list").val() == "") {
            alert("Please select the user first");
        } else {
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                success: function (data) {
                    $(".downline-report").html($(data).find(".downline-report").html())
                    init_tree();
                }
            })
        }
    })
    init_tree();
});

function init_tree(){
    var level1 = $.parseJSON($("#first_level").val())
    var level2 = $.parseJSON($("#second_level").val())
    var level3 = $.parseJSON($("#third_level").val())
    if (level1.length > 0) {
        $('#level1').treeview({
            data: level1,
            levels: 5,
            showTags: true,
            nodeIcon: 'glyphicon glyphicon-user',
        });
    }
    if (level2.length > 0) {
        $('#tree').treeview({
            data: level2,
            levels: 5,
            showTags: true,
            nodeIcon: 'glyphicon glyphicon-user',
        });
    }
    if (level3.length > 0) {
        $('#level3').treeview({
            data: level3,
            levels: 5,
            showTags: true,
            nodeIcon: 'glyphicon glyphicon-user',
        });
    }
}