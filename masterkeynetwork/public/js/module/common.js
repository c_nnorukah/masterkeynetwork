$(document).ready(function () {
    init_plugin()
    $(".my-accordion").on("mouseover", ".panel-heading", function () {
        $(this).find("a").addClass("white-color");
    })
    $(".my-accordion").on("mouseout", ".panel-heading", function () {
        $(this).find("a").removeClass("white-color");
    })
    $(document).off("click", ".sorting, .pagination a,.ajax-link");
    $(document).on("click", ".sorting, .pagination a,.ajax-link", function (e) {
        e.preventDefault();
        if ($(this).hasClass("reset-link")) {
            $("form#user_search,form#contact_search").find("select,[type='text']").val('');
            $(".search_conditions").find(".search_criteria_div").hide();
            $("form#user_search,form#contact_search").find(".error").remove();
        }
        url = $(this).hasClass("sorting") ? $(this).find("a").attr("href") : $(this).attr("href");
        $.get(url, function (data) {
            $(".ajax-table").html($(data).find(".ajax-table").html());
        })
    })
    $(document).off("change", ":file");
    $(document).on("change", ":file", function () {
        size = this.files[0].size;
        if (size == 0) {
            $(this).val("");
            alert("Selected file is empty. Please upload another file.")
            return false;

        }
    })

    $.validator.addMethod("editor_required", function (value, element) {
        value = value.replace(/<.*?>/g, '');
        if ($.trim(value) == "") {
            return false;
        }
        return true;
    }, "This field is required.");
    $.validator.addMethod("radio_required", function (value, element) {
        name = $(element).attr("name");
        return $("[name='" + name + "']:checked").length > 0;
    }, "This field is required.");

    $.validator.classRuleSettings = {
        editor_required: {
            editor_required: true
        },
        radio_required: {
            radio_required: true
        }
    }
    $(document).off("click", ".chat-menu-toggle");
    $(document).on("click", ".chat-menu-toggle", function (e) {
        e.preventDefault();
        if($(".downline-member-side-window").html() == "") {
            url = base_url + "get-downline-member";
            $.get(url, function (data) {
                $(".downline-member-side-window").html(data);
                var right_side_downline = $.parseJSON($("#right_side_downline_input").val())
                if (right_side_downline.length > 0) {
                    $('#right_side_downline').treeview({
                        data: right_side_downline,
                        levels: 5,
                        showTags: true,
                        nodeIcon: 'glyphicon glyphicon-user',
                        color: "#7A8188",
                        backColor: "#1b1e24",
                        onhoverColor: "#0F131D",
                        borderColor: "#1b1e24",
                        selectedColor: "#fff",
                        selectedBackColor: "#0F131D",
                        showBorder: false,
                    });
                }
            })
        }
    })
});
function init_plugin(element) {
    element = typeof element == "undefined" ? $(document) : element;
    init_datepicker(element)
    init_daterange(element)
}
function init_datepicker(element) {
    if (element.find('.datepicker').hasClass("current_date")) {
        element.find('.datepicker').datetimepicker({
            firstDay: 1,
            timeFormat: "hh:mm tt",
            changeMonth: true,
            changeYear: true,
            changeFirstDay: false,
            minDate: 0
        });
    } else {
        element.find('.datepicker').datepicker({
            firstDay: 1,
            changeMonth: true,
            changeYear: true,
            changeFirstDay: false,
            minDate: 0
        });
    }
}
function init_daterange(element) {
    element.find('#txtStartDate, #txtEndDate').datepicker({
        beforeShow: customRange,

        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        changeFirstDay: false
    });
}
function customRange(input) {
    var min = null,
        dateMin = min,
        dateMax = null;

    if (input.id === "txtStartDate") {
        if ($("#txtEndDate").datepicker("getDate") != null) {
            dateMax = $("#txtEndDate").datepicker("getDate");
            dateMin = $("#txtEndDate").datepicker("getDate");
            dateMin.setDate(dateMin.getDate());
            if (dateMin < min) {
                dateMin = min;
            }
        }
        else {
            dateMax = new Date; //Set this to your absolute maximum date
        }
        return {
            maxDate: dateMax
        };
    }
    else if (input.id === "txtEndDate") {
        dateMax = new Date; //Set this to your absolute maximum date
        if ($("#txtStartDate").datepicker("getDate") != null) {
            dateMin = $("#txtStartDate").datepicker("getDate");
            var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate());

            if (rangeMax < dateMax) {
                dateMax = rangeMax;
            }
        }
        return {
            minDate: dateMin,

        };
    }
}
function scrollto(div) {
    $('html,body').animate({
            scrollTop: $("#" + div).offset().top - 200
        },
        'slow');
}
function form_validate(form) {
    $(document).find(form).validate({
        ignore: ":hidden:not('.required_hidden')",
        onfocusout: false,
        rules: {
            image: {
                extension: "jpg|jpeg|png|gif"
            },
            txtPP: {
                extension: "jpg|jpeg|png|gif"
            },
            honor_image: {
                extension: "jpg|jpeg|png|gif"
            },
            txtCv: {
                extension: "docx|doc|pdf|txt"
            }
        },
        messages: {
            image: {
                extension: "Please upload a file with jpg, jpeg, png or gif extension."
            },
            txtPP: {
                extension: "Please upload a file with jpg, jpeg, png or gif extension."
            },
            honor_image: {
                extension: "Please upload a file with jpg, jpeg, png or gif extension."
            },
            txtCv: {
                extension: "Please upload a file with docx, doc, pdf or txt extension."
            }
        },
        unhighlight: function (element) {
            $(element).removeClass('error');
            if ($(element).next('label.error').length > 0) {
                $(element).next('label.error').hide();
            }
        },
        highlight: function (element) {
            $(element).addClass('error');
            if ($(element).next('label.error').length > 0) {
                $(element).next('label.error').show();
            }
        },
        errorPlacement: function (error, element) {
            if ($(error).html() != "") {
                if ($(element).parents(".user_search").length > 0) {
                    $(error).appendTo($(element).parent(".search_container"));
                } else if ($(element).next(".fa").length > 0) {
                    $(error).insertAfter($(element).next(".fa"));
                } else if ($(element).hasClass("editor_required")) {
                    $(error).insertAfter($(element).parents(".redactor_box"));
                } else if ($(element).hasClass("radio_required")) {
                    $(error).insertAfter($(element).parent(".radio"));
                } else {
                    $(error).insertAfter($(element));
                }
            }
        }

    });
}