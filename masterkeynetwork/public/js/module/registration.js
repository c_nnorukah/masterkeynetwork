/**
 * Created by malay on 25-04-2016.
 */

$(document).ready(function () {

//phon number
    $('#registerform')
        .find('[name="phone_number"]')
        .intlTelInput({
            utilsScript: app_url +'js/utils.js',
            autoPlaceholder: true,
            preferredCountries: ['fr', 'us', 'gb'],
        });
    $('#registerform').bootstrapValidator({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            phone_number: {
                validators: {
                    callback: {
                        message: 'The phone number is not valid',
                        callback: function(value, validator, $field) {
                            return {
                                valid: value === '' || $field.intlTelInput('isValidNumber'),
                                type: $field.intlTelInput('getNumberType')
                            };
                        }
                    }
                }
            }
        }
    })
        .on('success.validator.fv', function(e, data) {
            if (data.field === 'phone_number' && data.validator === 'callback' && data.element.val() !== '') {
                // You can see type of phone number by printing out data.result.type
                // console.log(data.result.type);
                if (data.result.type !== intlTelInputUtils.numberType.MOBILE) {
                    data.fv
                        // Mark the field as invalid
                        .updateStatus('phone_number', 'INVALID', 'callback')
                        // Update the message
                        .updateMessage('phone_number', 'callback', 'We accept the mobile numbers only');
                } else {
                    // Reset the message
                    data.fv.updateMessage('phone_number', 'callback', 'The phone number is not valid');
                }
            }
        })
        // Revalidate the number when changing the country
        .on('click', '.country-list', function() {
            $('#registerform').bootstrapValidator('revalidateField', 'phone_number');
        });

    $("#txtPhoneNumber").blur(function () {
var attr_code = $('.country-list').find('.active').attr('data-dial-code');
        if(attr_code!=undefined && attr_code>0){
            $('input[name="country_dial_code"]').val(attr_code);
        }
    });

    //end phon number

    $("#registerform").validate({
        ignore: ""
    });
    $("#password").rules( "add", {
        required: true,
        minlength: 6,
        messages: {
            required: "This field is required.",
            minlength: jQuery.validator.format("Use at least 6 characters.")
        }
    });
    $("#txtFirstName").rules( "add", {
        required: true,
        messages: {
            required: "This field is required.",
        }
    });

    $("#txtLastName").rules( "add", {
        required: true,
        messages: {
            required: "This field is required.",
        }
    });

    $("#txtFullName").rules( "add", {
        required: true,
        "remote" :
        {
            url: app_url + "checkusername",
            type: "POST",
            async:false,
            data:
            {
                username: function()
                {
                    return $("#txtFullName").val();
                }
            }
        },
        messages: {
            required: "This field is required.",
            remote: "Username is already exist.",
        }
    });
    $("#txtEmail").rules( "add", {
        required: true,
        "remote" :
        {
            url: app_url + "checkemail",
            type: "POST",
            async:false,
            data:
            {
                emails: function()
                {
                    return $("#txtEmail").val();
                }
            }
        },
        messages: {
            required: "This field is required.",
            remote: "Email is already exist.",
        }
    });

    $('select').select2()
        .on("change", function (e) {
            getStateDropdown(e.val)
        });

    var selected_country_id = '223';
    getStateDropdown(selected_country_id);

    $(function () {
        $("#country_id").customselect();
    });
   /* $("#tab4 input[type=radio]").on("click", function () {
        var index = $(this).data("index");
        var plan_name = $(this).data("plan-name");
        var price = $(this).data("plan-price");
        var desc = $(this).data("plan-desc");
        var interval = $(this).data("plan-interval");
        $("#tab4 .col-md-3 .member-table").removeClass("active");
        $("#tab4 .col-md-3:eq(" + index + ") .member-table").addClass("active");
        $("#plan_id").val($(this).val());
        $("#plan_name").val(plan_name);
        $("#price").val(price);
        $("#desc").val(desc);
        $("#interval").val(interval);
    });*/

});
function getStateDropdown(country_id) {
    $.ajax({
        type: "GET",
        url: app_url + "ajax/get_state_list/" + country_id,
        success: function (data) {
            $(".state_html").html(data);
            $('select').select2();
        }
    });
}




