$(document).ready(function () {

    //CSV File validation
    $("#import_via_csv_file").validate({
        rules: {
            import_file: {
                required: true,
                extension: "xls|csv|xlsx"
            }
        }
    });

    $(".form-validate").validate({
        ignore: ""
    });

    $("#import_via_get_response_service").validate({
        ignore: ""
    });

    //mailchimp validation
    $("#mailchimp_service").rules( "add", {
        required: true,
        messages: {
            required: "This field is required.",
        }
    });

    //getreponse validation
    $("#get_response_service").rules( "add", {
        required: true,
        messages: {
            required: "This field is required.",
        }
    });
})

$(document).on("click", ".mailchimp_submit", function (e) {
    e.preventDefault();
    var fd = $('#import_via_service').serialize();
    $.ajax({
        url:base_url + 'contacts/importMailchimp',
        type: 'post',
        async: false,
        data: fd,
        error       : function(err) { alert("Could not connect to the registration server."); },
        success: function (json) {
            if(json == "yes")
            {
                location.reload();
            }
            var $el = $(".mailchimp_list_id_select");
            $appendhtml='';
            $appendhtml+='<select name="mailchimp_list_id"><option>Please Select</option>';
            $el.empty(); // remove old options

            // $el.append($("<option></option>")
            //   .attr("value", '').text('Please Select'));
            $.each(json.data, function(value, key) {
                $appendhtml+="<option value='"+key.id+"'>"+key.name+"</option>";
                //  $el.append($("<option></option>")
                // .attr("value",key.id).text(key.name));
            });
            $appendhtml+='</select>';
            $el.html($appendhtml);

        }

    });
});

$(document).on("click", ".getreponse_submit", function (e) {
    e.preventDefault();
    var fd = $('#import_via_get_response_service').serialize();
    $.ajax({
        url:base_url + 'contacts/importgetResponse',
        type: 'post',
        async: false,
        data: fd,
        error       : function(err) { alert("Could not connect to the registration server."); },
        success: function (json) {
            if(json == "yes")
            {
                location.reload();
            }
        }

    });
});
