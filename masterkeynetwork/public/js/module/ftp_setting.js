$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.restart();
    });
    Pace.on("done", function () {
        show_pace()
    });
    Pace.on("stop", function () {
        show_pace()
    });
    Pace.on("hide", function () {
        show_pace()
    });
    function show_pace() {
        if ($.active > 0) {
            $(".pace").addClass("pace-active").removeClass("pace-inactive")
        }
    }

    $(document).off("submit", "#frmFtpSetting");
    $(document).on("submit", "#frmFtpSetting", function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    $('.uploadbutton').attr('value', 'Publishing.....').prop('disabled', true);
                },

                success: function (data) {
                    $(".pace").removeClass("pace-active").addClass("pace-inactive");
                    if (data.toLowerCase().indexOf("successfully") < 0) {
                        var d = new Date();
                        var date = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
                        $("#flash_msg").addClass("alert-error").removeClass("alert-success");
                        $(".active_link").find(".publish-block").html("");
                        $(".active_link").find(".publish_date").html(date);
                    } else {
                        $("#flash_msg").addClass("alert-success").removeClass("alert-error");
                    }
                    $('.uploadbutton').attr('value', 'Publish').prop('disabled', false);
                    $("#flash_msg .msg").html(data);
                    $("#flash_msg").show();
                    scrollto("flash_msg")
                }
            });
        }
    });

    $(document).off("click", ".get_data");
    $(document).on("click", ".get_data", function (e) {
        e.preventDefault();
        $this = $(this).parent().parent("a");

        var url = $this.attr("href");
        $(".active_link").removeClass("active_link");
        $this.addClass("active_link");
        $.get(url, function (data) {
            $(".data_div").html($(data).find(".data_div").html())
            form_validate("#frmFtpSetting");
        })
    })
    $(".get_data:first").trigger("click");
});
