$(document).ready(function () {
    form_validate("#contact_search");
    form_validate("#contact-list-form");
    manage_search_div();
    if($(".user_list").length > 0){
        $("select.search_criteria").attr("required",false);
    }
    $(document).on("change", ".search_criteria", function () {
        manage_search_div();
    })
    $("#contact_search").on("submit", function (e) {
        e.preventDefault();
        if($(".user_list").length > 0 && $(".user_list").val() == ""){
            alert("Please select the user first.");
        }else {
           if($(this).valid()) {
               url = $(this).attr("action");
               var fd = new FormData(this);
               $.ajax({
                   url: url,
                   type: 'post',
                   processData: false,
                   contentType: false,
                   data: fd,
                   success: function (data) {
                       $(".ajax-table").html($(data).find(".ajax-table").html());
                   }
               });
           }
        }
    })
    $(document).off("click", ".contact_delete");
    $(document).on("click", ".contact_delete", function (e) {
        e.preventDefault();
        $this = $(this);
        action = confirm("Are you sure want to delete?");
        if (action) {
            var url = $this.attr("href");
            $.get(url, function (data) {
                url = redirect_url + "/" + $("#sort_by").val() + "/" + $("#sort_order").val() + "/" + $("#limit").val()
                $.get(url, function (data) {
                    $(".ajax-table").html($(data).find(".ajax-table").html());
                    $("#flash_msg .msg").html("Contact deleted successfully.");
                    $("#flash_msg").show();
                    scrollto("flash_msg")
                })
            })
        }
    })
    $(document).on("change", ".contact-list", function () {
        $("[name='name']").val($(this).find("option:selected").html());
        $("[name='list_id']").val($(this).val());
    })
})

function manage_search_div(){
    val = $(".search_criteria").find("option:selected").val();
    $(".search_criteria_div").hide();
    $(".search_container").find("label.error").remove();
    $("." + val + "_div").removeClass("hide").show();
}
