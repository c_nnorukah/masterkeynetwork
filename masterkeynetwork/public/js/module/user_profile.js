$(document).ready(function () {
    form_validate("#about_me_form")
    form_validate("#portfolio_form")
    form_validate("#form_academic_positions")
    form_validate("#form_education")
    form_validate("#form_update_contact_info")
    form_validate("#form_honors")

    $(document).off("submit", ".profile-form");
    $(document).on("submit", ".profile-form", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var fd = new FormData(this);

        $parent = $(this).parents(".tab-pane");
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            processData: false,
            contentType: false,
            data: fd,
            beforeSend: function () {
                $('.userprofilesavebutton').attr('value', 'Saving.....').prop('disabled', true);
            },

            success: function (data) {

                $('.userprofilesavebutton').attr('value', 'Save').prop('disabled', false);

                if (data.toLowerCase().indexOf("successfully") < 0) {
                    $("#flash_msg").addClass("alert-error").removeClass("alert-success");
                } else {
                    $("#flash_msg").addClass("alert-success").removeClass("alert-error");
                }
                $("#flash_msg .msg").html(data);
                $("#flash_msg").show();
                if ($parent.find(".active_link").hasClass("get_data")) {
                    $parent.find(".active_link.get_data").trigger("click")
                } else {
                    $parent.find(".active_link").find(".get-data").trigger("click");
                }
                $(".user_first_name").html($("#about_me_form").find("#txtName").val());
                $(".profile-pic img").attr("src", $(".profile-pic-preview").attr("src"))
                scrollto("flash_msg");
            }
        });

    });
    $(document).off("change", ".uploader");
    $(document).on("change", ".uploader", function () {
        $parent = $(this).parents(".upload-file");
        if ($(this).valid()) {
            $parent.find("[name='image_id']").val('');
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $parent.find('.preview_div').attr('src', e.target.result);
                    $parent.find('.wrapper').removeClass("hide").show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        } else {
            $parent.find('.wrapper').attr('src', "").hide("slow");
        }
    })
    $(document).off("click", ".delete-image");
    $(document).on("click", ".delete-image", function () {
        is_confirm = confirm("Are you sure you want to delete image?");
        if (is_confirm) {
            $this = $(this);
            $parent = $this.parents(".upload-file:first");
            $parent.find("[name='image_id']").val("");
            $parent.find(".image_name").remove();
            src = $parent.find("img").attr("src");
            $parent.find(".wrapper").hide("slow");
            $parent.find(".uploader").val("").attr("required",true);
            if (src.indexOf("data:").length == 0) {
                $.ajax({
                    url: base_url + "delete-image",
                    type: 'post',
                    data: "image=" + src,
                    success: function (data) {
                    }
                });
            }
        }
    })
    $(document).off("click", ".delete-cv");
    $(document).on("click", ".delete-cv", function () {
        is_confirm = confirm("Are you sure you want to delete CV?");
        if (is_confirm) {
            $this = $(this);
            $parent = $this.parents(".upload-file:first");
            $parent.find("[name='user_cv']").val("");
            $parent.find(".cv-name").remove();
            $parent.find("#cv_file").val("").attr("required",true);
        }
    })
    $("#cv_file").on("change", function () {
        if ($(this).valid()) {
            $("[name='user_cv']").val('');
            $(this).parent().find("div").html($(this)[0].files[0].name)
        } else {
            $(this).parent().find("div").html("")
        }
    })
    $(document).off("click", ".get_data");
    $(document).on("click", ".get_data", function (e) {
        e.preventDefault();
        if ($(this).is("a")) {
            $this = $(this)
        } else {
            $this = $(this).parent().parent("a");
        }

        var url = $this.attr("href");
        $(".active_link").removeClass("active_link");
        $(this).addClass("active_link");
        $.get(url, function (data) {
            $parent = $this.parents(".tab-pane");
            $parent.find(".data_div").html($(data).find(".data_div").html())
            form_validate($parent.find(".data_div").find("form"));
            init_plugin($parent)
        })
    })
    $(document).off("click", ".close_button");
    $(document).on("click", ".close_button", function (e) {
        e.preventDefault();
        $this = $(this);
        action = confirm("Are you sure you want to delete?");
        if (action) {
            var url = $this.attr("data-url");
            $.get(url, function (data) {
                $this.parents(".detail").next("hr").remove();
                $this.parents(".detail").remove();
                if (data.indexOf("successfully") < 0) {
                    $("#flash_msg").addClass("alert-error").removeClass("alert-success");
                } else {
                    $("#flash_msg").addClass("alert-success").removeClass("alert-error");
                }
                $("#flash_msg .msg").html(data);
                $("#flash_msg").show();
                $this.parents(".tab-pane").find(".get_data:last").trigger("click");
            })
        }
    })
    $(document).off("change", "select.from_date");
    $(document).on("change", "select.from_date", function () {
        $(this).parents("form").find("select.to_date").attr("min", parseInt($(this).val()) + 1);
    })
    $(document).off("change", "select.to_date");
    $(document).on("change", "select.to_date", function () {
        $(this).parents("form").find("select.from_date").attr("max", parseInt($(this).val() - 1));
    })

});