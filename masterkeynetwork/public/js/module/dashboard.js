//$(document).off('change', '.interval');
//$(document).on('change', '.interval', function() {
//
// var x = document.getElementById("interval").value;
// if(x == 'Day' )
// {
//  $('#Month').css('display','none');
//  $('#Year').css('display','none');
//  $('#Day').css('display','block');
// }
// else if(x == 'Month')
// {
//
//  $('#Month').css('display','block');
//  $('#Year').css('display','none');
//  $('#Day').css('display','none');
// }
// else if(x == 'Year')
// {
//  $('#Month').css('display','none');
//  $('#Year').css('display','block');
//  $('#Day').css('display','none');
//
// }
// else
// {
//  $('#Month').css('display','none');
//  $('#Year').css('display','none');
//  $('#day').css('display','none');
//
// }
//});


$(document).ready(function () {
    /*This is for the dashboard tiles for the social media widgets and other small widgets*/
    $(".live-tile,.flip-list").liveTile();

    /* Following code is for loading the charts */
    // load bar chart
    /*var barChart = new CanvasJS.Chart("stacked-ordered-chart",
     {
     title:{
     text: ""
     },

     data: [
     {
     type: "stackedBar",
     dataPoints: [
     //{ x: new Date(2012, 01, 1), y: 71 },
     { x: 71, y: new Date(2012, 01, 1) },
     { x: 55, y: new Date(2012, 01, 1) },
     { x: 50, y: new Date(2012, 01, 1) },
     { x: 65, y: new Date(2012, 01, 1) },
     ]
     },

     {
     type: "stackedBar",
     dataPoints: [
     //{ x: new Date(2012, 01, 1), y: 71 },
     { x: 71, y: new Date(2012, 01, 1) },
     { x: 55, y: new Date(2012, 01, 1) },
     { x: 50, y: new Date(2012, 01, 1) },
     { x: 65, y: new Date(2012, 01, 1) },
     ]
     },

     {
     type: "stackedBar",
     dataPoints: [
     //{ x: new Date(2012, 01, 1), y: 71 },
     { x: 71, y: new Date(2012, 01, 1) },
     { x: 55, y: new Date(2012, 01, 1) },
     { x: 50, y: new Date(2012, 01, 1) },
     { x: 65, y: new Date(2012, 01, 1) },
     ]
     },

     {
     type: "stackedBar",
     dataPoints: [
     //{ x: new Date(2012, 01, 1), y: 71 },
     { x: 71, y: new Date(2012, 01, 1) },
     { x: 55, y: new Date(2012, 01, 1) },
     { x: 50, y: new Date(2012, 01, 1) },
     { x: 65, y: new Date(2012, 01, 1) },
     ]
     },

     {
     type: "stackedBar",
     dataPoints: [
     //{ x: new Date(2012, 01, 1), y: 71 },
     { x: 71, y: new Date(2012, 01, 1) },
     { x: 55, y: new Date(2012, 01, 1) },
     { x: 50, y: new Date(2012, 01, 1) },
     { x: 65, y: new Date(2012, 01, 1) },
     ]
     },

     ]


     });*/

    //barChart.render();

    // load pie chart
    /*var pieChart = new CanvasJS.Chart("PieChartContainer", {
     title:{
     text: ""
     },
     legend: {
     maxWidth: 350,
     itemWidth: 120
     },
     data: [
     {
     type: "pie",
     showInLegend: true,
     legendText: "{indexLabel}",
     dataPoints: [
     { y: 4181563, indexLabel: "Landing P1" },
     { y: 2175498, indexLabel: "Landing P2" },
     { y: 3125844, indexLabel: "Landing P3" },
     ]
     }
     ]
     });
     pieChart.render();*/


    $('#redactor_content').redactor({
        minHeight: 250,
        maxHeight: 500
    });


    $(".select_all").on("click", function () {
        $(this).select();
    })
    form_validate("#auto-responder-form");
    $(document).off("change", ".landingpage");
    $(document).on("change", ".landingpage", function () {
        $(".email_template").val("");
        $.get(base_url + "auto-responder-detail/" + $(this).val(), function (data) {
            data = $.parseJSON(data);
            $("[name='smtp_setting'],[name='subject']").val("");
            $("#redactor_content").redactor().setCode("<p></p>");
            if (typeof data.smtp_setting_id != "undefined") {
               $("[name='smtp_setting']").val(data.smtp_setting_id);
                $("[name='subject']").val(data.subject);
                $("#redactor_content").redactor().setCode(data.email_body);
            }
        })
    })
    prev_email_template = "";
    $(document).on("focus", ".email_template", function () {
        prev_email_template = $(this).val();
    })
    $(document).on("change", ".email_template", function () {
        action = confirm("If you change email template, you will lose your changes for email subject and body. Are you sure want to continue?");
        if(action) {
            $.get(base_url + "email-template/" + $(this).val(), function (data) {
                data = $.parseJSON(data);
                $("[name='subject']").val("");
                $("#redactor_content").redactor().setCode("<p></p>");
                if (typeof data.subject != "undefined") {
                    $("[name='subject']").val(data.subject);
                    $("#redactor_content").redactor().setCode("");
                    $("#redactor_content").redactor().insertHtml(data.content);
                }
            })
        }else{
            $(this).val(prev_email_template);
            return false;
        }
    })
    $(document).off("submit", "#auto-responder-form");
    $(document).on("submit", "#auto-responder-form", function (e) {
        e.preventDefault();

        var fd = new FormData(this);
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            processData: false,
            contentType: false,
            data: fd,
            beforeSend: function () {
                $('.uploadbutton').attr('value', 'Saving.....').prop('disabled', true);
            },

            success: function (data) {
                if (data.toLowerCase().indexOf("successfully") < 0) {
                    $("#flash_msg").addClass("alert-error").removeClass("alert-success");
                    $('.uploadbutton').attr('value', 'Save').prop('disabled', false);
                } else {
                    $("#flash_msg").addClass("alert-success").removeClass("alert-error");
                }
                $("#flash_msg .msg").html(data);
                $("#flash_msg").show();
                scrollto("flash_msg")
            }
        });
    });
});
