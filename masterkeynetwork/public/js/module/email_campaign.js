$(document).ready(function () {
    $('#redactor_content').redactor({
        minHeight: 200,
        maxHeight: 500
    });
    form_validate("#email-campaign-form");
    $("#email-campaign-form").on("submit",function(e){
        e.preventDefault();
        e.stopPropagation();
        valid = 1;
        if($("[name='contact_type']:checked").length > 0){
            if($("[name='contact_type']:checked").val() == "list" && $(".contact_list_div").find("input:checked").length == 0){
                $("<label class='error'>This field is required</label>").appendTo($(".contact_list_div"));
                valid = 0;
            }else if($("[name='contact_type']:checked").val() == "individual" && $(".individual_contact_div").find("input:checked").length == 0){
                $("<label class='error'>This field is required</label>").appendTo($(".individual_contact_div"));
                valid = 0;
            }
        }
        if(valid == 0){
            scrollto("email-campaign-form");
            return false;
        }else{
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    $('.savebutton').attr('value', 'Saving.....').prop('disabled', true);
                },

                success: function (data) {

                    $('.savebutton').attr('value', 'Save').prop('disabled', false);
                    $(".page-content").html($(data).find(".page-content").html());
                    window.history.pushState("Pending Email Campaign", "Pending Email Campaign", base_url + "email-campaign-list");
                    scrollto("flash_msg");
                }
            });
        }
    })
    prev_email_template = "";
    $(document).on("focus", ".email_template", function () {
        prev_email_template = $(this).val();
    })
    $(document).on("change", ".email_template", function () {
        action = confirm("If you change email template, you will lost your changes for email subject and body. Are you sure you want to continue?");
        if(action) {
            $.get(base_url + "email-template/" + $(this).val(), function (data) {
                data = $.parseJSON(data);
                $("[name='subject'],[name='email_template']").val("");
                $("#redactor_content").redactor().setCode("<p></p>");
                if (typeof data.subject != "undefined") {
                    $("[name='subject']").val(data.subject);
                    $("[name='email_template']").val(data.context);
                    $("#redactor_content").redactor().setCode("");
                    $("#redactor_content").redactor().insertHtml(data.content);
                }
            })
        }else{
            $(this).val(prev_email_template);
            return false;
        }
    })
    $(document).on("change", "[name='contact_type']", function () {
        $(".contact_list_div").toggleClass("hide",$("[name='contact_type']:checked").attr("id") != "contact_list");
        $(".individual_contact_div").toggleClass("hide",$("[name='contact_type']:checked").attr("id") == "contact_list");
    })
    $(document).on("change", "[name='send_on']", function () {
        $(".send_later_div").toggleClass("hide",$("[name='send_on']:checked").val() == "now");
    })
})
