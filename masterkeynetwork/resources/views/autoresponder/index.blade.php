@extends('layouts.master')
@section('title')
    Auto Responder
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="page-title">
    </div>
    <div class="heading-all">
    <div class="row">

        <div class="col-md-3">
            <p class="lefty">Autoresponders</p>
        </div>

        <div class="col-md-9 text-right">
            {{--<select class="email_template form-control">--}}
                {{--<option value="">Select predefined email templates</option>--}}
                {{--@if(!empty($EmailTemplate))--}}
                    {{--@foreach($EmailTemplate as $key=>$value)--}}
                        {{--<option value="{{ $value->email_template_id }}">{{ $value->subject  }}</option>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
            {{--</select>--}}
            <a role="button" class="btn btn-default my-button" href="{{url("add-email-template")}}">Manage email
                templates</a>
            <a role="button" class="btn my-button" href="{{url("add-email-template")}}">Add new email
                template</a>
        </div>
    </div>
    </div>



    <div class="row">
        <div class="col-xs-12 email-fields">
            <form role="form" class="editor-form m-b-20" id="auto-responder-form" method="post"
                  action="{{url("save-auto-responder")}}">
                <div class="form-group">
                    {{--{{ Form::select('landingpage_id',(['' => 'Choose Landing Page list'] + $LandingPageitems),null,['class' => 'select2 form-control select2-offscreen']) }}--}}


                    <select class="form-control landingpage"
                            id="landingpage_id" name="landingpage_id" required="true">
                        <option value="">Choose Landing Page</option>

                        @foreach($LandingPageitems as $key=>$value)

{{--                            @if(isset($value->user_projects->user_id ) && $value->user_projects->user_id == $user_id)--}}
                                <option value="{!! $value->id !!}"
                                        @if(isset($responder_detail->landingpage_id) && $responder_detail->landingpage_id == $value->id) selected @endif>{!! $value->name !!}</option>
                            {{--@endif--}}
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    @if(!empty($smtp))
                        <select name="smtp_setting" class="form-control" required="true">
                            <option value="">Please Select SMTP Setting</option>
                            @foreach($smtp as $val)
                                <option value="{{$val->smtp_setting_id}}"
                                        @if(isset($responder_detail->smtp_setting_id) && $responder_detail->smtp_setting_id == $key) selected @endif>{{$val->domain}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                 <div class="form-group">
                 <select class="email_template form-control">
                     <option value="">Select predefined email templates</option>
                    @if(!empty($EmailTemplate))
                      @foreach($EmailTemplate as $key=>$value)
                         <option value="{{ $value->email_template_id }}">{{ $value->subject  }}</option>
                     @endforeach
                        @endif
                 </select>
                     </div>
                <div class="form-group">
                    <input placeholder="Subject" class="form-control" required="true" name="subject" type="text"
                           value="{{isset($responder_detail->subject) ? $responder_detail->subject : ""}}">
                </div>

                <div class="form-group">
                    <div class="grid simple dashboard-block">

                                <textarea rows="10" cols="7" class="form-control editor_required required_hidden"
                                          id="redactor_content"
                                          placeholder="Enter text ..." name="email_body">
                                    {{isset($responder_detail->email_body) ? $responder_detail->email_body : ""}}
                                </textarea>
                    </div>
                </div>
                <div class="form-group">

                        <input type="submit" class="btn btn-primary save-button pull-right" value="Save" name="submit">

                </div>

            </form>
        </div>
    </div>
    <!-- END DASHBOARD TILES -->


    {{--@include("admin.dashboard.includes.graphs");--}}
            <!-- todo::need to implement chat functionlity -->
    {{--@include("dashboard.includes.chat");--}}
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}
@stop