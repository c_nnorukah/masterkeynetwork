@extends('admin.layouts.master')

@section('title')
    Dashboard
@stop

@section('before-styles-end')
    {!! Html::style('admin/css/plugin/redactor/redactor.css') !!}
@stop

@section('main')

    <div class="row heading-all m-b-20">

        <div class="col-md-12 col-sm-6 col-xs-12">
            {!! Html::link(URL::route('admin-landing-pages'), 'Create new landing page', ['class' => 'btn btn-info my-button', 'role' => 'button']) !!}
            {{--<a href="#" class="btn btn-info my-button" data-toggle="modal" data-target=".bs-example-modal-lg" role="button">Create new landing page</a>--}}
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <p class="lefty">Landing Page</p>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-12 text-right m-b-10">
            <label>
                <input type="text" aria-controls="example" id="search-landing-page-value" class="input-medium" placeholder="find landing pages">
                <a href="javascript:;" id="search-landing-page"><i class="fa fa-search srch-icon"></i></a>
            </label>
        </div>

    </div>

    <div class="row table-responsive my-table-contact">

        <table id="landingPagePreview" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="25%">Title</th>
                    <th width="20%">Created By</th>
                    <th width="15%">Create On</th>
                    <th width="15%">Update On</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data->count() > 0)
                    @foreach($data as $page)
                    <tr>
                        {{--<td align="center">
                            <div class="landing-page-preview">
                                {!! Html::image($page->preview_image, 'preview', ['width' => '150']) !!}
                            </div>
                        </td>--}}
                        <td>{!! $page->title !!}</td>
                        <td>{!! $page->user->user_name !!}</td>
                        <td>{!! \Carbon\Carbon::parse($page->created_at)->format('M, d Y') !!}</td>
                        <td>{!! \Carbon\Carbon::parse($page->updated_at)->format('M, d Y') !!}</td>
                        <td>
                            {!! Html::link(URL::route('admin-get-edit-landing-page', ['id' => $page->user_landing_page_id]), 'Edit', ['class' => 'btn btn-primary']) !!}
                            <a href="javascript:;" class="btn btn-primary btn-delete" data-row-url="{!! URL::route('admin-delete-landing-page', ['id' => $page->user_landing_page_id]) !!}" data-row-id="{!! $page->user_landing_page_id !!}">Delete</a>
                            <a href="javascript:;" class="btn btn-primary btn-preview" data-row-id="{!! $page->user_landing_page_id !!}">Preview</a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">
                            {!! trans('message.landingPage.noLandingPage') !!}
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

    </div>

@stop

@include('admin.includes.landingPagePreview')
@include('admin.includes.deleteConfirm')

@section('before-scripts-end')
    {!! Html::script('admin/js/plugin/html2canvas/html2canvas.js') !!}
@stop

@section('after-scripts-end')
    {!! Html::script('admin/js/module/landingpage.js') !!}
@stop