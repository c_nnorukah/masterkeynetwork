<script>
    var app_url = '{!! url('/') !!}/';
    var public_url = '{!! public_path().'/' !!}';
</script>

<div id="cssScriptsContainer"></div>

{!! Html::style('admin/css/plugin/bootstrap3/bootstrap.min.css') !!}
{!! Html::style('admin/css/plugin/bootstrap3/bootstrap-theme.min.css') !!}


{!! Html::style('admin/css/plugin/bootstrap-datepicker/datepicker.css') !!}
{!! Html::style('admin/css/plugin/bootstrap-timepicker/bootstrap-timepicker.css') !!}
{!! Html::style('admin/css/plugin/bootstrap-colorpicker/bootstrap-colorpicker.css') !!}

{!! Html::style('admin/landingPage/azexo/azexo_composer/azexo_composer.css') !!}
{!! Html::style('admin/landingPage/azexo/azexo_composer/azexo_composer_backend.css') !!}

<style>
    body > .container > .az-container { margin-top : 34px; margin-bottom : 100px; }
    .az-container .controls.btn-group { z-index: 9999 !important; }
</style>

<div class="grid-body no-border" style="float:left;">
    <div class="row-fluid m-t-20">
        <button type="submit" id="saveContainer" class="btn btn-success control save-container">
            <span class="bold">Save Customize</span>
            &nbsp;&nbsp;<i class="fa fa-cloud-upload"></i></button>
    </div>
</div>

<div class="az-container container" data-az-type="default" data-az-name="main" style="margin-top:50px;width:100%;">

    <?php $cont = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content); ?>

    {!! $cont !!}

</div>

{!! Html::script('admin/js/plugin/jquery/jquery-1.11.3.min.js') !!}
{!! Html::script('admin/js/plugin/bootstrap3/bootstrap.min.js') !!}

{!! Html::script('admin/landingPage/azexo/jquery-ui.min.js') !!}
{!! Html::script('admin/landingPage/azexo/underscore-min.js') !!}

{!! Html::script('admin/landingPage/azexo/azexo_composer/jquery-waypoints/waypoints.min.js') !!}
{!! Html::script('admin/landingPage/azexo/azexo_composer/js/smoothscroll.js') !!}
{!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_param_types.js') !!}
{!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_elements.min.js') !!}
{!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_composer.js') !!}
{!! Html::script('admin/landingPage/azexo/azexo_composer/chosen/chosen.jquery.js') !!}
{!! Html::script('admin/js/require.js') !!}

{!! Html::script('admin/js/module/landingpage.js') !!}

<script type="text/javascript">
    window.azexo_baseurl = app_url + 'admin/landingPage/azexo/azexo_composer/';
    window.azexo_prefix = '';
    window.azexo_editor = true;
    window.azexo_online = false;
    function getSave() {
        $("#saveContainer").trigger("click");
    }
</script>