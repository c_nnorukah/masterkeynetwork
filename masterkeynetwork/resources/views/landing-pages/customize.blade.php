@extends('admin.layouts.master')

@section('title')
    Dashboard
@stop

@section('before-styles-end')
    {!! Html::style('admin/landingPage/azexo/azexo_composer/azexo_composer.css') !!}
    {!! Html::style('admin/landingPage/azexo/azexo_composer/azexo_composer_backend.css') !!}

    <style>
        body > .container > .az-container { margin-top : 34px; margin-bottom : 100px; }
    </style>
    {!! Html::style('admin/css/plugin/redactor/redactor.css') !!}
@stop

@section('after-styles-end')
    {!! Html::style('admin/css/custom.css') !!}
@stop

@section('main')

    <div class="row">
        <div class="grid simple dashboard-block">
            <div class="grid-title no-border">
                <h4><span class="semi-bold">Landing Page Editor</span></h4>
                <div class="tools"> <a href="javascript:;" class="collapse"></a> </div>
            </div>

            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    <label>Page Title</label>
                    <input type="text" class="form-control required" name="templateName" id="templateName" value="{!! isset($template_name)?$template_name:'' !!}" placeholder="Name your landing page" />
                    <input type="hidden" name="short_code_id" id="short_code_id" value="{!! isset($template_id)?$template_id:'' !!}" />
                </div>
            </div>

            {{--<div class="az-container container" data-az-type="default" data-az-name="main" style="margin-top:20px;">
                @if(isset($template_id) && $template_id != '')
                    {!! $content !!}
                @else
                    {!! preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content) !!}
                @endif
            </div>--}}
            <div class="grid-body no-border col-md-12" style="float:left;">
                <div class="row-fluid m-t-20">
                    <label>Page Editor</label>
                    <iframe id="htmlEditor" class="form-control col-md-12" style="height: 500px;" src="{!! URL::route('admin-get-customize-landing-page') !!}" name="htmlEditor"></iframe>
                </div>
            </div>

            <div class="grid-body no-border" style="float:left;">
                <div class="row-fluid m-t-20">
                    <button type="submit" id="saveContainer" class="btn btn-success control save-container">
                        <span class="bold">Save Customize</span>
                        &nbsp;&nbsp;<i class="fa fa-cloud-upload"></i></button>
                </div>
            </div>

        </div>
    </div>
@stop


@include('admin.includes.messageDialogBox')

@section('before-scripts-end')
    {!! Html::script('admin/js/plugin/ace/ace.js', ['data-ace-base' => 'src']) !!}
    {!! Html::script('admin/js/plugin/redactor/redactor.min.js') !!}

    {!! Html::script('admin/landingPage/azexo/jquery-ui.min.js') !!}
    {!! Html::script('admin/landingPage/azexo/underscore-min.js') !!}

    {!! Html::script('admin/landingPage/azexo/azexo_composer/jquery-waypoints/waypoints.min.js') !!}
    {!! Html::script('admin/landingPage/azexo/azexo_composer/js/smoothscroll.js') !!}
    {!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_param_types.js') !!}
    {!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_elements.min.js') !!}
    {!! Html::script('admin/landingPage/azexo/azexo_composer/azexo_composer.js') !!}
    {!! Html::script('admin/landingPage/azexo/azexo_composer/chosen/chosen.jquery.js') !!}
    {!! Html::script('admin/js/require.js') !!}
@stop

@section('after-scripts-end')
    {!! Html::script('admin/js/module/landingpage.js') !!}
@stop

@section('core-js')
    <script type="text/javascript">
        window.azexo_baseurl = app_url + 'admin/landingPage/azexo/azexo_composer/';
        window.azexo_prefix = '';
        window.azexo_editor = true;
        window.azexo_online = false;
        $(document).ready(function() {
            $("#saveContainer").off("click");
            $("#saveContainer").on("click", function() {
                document.getElementById('htmlEditor').contentWindow.getSave();
                $(this).prop("disabled", true);
            });
        });
    </script>
@stop