@extends('admin.layouts.master')

@section('title')
    Install Landing Page
@stop

@section('main')

    <!-- BEGIN DASHBOARD TILES -->

    <div class="row">

        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Upload Template</span></h4>
                    <div class="tools"> <a href="javascript:;" class="collapse"></a> </div>
                </div>
                <div class="grid-body no-border">

                    {!! Form::open(['route' => ['admin-customize-landing-page', 'install'], 'method' => 'post', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}

                    <div class="row-fluid">

                        <div class="form-group">
                            <div class="file-upload">
                                {!! Form::input('file', 'uploadFile', '', ['class' => 'form-control', 'id' => 'uploadFile']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <p>Only .zip file allowed</p>
                        </div>

                        <button class="btn btn-primary btn-cons my-button" type="submit">
                            <i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;<span class="bold">Upload</span></button>

                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>


    </div>

    <!-- END DASHBOARD TILES -->

@stop

@section('after-scripts-end')
    {!! Html::script('admin/js/module/landingpage.js') !!}
@stop