@extends('admin.layouts.master')

@section('title')
    Dashboard
@stop

@section('before-styles-end')
    {!! Html::style('admin/css/plugin/redactor/redactor.css') !!}
@stop

@section('main')

    <!-- BEGIN DASHBOARD TILES -->

    <div class="row landing-template">
        <div class="col-md-6 col-sm-6 col-xs-12">
            {{--<a href="#" class="btn btn-info my-button" data-toggle="modal" data-target=".bs-example-modal-lg" role="button">Create new landing page</a>--}}
        </div>
        <div class="col-md-6 6 col-sm-6 col-xs-12">
            <p class="text-right">
                {!! Html::link(URL::route('admin-list-landing-page'), 'Manage landing page templates', ['class' => 'btn btn-info my-button', 'role' => 'button']) !!}
                {!! Html::link(URL::route('admin-list-landing-page'), 'Add landing page templates', ['class' => 'btn btn-info my-button', 'role' => 'button']) !!}
            </p>
        </div>
    </div>

    {{--<div class="row m-t-20 m-b-20">
        <div class="col-md-6 lnading-screenshots">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><img src="webarch/img/landing-screenshot.png" class="center-block img-thumbnail" alt="#"></a>
                    <p class="text-left">Cool Landnig Pages</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><img src="webarch/img/landing-screenshot.png" class="center-block img-thumbnail" alt="#"></a>
                    <p class="text-left">Naughty Landnig Pages</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><img src="webarch/img/landing-screenshot.png" class="center-block img-thumbnail" alt="#"></a>
                    <p class="text-left">Unnamed Landnig Pages</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 lnading-screenshots">
            <a href="#"><img src="webarch/img/landing-random.png" class="img-thumbnail center-block" alt="#"></a>
            <p class="text-left">Random 1</p>
        </div>
    </div>--}}

    <div class="row">
        <div class="grid simple dashboard-block">
            <div class="grid-title no-border">
                <h4><span class="semi-bold">Landing Page Editor</span></h4>
                <div class="tools"> <a href="javascript:;" class="collapse"></a> </div>
            </div>
            {!! Form::open(['route' => isset($data) ? ['admin-edit-landing-page', $data->landing_page_id] : 'admin-create-landing-page', 'method' => 'post', 'role' => 'form']) !!}
            {!! Form::token() !!}
            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    {!! Form::text('title', isset($data) ? $data->title : '', ['class' => 'form-control required', 'id' => 'title', 'placeholder' => 'Enter title']) !!}
                </div>
            </div>
            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    {!! Form::textarea('content', isset($data) ? $data->content : '', ['class' => 'form-control required', 'id' => 'text-editor']) !!}
                </div>
            </div>
            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    <pre id="css-editor">
                        /* write your css code */
                    </pre>
                </div>
            </div>
            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    <label>Custom Js</label>
                    <pre id="editor">
                        /* write your javascript code */
                    </pre>
                </div>
            </div>
            <div class="grid-body no-border">
                <div class="row-fluid m-t-20">
                    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="row text-center">

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">


            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                        </div>

                        <!-- Wrapper for slides -->

                        <div class="carousel-inner cr-inner-content">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-md-12 text-center m-b-20"><input type="text" name="txtName" value="" placeholder="Name your landing page">
                                        <h3>Select a Teemplate</h3>
                                    </div>
                                    <div class="col-sm-2"><a href="#x"><img src="webarch/img/gallery-1.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-3"><a href="#x"><img src="webarch/img/gallery-2.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-7"><a href="#x"><img src="webarch/img/gallery-3.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-md-12 m-t-10">
                                        <p class="text-center"><a role="button" class="btn btn-info my-button" href="#">Customize it</a></p>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-12 text-center m-b-20"><input type="text" name="txtName" value="" placeholder="Name your landing page">
                                        <h3>Select a Teemplate</h3>
                                    </div>
                                    <div class="col-sm-2"><a href="#x"><img src="webarch/img/gallery-1.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-3"><a href="#x"><img src="webarch/img/gallery-2.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-7"><a href="#x"><img src="webarch/img/gallery-3.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-md-12 m-t-10">
                                        <p class="text-center"><a role="button" class="btn btn-info my-button" href="#">Customize it</a></p>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-12 text-center m-b-20"><input type="text" name="txtName" value="" placeholder="Name your landing page">
                                        <h3>Select a Teemplate</h3>
                                    </div>
                                    <div class="col-sm-2"><a href="#x"><img src="webarch/img/gallery-1.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-3"><a href="#x"><img src="webarch/img/gallery-2.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-7"><a href="#x"><img src="webarch/img/gallery-3.jpg" alt="Image" class="img-responsive"></a>
                                    </div>
                                    <div class="col-md-12 m-t-10">
                                        <p class="text-center"><a role="button" class="btn btn-info my-button" href="#">Customize it</a></p>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4" style="display:none;">
        <h3>Slide <span class="semi-bold">Toggle</span></h3>
        <p>A cool iOS7 slide toggle. These are cutomize for all boostrap colors</p>
        <br>
        <div class="row-fluid">
            <div class="slide-primary">
                <input type="checkbox" name="switch" class="ios" checked="checked"/>
            </div>
            <div class="slide-success">
                <input type="checkbox" name="switch" class="iosblue" checked="checked"/>
            </div>
        </div>
    </div>

    <!-- END DASHBOARD TILES -->

@stop

@section('before-scripts-end')
    {!! Html::script('admin/js/plugin/ace/ace.js', ['data-ace-base' => 'src']) !!}
    {!! Html::script('admin/js/plugin/redactor/redactor.min.js') !!}
@stop

@section('after-scripts-end')
    {!! Html::script('admin/js/module/landingpage.js') !!}
@stop