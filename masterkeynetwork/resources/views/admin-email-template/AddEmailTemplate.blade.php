@extends('layouts.master')

@section('title')
    PaypalPaymentSetting
@stop

@section('main')
    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif


    <form action="{{url('add-email')}}" method="post" id="emailtemplateform">
        <div class="heading-all">
            <div class="row">
                <div class="col-md-3">
                    <p class="lefty">Email Template</p>
                </div>

                <div class="col-md-9 text-right">
                    <select class="emailtemplate-select edit_email_template" id="template" name="template">
                        <option value="">Select predefined email templates</option>
                        <optgroup label="User defined email template">
                            @foreach($template as $template_name)
                                <?php  $id = $template_name->email_template_id;
                                $subject = "";
                                ?>
                                <option value="{{$template_name->email_template_id}}" @if($subject==$template_name->subject) <?php $cancel = $template_name->subject; ?> selected @endif>{{$template_name->subject}}</option>
                            @endforeach
                        </optgroup>
                         <optgroup label="Default email template">
                             @foreach($default_template as $d_temp)
                             <option value="{{$d_temp->email_template_id}}">{{$d_temp->subject}}</option>
                                 @endforeach
                          </optgroup>
                     </select>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" name="subject" id="subject" class="form-control required" placeholder="Subject">
                </div>


                <div class="form-group">
                    <textarea id="redactor_content" rows="10" cols="7" name="content" required="required" class="form-control editor_required required_hidden required"></textarea>
                </div>

                <div class="text-right">
                    <input class="btn btn-lg btn-primary lefty" style="width: 150px;" id="temp" type="submit" value="Save">
                    {{--<a href="{{url('add-email-template')}}" class="btn btn-lg btn-primary lefty" style="width: 150px;">cancel</a>--}}
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->
        <!-- END DASHBOARD TILES -->
        </div>
    </form>

@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}
    <script type="text/javascript">

        $(document).off("change", ".edit_email_template");
        $(document).on("change", ".edit_email_template", function () {
            $('#emailtemplateform').attr('action', base_url + "update-email/" + $(this).val());
            var val = $(this).val();
            if (val) {
                $.get(base_url + "select-template/" + $(this).val(), function (data) {
                    data = $.parseJSON(data);
                    $("[name='subject']").val("");
                    $("#redactor_content").redactor().setCode("<p></p>");
                    if (typeof data.subject != "undefined") {
                        $("[name='subject']").val(data.subject);
                        $("#redactor_content").redactor().setCode("");
                        $("#redactor_content").redactor().insertHtml(data.content);
                    }
                })
            }
            else {
                window.location.href = 'add-email-template';
            }
        })

        $('#emailtemplateform').validate({
            ignore: [],
            rules: {
                subject: "required",
                content: "required",
            }
        });
    </script>

@stop