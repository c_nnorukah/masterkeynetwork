<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        var app_url = '{!! url('/') !!}/';
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>@yield('title', '')</title>
    <meta name="description" content="@yield('meta_description', 'Default Description')">
    <meta name="author" content="@yield('author', '')">
    <link rel="shortcut icon" href="{{ asset('images/mas-favicon.png')}}" />

    @yield('meta')

    {!! Html::style('css/plugin/bootstrap3/bootstrap.min.css') !!}
    {!! Html::style('css/plugin/bootstrap3/bootstrap-theme.min.css') !!}

    {!! Html::style('css/plugin/jquery-scrollbar/jquery.scrollbar.css') !!}
    {!! Html::style('css/plugin/bootstrap-select2/select2.css') !!}
    {!! Html::style('css/plugin/bootstrap-datepicker/datepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-timepicker/bootstrap-timepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-colorpicker/bootstrap-colorpicker.css') !!}
    {!! Html::style('css/plugin/boostrap-checkbox/bootstrap-checkbox.css') !!}
    {!! Html::style('css/intlTelInput.css') !!}

    @yield('before-styles-end')
    {!! Html::style('css/main-style.css') !!}
    @yield('after-styles-end')
    {!! Html::style('css/plugin/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/plugin/animate.min.css') !!}
</head>
<body class="error-body no-top">
<header class="main_h sticky2">
    <div class="container">
        <h1 id="logo">
            <a class="logo-1" href="http://masterkeynetwork.com"><img src="http://masterkeynetwork.com/wp-content/uploads/2016/05/masterlogo-new.png"></a>
        </h1>

        <nav class="custom-navbar navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="http://masterkeynetwork.com/">Home</a></li>
                        <li><a href="http://masterkeynetwork.com/tour/">Tour</a></li>
                        <li><a href="http://masterkeynetwork.com/pricing/">Pricing</a></li>
                        <li><a href="http://masterkeynetwork.com/blog">MKN Blog</a></li>
                        <li><a href="http://masterkeynetwork.alfyopare.com/beta/public/login">Login</a></li>
                        <li><a href="http://masterkeynetwork.com/pricing/">Sign up</a></li>
                    </ul>

                </div>
            </div>
        </nav>

    </div> <!-- / row -->



</header>

<div class="container">
    <div class="width-33pc width-100pc-xs hcenter">
        <div class="col-md-6 col-md-offset-3">
            @if (count($errors) > 0)
                <div class="alert alert-danger msg">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>


        {{--<div class="col-md-4 col-md-offset-4" style="position: relative; margin-top: 10%">--}}
        <div class="{{ (isset($classname) ? $classname : "col-md-4 col-md-offset-4 col-xs-12 form-alert-block" ) }}">
            <div class="form-alert-block">
            @if(Session::has('flash_success'))
                @include('includes.error', ['type' => 'success', 'message' => session('flash_success')])
            @elseif(Session::has('flash_warning'))
                @include('includes.error', ['type' => 'warning', 'message' => session('flash_warning')])
            @elseif(Session::has('flash_info'))
                @include('includes.error', ['type' => 'info', 'message' => session('flash_info')])
            @elseif(Session::has('flash_danger'))
                @include('includes.error', ['type' => 'danger', 'message' => session('flash_danger'), 'error' => $errors])
            @endif
            @if(isset($info))
                @include('includes.error', ['type' => 'info', 'message' => $info])
            @endif
            </div>
        </div>
        @yield('main')


    </div>
</div>

<div class="section white footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 xs-m-b-20">
                <img src="http://masterkeynetwork.com/wp-content/uploads/2016/05/masterlogo-new.png" alt="" data-src="http://masterkeynetwork.com/wp-content/uploads/2016/05/masterlogo-new.png" data-src-retina="http://masterkeynetwork.com/wp-content/uploads/2016/05/masterlogo-new.png">
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 xs-m-b-20">
                <address class="xs-no-padding  col-md-6 col-lg-6 col-sm-6  col-xs-12">
                    Crossraid
                    85/B Cross Street,
                    New York, USA
                    NA1 42SL						</address>
                <div class="xs-no-padding col-md-6 col-lg-6 col-sm-6">
                    <div>(0039) 389 957 5552</div>
                    support@example.com						</div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 xs-m-b-20">
                &copy; 2016  All Rights Reserved Masterkeynetwork.com - Design by <a target="_blank" href="http://www.alfyopare.com/">Alfyopare.com</a>
            </div>
        </div>

    </div>
</div>

{!! Html::script('js/plugin/jquery/jquery-1.11.3.min.js') !!}
{!! Html::script('js/plugin/bootstrap3/bootstrap.min.js') !!}
{!! Html::script('js/plugin/jquery-block-ui/jqueryblockui.min.js') !!}
{!! Html::script('js/plugin/jquery-unveil/jquery.unveil.min.js') !!}
{!! Html::script('js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') !!}
{!! Html::script('js/plugin/jquery-numberAnimate/jquery.animateNumbers.js') !!}
{!! Html::script('js/plugin/jquery-validation/jquery.validate.js') !!}
{!! Html::script('js/plugin/bootstrap-select2/select2.min.js') !!}
{!! Html::script('js/form_validations.js') !!}
@yield('before-scripts-end')
{!! Html::script('js/main.js') !!}
@yield('after-scripts-end')
<script type="text/javascript">
    $('.msg').fadeOut(7000);
</script>
{!! Html::script('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('js/plugin/boostrap-form-wizard/jquery.bootstrap.wizard.js') !!}
</body>
</html>