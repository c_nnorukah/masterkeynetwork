<!DOCTYPE html>
<html lang="en">
<head>


    <div id="cssScriptsContainer"></div>
    {!! Html::style('css/css/bootstrap.css') !!}
    {!! Html::style('css/css/font-awesome.min.css') !!}
    {!! Html::style('css/css/perfect-scrollbar-0.4.5.min.css') !!}
    {!! Html::style('css/css/magnific-popup.css') !!}
    {!! Html::style('css/css/style.css') !!}
    {!! Html::style('css/css/styles/default.css') !!}
    @yield('before-styles-end')

    @yield('after-styles-end')

</head>

<body class="">
<div id="wrapper">

            @include('includes.profileleftsidebar')
    <div id="main">
    @yield('content');




</div>
</div>

<!-- Javascript
================================================== -->

{!! Html::script('js/jquery-1.10.2.js') !!}
{!! Html::script('js/TweenMax.min.js') !!}
{!! Html::script('js/jquery.touchSwipe.min.js') !!}
{!! Html::script('js/jquery.carouFredSel-6.2.1-packed.js') !!}
{!! Html::script('js/modernizr.custom.63321.js') !!}

{!! Html::script('js/jquery.dropdownit.js') !!}
{!! Html::script('js/jquery.stellar.min.js') !!}
{!! Html::script('js/ScrollToPlugin.min.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/jquery.mixitup.min.js') !!}

{!! Html::script('js/masonry.min.js') !!}
{!! Html::script('js/perfect-scrollbar-0.4.5.with-mousewheel.min.js') !!}
{!! Html::script('js/magnific-popup.js') !!}
{!! Html::script('js/custom.js') !!}

@yield('before-scripts-end')

@yield('after-scripts-end')

        <!-- /JavaScript
================================================== -->

@yield('core-js')

</body>
</html>