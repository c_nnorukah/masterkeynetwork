<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        var app_url = '{!! url('/') !!}/';
        var public_url = '{!! public_path().'/' !!}';
    </script>
    <link rel="shortcut icon" type="image/png" href="images/mas-favicon.png"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>

@if(isset($meta_array))
        @foreach($meta_array as $k=>$v)
            <meta name="{{$k}}" content="{{$v}}"/>
        @endforeach
    @endif
    <title> @hasSection('title')
        @yield('title') | Master Key Network
        @else
            MasterKeyNetwork
        @endif</title>
    <link rel="shortcut icon" href="{{ asset('images/mas-favicon.png')}}" />
    <meta name="description" content="@yield('meta_description', 'Default Description')">
    <meta name="author" content="@yield('author', '')">
    @yield('meta')
    <div id="cssScriptsContainer"></div>
    {!! Html::style('css/plugin/jquery-scrollbar/jquery.scrollbar.css') !!}
    {!! Html::style('css/plugin/bootstrap3/bootstrap-theme.min.css') !!}
    {!! Html::style('css/plugin/bootstrap3/bootstrap.min.css') !!}

    {!! Html::style('css/plugin/bootstrap-datepicker/datepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-select2/select2.css') !!}
    {!! Html::style('css/plugin/bootstrap-timepicker/bootstrap-timepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-colorpicker/bootstrap-colorpicker.css') !!}
    {!! Html::style('css/plugin/ios-switch/ios7-switch.css') !!}
    {!! Html::style('css/plugin/pace/pace-theme-flash.css') !!}
    {!! Html::style('css/redactor.css') !!}
    {!! Html::style('css/plugin/jquery-metrojs/metrojs.css') !!}
    @yield('before-styles-end')
    {!! Html::style('css/webarch.css') !!}
    {!! Html::style('css/main-style.css') !!}
    {!! Html::style('css/jquery-ui.css') !!}
    {!! Html::style('css/dropzone.min.css') !!}
    @yield('after-styles-end')
    {!! Html::style('css/plugin/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/plugin/jquery-editable-select/jquery.editable-select.css') !!}
    {!! Html::style('css/plugin/animate.min.css') !!}
    {!! Html::style('css/bootstrap-treeview.css') !!}
    <script>
        var customHandlers = [];
        var base_url = {!! "'".URL::to('/')."/'" !!};
    </script>
</head>
<body>
@include('includes.topnav')
        <!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
    @include('includes.leftsidebar')
    <section class="intro-block intro-page boxed-section ">
        <div class="notification-container"></div>
    </section>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>
        <div class="clearfix"></div>
        <div class="content sm-gutter">
            <div class="col-md-12">
                @if(Session::has('flash_success'))
                    @include('includes.error', ['type' => 'success', 'message' => session('flash_success')])
                @elseif(Session::has('flash_warning'))
                    @include('includes.error', ['type' => 'warning', 'message' => session('flash_warning')])
                @elseif(Session::has('flash_info'))
                    @include('includes.error', ['type' => 'info', 'message' => session('flash_info')])
                @elseif(Session::has('flash_danger') || (isset($errors) && $errors->count() > 0))
                    @include('includes.error', ['type' => 'danger', 'message' => session('flash_danger'), 'error' => $errors])
                @endif
                @if(isset($info))
                    @include('includes.error', ['type' => 'info', 'message' => $info])
                @endif
            </div>
            @yield('main')
        </div>
    </div>
    <div class="chat-window-wrapper downline-member-side-window"></div>
</div>

<!-- Javascript
================================================== -->
<script data-pace-options='{ "ajax": {"trackMethods" :  ["GET","POST"]},"restartOnPushState" : "true","restartOnRequestAfter":"1500" }'
        src='{{url("js/plugin/pace/pace.min.js")}}'></script>
{!! Html::script('js/plugin/jquery/jquery-1.11.3.min.js') !!}
{{--{!! Html::script('js/plugin/pace/pace.min.js') !!}--}}
{!! Html::script('js/plugin/bootstrap3/bootstrap.min.js') !!}
{!! Html::script('js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') !!}
{!! Html::script('js/plugin/jquery-numberAnimate/jquery.animateNumbers.js') !!}
{!! Html::script('js/plugin/jquery-validation/jquery.validate.min.js') !!}
{!! Html::script('js/plugin/bootstrap-select2/select2.js') !!}
{!! Html::script('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('js/redactor.min.js') !!}

{!! Html::script('js/jquery-ui.js') !!}
{!! Html::script('js/jquery.blockUI.js') !!}
{!! Html::script('js/additional-methods.min.js') !!}
{!! Html::script('js/jquery-customselect.js') !!}
{!! Html::script('js/bootstrap-treeview.js') !!}
{!! Html::script('js/jquery-timepicker.js') !!}
    {!! Html::script('js/html5lightbox.js') !!}


{!! Html::script('js/plugin/jquery-editable-select/jquery.editable-select.js') !!}
        <!-- Dashboard page Tile Plugin -->
{!! Html::script('js/plugin/metrojs/metrojs.js') !!}
@yield('before-scripts-end')
{!! Html::script('js/main.js') !!}
{!! Html::script('js/plugin/jquery.custom-plugin.js') !!}
{!! Html::script('js/module/common.js') !!}
@yield('after-scripts-end')
@yield('core-js')
<script type="text/javascript" language="javascript">
    var csrf_token = '<?php echo csrf_token()?>';
</script>

</body>
</html>