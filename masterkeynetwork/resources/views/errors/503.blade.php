<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="width-33pc width-100pc-xs hcenter">
                    <div class="col-md-6 col-md-offset-3">
                        @if(Session::has('FlashSuccess'))
                            @include('includes.error', ['type' => 'success', 'message' => session('FlashSuccess')])
                        @elseif(Session::has('flashWarning'))
                            @include('includes.error', ['type' => 'warning', 'message' => session('flashWarning')])
                        @elseif(Session::has('flash_danger'))
                            @include('includes.error', ['type' => 'danger', 'message' => session('flash_danger')])
                        @endif
                        @if(isset($flashInfo))
                            @include('includes.error', ['type' => 'info', 'message' => $flashInfo])
                        @endif
                    </div>
                </div>
                <div class="title">Be right back.</div>
            </div>
        </div>
    </body>
</html>
