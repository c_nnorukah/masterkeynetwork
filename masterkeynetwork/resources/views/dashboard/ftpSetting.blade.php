@extends('layouts.master')
@section('title')
    FTP Setting
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="row heading-all">

        <div class="col-md-7">
            <p class="lefty">FTP Settings</p>
        </div>
    </div>
    <div>
        <div class=" ftp-setting">
            <div class="row form-row col-md-12">
                <div class="col-md-4 m-t-20 ">
                    <div class="scrollable_div">
                        @if(isset($landing_page) && count($landing_page) > 0)
                            @foreach($landing_page as $key=>$value)
                                @if(!empty($value->projects))
                                    <a href="{!! url("ftp-detail/$value->project_id" )!!}"
                                       class=" @if(isset($id) && $id == $value->project_id) active_link @endif">
                                        <div class="detail">
                                            <div class="get_data">
                                                <p><b>Title:</b> {!! $value->projects->name !!}</p>
                                                <p><b>Last Updated
                                                        At:</b> {!! format_date($value->projects->updated_at,"m/d/Y H:i A")!!}
                                                </p>
                                                <p><b>Last Published
                                                        At:</b><span class="publish_date"> {!! empty($value->projects->published_at) ? "Not Published" :
                                                        format_date($value->projects->published_at,"m/d/Y H:i A")!!}</span>
                                                </p>
                                            </div>
                                        </div>
                                        @if(strtotime($value->projects->updated_at) > strtotime($value->projects->published_at))
                                            <label class="label-important publish-block">Note: This page contains
                                                changes
                                                which are not published.</label>
                                        @endif
                                        <hr>
                                    </a>
                                @endif
                            @endforeach
                        @else
                            <p class="pd-l-10">No pages found.</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-7 mr-l-25 data_div m-t-20 ">
                    <form method="post" id="frmFtpSetting" name="frmFtpSetting"
                          action="{!!url("ftp-settings/".$id)!!}"
                          enctype="multipart/form-data" >
                        <div class="row form-row">
                            <div class="col-xs-11">
                                <input name="host_name"
                                       value="{!! isset($landing_page_detail->host_name) ? $landing_page_detail->host_name : "" !!}"
                                       type="text" class="form-control"  placeholder="Host Name"
                                       id="host_name" required="true">
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpHostname') }}"></i>

                            </div>


                        </div>
                        <div class="row form-row">
                            <div class="col-md-11">
                                <select name="protocol" id="protocol" class=" form-control"  required="true">
                                    <option value="">Please select</option>
                                    <option value="ftp" {!! isset($landing_page_detail->protocol) && $landing_page_detail->protocol == "ftp" ? "selected" : "" !!}>
                                        FTP - File Transfer Protocol
                                    </option>
                                    <option value="sftp" {!! isset($landing_page_detail->protocol) && $landing_page_detail->protocol == "sftp" ? "selected" : "" !!}>
                                        SFTP - SSH File Transfer Protocol
                                    </option>
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpProtocol') }}"></i>
                            </div>


                        </div>
                        <div class="row form-row">
                            <div class="col-md-11">
                                <input name="port_number"
                                       value="{!!  isset($landing_page_detail->port_number) ? $landing_page_detail->port_number : "" !!}"
                                       type="text" class="form-control" placeholder="Port Number"
                                       id="port_number" required="true" number="true">
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpPortNumber') }}"></i>

                            </div>

                        </div>
                        <div class="row form-row">
                            <div class="col-md-11">
                                <input name="username"
                                       value="{!! isset($landing_page_detail->username) ? $landing_page_detail->username : "" !!}"
                                       type="text" class="form-control" placeholder="Username"
                                       id="username" required="true">
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpUsername') }}"></i>
                            </div>

                        </div>
                        <div class="row form-row">
                            <div class="col-md-11">
                                <input name="password"
                                       value="{!! isset($landing_page_detail->password) ? $landing_page_detail->password : "" !!}"
                                       type="password" class="form-control" placeholder="Password"
                                       id="password" required="true">
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpPassword') }}"></i>

                            </div>
                        </div>
                        <div class="row form-row">
                            <div class="col-md-11">
                                <input name="remote_location"
                                       value="{!! isset($landing_page_detail->remote_directory) ? $landing_page_detail->remote_directory : "" !!}"
                                       type="text" class="form-control" placeholder="Default Remote Directory"
                                       id="remote_location" required="true">
                            </div>
                            <div class="col-xs-1">
                                <i class="fa fa-question-circle" title="{{ trans('message.tooltip_FtpRemoteDirectory') }}"></i>
                            </div>
                        </div>

                        <div class="row form-row">
                            <div class="col-md-2 pull-right">
                                <input type="submit" name="cmdPortfolio" value="Publish" id="portfolio_button"
                                       class="btn btn-block btn-primary uploadbutton">
                            </div>
                            <div class="col-xs-1">

                            </div>
                        </div>


                    </form>
                </div>
                <div class="separator"></div>
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/ftp_setting.js') !!}
@stop