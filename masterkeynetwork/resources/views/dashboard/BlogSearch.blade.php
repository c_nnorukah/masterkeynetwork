@extends('layouts.master')
@section('title')
    Admin Setting
@stop
@section('main')
    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif



    <div class="row">

        <div class="col-md-12">


            <div class="heading-all"><h2 class="title">Search Result</h2></div>
            @if(!empty($data['blog_post']))
                <div class="clearfix"></div>

                @foreach($data['blog_post'] as $k => $v)
                    @foreach($v as $key => $val)
                        <h2>
                            {{ $val['post_title']}}
                        </h2>

                        @if($val['guid']!='' &&  (strpos($val['guid'], '.jpg') > 0 ||  strpos($val['guid'], '.png') > 0 ||  strpos($val['guid'], '.gif') > 0 ))
                            <img src="{{ $val['guid'] }}" class="image-responsive-width hover-effect-img post-img"/>
                        @endif

                        <p>{!! $val['post_content'] !!}</p>

                        <div class="">
                            @if(isset($val['terms']) && is_array($val['terms']))
                                @foreach($val['terms'] as $term)
                                    <a href="#" class="">#{{ $term }}</a>
                                @endforeach
                            @endif
                            <a href="#" class="">#{{  $val['comment_count'] }}</a>
                        </div>
                    @endforeach
        </div>
        <hr>
        @endforeach
    </div>
    @else
        <h3>No result found</h3>

        </div>
    @endif
@stop
