@extends('layouts.master')
@section('title')
    Dashboard
@stop
@section('main')

    <div class="row heading-all m-b-20">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <p class="lefty" style="">{{$user_name=Auth::user()->first_name }}'s Dashboard</p>

            <div class="pull-left">
                <div id="fb-root"></div>
                {{--<a href="{{ URL::to('contacts')  }}" class="btn btn-primary">Manage Contacts</a>
                <a href="http://masterkeynetwork.alfyopare.com/beta/landingpage/architect/?user={{ Auth::user()->user_id }}#/dashboard" class="btn btn-primary">Create Landing page</a>--}}
            </div>
        </div>
        {{--  <div class="col-md-4 col-sm-4 col-xs-12 dashboard_link">
              <p>Copy your affilate link and share it on social sites.</p>
              <input type="text" class="form-control select_all" readonly="true"
                     value="{!! URL::to('http://masterkeynetwork.com//?referral_code='.Auth::user()->referral_code) !!}"> </input>

              <div class="pull-right">
                  <div id="fb-root"></div>
                  <b>Share affilate link using:</b>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <img src="{{ asset('/images/facebook-icon.png') }}" id="share_button"
                       style="height: 38px; width: 38px; cursor: pointer;">
                  <a href="http://twitter.com/home?status={{URL::to('referral/'.Auth::user()->referral_code)}}"
                     target="_blank" value="{!! URL::to('referral/'.Auth::user()->referral_code) !!}"><img
                              src="{{ asset('/images/tw.png') }}" style="height: 40px; width: 40px;"> </a>
              </div>
          </div>--}}
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="manage-contact">
                @if(Session::has('role'))
                    @foreach(Session::get('role') as $val)
                        @if($val == '2')
                            <i class="fa fa-cog"></i><a href="{{ URL::to('email-campaign')  }}">Start Email Campaign</a>
                        @else
                            <i class="fa fa-cog"></i><a href="{{ URL::to('contact-book')  }}">Manage Contacts</a>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="create-landing">
                <i class="fa fa-code"></i><a
                        href="http://masterkeynetwork.alfyopare.com/beta/landingpage/architect/?user={{ Auth::user()->user_id }}#/dashboard">Create
                    Landing page</a>chat-menu-toggle
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="newsletter">
                <i class="fa fa-send"></i><a href="#">Newsletter</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-vlg-6 col-sm-12 ">
            <!-- BEGIN BLOG POST BIG IMAGE WIDGET -->
            <div class="social-post-block" data-aspect-ratio="true">
                <div class="live-tile slide ha" data-speed="750" data-delay="3000" data-mode="carousel">
                    @foreach($data['blog_post'] as $k => $v)
                        @foreach($v as $key => $val)
                            <div class="slide-front ha tiles adjust-text">
                                <div class="tiles overflow-hidden full-height tiles-overlay-hover m-b-10 widget-item">
                                    <div class="controller overlay right"><a href="javascript:;" class="reload"></a> <a
                                                href="javascript:;" class="remove"></a></div>
                                    <div class="overlayer tiles-overlay auto blue post-info-overlay">
                                        <div class="overlayer-wrapper p-t-20 p-l-20 p-r-20 p-b-20">
                                            <h3 class="text-white">{{ $val['post_title']}} <span class="semi-bold"><br>
                                 </span>{!! $val['post_content'] !!}</h3>
                                        </div>
                                    </div>

                                    <div class="overlayer bottom-left fullwidth">
                                        <div class="overlayer-wrapper">

                                            <div class="tiles gradient-grey p-l-20 p-r-20 p-b-20 p-t-20">
                                                @if(isset($val['terms']) && is_array($val['terms']))
                                                    @foreach($val['terms'] as $term)
                                                        <a href="#" class="hashtags transparent">#{{ $term }}</a>
                                                    @endforeach
                                                    {{--<div class="profile-img-wrapper inline m-r-5"><img--}}
                                                    {{--src="assets/img/profiles/avatar_small.jpg" alt=""--}}
                                                    {{--data-src="assets/img/profiles/avatar_small.jpg"--}}
                                                    {{--data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="35"--}}
                                                    {{--height="35"></div>--}}
                                                    {{--<input type="text" class="dark m-r-5" id="txtinput1" placeholder="Write a comment"--}}
                                                    {{--style="width:60%">--}}
                                                    {{--<button type="button" class="btn btn-primary">Joing Group</button>--}}
                                                @endif
                                                <a href="#"
                                                   class="hashtags transparent">#{{  $val['comment_count'] }}</a>
                                                {{--
                                                                                                    <div class="profile-img-wrapper inline m-r-5"><img src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="35" height="35"></div>
                                                --}}
                                                <?php $flag = 0;?>
                                                @if(!empty(Auth::user()->User_medias->image_path))
                                                    <?php $flag = 1;?>
                                                    <div class="profile-img-wrapper inline m-r-5">
                                                        <img src="{{url(Auth::user()->User_medias->image_path)}}" alt=""
                                                             data-src="assets/img/profiles/avatar_small-new.jpg"
                                                             data-src-retina="assets/img/profiles/avatar_small2xx.jpg"
                                                             width="35" height="35"/>
                                                    </div>
                                                @else
                                                    <div class="profile-img-wrapper inline m-r-5">
                                                        {!! Html::image('images/profile_placeholder.png', '', ['class' => '', 'data-src' => asset(''), 'data-src-retina' => asset('images/profile_placeholder.png'), 'width' => '35','height' => '35']) !!}
                                                    </div>
                                                @endif

                                                <form method="get" action="{{ URL::route('blog-search') }}">
                                                    <input type="text" class="dark m-r-5" id="txtinput1"
                                                           placeholder="Search Post"
                                                           style="width:60%" name="search_post">
                                                    <button type="submit" class="btn btn-primary">Search</button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    @if($val['guid']!='' &&  (strpos($val['guid'], '.jpg') > 0 ||  strpos($val['guid'], '.png') > 0 ||  strpos($val['guid'], '.gif') > 0 ))
                                        )
                                        <img src="{{ $val['guid'] }}"
                                             class="image-responsive-width hover-effect-img post-img"/>
                                    @else
                                        {!! Html::image('images/others/9.jpg', 'logo', ['class' => 'image-responsive-width hover-effect-img', 'data-src' => url('../resources/assets/admin/images/others/9.jpg'), 'data-src-retina' => url('../resources/assets/admin/images/others/9.jpg')]) !!}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-vlg-6 col-sm-12">
            <div class="row ">
                <!-- BEGIN ANIMATED TILE -->
                <div class="col-md-6 col-sm-6 m-b-10" data-aspect-ratio="true">
                    <div class="live-tile slide ha " data-speed="750" data-delay="3000" data-mode="carousel">
                        <div class="slide-front ha tiles adjust-text">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                <i class="fa fa-map-marker fa-2x"></i>

                                <p class="text-white-opacity p-t-10">21 Jan</p>

                                <h3 class="text-white no-margin">New Year <span class="semi-bold">UI Bundle <br>
                                  </span> now on webarch</h3>

                                <p class="p-t-20 "><span class="bold">214</span> Comments <span
                                            class="m-l-10 bold">24k</span> Likes</p>
                            </div>
                        </div>
                        <div class="slide-back ha tiles adjust-text">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20"><i class="fa fa-map-marker fa-2x"></i>

                                <p class="text-white-opacity p-t-10">21 Jan</p>

                                <h3 class="text-white no-margin">New Year <span class="semi-bold">UI Bundle <br>
                    </span> now on webarch</h3>

                                <p class="p-t-20 "><span class="bold">214</span> Comments <span
                                            class="m-l-10 bold">24k</span> Likes</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END ANIMATED TILE -->
                <!-- BEGIN ANIMATED TILE -->
                <div class="col-md-6 col-sm-6 m-b-10" data-aspect-ratio="true">
                    <div class="live-tile slide ha " data-speed="750" data-delay="4000" data-mode="carousel">
                        <div class="slide-front ha tiles blue ">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                 @if(isset($data['fbpost']))
                                    <h4>All FB Post</h4>
                                    <div class="user-photo"><img src="{!!Session::get('fb_dp') !!}"
                                                                 style="height: 50px; width: 50px;"></div>
                                    <?php $count = 0 ?>

                                    @foreach($data['fbpost'] as $val)

                                        @if(isset($val['message']))
                                            <a href="{{url('social-posting')}}"
                                               style="text-decoration: none;">  {{substr($val['message'],0,100)}} </a>
                                            <br>
                                            {{--@foreach($val['created_time'] as $date)--}}
                                            {{--{{$date}}--}}
                                            {{--@endforeach--}}

                                            <?php $count++; ?>
                                            @if($count >= 4)
                                                @break
                                            @endif
                                        @endif
                                    @endforeach
                                @else
                                        <h4 class="text-white no-margin custom-line-height"><a
                                                    href="{{url('application-setting')}}"> <span class="semi-bold">login to facebook</span></a>
                                        </h4>
                                @endif
                            </div>
                            {{--<div class="overlayer bottom-left fullwidth">--}}
                            {{--<div class="overlayer-wrapper">--}}
                            {{--<div class="user-comment-wrapper">--}}
                            {{--<div class="profile-wrapper">--}}
                            {{--<img src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="35" height="35">--}}
                            {{--</div>--}}
                            {{--<div class="comment">--}}
                            {{--<div class="user-name text-white ">--}}
                            {{--<span class="bold"> David</span> Cooper--}}

                            {{--</div>--}}

                            {{--<p class="text-white-opacity">@ Revox</p>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="slide-back ha tiles blue">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                <div class="user-name text-white ">

                                    @if(isset($data['tweet']))
                                        <h4>All Tweets</h4>
                                            <div class="user-photo">
                                                <img src="{!!Session::get('tw_dp')!!}"/>
                                            </div>
                                        @foreach($data['tweet'] as $twitter_details)
                                            @if(isset($twitter_details->text))
                                                <a href="{{url('social-posting')}}"
                                                   style="text-decoration: none;"> {{substr($twitter_details->text,0,100)  }}</a>
                                                <br>
                                                {{--{{ $twitter_details->created_at  }}--}}
                                            @endif
                                        @endforeach
                                        @else
                                            <h4 class="text-white no-margin custom-line-height"><a
                                                        href="{{url('application-setting')}}"> <span class="semi-bold">login to twitter</span></a>
                                            </h4>
                                    @endif
                                    {{--<span class="bold"> Jane</span> Smith--}}
                                </div>
                                {{--<p class="text-white-opacity">@ Revox</p>--}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- END ANIMATED TILE -->
            </div>
            <div class="row">
                <!-- BEGIN ANIMATED TILE -->
                <div class="col-md-6  col-sm-6 m-b-10" data-aspect-ratio="true">
                    <div class="live-tile slide ha" data-speed="750" data-delay="4500" data-mode="carousel">
                        <div class="slide-front ha tiles green p-t-20 p-l-20 p-r-20 p-b-20">
                            <h1 class="semi-bold text-white">15% <i class="icon-custom-up icon-custom-2x"></i></h1>

                            <div class="overlayer bottom-left fullwidth">
                                <div class="overlayer-wrapper">
                                    <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                        <p class="bold">Webarch Dashboard</p>

                                        <p>2,567 USD <span class="m-l-10"><i class="fa fa-sort-desc"></i> 2%</span></p>

                                        <p class="bold p-t-15">Front-end Design</p>

                                        <p>1,420 USD <span class="m-l-10"><i class="fa fa-sort-desc"></i> 1%</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-back ha tiles green">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                <h5 class="text-white semi-bold no-margin p-b-5">Today Sale's</h5>

                                <h3 class="text-white no-margin">450 <span class="semi-bold">USD</span></h3>
                                Last Sale 23.45 USD
                            </div>
                            <div class="overlayer bottom-left fullwidth">
                                <div class="overlayer-wrapper">
                                    <div id="sales-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END ANIMATED TILE -->
                <!-- BEGIN ANIMATED TILE -->
                <div class="col-md-6  col-sm-6 m-b-10" data-aspect-ratio="true">
                    <div class="live-tile slide ha " data-speed="750" data-delay="6000" data-mode="carousel">
                        {{-- <div class="slide-front ha tiles green ">
                             <div class="overlayer bottom-left fullwidth">
                                 <div class="overlayer-wrapper">
                                     <div class="tiles gradient-black p-l-20 p-r-20 p-b-20 p-t-20">
                                         <h4 class="text-white semi-bold no-margin">RUN AWAY GO </h4>
                                         <h5 class="text-white semi-bold ">Queen's favourite</h5>

                                         <p class="text-white semi-bold no-margin"><i class="icon-custom-up "></i> Read
                                             More</p>
                                     </div>
                                 </div>
                             </div>
                            </div>--}}
                        @if(!empty($data['course']))

                            @foreach($data['course'] as $key => $course_data)



                                {{--                                             {{ dd($course_data->coursematerial) }}--}}
                                @if(!empty($course_data->coursematerial))
                                    @foreach($course_data->coursematerial as $img_url)

                                        <div class="slide-front ha tiles green home-page-course-matirial">
                                            @if(!empty($img_url['is_default']))
                                                @if($img_url['is_default']==1)
                                                    <a href="{{ (isset($img_url['material_url'])) ? $img_url['material_url']: "javascript:;" }}"
                                                       class="html5lightbox" data-group="mygroup"
                                                       data-width="480" data-height="320"
                                                       title="Big Buck Bunny">{!! (isset($img_url['material_url'])) ? Html::image($img_url['material_url'], 'feature', ['class'=>'html5lightbox']) : "" !!}
                                                    </a>
                                                @else
                                                    <a href="images/others/cover.jpg" class="html5lightbox"
                                                       data-group="mygroup"
                                                       data-width="480" data-height="320"
                                                       title="Big Buck Bunny">{!! Html::image('images/others/cover.jpg', '', ['class' => 'html5lightbox', 'data-src' => url('../resources/assets/admin/images/others/cover.jpg'), 'data-src-retina' => url('../resources/assets/admin/images/others/cover.jpg')]) !!}</a>
                                                @endif
                                            @endif
                                            <?php $url = 'javascript:;';?>
                                            @if(strpos($img_url['material_url'], 'youtube') > 0)
                                                <?php $url = "https://www.youtube.com/embed/" . $img_url['material_key'];?>
                                            @elseif(strpos($img_url['material_url'], 'vimeo') > 0)
                                                <?php $url = "http://player.vimeo.com/video/" . $img_url['material_key'] . "?api=1";?>
                                            @elseif(strpos($img_url['material_url'], 'soundcloud') > 0)
                                                <?php $url = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" . $img_url['material_key'] . "&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true";?>
                                            @else
                                                <?php $url = "javascript:;";?>
                                            @endif
                                            <a href="{{ $url }}" class="html5lightbox" data-group="mygroup"
                                               data-width="480" data-height="320" title="Big Buck Bunny">
                                                <div class="overlayer bottom-left fullwidth">
                                                    <div class="overlayer-wrapper">
                                                        <div class="tiles gradient-black p-l-20 p-r-20 p-b-20 p-t-20">
                                                            <h5 class="text-white semi-bold ">{{ $course_data->course_title }}</h5>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(strpos($img_url['material_url'], 'youtube') > 0)
                                                    <img src="{{ $img_url['material_video_src'] }}" height="250"
                                                         width="250"/>
                                                @elseif(strpos($img_url['material_url'], 'vimeo') > 0)
                                                    <img src="{{ $img_url['material_video_src'] }}" height="200"
                                                         width="250"/>
                                                @elseif(strpos($img_url['material_url'], 'soundcloud') > 0)
                                                    <img src="{{ $img_url['material_video_src'] }}" height="250"
                                                         width="250"/>
                                                @else
                                                    <img src="{{ $img_url['material_video_src'] }}"/>
                                                @endif
                                            </a>

                                        </div>
                                    @endforeach
                                @endif


                            @endforeach
                        @endif
                    </div>

                </div>
                <!-- END ANIMATED TILE -->
            </div>
        </div>
    </div>
    {{--  <a href="{{ url('confirmation') }}" data-remote="false" data-toggle="modal" data-target="#myModal" class="btn btn-default open-iframe">
          Launch Modal
      </a>
      <!-- Default bootstrap modal example -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div class="modal-body">

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
              </div>
          </div>
      </div>--}}
    {{--@include("admin.dashboard.includes.graphs");--}}
    {{--@include("dashboard.includes.chat");--}}
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}

    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>--}}
    <script type="text/javascript">
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1064859340253171', status: true, cookie: true,
                xfbml: true
            });
        };
        (function () {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());


        $(document).ready(function () {
            $('#share_button').click(function (e) {
                e.preventDefault();
                FB.ui(
                        {
                            method: 'feed',
                            name: 'Masterkeynetwork',
                            link: '{{URL::to('referral/'.Auth::user()->referral_code)}}',
                            picture: 'http://masterkeynetwork.alfyopare.com/beta/public/images/master-logo.png',
                            // caption: 'This is the content of the "caption" field.',
                            // description: 'This is the content of the "description" field, below the caption.',
                            message: ''
                        });
            });

            /* $('.home-page-course-matirial').off('click');
             $('.home-page-course-matirial').on('click', function () {
             var third_party_url = $(this).find('.third_party_url').val();
             if (third_party_url != undefined && third_party_url != '') {
             alert(third_party_url)
             $('.open-iframe').click();
             }
             });*/
        });
    </script>
@stop