<!-- BEGIN CHAT -->

<div id="main-chat-wrapper" class="inner-content">
    <div class="chat-window-wrapper scroller scrollbar-dynamic" id="chat-users">
        <div class="side-widget fadeIn">
            <h3><b>
                    <center>Share Your Affiliate Link</center>
                </b></h3>
            <hr>
            <div class="right-side-header">
                <ul>
                    <li>
                        <a id="share_button" href="javascript:void(0);"><i
                                    class="fa fa-facebook"></i>Facebook</a>
                    </li>
                    <li>
                        <a href="http://twitter.com/home?status={{URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code)}}"
                           target="_blank"><i
                                    class="fa fa-twitter"></i>Twitter</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="anchortag"
                           data-value="{!! URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code) !!}"><i
                                    class="fa fa-files-o"></i>Copy Code</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="side-widget fadeIn">
            <h3><b>
                    <center>Downline Members</center>
                </b></h3>
            <hr>


            <input type="hidden"
                   value="{!! URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code) !!}">
            @if(!empty($third_level))
                <div id="favourites-list">
                    <div class="side-widget-content">
                        @if(!empty(json_decode($third_level)))
                            <div id="right_side_downline"></div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<input type="hidden" id="right_side_downline_input" value='<?php echo($third_level)?>'>
<!-- END CHAT -->