<div class="row">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-vlg-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Basic Analytics</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid m-t-10">
                        <div id="stacked-ordered-chart" style="height:250px"></div>
                        <div id="stacked-ordered-chart" style="height: 250px; padding: 0px; position: relative;">
                            <canvas class="base" width="364" height="250"></canvas>
                            <canvas class="overlay" width="364" height="250" style="position: absolute; left: 0px; top: 0px;"></canvas>
                            <div class="tickLabels" style="font-size:smaller">
                                <div class="xAxis x1Axis" style="color:#545454">
                                    <div class="tickLabel" style="position:absolute;text-align:center;left:36px;top:234px;width:52px">Jan</div>
                                    <div class="tickLabel" style="position:absolute;text-align:center;left:103px;top:234px;width:52px">Feb</div>
                                    <div class="tickLabel" style="position:absolute;text-align:center;left:166px;top:234px;width:52px">Mar</div>
                                    <div class="tickLabel" style="position:absolute;text-align:center;left:233px;top:234px;width:52px">Apr</div>
                                    <div class="tickLabel" style="position:absolute;text-align:center;left:298px;top:234px;width:52px">May</div>
                                </div>
                                <div class="yAxis y1Axis" style="color:#545454">
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:217px;right:348px;width:16px">0</div>
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:173px;right:348px;width:16px">25</div>
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:129px;right:348px;width:16px">50</div>
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:84px;right:348px;width:16px">75</div>
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:40px;right:348px;width:16px">100</div>
                                    <div class="tickLabel" style="position:absolute;text-align:right;top:-4px;right:348px;width:16px">125</div>
                                </div>
                            </div>
                            <div class="legend">
                                <div style="position: absolute; width: 54px; height: 80px; top: 9px; right: 9px; opacity: 0.85; background-color: rgb(255, 255, 255);"></div>
                                <table style="position:absolute;top:9px;right:9px;;font-size:smaller;color:#545454">
                                    <tbody>
                                    <tr>
                                        <td class="legendColorBox">
                                            <div style="border:1px solid #ccc;padding:1px">
                                                <div style="width:4px;height:0;border:5px solid rgba(243, 89, 88, 0.7);overflow:hidden"></div>
                                            </div>
                                        </td>
                                        <td class="legendLabel">Product 1</td>
                                    </tr>
                                    <tr>
                                        <td class="legendColorBox">
                                            <div style="border:1px solid #ccc;padding:1px">
                                                <div style="width:4px;height:0;border:5px solid rgba(251, 176, 94, 0.7);overflow:hidden"></div>
                                            </div>
                                        </td>
                                        <td class="legendLabel">Product 2</td>
                                    </tr>
                                    <tr>
                                        <td class="legendColorBox">
                                            <div style="border:1px solid #ccc;padding:1px">
                                                <div style="width:4px;height:0;border:5px solid rgba(10, 166, 153, 0.7);overflow:hidden"></div>
                                            </div>
                                        </td>
                                        <td class="legendLabel">Product 3</td>
                                    </tr>
                                    <tr>
                                        <td class="legendColorBox">
                                            <div style="border:1px solid #ccc;padding:1px">
                                                <div style="width:4px;height:0;border:5px solid rgba(0, 144, 217, 0.7);overflow:hidden"></div>
                                            </div>
                                        </td>
                                        <td class="legendLabel">Product 4</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-vlg-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Landing Page Clicks</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid m-t-10">
                        <div>
                            <div id="sparkline-pie" class="col-md-12">
                                <canvas width="354" height="177" style="display: inline-block; width: 354px; height: 177px; vertical-align: top;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 col-sm-6 col-vlg-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Email Open Rate</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid m-t-20">
                        <div class="tiles white no-margin"><span id="spark-2"></span></div>
                        <span id="spark-2"><canvas width="364" height="200" style="display: inline-block; width: 364px; height: 200px; vertical-align: top;"></canvas></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-vlg-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Recent Referrals - Downline</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid m-t-20">
                        <div class="tiles white no-margin"><span id="spark-3"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>