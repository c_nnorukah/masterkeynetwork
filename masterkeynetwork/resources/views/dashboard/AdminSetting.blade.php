@extends('layouts.master')
@section('title')
    Admin Setting
@stop
@section('main')
    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif

    <div class="row heading-all m-b-20">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <p class="lefty">Admin Settings</p>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-12 text-right m-b-10">
            @if(Session::has('role'))
                <?php $role = Session::get('role') ?>
                @foreach($role as $val)
                    @if($val == '1')
                        <select class="select2 my-button" style="width: 200px;"
                                onChange="window.document.location.href=this.options[this.selectedIndex].value;"
                                value="GO">
                            <option value="{{URL::route('application-settings')}}">Application Settings</option>
                            <option value="{{URL::route('admin-settings')}}" selected>Admin Settings</option>
                        </select>
                    @endif
                @endforeach
            @endif
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Set up membership Levels</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="panel panel-default app-settings m-t-20">
                                <div class="panel-body">

                                    @if(!empty($edit))
                                        <?php $plan_name = $edit->plan_name;
                                        $price = $edit->price;
                                        $interval = $edit->interval;
                                        $days = $edit->days;
                                        $plan_desc = $edit->description;
                                        $plan = 'Update Plan';
                                        $id = $edit->plan_id;
                                        $action = url('updateplan', $id);
                                        $insert = "";
                                        ?>
                                    @else
                                        <?php $plan_name = "";
                                        $price = "";
                                        $days = "";
                                        $plan_desc = "";
                                        $interval = "";
                                        $plan = "";
                                        $id = "";
                                        $plan = 'Add Plan';
                                        $action = url('addplan');
                                        $insert = "select interval"
                                        ?>
                                    @endif
                                    <form accept-charset="UTF-8" action="{{$action}}" method="post" role="form" name="plan_form" id="plan_form">

                                        <fieldset>
                                            <div class="form-group">
                                                <label><b>Plan name:</b></label>
                                                <input class="form-control required" placeholder="Add Plan Name"
                                                       value="{{$plan_name}}"
                                                       name="plan_name" minlength=3 type="text" id="plan_name">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">

                                                    <div class="control-group">
                                                        <label><b>Plan price in $:</b></label>
                                                        <input class="form-control" placeholder="Add Price"
                                                               value="{{$price}}" name="price" class="form-control required nume" id="price">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="control-group">
                                                        <label><b>Plan interval:</b></label>
                                                        <select class="form-control" name="interval" id="interval">
                                                            <option value="@if(!empty($interval)) {{$interval}} @endif">@if(!empty($interval)) {{$interval}} @else {{$insert}} @endif</option>
                                                            <option value="Days">Days</option>
                                                            <option value="Month">Month</option>
                                                            <option value="Year">Year</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="control-group">
                                                        <label><b>Interval value:</b></label>
                                                        <input class="form-control" placeholder="interval value"
                                                               value="{{$days}}" name="days" id="days" class="form-control required">
                                                    </div>
                                                </div>
                                                <br><br><br><br><br>

                                                <div class="row">
                                                    <div class="row-fluid m-t-0 redactor-height" id="description">
                                                        <label><b>Plan Description:</b></label>

                                                        <textarea id="redactor_content" style="height: 100px; resize: none;" maxlength="5" placeholder="Enter text ..." required name="description"
                                                                  class="form-control">{!! $plan_desc !!}</textarea>
                                                    </div>
                                                    <br>
                                                    @if($plan == 'Update Plan')
                                                        <div class="text-right">
                                                            <input class="btn btn-lg btn-primary" type="submit"
                                                                   value="{{$plan}}">
                                                            <a href="{{url('admin-setting')}}"><input
                                                                        class="btn btn-lg btn-primary" style="width: 100px;"
                                                                        value="cancel"></a>
                                                        </div>
                                                    @else
                                                        <div class="text-right">
                                                            <input class="btn btn-lg btn-primary" type="submit"
                                                                   id="plan_submit" value="{{$plan}}">
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </fieldset>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Set up Payment options</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="panel panel-default app-settings m-t-20">

                                <div class="row form-row">
                                    @if(isset($data))
                                        @foreach($data as $val)
                                            <div class="flip-container">
                                                <div class="flipper">
                                                    <div class="front">
                                                        <h2>{{$val->plan_name}}</h2>

                                                        <div class="price">&#36;{{$val->price}}</div>
                                                    </div>

                                                    <div style="background:#f8f8f8;" class="back">
                                                        <h2>{{$val->plan_name}}</h2>

                                                        <div class="btn-block-flip">
                                                            <a href="{{url('deleteplan',$val->plan_id)}}"
                                                               onclick="return confirm('sure to delete !');"
                                                               class="btn btn-primary"><i class="fa fa-trash"></i> </a>
                                                            <a href="{{url('editplan',$val->plan_id)}}"
                                                               class="btn btn-primary"><i class="fa fa-edit"></i> </a>
                                                            <a href="#myModal{{$val->plan_id}}" data-toggle="modal" data-target="#myModal{{$val->plan_id}}" class="btn btn-primary"><i class="fa fa-eye"></i> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Model -->
                                            <div class="modal fade" id="myModal{{$val->plan_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">{{$val->plan_name}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! $val->description !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}
    <script type="text/javascript">
        $('#plan_form').validate({
            rules: {
                plan_name: "required",
                price: "required",
                interval: "required",
                days: "required",
                description: "required"

            }
        });
    </script>
    {!! Html::script('js/plugin/redactor/limiter.js') !!}
@stop
