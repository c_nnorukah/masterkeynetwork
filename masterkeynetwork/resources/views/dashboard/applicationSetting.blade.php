@extends('layouts.master')
@section('title')
    Application Setting
@stop
@section('main')

    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif
    <div class="row heading-all m-b-20">

        <div class="col-md-3 col-sm-3 col-xs-12">
            <p class="lefty">Application Settings</p>
        </div>
        @if(Session::has('role'))
            <?php $role = Session::get('role'); ?>
            @foreach($role as $val)
                @if($val == '1')
                    <div class="col-md-9 col-sm-9 col-xs-12 text-right m-b-10">
                        <select class="select2 my-button" style="width: 200px;"
                                onChange="window.document.location.href=this.options[this.selectedIndex].value;"
                                value="GO">
                            <option value="{{route('application-settings')}}">Application Settings</option>
                            <option value="{{route('admin-settings')}}">Admin Settings</option>
                        </select>
                    </div>
                @endif
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Twitter</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="panel panel-default app-settings m-t-20">
                                <div class="panel-body">
                                    <div class="text-left">

                                        @if(Session::has('twtoken') || Session::has('access_token'))
                                            <div class="login-details">
                                                @if(Session::has('twtoken'))
                                                    <img src="{!! Session::get('tw_dp') !!}" style="height: 100px; width: 100px;">
                                                    <p><h3>Dear {{Session::get('tw_name')}}</h3></p>
                                                @else
                                                    <img src="{!! Session::get('twitterprofilepic') !!}" style="height: 100px; width: 100px;">
                                                    <p><h3>Dear {{Session::get('twittername')}}</h3></p>
                                                @endif
                                                <a href="{{url('twitter/logout')}}">
                                                    <button class="btn btn-labeled btn-info">
                                                        <span class="btn-label"> <i class="fa fa-twitter-square"></i></span> Logout
                                                        From Twitter
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr>
                                            <form method="post" name="posttweet" action="{{url('post-tweet')}}">
                                                <label><b>Post tweet in twitter</b></label>
                                                <fieldset>
                                                    <div class="form-group">
                                                        <textarea class="form-control" required placeholder="Type Tweet Here..." rows="5" name="tweet" id="tweet"></textarea>
                                                    </div>
                                                    <div class="text-right">
                                                        <input class="btn btn-lg btn-primary" type="submit" onclick="return confirm('Would you like to display tweet on your MKN news feed?')" value="Post tweet">
                                                    </div>
                                                </fieldset>
                                            </form>
                                        @else
                                            <br>
                                            <a href="{{url('twitter/login')}}">
                                                <button type="button" class="btn btn-labeled btn-info">
                                                    <span class="btn-label"><i class="fa fa-twitter-square"></i></span>
                                                    Login Using Twitter
                                                </button>
                                            </a>
                                            <label><b>Login using twitter to see your post on dashboard</b> </label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Facebook</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="panel panel-default app-settings m-t-20">
                                <div class="panel-body">
                                    <div class="text-left">
                                        @if(Session::has('fbtoken') || Session::has('token_fb'))
                                            <div class="login-details">
                                                @if(Session::has('fbtoken'))
                                                    <img src="{!!Session::get('fb_dp') !!}" style="height: 100px; width: 100px; Display:inline;">
                                                    <p><h3>Dear {{Session::get('fb_name')}}</h3></p>
                                                @else
                                                    <img src="{!!Session::get('profilepic') !!}" style="height: 100px; width: 100px; Display:inline;">
                                                    <p><h3>Dear {{Session::get('username')}}</h3></p>
                                                @endif
                                                <a href="{{url('fb/logout')}}">
                                                    <button type="button" class="btn btn-labeled btn-info">
                                                        <span class="btn-label"><i class="fa fa-facebook-square"></i></span> Logout
                                                        From Facebook
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr>
                                            @if(Session::has('getallpage'))

                                                <?php $data = Session::get('getallpage') ?>
                                                @if(empty($data))
                                                    <label><b>No page found in your account</b></label>
                                                    <br><br><br><br><br><br>
                                                    <br><br><br><br><br><br>
                                                @else
                                                    <form accept-charset="UTF-8" action="{{url('fb/post')}}" method="post" id="commentform" role="form">
                                                        <select class="select2 my-button btn-block m-b-10" required name="pagename">
                                                            <option value="">select page</option>
                                                            @foreach($data as $d)
                                                                <option value="{{ $d['id'] . ','. $d['access_token'] }}">{{$d['name']}}</option>
                                                            @endforeach

                                                        </select>

                                                        <fieldset>
                                                            <div class="form-group">
                                                                <textarea class="form-control" required placeholder="Type Comment Here..." rows="5" name="comment" id="comment"></textarea>
                                                            </div>
                                                            <div class="text-right">
                                                                <input class="btn btn-lg btn-primary" onclick="return confirm('Would you like to display post from this page on your MKN news feed?')" type="submit" value=" Post ">
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                @endif
                                            @endif
                                        @else

                                            <a href="{{url('social/facebook')}}">
                                                <button type="button" class="btn btn-labeled btn-info">
                                                    <span class="btn-label"><i class="fa fa-facebook-square"></i></span>
                                                    Login Using Facebook
                                                </button>
                                            </a>
                                            <label><b>1. Login using facebook to see your post on dashboard</b> </label>
                                            <label><b>2. Also post comment to the facebook page</b> </label>

                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Coming Soon</span></h4>

                    {{--<div class="tools"><a href="javascript:;" class="collapse"></a></div>--}}
                    {{--</div>--}}
                    {{--<div class="grid-body no-border">--}}
                    {{--<div class="row-fluid">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="panel panel-default app-settings m-t-20">--}}
                    {{--<div class="panel-body">--}}
                    {{--<div class="text-left">--}}
                    {{--<button type="button" class="btn btn-labeled btn-info">--}}
                    {{--<span class="btn-label"><i class="fa fa-google-plus-square"></i></span> Google Hangout--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--<a href="#" class="btn btn-primary btn-block">Connect now</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-6">--}}
                    {{--<div class="grid simple dashboard-block">--}}
                    {{--<div class="grid-title no-border">--}}
                    {{--<h4><span class="semi-bold">Other Network</span></h4>--}}

                    {{--<div class="tools"><a href="javascript:;" class="collapse"></a></div>--}}
                    {{--</div>--}}
                    {{--<div class="grid-body no-border">--}}
                    {{--<div class="row-fluid">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="panel panel-default app-settings m-t-20">--}}
                    {{--<div class="panel-body">--}}
                    {{--<div class="text-left">--}}
                    {{--<button type="button" class="btn btn-labeled btn-info">--}}
                    {{--<span class="btn-label"><i class="fa fa-google-wallet"></i></span> Other--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--<a href="#" class="btn btn-primary btn-block">Connect now</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>

@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}
@stop