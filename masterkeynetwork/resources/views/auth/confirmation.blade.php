<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title> Confirmation</title>


    {!! Html::style('css/plugin/bootstrap3/bootstrap.min.css') !!}
    {!! Html::style('css/plugin/bootstrap3/bootstrap-theme.min.css') !!}

    {!! Html::style('css/plugin/jquery-scrollbar/jquery.scrollbar.css') !!}
    {!! Html::style('css/plugin/bootstrap-select2/select2.css') !!}
    {!! Html::style('css/plugin/bootstrap-datepicker/datepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-timepicker/bootstrap-timepicker.css') !!}
    {!! Html::style('css/plugin/bootstrap-colorpicker/bootstrap-colorpicker.css') !!}
    {!! Html::style('css/plugin/boostrap-checkbox/bootstrap-checkbox.css') !!}
    @yield('before-styles-end')
    {!! Html::style('css/main-style.css') !!}
    @yield('after-styles-end')
    {!! Html::style('css/plugin/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/plugin/animate.min.css') !!}
</head>
<body class="error-body no-top">
<div class="container">
    <div class="width-100pc width-100pc-xs hcenter">
        <div class="col-md-4 col-md-offset-4 col-xs-12 login-form">
            {!! Html::image('images/master-logo.png', 'logo', ['class' => 'center-block', 'width' => '240']) !!}
            <p class="lead">You have registered successfully.<br> Confirmation mail sent to you.</p>
            <div class="form-group text-center lead">
                Once You Verify Your Account<p class="lead"><a href="{!! URL::to('auth/login') !!}"></i>Sign In Here</a></p>
            </div>
        </div>


    </div>
</div>

{!! Html::script('js/plugin/jquery/jquery-1.11.3.min.js') !!}
{!! Html::script('js/plugin/bootstrap3/bootstrap.min.js') !!}
{!! Html::script('js/plugin/jquery-block-ui/jqueryblockui.min.js') !!}
{!! Html::script('js/plugin/jquery-unveil/jquery.unveil.min.js') !!}
{!! Html::script('js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') !!}
{!! Html::script('js/plugin/jquery-numberAnimate/jquery.animateNumbers.js') !!}
{!! Html::script('js/plugin/jquery-validation/jquery.validate.js') !!}
{!! Html::script('js/plugin/bootstrap-select2/select2.min.js') !!}
{!! Html::script('js/form_validations.js') !!}
@yield('before-scripts-end')
{!! Html::script('js/main.js') !!}
{!! Html::script('js/module/registration.js') !!}
@yield('after-scripts-end')

{!! Html::script('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('js/plugin/boostrap-form-wizard/jquery.bootstrap.wizard.js') !!}
</body>
</html>