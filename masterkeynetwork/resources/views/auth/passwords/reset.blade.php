@extends('layouts.signinpage')

@section('main')
    <div class="width-33pc width-100pc-xs hcenter">
        <div class="col-md-6 col-md-offset-3">
            @if(Session::has('FlashSuccess'))
                @include('includes.error', ['type' => 'success', 'message' => session('FlashSuccess')])
            @elseif(Session::has('flashWarning'))
                @include('includes.error', ['type' => 'warning', 'message' => session('flashWarning')])
            @elseif(Session::has('flash_danger'))
                @include('includes.error', ['type' => 'danger', 'message' => session('flash_danger')])
            @endif
            @if(isset($flashInfo))
                @include('includes.error', ['type' => 'info', 'message' => $flashInfo])
            @endif
        </div>
    </div>
    <div class="col-md-4 col-md-offset-4 col-xs-12 login-form">

        <br>
        {!! Html::image('images/master-logo.png', 'logo', ['class' => 'center-block', 'width' => '240']) !!}
        <p class="lead">Enter your Email and password</p>

        {!! Form::open(['route' => 'auth.reset.password', 'method' => 'post', 'role' => 'form','class' => '', 'id' => 'reset_password_form','name'=>'reset_password_form']) !!}
        <input type="hidden" name="token" value="{{ $token }}" class="form-control required">

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

            <div class="form-group has-feedback">
                {!! Form::input('email', 'email', '', ['class' => 'form-control required email', 'id' => 'txtusername', 'placeholder' => 'Enter Email Address','autocomplete' => 'off']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                                         <strong>{{ $errors->first('email') }}</strong>
                                     </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="form-group has-feedback">
                {!! Form::input('password', 'password', '', ['class' => 'form-control required', 'id' => 'password', 'placeholder' => 'Enter Password','autocomplete' => 'off']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

            <div class="form-group has-feedback">
                {!! Form::input('password', 'password_confirmation', '', ['class' => 'form-control', 'id' => 'password_confirmation', 'placeholder' => 'Enter Confirmation Password','autocomplete' => 'off']) !!}
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group text-center">
            {!! Form::input('submit', 'login', 'Reset', ['class' => 'btn btn-info btn-cons']) !!}
            {!! Form::input('reset', 'login', 'Clear', ['class' => 'btn btn-cons']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('after-scripts-end')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#reset_password_form').validate({
                rules: {
                    'password_confirmation': {
                        equalTo: "#password"
                    }
                }
            });
        });
        $('.msg').fadeOut(5000);
    </script>
@stop