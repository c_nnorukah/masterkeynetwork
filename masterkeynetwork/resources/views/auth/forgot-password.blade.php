@extends('layouts.signinpage')

@section('title')
    Forgot Password
@stop

@section('main')

    <div class="empty-block abs-filler">
        <div class="vcenter">
            <div class="vcenter-this">
                <div class="container">

                    <div class="width-33pc width-100pc-xs hcenter">
                        <div class="col-md-6 col-md-offset-3">
                            @if(Session::has('FlashSuccess'))
                                @include('includes.error', ['type' => 'success', 'message' => session('FlashSuccess')])
                            @elseif(Session::has('flashWarning'))
                                @include('includes.error', ['type' => 'warning', 'message' => session('flashWarning')])
                            @elseif(Session::has('flash_danger'))
                                @include('includes.error', ['type' => 'danger', 'message' => session('flash_danger')])
                            @endif

                            @if(isset($flashInfo))
                                @include('includes.error', ['type' => 'info', 'message' => $flashInfo])
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-4 col-xs-12 login-form">

                        <br>
                        {!! Html::image('images/master-logo.png', 'logo', ['class' => 'center-block', 'width' => '240']) !!}
                        <p class="lead">Enter your Email to reset Your Password

                        </p>

                        {!! Form::open(['route' => 'auth.password.reset.submit', 'method' => 'post', 'role' => 'form','class' => 'validate', 'id' => 'loginForm','name'=>'login_form']) !!}

                        <div class="form-group has-feedback">
                            {!! Form::input('email', 'email', '', ['class' => 'form-control required email', 'placeholder' => 'Enter Email Address', 'autocomplete' => 'off']) !!}
                            <span class="fa fa-bars form-control-feedback"></span>
                        </div>


                        <div class="form-group text-center">
                            {!! Form::input('submit', 'login', 'Submit', ['class' => 'btn btn-info btn-cons']) !!}
                            {!! Form::input('reset', 'login', 'Clear', ['class' => 'btn btn-cons']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
</div>
            </div>
        </div>
    </div>
                    @stop
                    @section('after-scripts-end')
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery('form[name="login_form"]').validate({});
                            });
                            $('.msg').fadeOut(5000);
                        </script>
@stop