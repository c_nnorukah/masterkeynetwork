@extends('global.email.email_template')
@section('main')
<?php echo $token;exit;?>
    <table width="100%" cellpadding="0" cellspacing="0" class="content_holder">
        @include('global.email.greeting')
        <tr>
            <td class="content-block">
                <p>Please <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($email) }}">Click here</a> to reset your password.</p>
            </td>
        </tr>
    </table>

@stop