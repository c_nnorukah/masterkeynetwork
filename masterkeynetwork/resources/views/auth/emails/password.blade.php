@extends('auth.emails.email_template')
@section('main')
    <table width="100%" cellpadding="0" cellspacing="0" class="content_holder">
        {{--@include('global.email.greeting')--}}
        <tr>
            <td class="content-block">
                {{--Click <a href="{!! $link = URL('reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) !!}">here</a> to reset your password.--}}
                Click <a href="{!! URL('reset', $token) !!}">here</a> to reset your password.
{{--                Click <a href="{{ $link = url('reset/'.$token,$token) }}">here</a> to reset your password.--}}
            </td>
        </tr>
    </table>

@stop