@extends('layouts.signinpage')

@section('title')
    Login
@stop

@section('main')
    {{--<div class="width-33pc width-100pc-xs hcenter">
        <div class="col-md-6 col-md-offset-3">
            @if(Session::has('messege'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                    <strong>{{ Session::get('messege') }}</strong>
                </div>
            @endif
        </div>
    </div>--}}
    <div class="col-md-4 col-md-offset-4 col-xs-12 login-form" style="margin-top: 0px;">
        {!! Html::image('images/master-logo.png', 'logo', ['class' => 'center-block', 'width' => '240']) !!}
        <p class="lead">Enter your username and password to login</p>
        {!! Form::open(['route' => 'post-login', 'method' => 'post', 'role' => 'form','class' => 'validate', 'id' => 'loginForm','name'=>'login_form' , 'autocomplete' => 'off']) !!}
        <div class="form-group has-feedback">
            {!! Form::input('email', 'email', '', ['class' => 'form-control required email', 'id' => 'txtusername', 'placeholder' => 'Enter Email Address','autocomplete' => 'off']) !!}
            <span class="fa fa-bars form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            {!! Form::input('password', 'password', '', ['class' => 'form-control required',  'id' => 'txtpassword', 'placeholder' => 'Enter Password','autocomplete' => 'off']) !!}
            <span class="fa fa-lock form-control-feedback"></span>
        </div>

        <div class="form-group text-right">
            <a href="{!! URL::to('reset-password') !!}" class="my-link">Forgot Password?</a>
        </div>


        <div class="form-group text-center">
            {!! Form::input('submit', 'login', 'Login', ['class' => 'btn btn-info btn-cons']) !!}
            {!! Form::input('reset', 'login', 'Clear', ['class' => 'btn btn-cons']) !!}
        </div>

        <div class="form-group text-center">
            <p class="lead"><a href="http://masterkeynetwork.com/pricing/"></i> Register Here</a></p>
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('after-scripts-end')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('form[name="login_form"]').validate({

            });
        });
       // $('.msg').fadeOut(7000);
    </script>
@stop
