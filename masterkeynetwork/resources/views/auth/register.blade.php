@extends('layouts.signinpage')

@section('title')
    Registration Page
@stop

@section('main')
    <?php $classname = "col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12"; ?>
    <div class="{{$classname}}">


        <div class="grid simple transparent registration-form" style="margin-top: 0px;">
            <div class="row text-center p-t-20">
                {!! Html::image('images/master-logo.png', 'logo', ['class' => 'center-block', 'width' => '240']) !!}
                <h2>Registration Form</h2>
            </div>

            <div class="grid-body ">
                <div class="row">

                    {{--{!! Form::open(['route' => 'post.register', 'method' => 'post', 'role' => 'form','class' => 'validate', 'id' => 'registerform','name'=>'register_form']) !!}--}}
                    <form action="{{ URL::to('post-register') }}" method="post" class="validate search_cars"
                          id="registerform" name="register_form" autocomplete="off">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        @if(isset($_GET['referral_code']))


                            <input type="hidden" name="referral_code" value="{{ $_GET['referral_code'] }}"/>
                        @endif
                        <div id="rootwizard" class="col-md-12">
                            <div class="form-wizard-steps">
                                <ul class="wizard-steps">
                                    <li class="" data-target="#step1"><a href="#tab1" data-toggle="tab">
                                            <span class="step">1</span> <span class="title">Basic information</span>
                                        </a></li>
                                    <li data-target="#step2" class=""><a href="#tab2" data-toggle="tab">
                                            <span class="step">2</span> <span class="title">Account information</span>
                                        </a></li>
                                    <li data-target="#step3" class=""><a href="#tab3" data-toggle="tab">
                                            <span class="step">3</span> <span
                                                    class="title">Location</span> </a></li>
                                    {{--<li data-target="#step4" class=""><a href="#tab4" data-toggle="tab">--}}
                                    {{--<span class="step">4</span> <span--}}
                                    {{--class="title">Membership <br>--}}
                                    {{--</span> </a></li>--}}
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-content transparent">
                                <div class="tab-pane" id="tab1"><br>

                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <input type="hidden" id="plan_id"  name="plan_id" value="{{Input::get('plan_id')}}">
                                            <input type="text" placeholder="Username"
                                                   class="form-control" name="user_name"
                                                   id="txtFullName">
                                            <span id="txtFullName-error-exist" class="error" style="display:none;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <input type="text" placeholder="First Name"
                                                   class="form-control" name="first_name"
                                                   id="txtFirstName">

                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" placeholder="Last Name"
                                                   class="form-control" name="last_name"
                                                   id="txtLastName">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2"><br>

                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Email address"
                                                   class="form-control email" autocomplete="off" name="email"
                                                   id="txtEmail">
                                            <span id="txtEmail-error-exist" class="error" style="display:none;"></span>
                                        </div>
                                    </div>
                                    <input type="text" style="display:none;">
                                    <input type="password" style="display:none;">

                                    <div class="row form-row">
                                        <div class="col-md-7">
                                            <input type="password" placeholder="Password"
                                                   class="form-control password" name="password" autocomplete="off"
                                                   id="password">
                                            <span id="txtpass-error" class="error" style="display:none;"></span>
                                        </div>
                                        <div class="col-md-5">

                                            <input type="hidden"
                                                   class=""
                                                   name="country_dial_code">
                                            <input type="tel"
                                                   class="form-control required"
                                                   name="phone_number"
                                                   id="txtPhoneNumber">
                                            <span id="txtphone-error" class="error" style="display:none;"></span>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="text-right">
                                            <div>
                                                <input type="checkbox"> Text notifications on
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab3">
                                    <br>
                                    <input type="hidden" name="usertoken" value="{{str_random(40)}}">

                                    <div class="row form-row">
                                        <div class="col-md-offset-3 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <select name="country_id" id="country_id" class="select2 form-control required" required="required">
                                                    <option value="">Select Country</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->country_id }}">{{ $country->country_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row form-row">
                                        <div class="col-md-offset-3 col-md-6 col-xs-12">
                                            <div class="form-group state_html">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <ul class=" wizard wizard-actions">
                                        <li class="previous first" style="display:none;"><a
                                                    href="javascript:;" class="btn btn-info">&nbsp;&nbsp;First&nbsp;&nbsp;</a>
                                        </li>

                                        <li class="previous" id="previous" style="display: none;"><a href="javascript:;" class="btn btn-info">&nbsp;&nbsp;Previous&nbsp;&nbsp;</a>
                                        </li>
                                        <li class="next last" style="display:none;"><a
                                                    href="javascript:;" class="btn btn-info">&nbsp;&nbsp;Last&nbsp;&nbsp;</a>
                                        </li>
                                        <li class="next" id="next"><a href="javascript:;" class="btn btn-info reg-submit">
                                                &nbsp;&nbsp;Next&nbsp;&nbsp;</a></li>
                                        <li class="submit hidden"><input href="javascript:;"
                                                                         class="btn btn-info search_button"
                                                                         type="submit"  value="Submit"/>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>

                    <center><a href="{{url('auth/login')}}">
                            <div id="loadingProgressG">Loading...</div>
                            <button class="btn btn-info">Already registered</button>
                        </a>

                    </center>
                    {{--{!! Form::close() !!}--}}
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
@stop
@section('before-scripts-end')
    {!! Html::script('js/intlTelInput.min.js') !!}
    {!! Html::script('js/bootstrapValidator.min.js') !!}
    {!! Html::script('js/module/registration.js') !!}

@stop
