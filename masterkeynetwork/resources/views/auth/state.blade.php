@if(count($state_list) > 0)
    {!! Form::select('state_id',$state_list,'1',['class'=>'form-control select2','data-init-plugin'=>'select2']) !!}
@else
    {!! Form::input('text','state_id','',['class'=>'form-control']) !!}
@endif