@extends('layouts.signinpage')

@section('main')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'post-login', 'method' => 'post', 'role' => 'form']) !!}

                        <fieldset>
                            <div class="form-group">
                                {!! Form::input('email', 'email', '', ['class' => 'form-control', 'placeholder' => 'Enter Email Address', 'autocomplete' => 'off']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::input('password', 'password', '', ['class' => 'form-control', 'placeholder' => 'Enter Password', 'autocomplete' => 'off']) !!}
                            </div>
                            <div class="checkbox">
                                <label>
                                    {!! Form::input('checkbox', 'remember', 1, null, ['class' => 'form-control']) !!}
                                    Remember Me
                                </label>
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            {!! Form::input('submit', 'login', 'Login', ['class' => 'btn btn-lg btn-success btn-block']) !!}
                        </fieldset>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@stop