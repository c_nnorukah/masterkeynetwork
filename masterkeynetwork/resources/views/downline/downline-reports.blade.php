@extends('layouts.master')
@section('title')
    Downline Reports
@stop
@section('main')
    <div class="row heading-all m-b-20">

        <div class="col-md-4">
            <p class="lefty">Downline Reports</p>
        </div>

        <div class="col-md-8 text-right m-b-10">

            <form id="downline-search" action="{{url("downline-reports")}}" method="post" class="p-t-5">
                Filter:
                <input id="txtStartDate" name="start_date" type="text" placeholder="From date" value="{{$start_date}}">
                <input id="txtEndDate" name="end_date" type="text" placeholder="To date"  value="{{$end_date}}">
                @if(Session::has('role'))
                    @foreach(Session::get('role') as $val)
                        @if($val == '1')
                            <select class="select2 my-button user_list" name="user_list" style="width:auto!important">
                                @if(!empty($user_list))
                                    <option value="">Please Select User</option>
                                    @foreach($user_list as $val)
                                        @if(isset($val->assignedRoles->role_id) && $val->assignedRoles->role_id == 2)
                                            <option value="{{$val->user_id}}">{{$val->first_name ." ". $val->last_name}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        @endif
                    @endforeach
                @endif
                <input type="submit" class="btn downline-search m-b-5" value="Search">
            </form>
        </div>

    </div>
    <div class="downline-report">
        @if($user_id == 0)
            <div class="row">
                <p claas="lead">You have to select the user from above dropdown to see the their downline reports.</p>
            </div>
        @else
            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <div class="grid simple dashboard-block">
                        <div class="grid-title no-border">
                            <h4><span class="semi-bold">Level 1 Sales</span></h4>
                            <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                        </div>
                        <div class="grid-body no-border">
                            <div class="row-fluid m-t-20 scrollar">
                                @if(!empty(json_decode($first_level)))
                                    <div id="level1"></div>
                                @else
                                    <div class="col-md-12">
                                        <div class="panel panel-default app-settings m-t-20">
                                            <div class="panel-body">
                                                <div class="text-left">
                                                    <button type="button" class="btn btn-block btn-info">Nothing Yet
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="grid simple dashboard-block">
                        <div class="grid-title no-border">
                            <h4><span class="semi-bold">Level 2 Sales</span></h4>
                            <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                        </div>
                        <div class="grid-body no-border">
                            <div class="row-fluid  m-t-20 scrollar">
                                @if(!empty(json_decode($second_level)))
                                    <div id="tree"></div>
                                @else
                                    <div class="col-md-12">
                                        <div class="panel panel-default app-settings m-t-20">
                                            <div class="panel-body">
                                                <div class="text-left">
                                                    <button type="button" class="btn btn-block btn-info">Nothing Yet
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="grid simple dashboard-block">
                        <div class="grid-title no-border">
                            <h4><span class="semi-bold">Level 3 Sales</span></h4>
                            <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                        </div>
                        <div class="grid-body no-border">
                            <div class="row-fluid  m-t-20 scrollar">
                                @if(!empty(json_decode($third_level)))
                                    <div id="level3"></div>
                                @else
                                    <div class="col-md-12">
                                        <div class="panel panel-default app-settings m-t-20">
                                            <div class="panel-body">
                                                <div class="text-left">
                                                    <button type="button" class="btn btn-block btn-info">Nothing Yet
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        @endif
            <input type="hidden" id="first_level" value='<?php echo($first_level)?>'>
            <input type="hidden" id="second_level" value='<?php echo($second_level)?>'>
            <input type="hidden" id="third_level" value='<?php echo($third_level)?>'>
    </div>
@stop
@section('after-scripts-end')
  {!! Html::script('js/module/downline-report.js') !!}
@stop

