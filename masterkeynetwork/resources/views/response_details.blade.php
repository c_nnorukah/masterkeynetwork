        <div class="modal-content modal-window"  id="modal1">

            <div class="modal-header">
                @if(count($data)!=0)
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                @foreach($data as $val)
                     <?php $new_val= json_decode($val,true) ;
                        $title=$new_val['projects']['name'];
                    ?>@endforeach
                <h4 class="modal-title">Response for {{ @$title }}</h4>
                @endif
            </div>
            <div class="modal-body pageresponse-model">
                @if(count($data)!=0)
                    <?php $i=0?>
                    <table id="example" class="table table-striped table-bordered nowrap" width="100%" cellspacing="0">
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Website</th>
                            <th>Message</th>
                            <th>Ip Add.</th>
                            <th>Browser Details.</th>
                        </tr>
                        <tbody class="table">
                        @foreach($data as $key=>$val)
                            <?php $response_data[]= json_decode($val['response'],true); ?>

                                <tr>
                                <td>{{ @$response_data[$i]['firstname'] }}</td>
                                <td> {{ @$response_data[$i]['lastname'] }}</td>
                                <td>{{ @$response_data[$i]['email'] }}</td>
                                <td>{{ @$response_data[$i]['phone'] }}</td>
                                <td>{{ @$response_data[$i]['website'] }}</td>
                                <td>{{ @$response_data[$i]['message'] }}</td>
                                <td>{{ @$response_data[$i]['SERVER_ADDR'] }}</td>
                                <td>{{ @$response_data[$i]['HTTP_USER_AGENT'] }}</td>

                            </tr>
                            <?php $i++ ?>
                        @endforeach
                            </tbody>
                    </table>
                        <?php echo $data->render(); ?>
                @else
                    <h4>No Any Response </h4>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>