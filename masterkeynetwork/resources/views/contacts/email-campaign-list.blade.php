@extends('layouts.master')
@section('title')
    Pending Email Campaign
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-5">
            <p class="lefty">Pending Email Campaign</p>
        </div>
        <div class="col-md-7 text-right m-b-10 dataTables_filter">
            <a href="{{ url('email-campaign') }}" class="btn btn-primary">Start an Email Campaign</a>
        </div>

    </div>
    <div class="row">

        <div class="col-md-12">
            <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <div class="ajax-table">
                    <input type="hidden" id="sort_by" value="{{$sort_by}}">
                    <input type="hidden" id="sort_order" value="{{$sort_order}}">
                    <input type="hidden" id="limit" value="{{$limit}}">
                    <table class="table table-bordered dataTable">
                        <thead>
                        <tr role="row">
                            <?php $class = ($sort_by == 'subject') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("email-campaign-list/subject/$need_to_sort/$limit") !!}">Email Subject</a>
                            </th>

                            <?php $class = ($sort_by == 'email_templates.subject') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("email-campaign-list/template/$need_to_sort/$limit") !!}">Template</a>
                            </th>

                            <?php $class = ($sort_by == 'send_on') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("email-campaign-list/send_on/$need_to_sort/$limit") !!}">Send On</a>
                            </th>

                            <?php $class = ($sort_by == 'created_at') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("email-campaign-list/created-at/$need_to_sort/$limit") !!}">Created
                                    On</a>
                            </th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @if(!empty($email_campaign->total()))
                            @foreach($email_campaign as $email)
                                <tr class="gradeX odd">

                                    <td class=" ">{!! $email->subject !!}</td>
                                    <td class=" ">{!! $email->template->subject !!}</td>
                                    <td class=" ">{!! format_date($email->send_on) !!}</td>
                                    <td class=" ">{!! $email->created_at != "" ? format_date($email->created_at)  : "" !!}</td>
                                    <td>
                                        <a href="{!! URL("email-campaign/".$email->id) !!}" class="mr-r-10"><i
                                                    class="fa fa-edit"></i></a>
                                        <a href="{!! URL("email-campaign-delete/".$email->id) !!}"
                                           class="campaign_delete"><i class="fa fa-trash"></i></a>
                                    </td>
                               </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">
                                    <center>No Result Found</center>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    @if(!empty($email_campaign->total()))
                        <div class="row">
                            <div class="col-md-12 text-right">
                                    <span class="dataTables_paginate paging_bootstrap pagination">
                                        {!! $email_campaign->render() !!}
                                    </span>
                                <?php $start = empty($email_campaign->total()) ? 0 : $email_campaign->currentPage() * $limit - ($limit - 1);
                                $to = empty($email_campaign->total()) ? 0 : $start + $email_campaign->count() - 1;
                                ?>
                                <span class="dataTables_info pull-left">Showing <b>{!! $start !!}
                                        to {!! $to !!}</b> of {!!  $email_campaign->total() !!}
                                    entries</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/contact_list.js') !!}
@stop
<script>
    var redirect_url = '{!! URL("email-campaign-list") !!}';
</script>