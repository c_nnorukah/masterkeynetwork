@extends('layouts.master')
@section('title')
    User List
@stop
@section('main')

    <div class="page-title">
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-3">
            <p class="lefty">Contact List</p>
        </div>
        <div class="pull-right">
        <a href="{{ url('contacts/create')  }}" class="btn btn-default" role="button">Add contacts</a>
        </div>
    </div>
    @if(count($contacts)>0)
        <table  class="table table-striped table-bordered nowrap" width="100%" cellspacing="0">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
            <tbody class="table">
            @foreach($contacts as $key=>$val)
                <tr>
                    <td>{{ @$val['first_name'] }}</td>
                    <td> {{ @$val['last_name'] }}</td>
                    <td>{{ @$val['email'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <?php echo $contacts->render(); ?>
        @else
                <h3>No Any Data To Show.</h3>
        @endif

@stop
