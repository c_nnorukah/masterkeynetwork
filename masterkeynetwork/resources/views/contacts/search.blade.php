<form method="post" action="{!! URL("contact-book/$sort_by/$sort_order/$limit") !!}" class="contact_search"
      id="contact_search">
    @if(Session::has('role'))
        @foreach(Session::get('role') as $val)
            @if($val == '1')

                <select class="select2 my-button user_list" name="user_id" style="width:auto!important">
                    @if(!empty($users))
                        <option value="">Please Select User</option>
                        @foreach($users as $val)
                            @if(isset($val->assignedRoles->role_id) && $val->assignedRoles->role_id == "2")
                                <option value="{{$val->user_id}}" @if(isset($user_id) && $val->user_id == $user_id) selected @endif>{{$val->first_name ." ". $val->last_name}}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            @endif
        @endforeach
    @endif
    <div class="search_container">
        <select name="search_criteria" class="search_criteria " required="true">
            <option value="">Please select</option>
            <option value="name" @if($selected_criteria == "name") selected @endif>Name</option>
            <option value="email" @if($selected_criteria == "email") selected @endif>Email Address</option>
            <option value="source_app_name" @if($selected_criteria == "source_app_name") selected @endif>Source
                Application
            </option>
            <option value="list" @if($selected_criteria == "list") selected @endif>List
            </option>
            <option value="signup_date" @if($selected_criteria == "signup_date") selected @endif>Signup Date</option>
        </select>
    </div>
    <div class="search_container search_conditions">
        <input type="text" name="name"
               class="search_criteria_div @if($selected_criteria != "name") hide @endif name_div" required="true"
               value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "name") {!! $post_data["name"]!!} @endif">
        <input type="text" name="email"
               class="search_criteria_div @if($selected_criteria != "email") hide @endif email_div" required="true"
               value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "email") {!! $post_data["email"]!!} @endif">
        <select name="source_app_name"
                class="search_criteria_div @if($selected_criteria != "source_app_name") hide @endif source_app_name_div" required="true">
            <option value="">Select Source</option>
            @foreach($source_app as $value)
                <option value="{!! $value!!}"
                        @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "source_app_name" &&
                        @$post_data["source_app_name"] == $value ) selected @endif>{!! $value!!}</option>
            @endforeach
        </select>
        <select name="source_app_name"
                class="search_criteria_div @if($selected_criteria != "source_app_name") hide @endif source_app_name_div" required="true">
            <option value="">Select Source</option>
            @foreach($source_app as $value)
                <option value="{!! $value!!}"
                        @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "source_app_name" &&
                        @$post_data["source_app_name"] == $value ) selected @endif>{!! $value!!}</option>
            @endforeach
        </select>
        <select name="list"
                class="search_criteria_div @if($selected_criteria != "list") hide @endif list_div" required="true">
            <option value="">Select List</option>
            @foreach($landingpage_list as $key=>$value)
                <option value="{!! $key!!}"
                        @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "list" &&
                        @$post_data["list"] == $key ) selected @endif>{!! $value!!}</option>
            @endforeach
        </select>
        <div class="search_criteria_div @if($selected_criteria != "signup_date") hide @endif signup_date_div">
            <div class="search_container">
                <input type="text" name="from_signup" class="from_date" id="txtStartDate" required="true"
                       value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "from_signup") {!! $post_data["from_signup"]!!} @endif">
            </div>
            <div class="search_container">
                <input type="text" name="to_signup" class="to_date" id="txtEndDate" required="true"
                       value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "to_signup") {!! $post_data["to_signup"]!!} @endif">
            </div>
        </div>
    </div>
    <div class="search_container">
        <input type="submit" class="btn user_search" value="Search" name="search">
        <a href="{!! URL("contact-book-reset") !!}" class="btn ajax-link reset-link">Reset</a>
        <a href="{{ url('contacts/create') }}" class="btn btn-primary">Add Contact</a>
    </div>
</form>