@extends('layouts.master')
@section('title')
    Contact Book
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-3">
            <p class="lefty">Contact Book</p>
        </div>
        <div class="col-md-9 text-right m-b-10 dataTables_filter">
            @include("contacts.search")
        </div>

    </div>
    <div class="row">

        <div class="col-md-12">
            <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <div class="ajax-table">
                    @if($user_id == 0)
                        <div class="row">
                            <p claas="lead">You have to select the user from above dropdown to see the their contact
                                book.</p>
                        </div>
                    @else
                        <input type="hidden" id="sort_by" value="{{$sort_by}}">
                        <input type="hidden" id="sort_order" value="{{$sort_order}}">
                        <input type="hidden" id="limit" value="{{$limit}}">
                        <table class="table table-bordered dataTable">
                            <thead>
                            <tr role="row">
                                <?php $class = ($sort_by == 'first_name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/first-name/$need_to_sort/$limit") !!}">Name</a>
                                </th>

                                <?php $class = ($sort_by == 'email') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/email/$need_to_sort/$limit") !!}">Email</a>
                                </th>

                                <?php $class = ($sort_by == 'users.first_name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/user/$need_to_sort/$limit") !!}">User</a>
                                </th>

                                <?php $class = ($sort_by == 'source_app_name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/source-app-name/$need_to_sort/$limit") !!}">Source
                                        Application</a>
                                </th>

                                <?php $class = ($sort_by == 'landingpage_list.name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/list/$need_to_sort/$limit") !!}">List</a>
                                </th>

                                <?php $class = ($sort_by == 'created_at') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                                <th class="{!! $class !!}"><a
                                            href="{!! URL("contact-book/created-at/$need_to_sort/$limit") !!}">Imported
                                        On</a>
                                </th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            @if(!empty($user_data->total()))
                                @foreach($user_data as $user)
                                    <tr class="gradeX odd">

                                        <td class=" ">{!! $user->first_name ." ".$user->last_name !!}</td>
                                        <td class=" ">{!! $user->email !!}</td>
                                        <td class=" ">{!! $user->user->first_name ." ". $user->user->last_name !!}</td>
                                        <td class=" ">{!!  $user->source_app_name  !!}</td>
                                        <td class=" ">{!!  isset($user->contact_list->name) ? $user->contact_list->name : ""  !!}</td>
                                        <td class=" ">{!! $user->created_at != "" ? format_date($user->created_at)  : "" !!}</td>
                                        <td>
                                            <a href="{!! URL("contact-delete/".$user->contacts_id) !!}"
                                               class="contact_delete"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">
                                        <center>No Result Found</center>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        @if(!empty($user_data->total()))
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <span class="dataTables_paginate paging_bootstrap pagination">
                                        {!! $user_data->render() !!}
                                    </span>
                                    <?php $start = empty($user_data->total()) ? 0 : $user_data->currentPage() * $limit - ($limit - 1);
                                    $to = empty($user_data->total()) ? 0 : $start + $user_data->count() - 1;
                                    ?>
                                    <span class="dataTables_info pull-left">Showing <b>{!! $start !!}
                                            to {!! $to !!}</b> of {!!  $user_data->total() !!}
                                        entries</span>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/contact_list.js') !!}
@stop
<script>
    var redirect_url = '{!! URL("contact-book") !!}';
</script>