@extends('layouts.master')
@section('title')
    Contact List
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-3">
            <p class="lefty">Contact List</p>
        </div>
        <div class="col-md-9 text-right m-b-10 dataTables_filter">
            <select name="contact-list" class="contact-list">
                <option>Select Predefined Contact List</option>
                @if(!empty($contact_list))
                    @foreach($contact_list as $key=>$val)
                        <option value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 email-fields">
            <form role="form" class="editor-form m-b-20" id="contact-list-form" method="post"
                  action="{{url("contact-list")}}">
                <input type="hidden" value="" name="list_id" id="list_id">
                <div class="form-group">
                    <input placeholder="Name" class="form-control" required="true" name="name" type="text"
                           value="">
                </div>
                <div class="form-group">
                    <div class="col-md-2 pull-right">
                        <input type="submit" class="btn btn-block btn-primary save-button" value="Save" name="submit">
                    </div>
                </div>

            </form>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/contact_list.js') !!}
@stop
<script>
    var redirect_url = '{!! URL("contact-list") !!}';
</script>