@extends('layouts.master')
@section('title')
    Import Contacts
@stop
@section('main')

    <div class="page-title">
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-3">
            <p class="lefty">Import Contacts</p>
        </div>
        <div class="pull-right">
            <a href="{{ url('contact-list') }}" class="btn">Add Contact List</a>
            <a href="{{ url('contact-book') }}" class="btn btn-primary">View Contacts</a>
        </div>
    </div>

    <div class="container">
        @if(Session::has('messege'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <strong>{{ Session::get('messege') }}</strong>
            </div>
        @endif


        <h2>Where would you like to import contacts from?</h2>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Copy & Paste</a></li>
            <li><a data-toggle="tab" href="#menu1">Upload File</a></li>
            <li><a data-toggle="tab" href="#menu2">Another Service</a></li>
        </ul>

        <div class="tab-content">

            <div id="home" class="tab-pane fade in active">
                <div class="row">
                    <h3>Copy Paste</h3>
                    <form action="{{ URL::to('contacts/storecp') }}" class="form-horizontal" method="post"
                          enctype="multipart/form-data">
                        <div class="form-group">
                            <label>List</label>
                            <select name="list_id">
                                <option value="">Please select list</option>
                                @if(!empty($landingpage_list))
                                    @foreach($landingpage_list as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Enter only one contact per line.with this format : John Doe,johndoe@gmail.com</label>
                    <textarea cols="67" rows="6" name="import_cp" id="import_cp"
                              placeholder="John Doe,johndoe@gmail.com"></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Send">
                    </form>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>Upload File</h3>
                <form action="{{ URL::to('contacts/importExcel') }}" class="form-horizontal" method="post"
                      enctype="multipart/form-data" id="import_via_csv_file">
                    <div class="form-group">
                        <label>List</label>
                        <select name="list_id">
                            <option value="">Please select list</option>
                            @if(!empty($landingpage_list))
                                @foreach($landingpage_list as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Upload the Csv or Excel File</label>
                        <input type="file" name="import_file" id="import_file"/>
                    </div>
                    <button class="btn btn-primary">Import File</button>
                </form>

            </div>
            <div id="menu2" class="tab-pane fade">
                <h3>Services</h3>

                <div class="row">
                    <div class="col-sm-3">
                        <form id="import_via_service" class="form-validate" method="post">
                            <div class="form-group">
                                <label>List</label>
                                <select name="list_id">
                                    <option value="">Please select list</option>
                                    @if(!empty($landingpage_list))
                                        @foreach($landingpage_list as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mail chimp Contacts</label>
                                <input type="text" name="mailchimp_service" class="required" placeholder="API_KEY"
                                       id="mailchimp_service"
                                       value="{{ (!empty($contactKey->mail_chimp)) ? $contactKey->mail_chimp : '' }}"/><br/>
                            </div>
                            <div class="form-group">
                                <div class="mailchimp_list_id_select"></div>
                                {{--<select name="mailchimp_list_id" id="mailchimp_list_id">--}}
                                    {{--@if(!empty($mailchimp_list_id))--}}
                                        {{--@foreach($mailchimp_list_id['data'] as $id=>$value)--}}
                                            {{--<option id="{{$value['id']}}">{{ $value['name'] }}</option>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                {{--</select><br/>--}}
                            </div>
                            <input type="button" name="mailchimp_submit" class="btn btn-info mailchimp_submit"
                                   value="Send">
                        </form>
                    </div>


                    <div class="col-sm-3">
                        <form id="import_via_get_response_service" class="form-validate" method="post">
                            <div class="form-group">
                                <label>List</label>
                                <select name="list_id">
                                    <option value="">Please select list</option>
                                    @if(!empty($landingpage_list))
                                        @foreach($landingpage_list as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>GetResponse Contacts</label>
                                <input type="text" name="get_response_service" class="required" placeholder="API_KEY"
                                       id="get_response_service"
                                       value="{{ (!empty($contactKey->get_response)) ? $contactKey->get_response : '' }}"/><br/>
                            </div>
                            {{--<select name="getResponse_list_id" id="getResponse_list_id">--}}
                            {{--@if(!empty($getResponse_list_id))--}}
                            {{--@foreach($getResponse_list_id['data'] as $id=>$value)--}}
                            {{--<option id="{{$value['id']}}">{{ $value['name'] }}</option>--}}
                            {{--@endforeach--}}

                            {{--@endif--}}
                            {{--</select><br/>--}}
                            <input type="button" name="get_response_submit" class="btn btn-info getreponse_submit"
                                   value="Send">
                        </form>
                    </div>

                    {{--<div class="col-sm-3">--}}
                    {{--<form action="{{url("contacts/importConstantContact")}}" method="post">--}}
                    {{--<div class="form-group">--}}
                    {{--<label>Constant Contacts</label>--}}
                    {{--<input type="submit" name="submit" class="btn btn-info" value="import">--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--@endif--}}
                    {{--</select><br/>--}}
                    {{--<input type="button" name="get_response_submit" class="btn btn-info getreponse_submit" value="Send">--}}
                    {{--</form>--}}

                    <div class="col-sm-3">
                        <form action="{{url("contacts/importConstantContact")}}" method="post">
                            <label>Constant Contacts</label>
                            <div class="form-group">
                                <label>List</label>
                                <select name="list_id">
                                    <option value="">Please select list</option>
                                    @if(!empty($landingpage_list))
                                        @foreach($landingpage_list as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <input type="submit" name="submit" class="btn btn-info" value="import">
                        </form>
                        <form action="{{url("contacts/importAweber")}}" method="post">
                            <label>Aweber Contacts</label>
                            <div class="form-group">
                                <label>List</label>
                                <select name="list_id">
                                    <option value="">Please select list</option>
                                    @if(!empty($landingpage_list))
                                        @foreach($landingpage_list as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <input type="submit" name="submit" class="btn btn-info" value="import">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('after-scripts-end')
    {!! Html::script('js/module/contacts.js') !!}
@stop

