@extends('layouts.master')
@section('title')
    Email Campaign
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="page-title">
    </div>
    <div class="row heading-all m-b-20">

        <div class="col-md-3">
            <p class="lefty">Email Campaign</p>
        </div>
        <div class="col-md-9 text-right m-b-10 dataTables_filter">
            <a href="{{ url('email-campaign-list') }}" class="btn btn-primary">Pending Email Campaign</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 email-fields">
            <form role="form" class="editor-form m-b-20" id="email-campaign-form" method="post"
                  action="{{url("send-campaign-mail")}}">
                <input type="hidden" name="email_campaign_id"
                       value="{{!empty($emailCampaign) ? $emailCampaign->id : 0}}">
                <div class="form-group">
                    <div class="radio">
                        <input type="radio" name="contact_type" value="list" id="contact_list"
                               class="radio_required required_hidden"
                               @if(!empty($emailCampaign) && $emailCampaign->contact_type=="list") checked @endif>
                        <label for="contact_list">Contact List</label>
                        <input type="radio" name="contact_type" value="individual"
                               id="indivial_contact" class="radio_required required_hidden"
                               @if(!empty($emailCampaign) && $emailCampaign->contact_type=="individual") checked @endif>
                        <label for="indivial_contact">Individual Contact</label>
                    </div>
                </div>
                <?php
                $hide = !empty($emailCampaign) && $emailCampaign->contact_type == "list" ? "" : "hide";
                ?>
                <div class="form-group contact_list_div {{$hide}}">
                    @if(count($landingpage_list) > 0)
                        <label>Select the contact list from following list to which you want to send mail.</label>
                        <div class="checkbox">
                            <?php $i = 0 ?>
                            @foreach($landingpage_list as $key=>$list)
                                <input type="checkbox" name="list[{{$i++}}]" value="{{$key}}" id="list1_{{$key}}"
                                       @if(!empty($list_array) && in_array($key,$list_array)) checked @endif>
                                <label for="list1_{{$key}}">{{$list}}</label><br>
                            @endforeach
                        </div>
                    @else
                        <label>No contact list exist. Please add new contact list and import contacts in that.</label>
                        <div><a href="{{ url('contact-list') }}" class="btn btn-primary">Add contact List</a></div>
                    @endif
                </div>
                <?php
                $hide = !empty($emailCampaign) && $emailCampaign->contact_type == "individual" ? "" : "hide";
                ?>
                <div class="form-group individual_contact_div {{$hide}}">
                    @if(count($contact_list) > 0)
                        <label>Select the individual contact from following list to which you want to send mail.</label>
                        <div class="checkbox">
                            <?php $i = 0 ?>
                            @foreach($contact_list as $key=>$list)
                                <input type="checkbox" name="contacts[{{$i++}}]" value="{{$list->contacts_id}}"
                                       id="list_{{$list->contacts_id}}"
                                       @if(!empty($contact) && in_array($list->contacts_id,$contact)) checked @endif>
                                <label for="list_{{$list->contacts_id}}">{{$list->first_name." ".$list->last_name ." [ ".$list->email." ] "}}</label>
                                <br>
                            @endforeach
                        </div>
                    @else
                        <label>No contacts exist. Please import new contacts.</label>
                        <div><a href="{{ url('contacts/create') }}" class="btn btn-primary">Import Contacts</a></div>
                    @endif
                </div>
                <div class="form-group">
                    <select class="m-r-5 email_template width-100" required="true" name="email_template_id">
                        <option value="">Select predefined email templates</option>

                        @foreach($EmailTemplate as $key=>$value)
                            <option value="{{ $value->email_template_id }}"
                                    @if(!empty($emailCampaign) && $emailCampaign->template_id == $value->email_template_id) selected @endif>{{ $value->subject  }}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name="email_template" value="">
                </div>
                <div class="form-group">
                    <input placeholder="Subject" class="form-control" required="true" name="subject" type="text"
                           value="{{isset($emailCampaign->subject) ? $emailCampaign->subject : ""}}">
                </div>

                <div class="row">
                    <div class="grid simple dashboard-block">
                        <div class="">
                            <div class="row-fluid m-t-0">
                                <textarea rows="10" cols="7" class="form-control editor_required required_hidden"
                                          id="redactor_content"
                                          placeholder="Enter body ..." name="email_body">
                                    {{isset($emailCampaign->body) ? $emailCampaign->body : ""}}
                                </textarea>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="radio">
                        <input type="radio" name="send_on" value="now" id="send_now"
                               class="radio_required required_hidden"
                               @if(empty($emailCampaign->send_on)) checked @endif>
                        <label for="send_now">Send Now</label>
                        <input type="radio" name="send_on" value="future"
                               id="send_future" class="radio_required required_hidden"
                               @if(!empty($emailCampaign->send_on)) checked @endif>
                        <label for="send_future">Send Later</label>
                    </div>
                </div>
                <?php
                $hide = !empty($emailCampaign->send_on)? "" : "hide";
                ?>
                <div class="form-group send_later_div {{$hide}}">
                    <input placeholder="Send on" class="form-control datepicker current_date" required="true"
                           name="send_date" type="text"
                           value="{{isset($emailCampaign->send_on) ? format_date($emailCampaign->send_on ,"m/d/Y h:i a"): ""}}">
                </div>
                <div class="form-group">
                    <div class="col-md-2 pull-right">
                        <input type="submit" class="btn btn-block btn-primary save-button savebutton" value="Save" name="submit">
                    </div>
                </div>

            </form>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/email_campaign.js') !!}
@stop