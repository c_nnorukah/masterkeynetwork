@extends('layouts.master')

@section('title')
    SMTP Setting
@stop

@section('main')
    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif
    <div class="row heading-course">
        <div class="col-md-12">
        </div>
    </div>
    <div class="heading-all">
        <div class="row">
            <div class="col-md-6">
                <h2 class="title">SMTP Settings</h2>
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="btn my-button" data-toggle="modal" data-target=".bs-example-modal-lg"
                   role="button">Add New</a>
            </div>

        </div>
    </div>

    <div class="row">
        @if(count($data)>0)
        <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 my-accordion">
            <div class="panel-group" id="accordion">

                @foreach($data as $key => $val)
                    @if($val->user_id == Auth::user()->user_id)
                    <form class="form-row" action="{{url('update-smtp',$val->smtp_setting_id)}}" id="form_smtp" method="post">
                        <div class="panel panel-default">

                        <div class="panel-heading actives">
                            <h4 class="panel-title">
                                <a                                                                                                                                                                                                                       data-toggle="collapse" data-parent="#accordion"  href="#collapse{{$key}}" aria-expanded="false">
                                    <span class="edu-title">{{$val->title}}</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{$key}}" class="panel-collapse collapse {{($key==0) ? "" : ""}}" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label class="control-label col-sm-3" for="email">Title:</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="title" value="{{$val->title}}" id="title" autocomplete="off" class="form-control required"  placeholder="Enter title">
                                            </div>
                                        </div>

                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <label class="control-label col-sm-3" for="email">Email-Id</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="email" value="{{$val->email_for_smtp}}" name="email" id="email" class="form-control"  placeholder="Enter email">
                                                </div>
                                            </div>
                                            <input type="text" style="display:none;">
                                            <input type="password" style="display:none;">

                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <label class="control-label col-sm-3" for="pwd">Port:</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="port" id="port" value="{{$val->port}}" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <label class="control-label col-sm-3" for="pwd">Domain:</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="domain" id="domain" value="{{$val->domain}}" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <label class="control-label col-sm-3" for="pwd">Username:</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="user_name" id="user_name" value="{{$val->user_name}}" class="form-control" >
                                                </div>
                                               <br><br><br>
                                               <center> <input type="submit" class="btn btn-info" name="Save" value="Save">
                                                &nbsp;&nbsp;&nbsp;<a href="{{url('delete-smtp',$val->smtp_setting_id)}}" onclick="return confirm('Sure to delete?')" class="btn btn-info">Delete</a></center>
                                            </div>
                                           </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    </form>
                    @endif
                @endforeach
                </div>
            </div>
        @else
            <center>
                <h3>No SMTP Settings Available</h3>
            </center>
        @endif
    </div>    <!-- END DASHBOARD TILES -->
    @include('smtp-setting.AddSmtpSetting')
    <div id="AddSmtpSettingForm"></div>
@stop