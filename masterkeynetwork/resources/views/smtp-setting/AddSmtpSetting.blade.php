<div class="row text-center">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" id="scroll-off">
            <div class="modal-content-2">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                </div>
                <div class="edu-pop-content">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                                <form class="form-row" method="post" id="smtp_form" action="{{url('add-smtp')}}">
                                    <div class="form-group row">
                                                                                 <div class="col-sm-3">
                                                                                       <label class="control-label col-sm-3" for="email">Title:</label>
                                                                                     </div>
                                                                            <div class="col-sm-6">
                                                                            <input type="text" name="title" id="title" autocomplete="off" class="form-control required"  placeholder="Enter title">
                                                                                     </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtpTitle') }}"></i>

                                        </div>
                                                                          </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="control-label col-sm-3" for="email">Email-Id:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="email" name="email_for_smtp" id="email_for_smtp" autocomplete="off" class="form-control required" id="email" placeholder="Enter email">
                                        </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtpEmail') }}"></i>

                                        </div>
                                    </div>
                                    <input type="text" style="display:none;">
                                    <input type="password" style="display:none;">

                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="control-label col-sm-3" for="email">Password:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" autocomplete="off" class="form-control required" id="password" placeholder="Enter Password">
                                        </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtPassword') }}"></i>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="control-label col-sm-3" for="pwd">Port:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="port" id="port" pattern="[0-9]{3,4}" class="form-control required" id="pwd" placeholder="Enter Port">
                                        </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtpPort') }}"></i>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="control-label col-sm-3" for="pwd">Domain:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="domain" id="domain" class="form-control" id="pwd" placeholder="Enter Domain">
                                        </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtpDomain') }}"></i>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="control-label col-sm-3" for="pwd">Username:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="name" name="user_name" id="user_name" class="form-control" id="pwd" placeholder="Enter Username">
                                        </div>
                                        <div class="col-sm-2">
                                            <i class="fa fa-question-circle" title="{{ trans('message.tooltip_SmtpUserame') }}"></i>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Save Settings</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('after-scripts-end')
    <script type="text/javascript">
        $('#smtp_form').validate({
            rules: {
                email_for_smtp: "required",
                password: "required",
                port: "required",
                domain: "required",
                user_name: "required",
                title: "required"
            }
        });
    </script>
@stop