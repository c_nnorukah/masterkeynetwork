@extends('layouts.master')

@section('title')
    Education
@stop

@section('main')
    <div class="clearfix"></div>
        <button class="btn btn-primary pull-left" onclick="history.go(-1);"><i class="fa fa-arrow-left"></i> Back</button>
        <div class="clearfix"></div>
    <div class="row heading-course m-b-20">
        <div class="col-md-12"><h3>Education</h3></div>
        <div class="col-md-10 col-xs-12 text-center"><h2>{{ $data->coursecategory['category_name'] }}</h2></div>
    </div>

    <div class="row">
        <div class="col-md-4 m-b-10">
            <div class="">
                <div style="max-height:214px" class="tiles green  overflow-hidden full-height">
                    <div class="overlayer bottom-right fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="tiles gradient-black p-l-20 p-r-20 p-b-20 p-t-20">
                                <div class="pull-right"><a class="hashtags transparent" href="#"> #Art Design </a></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="tiles white">
                    <div class="tiles-body">
                        <div class="row">
                            <div class="">
                                <?php $flag = 0;?>
                                @foreach($data->coursematerial as $img_url)
                                    @if($img_url['is_default']==1)
                                        <?php $flag = 1;?>
                                        {!! (isset($img_url['material_url'])) ? Html::image($img_url['material_url'], 'feature', ['style' => 'width:200px;height:200px;','class'=>'widget-item']) : "" !!}
                                    @endif
                                @endforeach

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="{{ ($flag) ? "col-md-8" : "col-md-12" }}">
            <div class="edu-content-block">
                <h1>{{ $data->course_title }}</h1>

                <p class="lead">{{ $data->course_description }}</p>

            </div>
        </div>
    </div>
     @if(sizeof($data->coursematerial) >0)
    <div class="row course-material">
        <h3 class="text-center m-b-20">Course material</h3>


        <div class="col-md-4 col-sm-4 m-b-10 widget-item" data-aspect-ratio="true">
            <div class="live-tile slide ha" data-speed="750" data-delay="6000" data-mode="carousel">


                @foreach($data->coursematerial as $img_url)
                    @if($img_url['type']==0)
                            <img class="image-responsive-width xs-image-responsive-width"
                                 src="{{url($img_url['material_url'])}}" alt="" height="130" width="130">
                    @endif
                @endforeach

            </div>
        </div>

        <div class="col-md-4 col-sm-4 m-b-10" data-aspect-ratio="true">
            <div class="live-tile slide ha " data-speed="750" data-delay="6000" data-mode="carousel">
            @foreach($data->coursematerial as $img_url)
                    @if($img_url['type']==2)
                    @if(strpos($img_url['material_url'], 'youtube') > 0)
                            <div class="slide-front ha tiles blue slide active widget-item">

                            <iframe width="340" height="340"
                                src="https://www.youtube.com/embed/{{ $img_url['material_key'] }}" frameborder="0"
                                allowfullscreen></iframe></div>

                    @elseif(strpos($img_url['material_url'], 'vimeo') > 0)
                            <div class="slide-front ha tiles blue slide active widget-item">

                            <iframe width="340" height="340"
                                src="http://player.vimeo.com/video/{{ $img_url['material_key'] }}?api=1" frameborder="0"
                                allowfullscreen></iframe></div>
                    @elseif(strpos($img_url['material_url'], 'soundcloud') > 0)
                            <div class="slide-front ha tiles blue slide active widget-item">

                            <iframe width="340" height="340" scrolling="no" frameborder="no"
                                src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $img_url['material_key'] }}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
</div>
                    @endif
                @endif
                @endforeach
                </div>
        </div>

        <div class="col-md-4 col-sm-4 m-b-10" data-aspect-ratio="true">
            <div class="live-tile slide ha widget-itemf" data-speed="750" data-delay="4000" data-mode="carousel">
                @foreach($data->coursematerial as $img_url)
                    @if($img_url['type']==1)
                        <div class="slide-front ha tiles blue ">
                            <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                                <h4 class="text-white no-margin custom-line-height">Click <span class="semi-bold"> to </span>
                                    <span class="semi-bold"> Download </span> Document
                                    </h4>
                            </div>
                            <div class="overlayer bottom-left fullwidth">
                                <div class="overlayer-wrapper">
                                    <div class="user-comment-wrapper">
                                        <div class="profile-wrapper">
                                            {{--<a href="{{ URL::to('course/get-download?file_url='.$img_url['material_url'].'&file_name='.$img_url['material_name']) }}" data-href="{!! $img_url['material_url'] !!}"
                                               id="doc_download" class="icon-custom-down download">Download</a>--}}
                                        </div>
                                        <div class="comment">
                                          <a href="{{ URL::to('course/download-file?file_url='.$img_url['material_url'].'&file_name='.$img_url['material_name']) }}" data-href="{!! $img_url['material_url'] !!}"
                                               id="doc_download" class="icon-custom-down download">Download</a>
{{--
                                            <div class="user-name text-white "><span class="bold">In Progress</span>
--}}
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
    @endif
    <div class="row" style="display:none;">
        <div class="tiles green" id="mapplic_demo"></div>
        <div id="chart"></div>
        <div class="" id="shares-chart-01"></div>
        <div id="sales-graph"></div>
        <canvas id="white_widget_cloudy_big" width="48" height="48" class="h-align-middle "></canvas>
        <canvas id="white_widget_13" width="16" height="16" class="inline"></canvas>
        <canvas id="white_widget_14" width="16" height="16" class="inline"></canvas>
        <canvas id="white_widget_01" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget_03" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget_02" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget_04" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget_05" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget_06" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_07" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_08" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_09" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_10" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_11" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget_12" width="20" height="20" class="inline m-l-10"></canvas>
        <canvas id="white_widget2_cloudy_big" width="48" height="48" class="h-align-middle "></canvas>
        <canvas id="white_widget2_13" width="16" height="16" class="inline"></canvas>
        <canvas id="white_widget2_14" width="16" height="16" class="inline"></canvas>
        <canvas id="white_widget2_01" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget2_02" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget2_03" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget2_04" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget2_05" width="20" height="20" class="h-align-middle m-t-10"></canvas>
        <canvas id="white_widget2_06" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_07" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_08" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_09" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_10" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_11" width="20" height="20" class="inline m-l-10"></canvas>

        <canvas id="white_widget2_12" width="20" height="20" class="inline m-l-10"></canvas>
        <div id="world-map" style="height:450px"></div>
        <div id="ricksaw" class="ricksaw"></div>
        <div id="legend"></div>
        <div id="mini-chart-orders_2"></div>
        <div id="mini-chart-other_2"></div>
        <div id="ricksaw_2" class="ricksaw"></div>
        <div id="legend_2"></div>
    </div>
    <!-- BEGIN CHAT -->
    <!-- END CHAT -->

@stop


@section('after-scripts-end')
    {!! Html::script('js/plugin/jquery-metrojs/MetroJs.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
//            $(".download").click(function (e) {
//                e.preventDefault();
//                var href_current = $(this).attr('data-href');
//                $.ajax({
//                    url: app_url + 'course/get-download?file_name='+href_current,
//                    type: 'GET',
//                    dataType: 'html',
//                    success: function (data) {
//
//                    },
//                    error: function () {
//
//                    },
//                });
//            });
        });
    </script>
@stop