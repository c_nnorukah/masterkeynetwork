@extends('layouts.master')

@section('title')
    Education
@stop

@section('main')

    <div class="row heading-course">
        <div class="col-md-12">
        </div>
    </div>
    <div class="heading-all">
    <div class="row">
        <div class="col-md-6">
            <h2 class="title">Welcome to Our Education Module</h2>
        </div>
        @if(Session::has('role'))
            @foreach(Session::get('role') as $val)
                @if($val == '1')

                <div class="col-md-6 text-right">
            <a href="#" class="btn my-button" data-toggle="modal" data-target=".bs-example-modal-lg"
               role="button">Add New</a>
        </div>
                @endif
            @endforeach
            @endif
    </div>
    </div>

    <div class="row">
    <div class="col-md-12">
        @if(count($data)>0)

            <div class="my-accordion edu-block">
                <div class="panel-group" id="accordion">
                    @foreach($data as $key => $course_data)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title text-center">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                                        <span class="edu-title">{{ $course_data->course_title }}</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key}}" class="panel-collapse collapse {{($key==0) ? "in" : ""}}">
                                <div class="panel-body">

                                    <div class="row">
                                        @if(Auth::user()->roles['role_id']==config('constants.Constants_Value.ADMIN'))
                                            <p class="text-right">
                                                <a href="#" class="btn btn-info my-button edit-course"
                                                   data-id="{!! $course_data->course_id !!}" data-toggle="modal"
                                                   data-target=".bs-example-modal-lg" role="button"><i
                                                            class="fa fa-edit"></i></a>
                                                <a href="{!! URL::route('delete-course',$course_data->course_id) !!}"
                                                   class="btn btn-info my-button"
                                                   data-id="{!! $course_data->course_id !!}"
                                                   onclick="return confirm('Are you sure want to delete?');"
                                                   role="button"><i class="fa fa-trash"></i></a>
                                            </p>
                                        @endif
                                    </div>
                                    <?php $flag = 0;?>
                                    @if(!empty($course_data->coursematerial))
                                        <div class="col-md-4 col-lg-2 m-b-10">
                                            @foreach($course_data->coursematerial as $img_url)
                                                @if(!empty($img_url['is_default']))
                                                    @if($img_url['is_default']==1)
                                                        <?php $flag = 1;?>
                                                        {!! (isset($img_url['material_url'])) ? Html::image($img_url['material_url'], 'feature', ['style' => 'width:200px;height:200px;','class'=>'widget-item']) : "" !!}
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="{{ ($flag) ? "col-md-8 col-lg-10" : "col-md-12" }}">
                                        <div class="edu-content-block">
                                            <h1>{!! $course_data->course_title !!}</h1>

                                            <p class="lead">{{ wordwrap($course_data->course_description,500,"\n,true") }}</p>

                                            <p class="text-right">
                                                <a class="btn btn-info my-button"
                                                   href="{!! URL::route('course-details',$course_data->course_id) !!}">Take
                                                    course</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--Modal Start -->
                                <!--Modal End-->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <center>
                <h3>No Course Available</h3>
            </center>
        @endif
</div>
    </div>
    <!-- END DASHBOARD TILES -->
    @include('includes.courseDialogBox')
    <div id="courseDialogForm"></div>
@stop

@section('core-js')
    <script type="text/javascript" language="javascript">

        $('.panel-heading a').click(function () {
            $('.panel-heading').removeClass('actives');
            $(this).parents('.panel-heading').addClass('actives');
        });
        $('#fileupload').each(function () {
            this.reset();
        })


        function append_response_data(request, response) {

            $.ajax({
                method: 'post',
                url: app_url + 'course/get-category',
                data: {category_name: request.term},
                success: function (data) {
                    response(data);
                }
            });
            return false;
        }

        /*  $(document).on('keydown.autocomplete', selector, function() {
         $(this).autocomplete(options);
         });*/


        $(document).ready(function () {


            $(".search_cars").submit(function (e) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        'z-index': '99999'
                    }
                });
            });
            $(document).on('keydown.autocomplete', "#category_name", function () {
                $("#category_name").autocomplete({
                    source: function (request, response) {
                        if (jQuery.trim(request.term) != "") {
                            append_response_data(request, response);
                        }
                    },
                    minLength: 2
                });
                $("#category_name").autocomplete("option", "appendTo", ".course-form");
            });

            $(document).tooltip();
            update_image_index();
            update_video_index();
            update_document_index();

            $.validator.addMethod("ImageExtension", function (value, element, param) {
                $(element).removeClass('error');

                param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g|gif";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            }, $.validator.format("Please upload jpg,png,jpeg,gif image ."));

            $.validator.addClassRules({
                imageInput: {
                    ImageExtension: true,

                },

            });

            $.validator.addMethod("DocumentExtension", function (value, element, param) {
                param = typeof param === "string" ? param.replace(/,/g, "|") : "txt|doc|docx|pdf|xml";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            }, $.validator.format("Please upload txt,doc,pdf,docx file ."));

            $.validator.addClassRules({
                document: {
                    DocumentExtension: true,

                }
            });


            $("#fileupload").validate({


                unhighlight: function (element) {
                    $(element).removeClass('error');
                    if ($(element).next('label.error').length > 0) {
                        $(element).next('label.error').hide();
                    }
                },
                highlight: function (element) {
                    $(element).addClass('error');
                    if ($(element).next('label.error').length > 0) {
                        $(element).next('label.error').show();
                    }
                },
                errorPlacement: function (error, element) {
                    if ($(element).hasClass("images")) {
                        $(error).appendTo($(element).parents(".add-img"));
                    } else if ($(element).hasClass("video")) {
                        $(error).appendTo($(element).parents(".add-video"));
                    } else if ($(element).hasClass("document")) {
                        $(error).appendTo($(element).parents(".add-doc:last"));
                    } else {
                        $(error).insertAfter($(element));
                    }
                },

            });
            $("#category_name").rules("add", {
                required: true,
            });
            $("#course_title").rules("add", {
                required: true,
            });
            $("#course_description").rules("add", {
                required: true,
            });
            $(document).off("change", '.images');
            $(document).on("change", ".images", function () {
                var chk_img = $(this).val()
                var chk_img_pre = chk_img.split('.');
                $(".edu-pop-content").find("#add-btn").html('<button id="img-btn" class="control-label pull-right btn btn-default"><i class="fa fa-plus"></i></button>');
                if (chk_img_pre[1] == "jpg" || chk_img_pre[1] == "png" || chk_img_pre[1] == "jpeg" || chk_img_pre[1] == "gif") {
                    $(this).parents(".add-img").find(".image-upload-preview").css("display", "inline-block");
                } else {
                    $(this).parents(".add-img").find(".image-upload-preview").css("display", "inline-none");
                }
                $(document).off("click", '#img-btn');
                $(document).on("click", "#img-btn", function () {
                    update_image_index();
                    $(".edu-pop-content").find("#p .append").append('<div class="add-img" style="margin-top:5%;">{!! Form::file('images[]', array('class' => 'imageInput pull-left img images','id'=>'')) !!}  <div class="clearfix"></div> <span><img src="" class="image-upload-preview" alt="no image" style="display: none" width="90px" height="90px" /></span><span><button class="btn btn-default del-btn pull-right del-img"><i class="fa fa-trash"></i></button></span><p class="errors">{!!$errors->first('images')!!}</p>@if(Session::has('error'))<p class="errors">{!! Session::get('error') !!}</p>@endif
                            </div>');
                    return false;
                });
            });

            $(document).on("click", ".image-upload-preview", function () {
                $(".edu-pop-content").find('.image-upload-preview').parent().removeClass('select-img');
                $(this).parent().addClass("select-img");
                var material_id = $(this).attr('material_id');
                if (material_id == undefined) {
                    $(".edu-pop-content").find('.hidden_matirial_id_is_default').val(0);
                } else {
                    $(".edu-pop-content").find('.hidden_matirial_id_is_default').val(material_id);
                }
                var featured_src = $(this).parents('.add-img').find('input[type="file"]').val();

                if (featured_src == undefined) {
                    featured_src = $(this).attr('src');
                }
                $(".edu-pop-content").find('#feature_image').val(featured_src);
            });

            $(document).on('change', '.imageInput', function () {
                var $this = $(this).val()
                readURL(this);
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(input).parent().find('img.image-upload-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).on('change', '.document', function () {
                $(this).removeClass('error');
                $(this).next('label.error').hide();
                $(".edu-pop-content").find(".document").parents('.add-doc').find('.add_new_button').html('<button id="doc-btn" class="add_new_doc control-label btn btn-default"><i class="fa fa-plus"></i></button></div>');

            });

            $(document).off("click", '.add_new_doc');
            $(document).on("click", '.add_new_doc', function (e) {
                update_document_index();
                var html = $(this).parents('.form-group').clone();
                $(html).find('.control-label').text('');
                $(html).find('.video-pre,.document').val('');
                //$(html).find('.document').addClass("required");
                $(html).find('span.add_new_button').remove();
                $(html).find(".add-doc:first").append('<span class="pull-right"><button class="btn btn-default del-btn delete-doc"><i class="fa fa-trash"></i></button></span>');
                if ($(".edu-pop-content").find('.doc-container').find(".added-doc").length > 0) {
                    $(html).insertBefore($(".edu-pop-content").find('.doc-container').find(".added-doc:first"));
                } else {
                    $(".edu-pop-content").find('.doc-container').append($(html))
                }
                e.preventDefault();
            });

            $(document).on("click", ".del-img", function (e) {
                e.preventDefault();
                var $_this = $(this);
                $(this).parent().parent().remove();

            });
            $(document).off("click", ".del-video");
            $(document).on("click", ".del-video", function (e) {
                e.preventDefault();
                var $_this = $(this);
                if ($(".edu-pop-content").find('.video-pre').length == 1) {
                    $_this.parents('.add-video').find('span.img').remove();
                    $_this.parents('.add-video').find('.control-label').text('');
                    $_this.parents('.add-video').find('.video-pre').val('');
                    $_this.parents('.add-video').find('.add-new-video').addClass('hidden');
                } else {
                    //$(this).parent().parent().remove();
                    $(this).parents(".form-group.row:first").remove();
                }
            });
            $(document).off("click", ".delete-doc");
            $(document).on("click", ".delete-doc", function (e) {
                e.preventDefault();
                var $_this = $(this);
                $(this).parents(".form-group.row:first").remove();

            });
            $(".add-course").click(function () {
                $.ajax({
                    url: app_url + 'course/get-edit-course/' + courseId,
                    type: 'GET',
                    dataType: 'html',
                    success: function (data) {
                        $("#courseEditForm").html(data);
                    },
                    error: function () {
                        $('#info').html('<p>An error has occurred</p>');
                    },
                });
            });
            $(document).off("click", ".edit-course");
            $(document).on("click", ".edit-course", function (e) {
                //$(".edit-course").click(function () {
                var courseId = $(this).data("id");
                $('.added-doc').remove();
                $(".edu-pop-content").find('.error').removeClass('error');
                $('#material_video-error, #images-error, #document-error, #course_title-error, #tags-error, #course_description-error').remove();
                $.ajax({
                    url: app_url + 'course/edit/' + courseId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {

                        $('#course_title').val(data.course_title);
                        $('#course_description').val(data.course_description);
                        $('#course_id').val(data.course_id);
                        if (data != undefined && data.coursecategory != undefined && data.coursecategory.category_name != undefined) {
                            $('#category_name').val(data.coursecategory.category_name);
                        }
                        $.each(data.coursematerial, function (index, element) {
                            if ((element.type) == "0") {

                                if ((element.is_default) == "1") {
                                    $(".edu-pop-content").find(".image-container").append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label></div><div class="col-sm-8 add-video"><span class="select-img"><img src="' + app_url + '' + element.material_url + '" class="image-upload-preview" name="images[]" alt="your image"  width="90" height="90" material_id="' + element.course_material_id + '"/><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span></div></div>');
                                    $(".edu-pop-content").find('.hidden_matirial_id_is_default').val(element.course_material_id);
                                } else {
                                    $(".edu-pop-content").find(".image-container").append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label></div><div class="col-sm-8 add-video"><span class=""><img src="' + app_url + '' + element.material_url + '" class="image-upload-preview" name="images[]" alt="your image"  width="90" height="90" material_id="' + element.course_material_id + '"/><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span></div></div>');
                                }
                            }
                            else if ((element.type) == "1") {
                                $(".edu-pop-content").find('.doc-container').append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label>' +
                                        '</div><div class="col-sm-8 add-video"><div class="add-doc"><input type="text"  readonly class="pull-left" id="" value="' + element.material_name + '" name="docs[]"><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span>');
                            }
                            else if ((element.type) == "2") {
                                if (element.material_url.indexOf('youtube') !== -1) {


                                    $(".edu-pop-content").find(".video-container").append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label>' +
                                            '</div><div class="col-sm-8 add-video"><div class="add-doc">'
                                            + '<input type="text" class="hidden material_video_src" name="material_video_src[]" value="' + element.material_video_src + '">'
                                            + '<input type="text" readonly class="material_video" id="" value="' + element.material_url + '" name="material_video[]"><div class="clearfix"></div><span id="" class=" error txturl-error" style="display:none;"></span><img src="' + element.material_video_src + '" class="image-upload-preview" name="images[]" alt="your image"  width="90" height="90" material_id="' + element.course_material_id + '"/><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span></div></div></div>')
                                } else if (element.material_url.indexOf('vimeo') !== -1) {

                                    $(".edu-pop-content").find(".video-container").append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label>' +
                                            '</div><div class="col-sm-8 add-video"><div class="add-doc">'
                                            + '<input type="text" class="hidden material_video_src" name="material_video_src[]" value="' + element.material_video_src + '">'
                                            + ' <input type="text" readonly class="material_video" id="" value="' + element.material_url + '" name="material_video[]"><div class="clearfix"></div><span id="" class="error txturl-error" style="display:none;"></span><img src="' + element.material_video_src + '" class="image-upload-preview" name="images[]" alt="your image"  width="90" height="90" material_id="' + element.course_material_id + '"/><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span></div></div></div>')

                                } else if (element.material_url.indexOf('soundcloud') !== -1) {
                                    $(".edu-pop-content").find(".video-container").append('<div class="form-group row added-doc"><div class="col-sm-4"><label class="control-label" for="email">&nbsp;</label>' +
                                            '</div><div class="col-sm-8 add-video">'
                                            + '<input type="text" class="hidden material_video_src" name="material_video_src[]" value="' + element.material_video_src + '">'
                                            + '<div class="add-doc"><input type="text" readonly class="material_video" id="" value="' + element.material_url + '" name="material_video[]"><div class="clearfix"></div><span id="" class="error txturl-error" style="display:none;"></span><img src="' + element.material_video_src + '" class="image-upload-preview" name="images[]" alt="your image"  width="90" height="90" material_id="' + element.course_material_id + '"/><span><a  href="#" class="btn btn-default del-btn pull-right image-del" data-id="' + element.course_material_id + '"><i class="fa fa-trash"></i></a></span></div></div></div>')

                                }
                            }
                        });
                    },
                    error: function () {
                        $('#info').html('<p>An error has occurred</p>');
                    },
                });
            });
            $(document).on("click", ".image-del", function () {
                var id = $(this).data("id");
                var $_this = $(this);
                var function_name = $(this).attr("function");
                $.ajax({
                    url: app_url + 'course/delete-material/' + id,
                    type: 'GET',
                    success: function (data) {
                        if (data == "true") {
                            $_this.parents('.added-doc').remove();
                        }
                        else {
                            alert("ERROR");
                        }
                    },

                });
            });
            $(document).off("keyup", ".video-pre");
            $(document).on("keyup", ".video-pre", function () {

                var $this = $(this);
                var vid = $(this).val();
                if (vid.length === 0) {
                    $this.parents('.add-video').find('span.img').remove()
                }
            });
            //alert($("#video_add_btn, .material_video").length)
            $(document).off("blur", "#video_add_btn, .material_video");
            $(document).on('blur', '#video_add_btn, .material_video', function () {
                var $this = $(this);
                var first_video_url = $(this).val();
                if (first_video_url !== "") {
                    $(".edu-pop-content").find(".material_video").parents('.add-video').find('.add_new_video_button').html('<button id="video-btn" class="add-new-video pull-right btn btn-default"><i class="fa fa-plus-square"></i></button>');
                }
            });

            $(document).off("blur", ".video-pre");
            $(document).on("blur", ".video-pre", function () {

                var $this = $(this);
                var vid = $(this).val();
                if (vid == "")
                    return;
                if ($this.parents('.add-video').find('span.img').length == 0) {
                    $.ajax({
                        url: app_url + 'course/check-url',
                        type: 'POST',
                        data: {'vid': vid},
                        success: function (msg) {
                            if (msg == "false") {
                                //alert("URL is not valid, Please Enter Valid URL");image-del
                                //            $this.focus()
                                $this.parents('.add-video').find('.txturl-error').css('display', 'block');
                                $this.parents('.add-video').find('.txturl-error').html('Please Enter only Youtube,vimeo or SoundCloud Video URL');
                            } else {
                                $this.parents('.add-video').find('.txturl-error').css('display', 'none');
                                $this.parents('.add-video').find('.txturl-error').html('');
                                $this.parents(".add-video").append('<span class="img"><img src=' + msg + ' class="thumb-url" name="video-preview" alt="your image"  width="90" height="90" /></span>');
                                if ($this.parents(".add-video").find(".thumb-url").length > 0) {
                                    $this.parents(".add-video").find('.material_video_src').val(msg);
                                }
                            }
                        }
                    });
                }
            });
            $(document).on("click", ".add-new-video", function (e) {
                update_video_index();
                var html = $(this).parents('.form-group').clone();
                $(html).find('span.img').remove();
                $(html).find('.control-label').text('');
                $(html).find('.video-pre').val('');
                //$(html).find('.video-pre').addClass("required");
                $(html).find('.add-new-video').remove();
                $(html).find(".add-video").append('<span class=""><a  href="#" class="btn btn-default del-btn pull-right image-del del-video" data-id=""><i class="fa fa-trash"></i></a></span>');
                if ($(".edu-pop-content").find('.video-container').find(".added-doc").length > 0) {
                    $(html).insertBefore($(".edu-pop-content").find('.video-container').find(".added-doc:first"));
                } else {
                    $(".edu-pop-content").find('.video-container').append($(html))
                }
                e.preventDefault();
            });


            $('.msg').fadeOut(5000);
            $(".edu-pop-content-hidden").html($(".edu-pop-content").html());
            $(document).on('hidden.bs.modal', function (e) {
                if ($(this).find(".edu-pop-content")) {
                    $(".edu-pop-content #fileupload").html($(".edu-pop-content-hidden #fileupload").html());
                    $(document).tooltip();
                    update_image_index();
                    update_video_index();
                    update_document_index();
                }


            });

        });

        function update_image_index() {
            setTimeout(function () {
                $(".edu-pop-content").find(".imageInput").each(function (i) {
                    $(this).attr('name', 'images[' + i + ']')
                });
            }, 500)

        }
        function update_document_index() {
            setTimeout(function () {
                $(".edu-pop-content").find(".document").each(function (i) {
                    $(this).attr('name', 'docs[' + i + ']')
                });
            }, 500)

        }
        function update_video_index() {
            setTimeout(function () {
                $(".edu-pop-content").find(".video").each(function (i) {
                    $(this).attr('name', 'material_video[' + i + ']')
                });
                $(".edu-pop-content").find(".material_video_src").each(function (i) {
                    $(this).attr('name', 'material_video_src[' + i + ']')
                });
            }, 500)

        }
    </script>
@stop
