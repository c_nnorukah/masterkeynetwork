@extends('layouts.master')

@section('title')
    PaypalPaymentSetting
    @stop

    @section('main')

            <!-- BEGIN DASHBOARD TILES -->
    @if(Session::has('messege'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <strong>{{ Session::get('messege') }}</strong>
        </div>
    @endif

    @if(!empty($data_paypal))
        <?php
        $payment_date = $data_paypal->payment_date;
        $paypal_email = $data_paypal->paypal_email;
        $payment_currency = $data_paypal->payment_currency;
        $message = $data_paypal->message;
        $paypal_IPN_URL = $data_paypal->paypal_IPN_URL;
        $paypal_API_username = $data_paypal->paypal_API_username;
        $paypal_API_signature = $data_paypal->paypal_API_signature;
        $paypal_API_password = $data_paypal->paypal_API_password;
        $paypal_API_URL = $data_paypal->paypal_API_URL;
        $paypal_API_password = $data_paypal->paypal_API_password;
        $action = url('update-paypal-setting',$data_paypal->paypal_payment_setting_id);
        ?>
    @else
        <?php
        $payment_date = "";
        $paypal_email = "";
        $payment_currency = "";
        $message = "";
        $paypal_IPN_URL = "";
        $paypal_API_username = "";
        $paypal_API_signature = "";
        $paypal_API_password = "";
        $paypal_API_URL = "";
        $paypal_API_password = "";
        $insert = "Select currency";
        $action = url('add-paypal-setting');
        ?>
    @endif
    <div class="row heading-all m-b-20">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <p class="lefty">Paypal Payment Settings</p>
        </div>
    </div>

    <div class="row">
        <form id="paypal_form" action="{{$action}}" method="post">
            <div class="col-md-6">
                <div class="grid simple dashboard-block">
                    <div class="grid-title no-border">
                        <h4><span class="semi-bold">Pay Affiliates using PayPal</span></h4>

                        <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="panel panel-default app-settings m-t-20">
                                    <small>Use this tool to pay affiliates using Paypal Mass Pay</small>
                                    <div class="panel-body">

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Payment Date</label>
                                            </div>

                                            <div class="col-sm-7">
                                                <input type="text" value="{{$payment_date}}" id="payment_date"
                                                       name="payment_date" class="form-control required">
                                                {{--<span class="add-on" ><i class="fa fa-th" ></i></span>--}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Your Paypal Email</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control required"
                                                       value="{{$paypal_email}}" name="paypal_email" id="paypal_email"
                                                       placeholder="enter email">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Select Payment Currency</label>
                                            </div>
                                            <div class="col-sm-7">
                                                @if(!empty($currency))
                                                    <select class="select2 my-button form-control required"
                                                            name="payment_currency" id="payment_currency">
                                                        @foreach($currency as $val)
                                                            <option value="{{$val}}"
                                                                    @if($payment_currency == $val)selected="selected" @endif>
                                                                {{$val}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Message to Affiliates</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" value="{{$message}}" name="message" id="message"
                                                       value="" class="form-control required"
                                                       placeholder="Payment for 2017 Sales">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="grid simple dashboard-block">
                    <div class="grid-title no-border">
                        <h4><span class="semi-bold">Paypal Settings</span></h4>

                        <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="panel panel-default app-settings m-t-20">
                                    <div class="panel-body">
                                        {{--<form class="form-row m-t-20">--}}
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Paypal IPN URL</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" value="{{$paypal_IPN_URL}}" name="paypal_IPN_URL"
                                                       id="paypal_IPN_URL" value="" class="form-control required"
                                                       placeholder="https://">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Paypal API Username</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" value="{{$paypal_API_username}}"
                                                       id="paypal_API_username" name="paypal_API_username" value=""
                                                       class="form-control required" placeholder="Enter Username">
                                            </div>
                                        </div>
                                        <input type="text" style="display:none;">
                                        <input type="password" style="display:none;">

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Paypal API Password</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="password" value="{{$paypal_API_password}}"
                                                       name="paypal_API_password" id="paypal_API_password" value=""
                                                       class="form-control required" placeholder="********">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Paypal API Signature</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" value="{{$paypal_API_signature}}"
                                                       name="paypal_API_signature" id="paypal_API_signature" value=""
                                                       class="form-control required" placeholder="23423542">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <label class="control-label">Paypal API URL</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" value="{{$paypal_API_URL}}" name="paypal_API_URL"
                                                       id="paypal_API_URL" value="" class="form-control required"
                                                       placeholder="https://">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-5">

                                            </div>
                                            <div class="col-sm-7 text-right">
                                                <input class="btn btn-lg btn-primary btn-block" type="submit"
                                                       value=" Submit ">
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

@if(!empty($data_level))
    <?php
    $payment_level_1 = $data_level->payment_level_1;
    $payment_level_2 = $data_level->payment_level_2;
    $payment_level_3 = $data_level->payment_level_3;
    $action_level = url('update-paypal-level',$data_level->payment_level_history_id);
?>
@else
  <?php
    $payment_level_1 = "";
    $payment_level_2 = "";
    $payment_level_3 = "";
  $action_level = url('add-paypal-level');
?>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Select Affiliate groups For Payment</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="panel panel-default app-settings m-t-20">
                                <div class="panel-body">
                                    <form class="form-row m-t-20">
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label class="control-label"></label>
                                            </div>

                                            <div class="col-sm-6">
                                                <select class="select2 my-button form-control select2-offscreen"
                                                        name="perEmails" data-init-plugin="select2" tabindex="-1">
                                                    <option value="">Select group</option>
                                                   @if(!empty($affilate_user))
                                                    @foreach($affilate_user as $user)
                                                       @foreach($user as $val)
                                                        <option value="{{$val['user_id']}}">{{$val['user_name']}}</option>
                                                   @endforeach
                                                    @endforeach
                                                       @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6">

                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control"
                                                       placeholder="Auto calculate payout">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Total</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" placeholder="$8000">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6">

                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <input class="btn btn-lg btn-primary btn-block" type="submit"
                                                       value=" Submit for mass Pay ">
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4><span class="semi-bold">Set Payment Levels</span></h4>

                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <form method="post" action="{{$action_level}}" id="level_form">
                                <div class="panel panel-default app-settings m-t-20">
                                    <div class="panel-body">
                                        <label class="control-label">Enter how much you want to pay users</label>
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label class="control-label">Level 1</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" value="{{$payment_level_1}}"
                                                           name="payment_level_1" id="payment_level_1"
                                                           class="form-control required">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label class="control-label">Level 2</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" value="{{$payment_level_2}}"
                                                           name="payment_level_2" id="payment_level_2"
                                                           class="form-control required">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label class="control-label">Level 3</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" name="payment_level_3"
                                                           value="{{$payment_level_3}}" id="payment_level_3"
                                                           class="form-control required">
                                                </div>
                                            </div>
                                            <br>

                                            <div class="form-group row">
                                                <input class="btn btn-lg btn-primary btn-block" type="submit"
                                                       value=" Save ">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <!-- END DASHBOARD TILES -->
@stop
@section('after-scripts-end')
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $('#payment_date').datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

        $('#paypal_form').validate({
            rules: {
                payment_date: "required",
                paypal_email: "required",
                payment_currency: "required",
                message: "required",
                paypal_IPN_URL: "required",
                paypal_API_username: "required",
                paypal_API_signature: "required",
                paypal_API_password: "required",
                paypal_API_URL: "required"
            }
        });

        $('#level_form').validate({
            rules: {
                payment_level_1: "required",
                payment_level_2: "required",
                payment_level_3: "required",
            }
        });
    </script>

@stop