@extends('layouts.master')
@section('title')
    Dashboard
@stop
@section('main')
    <?php
    $roles = Auth::user()->roles;  //check role of User
    $ADMIN = config('constants.Constants_Value.ADMIN');
    ?>
    <div class="heading-all">
    <div class="row">
        <div class="col-md-4">
            <h2 class="title">Lead Statuses</h2>
        </div>
        <div class="col-md-8 text-right">

            <form id="lead-search" action="{{url("landing-page/responses")}}" method="post" class="form-inline">
                <div class="form-group">
                Filter:
                <input id="txtStartDate" name="start_date" class="form-control" type="text" placeholder="From date" value="{{$start_date}}">
                <input id="txtEndDate" name="end_date" class="form-control" type="text" placeholder="To date" value="{{$end_date}}">
                @if(Session::has('role'))
                    @foreach(Session::get('role') as $val)
                        @if($val == '1')
                            <select class="select2 my-button user_list" name="user_list" style="width:auto!important">
                                @if(!empty($user_list))
                                    <option value="">Please Select User</option>
                                    @foreach($user_list as $val)
                                        @if(isset($val->assignedRoles->role_id) && $val->assignedRoles->role_id == 2)
                                            <option value="{{$val->user_id}}">{{$val->first_name ." ". $val->last_name}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        @endif
                    @endforeach
                @endif
                <input type="submit" class="btn downline-search m-b-5" value="Search" name="search">
                </div>
            </form>
        </div>
    </div>
    </div>
    <div class="lead-status-data">
        @if($user_id == 0)
            <div class="row p-t-10">
                <p claas="lead">You have to select the user from above dropdown to see the their lead statuses.</p>
            </div>
        @else
            <table class="table table-bordered dataTable" id="example2_filter">
                <tr>
                    <th class="">Landing Page Title</th>
                    <th class="">Response Count</th>
                    @if($roles->role_id == $ADMIN)
                        <th class="">Users</th>
                    @endif
                    <th>Created on</th>
                    <th>View Details</th>
                </tr>

                @if (count($projects) > 0)

                    @foreach($projects as $key=>$val)
                        <tr>
                            <td>{{ $val['projects']['name'] }}</td>
                            <td> {{ count(@$response[$val['projects']['id']])}}</td>
                            @if($roles->role_id == $ADMIN)
                                <td>{{ $val['users']['first_name']}}</td>
                            @endif
                            <td>{{format_date($val['projects']['created_at'])}}</td>
                            <td><a class="response_modal" data-id="{{$val['projects']['id']}}" data-toggle="modal"
                                   data-target="#myModal"><i class="fa fa-list"/></a></td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <td colspan="5" align="center">No Data is available.</td>
                    </tr>
                @endif
            </table>
            <span class="dataTables_paginate paging_bootstrap pagination">
            <?php echo $projects->render(); ?>
                </span>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" id="modal">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>No Response </h4>

                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        @endif
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/landing-page-response.js') !!}
@stop