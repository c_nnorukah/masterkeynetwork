<form method="post" action="{!! URL("user-list/$sort_by/$sort_order/$limit") !!}" class="user_search" id="user_search">
    <div class="search_container">
        <select name="search_criteria" class="search_criteria required">
            <option value="" @if($selected_criteria == "") selected @endif>Please select</option>
            <option value="name" @if($selected_criteria == "name") selected @endif>Name</option>
            <option value="email" @if($selected_criteria == "email") selected @endif>Email Address</option>
            <option value="role" @if($selected_criteria == "role") selected @endif>Role</option>
            <option value="plan" @if($selected_criteria == "plan") selected @endif>Plan</option>
            <option value="status" @if($selected_criteria == "status") selected @endif>Status</option>
            <option value="signup_date" @if($selected_criteria == "signup_date") selected @endif>Signup Date</option>
        </select>
    </div>
    <div class="search_container search_conditions">
        <input type="text" name="name"
               class="search_criteria_div @if($selected_criteria != "name") hide @endif required name_div"
               value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "name") {!! $post_data["name"]!!} @endif">
        <input type="text" name="email"
               class="search_criteria_div @if($selected_criteria != "email") hide @endif required email_div"
               value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "email") {!! $post_data["email"]!!} @endif">
        <select name="role" class="search_criteria_div @if($selected_criteria != "role") hide @endif required role_div">
            <option value="">Select Role</option>
            @foreach($role as $value)
                <option value="{!! $value->role_id !!}"
                        @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "role" &&  @$post_data["role"] == $value->role_id ) selected @endif>{!! $value->title !!}</option>
            @endforeach
        </select>
        <select name="plan" class="search_criteria_div @if($selected_criteria != "plan") hide @endif required plan_div">
            <option value="">Select Plan</option>
            @foreach($plan as $value)
                <option value="{!! $value->plan_id !!}"
                        @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "plan" &&  @$post_data["plan"] == $value->plan_id ) selected @endif>{!! $value->plan_name !!}</option>
            @endforeach
        </select>
        <select name="status"
                class="search_criteria_div @if($selected_criteria != "status") hide @endif required status_div">
            <option value="">Select Status</option>
            <option value="1"
                    @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "status" &&  @$post_data["status"] == "1" ) selected @endif>
                Active
            </option>
            <option value="0"
                    @if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "status" &&  @$post_data["status"] == "0" ) selected @endif>
                Inactive
            </option>
        </select>
        <div class="search_criteria_div @if($selected_criteria != "signup_date") hide @endif required signup_date_div">
            <div class="search_container">
                <input type="text" name="from_signup" class="required from_date" id="txtStartDate"
                       value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "from_signup") {!! $post_data["from_signup"]!!} @endif">
            </div>
            <div class="search_container">
                <input type="text" name="to_signup" class="required to_date" id="txtEndDate"
                       value="@if(isset($post_data["search_criteria"]) && $post_data["search_criteria"] == "to_signup") {!! $post_data["to_signup"]!!} @endif">
            </div>
        </div>
    </div>
    <div class="search_container">
        <input type="submit" class="btn user_search" value="Search" name="search">
        <a href="{!! URL("user-reset") !!}" class="btn ajax-link reset-link">Reset</a>
    </div>
</form>