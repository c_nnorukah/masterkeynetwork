@extends('layouts.master')
@section('title')
    User Detail
@stop
@section('main')
    <div class="row heading-all m-b-20">

        <div class="col-md-7">
            <p class="lefty">Details of {!! $user_data[0]->first_name . " " . $user_data[0]->last_name !!}</p>
        </div>
        <div class="col-md-5">
            <div class="pull-right">
                <h3>Customer Status:
                    <span class="label label-success label-info">@if($user_data[0]->status == 1) Active @else
                            Inactive @endif</span>
                </h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4>Billing Account Info</h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body user-detail no-border">
                    <div class="row m-t-20">
                        <div class="col-md-4">
                            <p>Current Cycle Date</p>
                            <p>Next Cycle Date</p>
                            <p>Amount</p>
                            <p>Plan</p>
                        </div>
                        <div class="col-md-8">
                            <p>{!! empty($min_date) ? "NA" : format_date($min_date)!!}</p>
                            <p>{!! empty($next_date) ? "NA" : format_date($next_date)!!}</p>
                            <p> {!! (isset($user_data[0]->plan->price)) ?format_price($user_data[0]->plan->price) : "NA"!!}</p>
                            <p>
                                <span class="label">{!! isset($user_data[0]->plan->plan_name) ? $user_data[0]->plan->plan_name : "NA"!!}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4>User Details</h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body user-detail no-border">
                    <div class="row  m-t-20">
                        <div class="col-md-3">
                            <p>Name</p>
                            <p>Email Address</p>
                            <p>Phone Number</p>
                        </div>
                        <div class="col-md-9">
                            <p>{!! $user_data[0]->first_name . " " . $user_data[0]->last_name !!}</p>
                            <p>{!! $user_data[0]->email !!}</p>
                            <p>{!! $user_data[0]->phone_number !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4>Transaction History</h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border  m-t-20">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="ajax-table wrap">
                            <table class="table table-bordered dataTable tableSection">
                                <thead>
                                <tr role="row">
                                    <th>Plan</th>
                                    <th>Amount</th>
                                    <th>Transaction Status</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>

                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                @if(count($transaction_history) > 0)
                                    @foreach($transaction_history as $value)
                                        <tr>
                                            <td>{!! isset($value->plan->plan_name) ? $value->plan->plan_name : "NA" !!}</td>
                                            <td>$ {!! isset($value->amount) ? $value->amount : "NA" !!}</td>
                                            <?php $response = json_decode($value->transaction_response); ?>
                                            <td>{!! $response->ACK !!}</td>
                                            <td>{!! format_date($value->created_at,"m/d/Y H:i A") !!}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>
                                            No Transaction History Found.
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            @if(count($transaction_history) > 0)
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                    <span class="dataTables_paginate paging_bootstrap pagination">
                                        {!! $transaction_history->render() !!}
                                    </span>
                                        <?php $start = $transaction_history->currentPage() * $limit - ($limit - 1); ?>
                                        <span class="dataTables_info pull-left">Showing <b>{!! $start !!}
                                                to {!! $start + $transaction_history->count() -1 !!}</b> of {!!  $transaction_history->total() !!}
                                            entries</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
