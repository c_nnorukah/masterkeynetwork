@extends('layouts.master')
@section('title')
    User List
@stop
@section('main')
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
    <div class="row heading-all m-b-20">
        <div class="col-md-3">
            <p class="lefty">User List</p>
        </div>
        <div class="col-md-9 text-right m-b-10 dataTables_filter">
            @include("user.search")
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <div class="ajax-table">
                    <input type="hidden" id="sort_by" value="{{$sort_by}}">
                    <input type="hidden" id="sort_order" value="{{$sort_order}}">
                    <input type="hidden" id="limit" value="{{$limit}}">
                    <table class="table table-bordered dataTable">
                        <thead>
                        <tr role="row">
                            <?php $class = ($sort_by == 'first_name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/first-name/$need_to_sort/$limit") !!}">Name</a>
                            </th>

                            <?php $class = ($sort_by == 'email') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/email/$need_to_sort/$limit") !!}">Email</a>
                            </th>

                            <?php $class = ($sort_by == 'title') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/title/$need_to_sort/$limit") !!}">Role</a>
                            </th>

                            <?php $class = ($sort_by == 'plan-name') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/plan_name/$need_to_sort/$limit") !!}">Plan</a>
                            </th>

                            <?php $class = ($sort_by == 'status') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/status/$need_to_sort/$limit") !!}">Status
                                </a></th>

                            <?php $class = ($sort_by == 'created-at') ? 'sorting sorting_' . $sort_order : 'sorting'; ?>
                            <th class="{!! $class !!}"><a
                                        href="{!! URL("user-list/created_at/$need_to_sort/$limit") !!}">Signup
                                    Date</a></th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @if(!empty($user_data->total()))
                            @foreach($user_data as $user)
                                <tr class="gradeX odd">

                                    <td class=" ">{!! $user->first_name ." ".$user->last_name !!}</td>
                                    <td class=" ">{!! $user->email !!}</td>
                                    <td class=" ">{!! $user->title !!}</td>
                                    <td class=" ">{!! (isset($user->plan->plan_name) ? $user->plan->plan_name : "-NA-") !!}</td>
                                    <td class=" ">@if($user->status == "1") Active @else Inactive @endif</td>
                                    <td class=" ">{!! $user->created_at != "" ? format_date($user->created_at)  : "" !!}</td>
                                    <td>
                                        <a href="{!! URL("user-detail/".$user->user_id) !!}" class="mr-r-10"><i
                                                    class="fa fa-edit"></i></a>
                                        <a href="{!! URL("user-delete/".$user->user_id) !!}"
                                           class="user_delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">
                                    <center>No Result Found</center>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    @if(!empty($user_data->total()))
                        <div class="row">
                            <div class="col-md-12 text-right">
                                    <span class="dataTables_paginate paging_bootstrap pagination">
                                        {!! $user_data->render() !!}
                                    </span>
                                <?php $start = empty($user_data->total()) ? 0 : $user_data->currentPage() * $limit - ($limit - 1);
                                $to = empty($user_data->total()) ? 0 : $start + $user_data->count() - 1;
                                ?>
                                <span class="dataTables_info pull-left">Showing <b>{!! $start !!}
                                        to {!! $to !!}</b> of {!!  $user_data->total() !!}
                                    entries</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/user.js') !!}
@stop
<script>
    var redirect_url = '{!! URL("user-list") !!}';
</script>