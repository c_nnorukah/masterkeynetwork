@extends('layouts.master')
@section('title')
    Plan & Pricing
@stop
@section('main')
    <div class="row heading-all m-b-20">

        <div class="col-md-7">
            <p class="lefty">Plan & Pricing</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4>Current Plan</h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body user-detail no-border">
                    <div class="row m-t-20">
                        <div class="col-md-4">
                            <p>Next Cycle Date</p>
                            <p>Amount</p>
                            <p>Interval</p>
                            <p>Plan</p>
                        </div>
                        <div class="col-md-8">
                            <p>{!! empty($next_date) ? "NA" : date_format($next_date,"m/d/Y")!!}</p>
                            <p> {!! (isset($user_data[0]->plan->price)) ? format_price($user_data[0]->plan->price) : "NA"!!}</p>
                            <p> {!! (isset($user_data[0]->plan->days)) ? $user_data[0]->plan->days ." " .$user_data[0]->plan->interval : "NA"!!}</p>
                            <p>
                                <span class="label label-success label-info">{!! isset($user_data[0]->plan->plan_name) ? $user_data[0]->plan->plan_name : "NA"!!}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @if(count($upgraded_plan) > 0)
                <div class="grid simple dashboard-block">
                    <div class="grid-title no-border">
                        <h4>Upgraded Plan</h4>
                        <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                    </div>
                    <div class="grid-body user-detail no-border">
                        <div class="row m-t-20">
                            <i class="mr-b-10">Note:This plan will be activated after your current plan expire.</i>
                            <div class="col-md-4">
                                <p>Amount</p>
                                <p>Interval</p>
                                <p>Plan</p>
                            </div>
                            <div class="col-md-8">
                                <p> {!! (isset($upgraded_plan[0]->plan->price)) ? format_price($upgraded_plan[0]->plan->price) : "NA"!!}</p>
                                <p> {!! (isset($upgraded_plan[0]->plan->days)) ? $upgraded_plan[0]->plan->days ." " .$upgraded_plan[0]->plan->interval : "NA"!!}</p>
                                <p>
                                    <span class="label label-info">{!! isset($upgraded_plan[0]->plan->plan_name) ? $upgraded_plan[0]->plan->plan_name : "NA"!!}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <div class="grid simple dashboard-block">
                <div class="grid-title no-border">
                    <h4>Change Current Plan</h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body user-detail no-border">
                    <div class="row  m-t-20">

                        @if(!empty($user_plan))
                            <p>Select any plan from following list to change your current plan.</p>
                            @foreach($user_plan as $val)
                                @if($val->plan_id != $user_data[0]->plan->plan_id && $val->plan_id != @$upgraded_plan[0]->plan->plan_id)
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front">
                                                <h2>{{$val->plan_name}}</h2>

                                                <div class="interval">{{$val->days ." " . $val->interval}}</div>
                                                <div class="price">&#36;{{$val->price}}</div>
                                            </div>
                                            <div style="background:#f8f8f8;" class="back">
                                                <p>{!! strip_tags($val->description) !!}</p>
                                                <a href="{{url('upgrade-plan/'.$val->plan_id)}}"
                                                   onclick="return confirm('Are you sure you want to change current plan to {{$val->plan_name}} ?');"
                                                   class="btn btn-primary"><i class="fa fa-edit"></i> Upgrade </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            @else
                            <p>There is no plan to change.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
