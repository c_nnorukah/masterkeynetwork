<div>

    <div class="data_div">
        <input type="hidden" name="no_of_portfolio" id="no_of_portfolio"
               value="{{isset($portfolio_data[0]->user_portfolios) ? count($portfolio_data[0]->user_portfolios) : ""}}">

        <div class="row form-row">
            <div class="col-md-3">
                <div class="scrollable_div">
                    @if(isset($portfolio_data[0]->user_portfolios) && count($portfolio_data[0]->user_portfolios) != '0')
                        @foreach($portfolio_data[0]->user_portfolios as $key=>$value)
                            <a href="{{url("portfolio/$value->id" )}}"
                               class=" @if(isset($id) && $id == $value->id) active_link @endif">
                                <div class="detail">
                                    <i class="close_button fa fa-trash pull-right"
                                       data-url="{{url("portfolio/delete/$value->id" )}}"></i>

                                    <div class="get_data">
                                        <p><b>Title:</b> {!! $value->title !!}</p>

                                        <p><b>Summary:</b> {!! str_limit($value->summary, 100)!!}</p>
                                    </div>
                                </div>
                                <hr>
                            </a>
                        @endforeach
                    @else
                        <p class="pd-l-10">No portfolio found. Start adding portfolios.</p>
                    @endif
                </div>
            </div>
            <div class="col-md-8 mr-l-25">
                <form method="post" id="portfolio_form" name="frmPortfolio"  class="profile-form"
                      action="user-profile/update-portfolio" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{isset($portfolio->id) ? $portfolio->id: 0}}">
                    <div class="row form-row">
                        <div class="col-md-12">
                            <input name="title" value="{{ isset($portfolio->title  ) ? $portfolio->title  : ""}}"
                                   type="text" class="form-control" placeholder="Title"
                                   id="portfolio_title" required="true">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <textarea name="summary" class="form-control" placeholder="Portfolio Summary"
                                      id="portfolio_summary"
                                      required="true">{{ isset($portfolio->summary ) ? $portfolio->summary : ""}}</textarea>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-2">
                            <label>Upload Image</label>
                        </div>
                        <div class="col-md-9 upload-file">
                            <input type="file" name="image" class="filestyle uploader"
                                   data-buttonText=" Upload " id="portfolio_image"
                                   @if(empty($portfolio->image_id)) required="true"
                                    @endif>

                            <input type="hidden" name="image_id"
                                   value="{{isset($portfolio->image_id) ? $portfolio->image_id : ""}}">
                            @if(isset($portfolio->user_medias) && $portfolio->user_medias!=null)

                                <div class="wrapper">
                                    <img src="{{$portfolio->user_medias->image_path}}" class="preview_div img-border"
                                         width="150">
                                    <span class="delete delete-image fa fa-trash"></span>
                                </div>
                            @else
                                <div class="wrapper hide">
                                    <img src="" class="preview_div img-border"
                                         width="150">
                                    <span class="delete delete-image fa fa-trash"></span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-2 pull-right">
                            <input type="submit" name="cmdPortfolio" value="Save" id="portfolio_button"
                                   class="btn btn-block btn-primary userprofilesavebutton">
                        </div>
                        <div class="col-md-2 pull-right">
                            <a href="{{url("portfolio/0")}}" id="addportfolio"
                               class="btn btn-block get_data pull-right  @if(empty($id)) active_link @endif"
                               role="button">Clear</a>
                        </div>

                    </div>
                </form>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>