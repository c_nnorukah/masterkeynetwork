@extends('layouts.master')
@section('title')
    User Profile
    @stop
    @section('main')
            <!-- BEGIN DASHBOARD TILES -->
    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>
<div class="heading-all">
    <div class="row">
        <div class="col-md-6">
            <h2 class="title">User Profile Data</h2>
        </div>

        <div class="col-md-6  text-right">
            <a href="{{ URL::route('view-user-profile',Auth::user()->usertoken ) }}"
               class="btn my-button" role="button">View Profile</a>
        </div>
    </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#about-me">About Me</a></li>
                <li><a data-toggle="tab" href="#my-portfolio">My Portfolio</a></li>
                <li><a data-toggle="tab" href="#academic-position">Academic Positions</a></li>
                <li><a data-toggle="tab" href="#education" id="Education">Education & Training</a></li>
                <li><a data-toggle="tab" href="#award">Honors, Awards and Grants</a></li>
                <li><a data-toggle="tab" href="#contact-info">Contact Info</a></li>
            </ul>

            <div class="tab-content">
                <div id="about-me" class="tab-pane fade in active">
                    @include("profile.about-me")
                </div>
                <div id="my-portfolio" class="tab-pane fade">
                    @include("profile.my-portfolio")
                </div>
                <div id="academic-position" class="tab-pane fade">
                    @include("profile.academic-position")
                </div>
                <div id="education" class="tab-pane fade">
                    @include("profile.education")
                </div>
                <div id="award" class="tab-pane fade">
                    @include("profile.award")
                </div>
                <div id="contact-info" class="tab-pane fade">
                    @include("profile.contact-info")
                </div>
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    {!! Html::script('js/module/dashboard.js') !!}
    {!! Html::script('js/module/user_profile.js') !!}
@stop