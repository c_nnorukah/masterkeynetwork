<form id="form_update_contact_info" name="frmContact" method="post" class="profile-form"
      action="user-profile/update-contact-info">
    <div class="row form-row">
        <div class="col-md-12">
            <input name="txtName" id="contact_name" type="text"
                   value="{{Auth::user()->first_name}}" class="form-control"
                   placeholder="Name" required="true" readonly>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-12">
                                            <textarea name="textareaContact" class="form-control"
                                                      placeholder="Contact Summary"
                                                      required="true">{{Auth::user()->contact_summary}}</textarea>
        </div>
    </div>
    <div class="row form-row mr-b-10">
        <div class="col-md-6 col-sm-6">
            <div class="left-inner-addon">
                <input name="numPhn" id="numPhn" type="text" class="form-control"
                       value="{{Auth::user()->phone_no}}" placeholder="Phone Number"
                       required="true" maxlength="10"  style="padding-left: 25px!important;">
                <span class="fa fa-phone form-control-feedback right-none"></span>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="left-inner-addon">
                <input name="txtMail" id="txtMail" type="email"
                       value="{{Auth::user()->email}}" class="form-control"
                       placeholder="Email ID" required="true" readonly  style="padding-left: 25px!important;">
                <span class="fa fa-envelope form-control-feedback right-none"></span>
            </div>
        </div>
    </div>
    <div class="row form-row mr-b-10">
        <div class="col-md-4 col-sm-4">

            <div class="left-inner-addon">
                <input name="txtSkype" id="txtSkype" type="text" class="form-control"
                       value="{{Auth::user()->skype_id}}" placeholder="Skype ID"
                       required="true" style="padding-left: 25px!important;">
                <span class="fa fa-skype form-control-feedback right-none"></span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="left-inner-addon">
                <input name="txtLinkedin" id="txtLinkedin" type="url"
                       class="form-control" value="{{Auth::user()->linked_id}}"
                       placeholder="LinkedIn ID" required="true"style="padding-left: 25px!important;"
                >
                <span class="fa fa-linkedin form-control-feedback right-none"></span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="left-inner-addon">
                <input name="txtTwitter" id="txtTwitter" type="text"
                       class="form-control" value="{{Auth::user()->twitter_id}}"
                       placeholder="Twitter ID" required="true" style="padding-left: 25px!important;">
                <span class="fa fa-twitter form-control-feedback right-none"></span>
            </div>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-1 pull-right">
            <input type="submit" name="cmdContact" value="Save"
                   class="btn btn-block btn-primary userprofilesavebutton">
        </div>
    </div>
</form>