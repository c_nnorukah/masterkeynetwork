<form method="post" id="about_me_form" name="frmAbout" class="profile-form"
      action="user-profile/update-about-me">
    <div class="row form-row">
        <div class="col-md-12">
            <div class="left-inner-addon">
                <input type="hidden" value="{{Auth::user()->user_id}}" id="about_me_id">
                <input name="txtName" id="txtName" type="text" class="form-control pd-l-25"
                       placeholder="Name" value="{{Auth::user()->first_name}}"
                       id="about_me_name" required="true" style="padding-left: 25px!important">
                <span class="fa fa-user form-control-feedback right-none" style="top: 11px"></span>
                <div id="aboutme_name" style="color: red;">

                </div>
            </div>
        </div>
    </div>

    <div class="row form-row">
        <div class="col-md-12">
            <textarea name="textareaBio" class="form-control" placeholder="Bio" row="5"
                      id="about_me_bio" required="true">{{Auth::user()->bio}}</textarea>
        </div>
    </div>

    <div class="row form-row mr-b-10">
        <div class="col-md-2">
            <label>Profile Picture</label>
        </div>
        <div class="col-md-6 upload-file">
            <input type="file" name="txtPP" id="about_me_file" class="filestyle uploader"
                   data-buttonText=" Upload " @if($portfolio_data[0]->image_id == "") required="true" @endif>
            <input type="hidden" name="image_id" value="{{$portfolio_data[0]->image_id}}">
            @if($portfolio_data[0]->user_medias!=null)
                <div class="image_name">{{$portfolio_data[0]->user_medias->image_name}}</div>
                <div class="wrapper">
                    <img src="{{$portfolio_data[0]->user_medias->image_path}}"
                         class="profile-pic-preview preview_div img-border"
                         width="150">
                    <span class="delete delete-image fa fa-trash"></span>
                </div>

            @else
                <div class="wrapper hide">
                    <img src="" class="preview_div profile-pic-preview img-border"
                         width="150">
                    <span class="delete delete-image fa fa-trash"></span>
                </div>

            @endif

        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-2">
            <label>CV</label>
        </div>
        <div class="col-md-6 upload-file">
            <input type="file" name="txtCv" id="cv_file" class="filestyle"
                   data-buttonText=" Upload " @if($portfolio_data[0]->user_cv == "") required="true" @endif>
            <input type="hidden" name="user_cv" value="{{$portfolio_data[0]->user_cv}}">
            <div class="cv-name">{{$portfolio_data[0]->user_cv}}<i class="m-l-5 cursor-pointer fa fa-trash delete-cv"></i></div>
        </div>
    </div>


    <div class="row form-row">
        <div class="col-md-1  pull-right">
            <input type="submit" name="cmdAbout" value="Save"
                   class="btn btn-block btn-primary userprofilesavebutton" id="about_me">
        </div>
    </div>
</form>