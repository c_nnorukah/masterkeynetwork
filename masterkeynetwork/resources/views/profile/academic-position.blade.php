{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0-alpha/css/bootstrap-datepicker.css">--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0-alpha/js/bootstrap-datepicker.js"></script>--}}

<div>

    <div class="data_div">
        <input type="hidden" name="no_of_academic_positions" id="no_of_academic_positions"
               value="{{isset($portfolio_data[0]->User_academic_positions) ? count($portfolio_data[0]->User_academic_positions) : ""}}">
        <div class="row form-row">
            <div class="col-md-3">
                <div class="scrollable_div">

                    @if(count($portfolio_data[0]->User_academic_positions) != '0')

                        @foreach($portfolio_data[0]->User_academic_positions as $key=>$value)
                            <a href="academic/{!! $value->id !!}"
                               class="get_data @if(isset($id) && $id == $value->id) active_link @endif">
                                <div class="detail">
                                    <i class="close_button fa fa-trash pull-right"
                                       data-url="{{url("academic/delete/$value->id" )}}"></i>
                                    <div class="get_data">
                                        <p><b>position:</b> {!! $value->title !!}</p>
                                        <p><b>Summary:</b> {!! str_limit($value->summary, 100)!!}</p>
                                        <p><b>from year:</b> {!! $value->from_date !!}</p>
                                        <p><b>to year:</b> {!! $value->to_date!!}</p>

                                    </div>
                                </div>
                                <hr>

                            </a>
                        @endforeach

                    @else
                        <p class="pd-l-10">No academic position found. Start adding positions.</p>
                    @endif
                </div>
            </div>

            <div class="col-md-8 mr-l-25">

                <form id="form_academic_positions" name="frmContact" method="post"  class="profile-form"
                      action="user-profile/update-academic_positions">

                    <input type="hidden" name="id" value={{isset($academic->id) ? $academic->id : ""}}>

                    <div class="row form-row">
                        <div class="col-md-12">
                            <input name="position" id="contact_name"
                                   value="{{ isset($academic->title ) ? $academic->title  : ""}}" type="text"
                                   class="form-control" placeholder="Position"
                                   required="true">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                                                            <textarea name="details" class="form-control"
                                                                      placeholder="Position Details"
                                                                      required="true">{{ isset($academic->summary ) ? $academic->summary  : ""}}</textarea>

                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-5 col-sm-5">
                            <div class="left-inner-addon">
                                {!! Form::selectRange('year',Carbon\Carbon::now()->subYears('100')->format('Y') , Carbon\Carbon::now()->addYears('15')->format('Y'),isset($academic->from_date) ? $academic->from_date : "",['name' => 'from_date','class'=>'from_date','required'=>'true','placeholder'=>'Select From Year','pattern'=>'[0-9]{4}']) !!}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="left-inner-addon">
                                {!! Form::selectRange('year', Carbon\Carbon::now()->subYears('100')->format('Y') , Carbon\Carbon::now()->addYears('15')->format('Y'),isset($academic->to_date) ? $academic->to_date : "",['name' => 'to_date','class'=>'to_date','required'=>'true','placeholder'=>'Select To Year','pattern'=>'[0-9]{4}']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-2  pull-right">
                            <div class="left-inner-addon">
                                <input type="submit" name="cmdContact" value="Save"
                                       class="btn btn-block btn-primary userprofilesavebutton">
                            </div>
                        </div>
                        <div class="col-md-2  pull-right">
                            <div class="left-inner-addon">
                                <a href="{{url("academic/0")}}" id="add_academic"
                                   class="btn btn-block get_data pull-right get_data @if(empty($id)) active_link @endif"
                                   role="button">Clear</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="separator"></div>

        </div>
    </div>
</div>