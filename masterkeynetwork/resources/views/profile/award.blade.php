<div>

    <div class="data_div">
        <input type="hidden" name="no_of_honors" id="no_of_honors"
               value="{{isset($portfolio_data[0]->User_honors) ? count($portfolio_data[0]->User_honors) : ""}}">
        <div class="row form-row">
            <div class="col-md-3">
                <div class="scrollable_div">

                    @if(isset($portfolio_data[0]->User_honors) && count($portfolio_data[0]->User_honors) != '0')

                        @foreach($portfolio_data[0]->User_honors as $key=>$value)
                            <a href="award/{!! $value->id !!}"
                               class="get_data @if(isset($id) && $id == $value->id) active_link @endif">

                                <div class="detail">
                                    <i class="close_button fa fa-trash pull-right"
                                       data-url="{{url("award/delete/$value->id" )}}"></i>
                                    <div class="get_data">
                                        <p><b>Honor:</b> {!! $value->title !!}</p>
                                        <p><b>detail:</b> {!! str_limit($value->details, 100)!!}</p>
                                        <p><b>year:</b> {!! $value->date !!}</p>

                                    </div>
                                </div>
                                <hr>

                            </a>
                        @endforeach

                    @else
                        <p class="pd-l-10"> No honor, awards and grant related information found. Start adding
                            Information.</p>
                    @endif
                </div>
            </div>


            <div class="col-md-8 mr-l-25">

                <form id="form_honors" name="frmContact" method="post"  class="profile-form"
                      action="user-profile/update-honors">

                    <input type="hidden" name="id" value={{isset($honor->id) ? $honor->id : ""}}>


                    <div class="row form-row">
                        <div class="col-md-12">
                            <input name="name_of_honor" id="contact_name"
                                   value="{{isset($honor->title) ? $honor->title : ""}}" type="text"
                                   class="form-control" placeholder="Name of Honor"
                                   required="true">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                                                            <textarea name="honor_details" class="form-control"
                                                                      placeholder="Honor Details"
                                                                      required="true">{{isset($honor->details) ? $honor->details : ""}}</textarea>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <div class="left-inner-addon">
                                {!! Form::selectRange('year', Carbon\Carbon::now()->subYears('100')->format('Y') , Carbon\Carbon::now()->addYears('15')->format('Y'),isset($honor->date) ? $honor->date : "",['name' => 'date','required'=>'true','placeholder'=>'Select Year','pattern'=>'[0-9]{4}']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-2">
                            <label>Upload Image</label>
                        </div>
                        <div class="col-md-9 upload-file">
                            <input type="file" name="honor_image" class="filestyle uploader" data-buttonText=" Upload "
                                   id="honor_image"
                                   @if(empty($honor->image_id) ) required="true" @endif>

                            <input type="hidden" name="image_id"
                                   value="{{isset($honor->image_id) ? $honor->image_id : ""}}">
                            @if(isset($honor->user_medias) && $honor->user_medias!=null)
                                <div class="wrapper">
                                    <img src="{{$honor->user_medias->image_path}}" class="preview_div img-border"
                                         width="150">
                                    <span class="delete delete-image fa fa-trash"></span>
                                </div>
                            @else
                                <div class="wrapper hide">
                                    <img src="" class="preview_div img-border"
                                         width="150">
                                    <span class="delete delete-image fa fa-trash"></span>
                                </div>
                            @endif

                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-2 pull-right">
                            <div class="left-inner-addon">
                                <input type="submit" name="cmdContact" value="Save"
                                       class="btn btn-block btn-primary userprofilesavebutton">
                            </div>
                        </div>
                        <div class="col-md-2 pull-right">
                            <div class="left-inner-addon">
                                <a href="{{url("award/0")}}" id="add_honor"
                                   class="btn btn-block get_data pull-right get_data @if(empty($id)) active_link @endif"
                                   role="button">Clear</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>