<div>
    <div class="data_div">
        <input type="hidden" name="no_of_education_details" id="no_of_education_details"
               value="{{isset($portfolio_data[0]->User_education_details) ? count($portfolio_data[0]->User_education_details) : ""}}">
        <div class="row form-row">
            <div class="col-md-3">
                <div class="scrollable_div">

                @if(isset($portfolio_data[0]->User_education_details) && count($portfolio_data[0]->User_education_details) != '0')

                        @foreach($portfolio_data[0]->User_education_details as $key=>$value)
                            <a href="education/{!! $value->id !!}" class="get_data @if(isset($id) && $id == $value->id) active_link @endif">
                                <div class="detail">
                                    <i class="close_button fa fa-trash pull-right" data-url="{{url("education/delete/$value->id" )}}"></i>
                                    <div class="get_data">
                                        <p><b>degree:</b> {!! $value->degree !!}</p>
                                        <p><b>title:</b> {!! $value->title !!}</p>
                                        <p><b>university:</b> {!! $value->university !!}</p>
                                        <p><b>passing year:</b> {!! $value->passing_year!!}</p>

                                    </div>
                                </div>
                                <hr>

                            </a>
                        @endforeach

                @else
                    <p class="pd-l-10">No educational information foun. Start adding educational information.</p>
                @endif
            </div>
                </div>

            <div class="col-md-8 mr-l-25">
                <form id="form_education" name="frmContact" method="post"  class="profile-form"
                      action="user-profile/update-education">

                    <input type="hidden" name="id" value={{isset($education->id) ? $education->id : ""}}>


                    <div class="row form-row">
                        <div class="col-md-12">
                            <input name="degree" id="contact_name" type="text"
                                   value="{{isset($education->degree) ? $education->degree : "" }}" class="form-control"
                                   placeholder="Degree" required="true" maxlength="6">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <input name="title" id="contact_name"
                                   value="{{isset($education->title) ? $education->title : "" }}" type="text"
                                   class="form-control" placeholder="Title"
                                   required="true">

                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6 col-sm-6">
                            <div class="left-inner-addon">

                                <input name="university" id="numPhn"
                                       value="{{isset($education->university) ? $education->university : "" }}" type="text"
                                       class="form-control" placeholder="university"
                                       required="true">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="left-inner-addon">

                                {!! Form::selectRange('year',Carbon\Carbon::now()->subYears('100')->format('Y') , Carbon\Carbon::now()->addYears('15')->format('Y'),isset($education->passing_year) ? $education->passing_year : "",['name' => 'passing_year','required'=>'true','placeholder'=>'Select passing year','pattern'=>'[0-9]{4}']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-2 pull-right">
                            <div class="left-inner-addon">
                                <input type="submit" name="cmdContact" value="Save"
                                       class="btn btn-block btn-primary userprofilesavebutton">
                            </div>
                        </div>
                        <div class="col-md-2 pull-right">
                            <div class="left-inner-addon">
                                <a href="{{url("education/0")}}" id="add_education" class="btn btn-block get_data pull-right get_data @if(empty($id)) active_link @endif" role="button">Clear</a>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
