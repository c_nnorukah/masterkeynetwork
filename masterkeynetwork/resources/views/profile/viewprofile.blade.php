@extends('layouts.masterprofile')

@section('content')
    <div id="biography" class="page home" data-pos="home">
        <div class="pageheader">
            <div class="headercontent">
                <div class="section-container">


                    <div class="row">
                        <div class="col-sm-2 visible-sm"></div>
                        <div class="col-sm-8 col-md-4">
                            <div class="biothumb">
                                @if(!empty($user_data[0]->user_medias))
                                <img alt="image" height="250px" width="250px"
                                     src="{!! @asset($user_data[0]->user_medias->image_path) !!}">
                                @else
                                    <img alt="image" height="250px" width="250px" src="{!! @asset("images/no-image.gif") !!}">
                                @endif
                            </div>

                        </div>
                        <div class="clearfix visible-sm visible-xs"></div>
                        <div class="col-sm-12 col-md-7">
                            <h3 class="title">Bio</h3>
                            <p>{{ $user_data[0]->bio}}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="pagecontents">
            <div class="section color-1">
                <div class="section-container">
                    <div class="row">

                        @if(count($user_data[0]->User_academic_positions)!=0)

                            <div class="col-md-5 col-md-offset-1">
                                <div class="title text-center">
                                    <h3>Academic Positions</h3>
                                </div>


                                <ul class="ul-dates">
                                    @foreach($user_data[0]->User_academic_positions as $key=>$value)
                                        <li>
                                            <div class="dates">
                                                <span>{{$value->to_date}}</span>
                                                <span>{{$value->from_date}}</span>
                                            </div>
                                            <div class="content">
                                                <h4>{{$value->title}}</h4>
                                                <p>{{$value->summary}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        @else

                        @endif

                        @if(count($user_data[0]->User_education_details)!=0)
                            <div class="col-md-5">
                                <div class="title text-center">
                                    <h3>Education & Training</h3>
                                </div>

                                <ul class="ul-card">
                                    @foreach($user_data[0]->User_education_details as $key=>$value)
                                        <li>
                                            <div class="dy">
                                                <span class="degree">{{$value->degree}}</span>
                                                <span class="year">{{$value->passing_year}}</span>
                                            </div>
                                            <div class="description">
                                                <p class="waht">{{$value->title}}</p>
                                                <p class="where">{{$value->university}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        @else
                        @endif

                    </div>
                </div>

            </div>

            <div class="section color-2">
                <div class="section-container">
                    <div class="row">
                        @if(count($user_data[0]->User_honors)!=0)

                            <div class="col-md-10 col-md-offset-1">
                                <div class="title text-center">
                                    <h3>Honors, Awards and Grants</h3>
                                </div>
                                <ul class="timeline">
                                    @foreach($user_data[0]->User_honors as $key=>$value)

                                        <li class="open">
                                            <div class="date">{{$value->date}}</div>
                                            <div class="circle"></div>
                                            <div class="data">
                                                <div class="subject">{{$value->title}}</div>
                                                <div class="text row">
                                                    <div class="col-md-2">
                                                        <img alt="image" class="thumbnail img-responsive"
                                                             src={!! @asset($value->User_medias->image_path) !!} >
                                                    </div>
                                                    <div class="col-md-10">
                                                        {{$value->details}}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @else

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="portfolio" class="page">
        <div class="pagecontents">

            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <style>
                .carousel-inner > .item > img,
                .carousel-inner > .item > a > img {
                    width: 80%;
                    height: 650px;
                    margin: auto;
                }
            </style>

            @if(count($user_data[0]->user_portfolios)!=0)

                <div id="profile-slider">


                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            @foreach($user_data[0]->user_portfolios as $key=>$value)
                                @if($key==0)

                                    <div class="item active">
                                        <img src="{{asset($user_data[0]->user_portfolios[0]->user_medias['image_path'])}}"
                                             alt="Chania" width="100" height="100">
                                        <div class="carousel-caption">
                                            <h3>{{ $value->title  }}</h3>
                                            <p>{{ $value->summary }}</p>
                                        </div>

                                    </div>

                                @else

                                    <div class="item">
                                        <img src="{!! asset($value->user_medias['image_path']) !!}" alt="Chania"
                                             width="100" height="100">
                                        <div class="carousel-caption">
                                            <h3>{{ $value->title  }}</h3>
                                            <p>{{ $value->summary }}</p>
                                        </div>
                                    </div>

                                @endif
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            @else

            @endif


        </div>

    </div>



    <div id="contact" class="page stellar">
        <div class="pageheader">
            <div class="headercontent">

                <div class="section-container">

                    <h2 class="title">Contact & Meet Me</h2>

                    <div class="row">
                        <div class="col-md-8">
                            <p>{{ $user_data[0]->contact_summary}}</p>
                        </div>
                        <div class="col-md-4">
                            <ul class="list-unstyled">
                                @if( $user_data[0]->phone_no!="")

                                    <li>
                                        <strong><i class="icon-phone"></i>&nbsp;&nbsp;</strong>
                                        <span><a href="tel:{{ $user_data[0]->phone_no}}">{{ $user_data[0]->phone_no}}</a></span>
                                    </li>
                                @else

                                @endif

                                @if($user_data[0]->user_name!="")

                                    <li>
                                        <strong><i class="icon-envelope"></i>&nbsp;&nbsp;</strong>
                                        <span><a href="mailto:{{$user_data[0]->user_name}}">{{$user_data[0]->user_name}}</a></span>
                                    </li>
                                @else

                                @endif

                                @if($user_data[0]->skype_id!="")

                                    <li>
                                        <strong><i class="icon-skype"></i>&nbsp;&nbsp;</strong>
                                        <span><a href="skype:{{ $user_data[0]->skype_id}}?userinfo ">{{ $user_data[0]->skype_id}}</a></span>
                                    </li>
                                @else

                                @endif
                                @if($user_data[0]->twitter_id!="")

                                    <li>
                                        <strong><i class="icon-twitter"></i>&nbsp;&nbsp;</strong>
                                        <span><a href="https://twitter.com/{{$user_data[0]->twitter_id}}">{{$user_data[0]->twitter_id}}</a></span>
                                    </li>
                                @else
                                @endif
                                @if($user_data[0]->linked_id!="")

                                    <li>
                                        <strong><i class="icon-linkedin-sign"></i>&nbsp;&nbsp;</strong>
                                        <span><a href="{{ $user_data[0]->linked_id}}">Linkedin</a></span>
                                    </li>

                                @else
                                @endif
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="overlay"></div>
@stop
