<!-- BEGIN SIDEBAR -->

<div class="page-sidebar " id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper">

            <div class="user-info">
                <div class="greeting">
                    <div class="profile-wrapper">
                        @if(!empty(Auth::user()->User_medias->image_path))
                            <img src="{{url(Auth::user()->User_medias->image_path)}}" alt="" data-src="assets/img/profiles/avatar_small-new.jpg" data-src-retina="assets/img/profiles/avatar_small2xx.jpg" width="69" height="69"/>
                        @else
                            {!! Html::image('images/profile_placeholder.png', '', ['class' => '', 'data-src' => asset(''), 'data-src-retina' => asset('images/profile_placeholder.png'), 'width' => '69','height' => '69']) !!}
                        @endif
                    </div>


                    {{ isset(auth()->user()->plan->plan_name)?ucwords(auth()->user()->plan->plan_name) : 'Free' }}
                    <br>
                    <small>{{$user_name=Auth::user()->first_name }} {{Auth::user()->last_name}}</small>
                </div>
            </div>
        </div>
        <!-- END MINI-PROFILE -->

        <!-- BEGIN SIDEBAR MENU -->
        <ul>
            <li class="start" data-identifier="admin-dashboard"><a href="#"> <span class="title">Admin Dashboard</span>
                    <span class="selected"></span> <span class="open "> <i class="fa fa-bar-chart"></i>
</span> </a>
                <ul class="sub-menu">
                    <li class="" data-identifier="application-setting"><a
                                href="{!! URL::route('application-settings') !!}"><i class="fa fa-cog"></i> Application
                            Settings </a></li>
                    <li class="hidden" data-identifier="admin-setting"><a
                                href="{!! URL::route('application-settings') !!}"><i class="fa fa-cog"></i></a></li>

                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '1')

                                <li class="" data-identifier="paypal-payment-settings"><a
                                            href="{!! URL::route('paypal-payment-settings') !!}"><i class="fa fa-paypal"></i> Paypal
                                        Payment Settings</a></li>
                            @endif
                        @endforeach
                    @endif
                    <li class="" data-identifier="smtp-setting"><a href="{!! URL::route('smtp-setting') !!}"><i
                                    class="fa fa-database"></i> SMTP Settings</a></li>
                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '2')
                                <li class="" data-identifier="ftp-settings"><a href="{!! URL::to('ftp-settings') !!}"><i
                                                class="fa fa-database"></i> FTP Settings</a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </li>

            <li data-identifier="user-setting"><a href="javascript:;"> <span class="title"> User Related Settings</span>
                    <span class="open"><i class="fa fa-laptop"></i></span></a>
                <ul class="sub-menu">
                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '2')
                                <li class="" data-identifier="auto-responder"><a href="{!! URL::to('auto-responder') !!}"><i
                                                class="fa fa-paper-plane-o"></i>
                                        Autoresponders</a></li>
                            @endif
                        @endforeach
                    @endif
                    {{--for admin--}}
                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '1')
                                <li data-identifier="user-list"><a href="{!! URL::to('user-list') !!} "><i
                                                class="fa fa-phone"></i>Contacts</a></li>
                            @endif
                        @endforeach
                    @endif
                    <li data-identifier="contact-book"><a href="{!! URL::to('contact-book') !!} "><i
                                    class="fa fa-phone"></i>Contact Book</a></li>
                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '2')
                                <li data-identifier="contact-list"><a href="{!! URL::to('contact-list') !!} "><i
                                                class="fa fa-phone"></i>Contact List</a>
                                </li>
                                <li class="" data-identifier=""><a href="{!! URL::to('email-campaign') !!}"><i
                                                class="fa fa-envelope"></i> Email Campaign</a>
                                </li>

                            @endif
                        @endforeach
                    @endif
                    @if($_SERVER['HTTP_HOST'] == "site.masterkeynetwork.com")
                        <li class="" data-identifier="architect"><a
                                    href="/landingpage/architect/?user={{ Auth::user()->user_id }}#/dashboard"><i
                                        class="fa fa-file-text-o"></i> Landing Pages</a></li>
                    @else
                        <li class="" data-identifier="architect"><a
                                    href="http://masterkeynetwork.alfyopare.com/beta/landingpage/architect/?user={{ Auth::user()->user_id }}#/dashboard"><i
                                        class="fa fa-file-text-o"></i> Landing Pages</a></li>
                    @endif
                    <li class="" data-identifier="responses"><a
                                href="{!! URL::route('lead-capture-responses') !!}"><i class="fa fa-file-text-o"></i>Lead
                            Statuses</a></li>
                    <li class="" data-identifier="social-posting"><a href="{{url('social-posting')}} "><i
                                    class="fa fa-thumbs-o-up"></i> Social
                            Postings</a></li>
                    @if(Session::has('role'))
                        @foreach(Session::get('role') as $val)
                            @if($val == '2')
                                <li class="" data-identifier="plans"><a href="{!! URL::route('plans') !!}"><i
                                                class="fa fa-usd"></i>
                                        Plans and Pricing</a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </li>


            <li data-identifier=""><a href="javascript:;"> </i> <span class="title">Templates</span> <span class="open"><i
                                class="fa fa-folder"></i></span> </a>
                <ul class="sub-menu">
                    <li data-identifier="add-email-template" class=""><a href="{{ url('add-email-template') }}"><i class="fa fa-envelope"></i>
                            Email
                            Templates</a></li>
                </ul>
            </li>

            <li data-identifier=""><a href="javascript:;"><span class="title">Affiliate Dashboard</span> <span
                            class="open"><i class="fa fa-life-ring"></i></span> </a>
                <ul class="sub-menu">
                    <li class="" data-identifier="downline-members"><a href="{{ url('downline-members') }}"><i class="fa fa-users"></i>
                            Members</a></li>
                    <li class="" data-identifier="downline-reports"><a href="{{ url('downline-reports') }}"><i
                                    class="fa fa-file-text"></i> Reports</a>
                    </li>
                </ul>
            </li>

            <li data-identifier=""><a href="javascript:;"> <span class="title">User Profile</span> <span class="open"><i
                                class="fa fa-user"></i></span> </a>
                <ul class="sub-menu">
                    <li class="" data-identifier="user-profile"><a href="{!! URL::route('user-profile') !!}"><i
                                    class="fa fa-users"></i> Update Profile</a></li>
                    <li class="" data-identifier=""><a
                                href="{{ URL::route('view-user-profile',Auth::user()->usertoken ) }}"><i
                                    class="fa fa-file-text"></i> Profile</a></li>
                </ul>
            </li>

            <li data-identifier="forms"><a href="javascript:;"> <span class="title">Blogs</span> <span class=" open"> <i
                                class="fa fa-align-left"></i> </span> </a>
                <ul class="sub-menu">
                    <li class="" data-identifier="education"><a href="{!! URL::route('education') !!}"><i
                                    class="fa fa-question-circle"></i>Education</a></li>
                    <li data-identifier="blogs"><a href="{!! URL::route('myblog') !!}" target="_blank"><i
                                    class="fa fa-comment"></i> My Blog </a></li>
                    <li data-identifier="blogs"><a href="#"><i
                                    class="fa fa-commenting-o"></i>MasterKeyNetwork Blog</a></li>
                </ul>
            </li>
            <li data-identifier="forms"><a href="javascript:;"> <span class="title">Share Your Affiliate Link</span> <span class=" open"> <i
                                class="fa fa-share-alt"></i> </span> </a>
                <ul class="sub-menu">
                    <div id="fb-root"></div>
                    <li class="" data-identifier="education"><a id="share_button" style="cursor:pointer;"><i
                                    class="fa fa-facebook"></i>Facebook</a></li>

                    <li data-identifier="blogs"><a style="cursor:pointer;" href="http://twitter.com/home?status={{URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code)}}" target="_blank"><i
                                    class="fa fa-twitter"></i>Twitter</a></li>

                    <li><a id="anchortag" style="cursor:pointer;" data-value="{{URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code)}}"><i
                                    class="fa fa-files-o"></i>Copy Code</a>
                        {{--<input type="hidden" value="{!! URL::to('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code) !!}" ">--}}
                    </li>
                </ul>
            </li>
        </ul>


        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
{{--<a href="#" class="scrollup">Scroll</a>--}}
{{--<div class="footer-widget">--}}
{{--<div class="progress transparent progress-small no-radius no-margin">--}}
{{--<div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>--}}
{{--</div>--}}
{{--<div class="pull-right">--}}
{{--<div class="details-status"> <span class="animate-number" data-value="86" data-animation-duration="560">86</span>% </div>--}}
{{--<a href="lockscreen.html"><i class="fa fa-power-off"></i></a></div>--}}
{{--</div>--}}
<!-- END SIDEBAR -->