<!-- Modal -->
<div id="confirmDelete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Need Delete?</h4>
            </div>
            <div class="modal-body">
                <p>{!! trans('message.confirmDelete') !!}</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary delete-button">Delete</a>
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>

    </div>
</div>