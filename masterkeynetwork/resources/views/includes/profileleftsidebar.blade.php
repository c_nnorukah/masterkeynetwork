<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $("#myport").click(function () {
            $("#slider-divyank").show();
        });
    });
</script>
<a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a>

<div id="sidebar">
    <div id="main-nav">
        <div id="nav-container"><br>
               <center> <a href="{{url('dashboard')}}" ><button class="btn pull-center"><i class="fa fa-arrow-left"></i> Back to dashboard</button></a></center>
            <div id="profile" class="clearfix">
                    @if(!empty($user_data[0]->user_medias))
                    <div class="portrate hidden-xs"
                         style="background-image: url({!! @asset($user_data[0]->user_medias->image_path) !!})"></div>
                @else
                    <div class="portrate hidden-xs"
                         style="background-image: url({!! @asset("images/no-image.gif") !!})"></div>
                @endif
                <div class="title">
                    <h2>{{ $user_data[0]->first_name }} {{ $user_data[0]->last_name }} </h2>
                    @if(count($user_data[0]->User_education_details)!=0)
                        <h3>{{$user_data[0]->User_education_details[0]->university}}</h3>
                    @else
                    @endif
                </div>

            </div>

            <ul id="navigation" class="profilemenu">
                <li class="main-menu">
                    <a href="#biography">
                        <div class="icon icon-user"></div>
                        <div class="text">About Me</div>
                    </a>
                </li>

                <li class="main-menu">
                    <a href="#portfolio">
                        <div class="icon icon-picture"></div>
                        <div class="text" id="myport">My Portfolio</div>
                    </a>
                </li>


                <li class="external">
                    <a href="#">
                        <div class="icon icon-download-alt"></div>
                        <div class="text"><a href="{{ asset("profile/cv/".$user_data[0]->user_cv) }}" download>Download
                                MY CV</a></div>
                    </a>
                </li>

                <li class="main-menu">
                    <a href="#contact" id="conracr-detail">
                        <div class="icon icon-calendar"></div>
                        <div class="text" id="contactmemenu">Contact Me</div>
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="social-icons">
        <ul>
            <li><a href="skype:{{ $user_data[0]->skype_id}}?userinfo"><i class="icon-skype"></i></a></li>
            <li><a href="https://twitter.com/{{$user_data[0]->twitter_id}}"><i class="icon-twitter"></i></a></li>
            <li><a href="{{ $user_data[0]->linked_id}}"><i class="icon-linkedin"></i></a></li>
        </ul>
    </div>
</div>

