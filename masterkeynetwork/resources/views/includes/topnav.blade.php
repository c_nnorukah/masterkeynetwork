<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="header-seperation">
            <ul class="nav pull-left notifcation-center visible-xs visible-sm">
                <li class="dropdown">
                    <a href="#main-menu" data-webarch="toggle-left-side">
                        <div class="iconset top-menu-toggle-white"></div>
                    </a>
                </li>
            </ul>
            <!-- BEGIN LOGO -->
            <a href="{!! URL::route('dashboard') !!}">
                {!! Html::image('images/master-logo.png', 'logo', ['class' => 'logo-new center-block', 'data-src' => asset('images/master-logo.png'), 'data-src-retina' => asset('images/master-logo.png'), 'width' => '200']) !!}
            </a>
            <!-- END LOGO -->

        </div>

        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="header-quick-nav">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="pull-left">
                <ul class="nav quick-section">
                    <li class="quicklinks">
                        <a href="#" class="" id="layout-condensed-toggle">
                            <div class="iconset top-menu-toggle-dark"></div>
                        </a>
                    </li>
                </ul>
                @if(Route::getCurrentRoute()->uri() == 'dashboard')
                    <ul class="nav quick-section">
                        <li class="quicklinks"><a href="#" class="">
                                <div class="iconset top-reload"></div>
                            </a></li>
                        <li class="quicklinks"><span class="h-seperate"></span></li>
                        <li class="quicklinks"><a href="#" class="">
                                <div class="iconset top-tiles"></div>
                            </a></li>
                        <li class="m-r-10 input-prepend inside search-form no-boarder">
                            <span class="add-on top-search-icon"> <span class="iconset top-search"></span></span>
                            <input name="" type="text" class="no-boarder form-control" placeholder="Search Dashboard"
                                   style="width:250px;">
                        </li>
                    </ul>
                @endif
            </div>
            <!-- END TOP NAVIGATION MENU -->
            <!-- BEGIN CHAT TOGGLER -->
            <div class="pull-right">
                <div class="chat-toggler">
                    <?php $flag = 0;?>
                    @if(!empty(Auth::user()->User_medias->image_path))
                        <?php $flag = 1;?>
                        <div class="profile-pic pull-right">
                            <img src="{{url(Auth::user()->User_medias->image_path)}}" alt=""
                                 data-src="assets/img/profiles/avatar_small-new.jpg"
                                 data-src-retina="assets/img/profiles/avatar_small2xx.jpg" width="35" height="35"/>
                        </div>
                    @else
                        <div class="profile-pic pull-right">
                            {!! Html::image('images/profile_placeholder.png', '', ['class' => '', 'data-src' => asset(''), 'data-src-retina' => asset('images/profile_placeholder.png'), 'width' => '35','height' => '35']) !!}
                        </div>
                    @endif
                    <a href="javascript:void(0);" class="dropdown-toggle pull-right" id="my-task-list"
                       data-placement="bottom" data-content='' data-toggle="dropdown"
                       data-original-title="Notifications">
                        <div class="user-details">
                            <div class="username">
                                <span class="badge badge-important">3</span>
                                <span class="user_first_name">{{$user_name=Auth::user()->first_name }}</span>
                                <span class="bold">{{Auth::user()->last_name}}</span>
                            </div>
                        </div>
                        <div class="iconset top-down-arrow"></div>
                    </a>
                    <div id="notification-list" style="display:none">
                        <div>
                            <div class="notification-messages info">
                                <div class="user-profile">
                                    <img src="assets/img/profiles/d.jpg" alt="" data-src="assets/img/profiles/d.jpg"
                                         data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35">
                                </div>
                                <div class="message-wrapper">
                                    <div class="heading">
                                        David Nester - Commented on your wall
                                    </div>
                                    <div class="description">
                                        Meeting postponed to tomorrow
                                    </div>
                                    <div class="date pull-left">
                                        A min ago
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="notification-messages danger">
                                <div class="iconholder">
                                    <i class="icon-warning-sign"></i>
                                </div>
                                <div class="message-wrapper">
                                    <div class="heading">
                                        Server load limited
                                    </div>
                                    <div class="description">
                                        Database server has reached its daily capicity
                                    </div>
                                    <div class="date pull-left">
                                        2 mins ago
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="notification-messages success">
                                <div class="user-profile">
                                    <img src="assets/img/profiles/h.jpg" alt="" data-src="assets/img/profiles/h.jpg"
                                         data-src-retina="assets/img/profiles/h2x.jpg" width="35" height="35">
                                </div>
                                <div class="message-wrapper">
                                    <div class="heading">
                                        You haveve got 150 messages
                                    </div>
                                    <div class="description">
                                        150 newly unread messages in your inbox
                                    </div>
                                    <div class="date pull-left">
                                        An hour ago
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>

                <ul class="nav quick-section user-setting-nav">

                    <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="javascript:void(0);"
                           id="user-options">
                            <div class="iconset top-settings-dark "></div>
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            <li>
                                <a href="{!! URL::route('user-profile') !!}"> My Account</a>
                            </li>
                            <li>
                                <a href="#">My Calendar</a>
                            </li>
                            <li>
                                <a href="#"> My Inbox&nbsp;&nbsp;
                                    <span class="badge badge-important animated bounceIn">2</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ URL::route('logout') }}"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <li class="quicklinks"><span class="h-seperate"></span></li>
                    <li class="quicklinks">
                        @if(Session::has('role'))
                            @foreach(Session::get('role') as $val)
                                @if($val == '2')
                                    <a href="#" class="chat-menu-toggle" data-webarch="toggle-right-side">
                                        <div class="iconset top-chat-dark "><span
                                                    class="badge badge-important hide">1</span></div>
                                    </a>
                                    <div class="simple-chat-popup chat-menu-toggle hide">
                                        <div class="simple-chat-popup-arrow"></div>
                                        <div class="simple-chat-popup-inner">
                                            <div style="width:100px">
                                                @if(isset(Auth::user()->user_name))
                                                    <div class="semi-bold">{{$user_name=Auth::user()->user_name }}</div>
                                                    <div class="message">Hey you there</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </li>
                </ul>
            </div>
            <!-- END CHAT TOGGLER -->
        </div>
        <!-- END TOP NAVIGATION MENU -->

    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>