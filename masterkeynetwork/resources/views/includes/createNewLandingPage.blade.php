<!-- Modal -->
<div id="createNewLandingPage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Landing Page</h4>
            </div>
            <div class="modal-body">
                <!-- Wrapper for slides -->
                @if($landingPage->count() > 0)
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner cr-inner-content">

                            <?php $isActive = 'active'; ?>
                            @foreach($landingPage as $page)
                                <div class="item {!! $isActive !!}">
                                {!! Form::open(['route' => ['admin-create-customize-landing-page'], 'method' => 'post', 'role' => 'form']) !!}

                                    <div class="col-md-12 text-center m-b-20">
                                        {!! Form::hidden('template_id', $page->landing_page_id) !!}
                                        {!! Form::text('template_name', '', ['placeholder' => 'Name your landing page']) !!}
                                        <h3>Select a Template</h3>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <a href="#x">
                                                <div class="LandingPageContainer">
                                                    {!! Html::image('admin/images/landing_pages/admin_landing_pages/template-'.$page->landing_page_id.'.png', 'landing page', ['width' => '530']) !!}
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-12 m-t-10">
                                            <p class="text-center">
                                                {!! Form::input('submit', 'customize', 'Customize it', ['class' => 'btn btn-info my-button']) !!}
                                            </p>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <!--/row-->
                                </div>
                                <?php $isActive = ''; ?>
                            @endforeach
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                @else
                    <p>{!! trans('message.landingPage.noTemplates') !!}</p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>