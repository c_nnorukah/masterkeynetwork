<div class="row text-center">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" id="scroll-off">
            <div class="modal-content-2">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                    <h2>Course Detail</h2>
                </div>
                <div class="edu-pop-content">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                                <form id="fileupload" class="form-row m-t-20 course-form search_cars" method="post"
                                      enctype="multipart/form-data" action="{{ URL::route('store-course') }}"
                                      name="courseForm">
                                    <input type="hidden" id="course_id" name="course_id">

                                    <div class="form-group row m-b-10 course-cat">
                                        <div class="col-sm-12 ">
                                            <div class="ui-widget">
                                                <input type="text" name="category_name"
                                                       class="form-control tags cat-validation"
                                                       placeholder="Add Category" id="category_name">
                                            <span id="" class="error txt-error"
                                                  style="display:none;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control required txt"
                                                   id="course_title" name="course_title"
                                                   value="@if(isset($courseData)){{$courseData->course_title}}@endif"
                                                   placeholder="Add New Course Title">
                                            <span id="" class="error txt-error"
                                                  style="display:none;"></span>

                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <div class="col-sm-12">
                                            <textarea class="form-control required txt"
                                                      id="course_description"
                                                      name="course_description"
                                                      placeholder="Add New Course Description"></textarea>
                                            <span id="" class="error txt-error"
                                                  style="display:none;"></span>
                                        </div>
                                    </div>
                                    <div class="image-container">
                                        <div class="form-group row" id="p">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="email"><i
                                                            class="fa fa-upload"></i> Upload Course
                                                    <i class="fa fa-question-circle"
                                                       title="{{ trans('message.tooltip_image') }}"></i></label>
                                            </div>
                                            <div class="col-sm-8 append">
                                                <div class="add-img">
                                                    <span id="add-btn"></span>
                                                    {!! Form::file('images', array('class' => 'imageInput images','id'=>'first_image','multiple'=>'multiple')) !!}
                                                    <span><img class="image-upload-preview" style="display: none" src=""
                                                               alt="" width="90" height="90" class="pull-left"/></span>

                                                    <p class="errors">{!!$errors->first('images')!!}</p>
                                                    @if(Session::has('error'))
                                                        <p class="errors">{!! Session::get('error') !!}</p>
                                                    @endif
                                                </div>
                                                <input type="hidden" name="material_id_hidden"
                                                       class="hidden_matirial_id_is_default">
                                            </div>
                                            <div class="" id="">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-container">
                                        <div class="form-group row edit-doc">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="email"><i
                                                            class="fa fa-play-circle-o"></i> Add
                                                    Video Resource <i class="fa fa-question-circle"
                                                                      title="{{ trans('message.tooltip_video') }}"></i></label>

                                            </div>
                                            <div class="col-sm-8 add-video">
                                                <input type="hidden" class="material_video_src"
                                                       name="material_video_src[0]">
                                                <input type="text" class="video video-pre material_video"
                                                       name="material_video[0]" id="video_add_btn">
                                                <span style="float:right;" class="add_new_video_button"></span>
                                                <span class="clearfix"></span>
                                                

                                                <span id="txturl-error" class="error txturl-error"
                                                      style="display:none;"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="doc-container">
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="email"><i
                                                            class="fa fa-file-text-o"></i> Upload
                                                    Course Docs<i class="fa fa-question-circle"
                                                                  title="{{ trans('message.tooltip_document') }}"></i></label>


                                            </div>
                                            <div class="col-sm-8 add-doc">
                                                <div class="add-doc">
                                                    {!! Form::file('docs', array('class' => 'document','id'=>'first_doc','style'=>' display: inline;')) !!}
                                                    <span style="float:right;" class="add_new_button"></span>
                                                    <span id="" class="error txtdoc-error" style="display:none;"></span>

                                                    <p class="errors">{!!$errors->first('doc')!!}</p>
                                                    @if(Session::has('error'))
                                                        <p class="errors">{!! Session::get('error') !!}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <center>
                                    <div id="loadingProgressG">Loading...</div>
                                    </center>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="hidden" id="feature_image" name="feature_image">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block">Load
                                                Course
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="edu-pop-content-hidden" style="display:none"></div>