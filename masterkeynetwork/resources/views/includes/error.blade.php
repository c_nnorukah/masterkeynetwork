<div class="alert alert-{{ $type }} msg" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
    {!! $message !!}
</div>


