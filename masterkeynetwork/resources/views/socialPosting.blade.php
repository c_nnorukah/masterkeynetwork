@extends('layouts.master')
@section('title')
    Social Posting
@stop
@section('main')

    <div class="alert alert-success" id="flash_msg" style="display: none">
        <button class="close" data-dismiss="alert"></button>
        <div class="msg"></div>
    </div>

    <div class="row heading-all">
        <div class="col-md-6">
            <p class="lefty">Social Posting Data</p>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#facebook">Facebook Post</a></li>
                <li><a data-toggle="tab" href="#twitter">Twitter Post</a></li>
            </ul>

            <div class="tab-content">
                <div id="facebook" class="tab-pane fade in active">
                    @if(isset($data['fbpost']))
                        <div class="col-md-4">
                            <div class="post-list">
                                <div><img src="{!! Session::get('fb_dp') !!}" style="height: 100px; width: 100px;"></div>
                            </div>
                        </div>

                        @foreach($data['fbpost'] as $val)

                            <div class="col-md-4">
                                <div class="post-list">
                                    @if(!empty($val['picture']))
                                        <div class="user-photo"><img src="{!!$val['picture'] !!}"></div>
                                    @endif

                                    @if(!empty($val['message']))
                                        <?php $length_text = strlen($val['message']); ?>
                                        <div class="user-post">{{substr($val['message'],0,39)}}</div>
                                    @else
                                        <div class="user-post"></div>
                                        <?php $length_text = 0; ?>
                                    @endif

                                    @foreach($val['created_time'] as $date)
                                        @if($length_text > 30)
                                            <span class="date">
                                                <?php $start_day = date("d-m-Y", strtotime($date)); ?>
                                                {{substr($start_day,0,19)}}</span>
                                            <a href="#myModal{{$val['id']}}" data-toggle="modal" data-target="#myModal{{$val['id']}}" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> </a>@break
                                            {{--<a href="#myModal{{$val['id']}}" data-toggle="modal" data-target="#myModal{{$val['id']}}" class="btn btn-labeled btn-info"><i class="fa fa-eye"></i> View more </a>@break</p>--}}
                                        @else
                                            <?php $start_day = date("d-m-Y", strtotime($date)); ?>
                                            <span class="date">{{substr($start_day,0,19)}}@break</span>                                            @endif
                                    @endforeach

                                        @if(!empty($val['likes']))
                                            <?php $i = 0; ?>
                                            @foreach($val['likes'] as $likes)
                                        <?php $i ++;?>
                                        @endforeach
                                                <span>likes:{{$i}}</span>
                                        @else
                                            <span>likes:{{0}}</span>
                                        @endif

                                        @if(!empty($val['comments']))
                                            <?php $i = 0; ?>
                                            @foreach($val['comments'] as $comments)
                                                <?php $i ++;?>
                                            @endforeach
                                            <span>Comments:{{$i}}</span>
                                        @else
                                            <span>Comments:{{0}}</span>
                                        @endif

                                </div>
                            </div>
                            <!-- Model -->
                            <div class="modal fade" id="myModal{{$val['id']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel"></h4>
                                        </div>
                                        <div class="modal-body">
                                            @if(!empty($val['message']))
                                                {{$val['message']}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    @else
                        <br><br>
                        <a href="{{url('application-setting')}}">
                            <button type="button" class="btn btn-labeled btn-info">Login first to see post here</button>
                        </a>
                    @endif
                </div>
                <div id="twitter" class="tab-pane fade">

                    @if(isset($data['tweet']))
                        <div class="col-md-4">
                            <div class="post-list">
                                <div><img src="{!! Session::get('tw_dp') !!}" style="height: 100px; width: 100px;"></div>
                            </div>
                        </div>
                        @foreach($data['tweet'] as $twitter_details)
                            <div class="col-md-4">
                                <div class="post-list">
                                    @if(isset($twitter_details->extended_entities->media))

                                        @foreach($twitter_details->extended_entities->media as $media)
                                            <div class="user-photo"><img src="{!! $media->media_url !!}"></div>
                                            <?php $length_text = strlen($twitter_details->text); ?>
                                            <div class="user-post"> {{$text = substr($twitter_details->text,0,30 )}}</div>
                                            <?php $start_day = date("d-m-Y", strtotime($twitter_details->created_at)); ?>
                                            <span class="date">{{substr($start_day,0,19)  }}</span>
                                        <span>Likes:{{$twitter_details->favorite_count}}</span>
                                            @if($length_text > 30)
                                                <a href="#myModal{{$twitter_details->id}}" data-toggle="modal" data-target="#myModal{{$twitter_details->id}}" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> </a>
                                                {{--<a href="#myModal{{$twitter_details->id}}" data-toggle="modal" data-target="#myModal{{$twitter_details->id}}" class="btn btn-labeled btn-info"></a></p>--}}
                                            @endif
                                        @endforeach
                                    @else
                                        @if(isset($twitter_details->text))
                                            <?php $length_text = strlen($twitter_details->text); ?>
                                            <div class="user-post"> {{ substr($twitter_details->text,0,30)  }}</div>
                                            <?php $start_day = date("d-m-Y", strtotime($twitter_details->created_at)); ?>
                                            <span class="date">{{substr($start_day,0,19)  }}</span>
                                                <span>Likes:{{$twitter_details->favorite_count}}</span>
                                            @if($length_text > 30)
                                                <a href="#myModal{{$twitter_details->id}}" data-toggle="modal" data-target="#myModal{{$twitter_details->id}}" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> </a>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <!-- Model -->
                            <div class="modal fade" id="myModal{{$twitter_details->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel"></h4>
                                        </div>
                                        <div class="modal-body">
                                            {{$twitter_details->text}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @else
                        <br><br>
                        <a href="{{url('application-setting')}}">
                            <button type="button" class="btn btn-labeled btn-info">Login first to see post here</button>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop