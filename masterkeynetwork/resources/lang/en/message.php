<?php
/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 3/10/2016
 * Time: 10:33 AM
 */
return [
    'welcome' => 'Wellcome to our application',
    'password'=>'Email or Password is not correct',
    'loggedIn'=>'You are LoggedIn...Welcome to MasterKeyNetwork',
    'loggedOut'=>'You are Logged Out',
    'registered'=>'Congratulations! Your account has been registered. You can do login now.',
    'notRegistered'=>'You have not registered for now. Please try again later.',
    'courseDeleted'=>'Course has been Deleted Successfully',
    'courseUpdated'=>'Course has been Updated Successfully',
    'filenotfound'=>'File Not Found',
    'user_not_exist'=>'User Not Exist.',
    'mail_sent'=>'Mail sent to Reset Your password',
    'reset'=>'Your Password Reset Successfully',
    'error_msg'=>'Something Went Wrong!!!!',
    'tooltip_image'=>'You can Upload image file and choose any one feature image',
    'tooltip_document'=>'You Can upload your Course Document file',
    'tooltip_video'=>'You Can Enter Youtube,Vimeo and SoundCloud video URL',
    'invalid_confirmation_code'=>'You have already active your account or your confirmationcode is invalid!!!',
    'verified'=>'You have successfully verified your account',
    'paymentSucess'=>'You are successfully subscribed for the site with payment successfully done.',
    'paymentCancle'=>'Something went wrong Please try again!',
    'paymentTrouble'=>'',
    'userNotActive'=>'User is not active.',
    'planNotSelected'=>"You Don't have Selected Plan,Please Select Plan",

    'landingPage' => [
        'createSuccess' => 'Landing Page created successfully',
        'createError' => 'Landing Page could not be created',
        'updateSuccess' => 'Landing Page updated successfully',
        'updateError' => 'Landing Page could not be updated',
        'deleteSuccess' => 'Landing Page deleted successfully',
        'deleteError' => 'Landing Page could not be delete',
        'noLandingPage' => 'Currently there are no landing page found',
        'noTemplates' => 'Currently there are no templates found',
        'multipleHtmlInZip' => 'There are multiple files found in your zip',
        'noHtmlContentError' => 'Please enter your html code in editor',
        'noHtmlFileError' => 'Only html file is allowed',
    ],
    'confirmDelete' => 'Do you ready want to delete this record?',
    'courseAdded'=>'Your Course Added Successfully.',
    'tooltip_SmtpEmail' => 'Enter valid  E-mail address to send mail',
    'tooltip_SmtPassword' => 'Enter Password for SMTP setting ',
    'tooltip_SmtpPort' => 'Enter valid Port number to send mail',
    'tooltip_SmtpDomain' => 'Enter valid Port number to send mail',
    'tooltip_SmtpUserame' => 'Enter Username for SMTP setting',
    'tooltip_SmtpTitle'=>'Enter Your SMTP Title',

    'tooltip_FtpHostname' => 'Enter Hostname',
    'tooltip_FtpProtocol' => 'Select Protocol',
    'tooltip_FtpPortNumber' => 'Enter Port Number',
    'tooltip_FtpUsername' => 'Enter Username',
    'tooltip_FtpPassword' => 'Enter Password',
    'tooltip_FtpRemoteDirectory' => 'Enter Default Directory',

];