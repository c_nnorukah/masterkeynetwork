<?php

use Illuminate\Database\Seeder;


class EmailTemplateVariableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results = \DB::select('select count(*) as exist_rows from `email_templates_variables` where `variable_tag` = ? and `email_template_id` = ?', array('#CONFIRMATION_LINK#',1));

        if(isset($results) && !empty($results) && $results[0]->exist_rows > 0) {
            \DB::update("update email_templates_variables set email_template_id = '1', `variable_name`='Confirmation link',`variable_name`='#CONFIRMATION_LINK#',`variable_description`='Confirmation link',`type`='0' where `variable_tag` = ? and `email_template_id` = ?",array('#CONFIRMATION_LINK#',1));
        }else{
            \DB::insert("INSERT INTO `email_templates_variables`(`email_template_id`, `variable_name`, `variable_tag`, `variable_description`, `type`) values(1, 'Username', '#USER_NAME#', 'An user name', '0'), (1, 'Email', '#EMAIL#', 'An user email', '0'), (1, 'Plan Name', '#PLAN_NAME#', 'An user plan', '0'),(1, 'Plan amount', '#PLAN_AMOUNT#', 'Amount ', '0'),(1, 'Confirmation link', '#CONFIRMATION_LINK#', 'Confirmation link', '0')");
        }

    }
}
