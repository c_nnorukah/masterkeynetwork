<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplate;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $temp = addslashes('<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Really Simple HTML Email Template</title>
<style>
* {font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;
	font-size: 100%;  line-height: 1.6em;  margin: 0;  padding: 0;}
</style>
</head>
<body bgcolor="#f6f6f6">
<div style="clear: both !important;  display: block !important;  Margin: 0 auto !important;  max-width: 690px !important; padding:0px; font-size: 100%;  line-height: 1.6em;">
<table class="body-wrap" bgcolor="#f6f6f6" style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;  font-size: 100%;
  line-height: 1.6em;  margin: 0;  padding: 20px; ">
  <tr>
    <td></td>
    <td class="container" bgcolor="#FFFFFF" style="padding:20px; border: 1px solid #f0f0f0;">
      <div class="content" style="display: block;  margin: 0 auto;  max-width: 640px;">
      <table>
      <tr>
                        <td style="text-align:center"><img  src="http://masterkeynetwork.alfyopare.com/beta/public/images/master-logo.png" class="center-block" width="240" alt="logo"></td>
                    </tr>
        <tr>
          <td>
            <p style="font-size: 14px;  font-weight: normal;  margin-bottom: 10px; margin-top:0px;">Hi #USER_NAME# ,</p>
            <h1 style="color: #111111;  font-weight: 200;     font-size: 36px;
  line-height: 1.2em;  margin: 20px 0 20px;  background-color:#273B97;  color:#fff;  text-align:center;">Thanks for creating an account with masterkeynetwork.com</h1>
			<p style="font-size: 14px;  font-weight: normal;  margin-bottom: 10px;">You are just one step behind in getting started generating leads. </p>
            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0" style="margin-bottom:10px;">
              <tr>
                <td  style="font-family: Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;   font-size: 14px;text-align: center;  vertical-align: top; ">
                  <a href="#CONFIRMATION_LINK#" style="background-color: #348eda;  border: solid 1px #348eda;  border-radius: 25px;  border-width: 10px 20px;
  display: inline-block;  color: #ffffff;  cursor: pointer;  font-weight: bold;  line-height: 2;  text-decoration: none;">Click Here to Verify Your Account</a>
                </td>
              </tr>
            </table>
            <p style="font-size: 14px;  font-weight: normal;  margin-bottom: 10px;">Thanks, have a lovely day.</p>
          </td>
        </tr>
      </table>
      </div>
    </td>
    <td></td>
  </tr>
</table>
<table class="footer-wrap" style="clear:both; width:100%;">
  <tr>
    <td></td>
    <td class="container">
      <div class="content" style="text-align:center;">
        <table width="100%">
          <tr>
            <td align="center">
              <p style="  color: #666666;  font-size: 12px; text-align:center;">Don\'t like these annoying emails? <a style="color:#999;" href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.
              </p>
            </td>
          </tr>
        </table>
      </div>
    </td>
    <td></td>
  </tr>
</table>
</div>
</body>
</html>');
        $results = \DB::select('select count(*) as exist_rows from `email_templates`  where  `context` = ? and `email_template_id` = ?',array('user_registration',1));

        if (isset($results) && !empty($results) && $results[0]->exist_rows > 0) {
            \DB::update("update `email_templates` set `content`='" . $temp . "' where `context` = ? and `email_template_id` = ?", array('user_registration', 1));
        } else {

            \DB::insert("INSERT INTO `email_templates` VALUES (1,'html','User Registration','" . $temp . "','user_registration',1,'0000-00-00 00:00:00')");

        }
    }
}