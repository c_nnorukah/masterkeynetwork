<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\UserPlan;
use App\Models\Role;
use App\Models\Admin;
use App\Models\sys_module;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);

        // set user subscrtion plans


        //set role
        Role::create([
            'title' => 'Admin',
            'slug'  => 'admin'
        ]);

        //set role
        $roleId = Role::create([
            'title' => 'Member',
            'slug'  => 'member'
        ]);

        // set user

        $userData = User::create([
            'user_name' => 'Alfyopare@gmail.com',
            'first_name' => 'Alfred',
            'last_name' => 'Opare',
            'email' => 'Alfyopare@gmail.com',
            'password' => bcrypt('Mkey2016'),
            'phone_number' => '12345678',
            'status' => 1,
            'country_id' => 223,
            'state_id'   => 1,
            'plan_id'    => 1
        ]);

        \DB::table('assigned_roles')->insert(
            ['user_id' => $userData->user_id, 'role_id' => 2]
        );

        $adminData = Admin::create([
            'user_name' => 'Alfyopare@gmail.com',
            'first_name' => 'Alfred',
            'last_name' => 'Opare',
            'email' => 'Alfyopare@gmail.com',
            'password' => bcrypt('Mkey2016'),
            'status' => 1
        ]);

    }
}
