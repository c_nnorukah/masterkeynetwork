<?php

use Illuminate\Database\Seeder;

class AffiliateVariableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results = \DB::select('select count(*) as exist_rows from `email_templates_variables` where `variable_tag` = ? and `email_template_id` = ?', array('#REF_NAME#',3));

        if(isset($results) && !empty($results) && $results[0]->exist_rows > 0) {
            \DB::update("update email_templates_variables set email_template_id = '3', `variable_name`='Affiliate name',`variable_tag`='#AFF_NAME#',`variable_description`='Affiliate name'',`type`='0' where `variable_tag` = ? and `email_template_id` = ?",array('#AFF_NAME#',3));
        }else{
            \DB::insert("INSERT INTO `email_templates_variables`(`email_template_id`, `variable_name`, `variable_tag`, `variable_description`, `type`) values(3, 'Username', '#USER_NAME#', 'An user name', '0'), (3, 'Email', '#AFF_NAME#', 'Affiliate name', '0'), (3, 'Plan Name', '#PLAN_NAME#', 'An user plan', '0'),(1, 'Plan amount', '#PLAN_AMOUNT#', 'Amount ', '0')");
        }
    }
}
