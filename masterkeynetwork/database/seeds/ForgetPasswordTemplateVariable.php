<?php
/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 4/5/2016
 * Time: 3:31 PM
 */

use Illuminate\Database\Seeder;

class ForgetPasswordTemplateVariable extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
    Add a comment to this line
         */
        public function run()
    {
                \DB::insert("INSERT INTO `email_templates_variables`(`email_template_id`, `variable_name`, `variable_tag`, `variable_description`, `type`) values(2, 'Username', '#USER_NAME#', 'An user name', '0'), (2, 'Email', '#EMAIL#', 'An user email', '0'),(2, 'Token', '#TOKEN#', 'User Token ', '0')");
            }
}