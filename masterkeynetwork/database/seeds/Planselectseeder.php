<?php

use Illuminate\Database\Seeder;
use App\Models\UserPlan;
use App\Models\sys_module;

class Planselectseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::statement("TRUNCATE TABLE user_plans");
    //   DB::statement("TRUNCATE TABLE sys_module");
        UserPlan::create([
            'plan_name' => 'Platinum',
            'price' => '30.00',
            'interval' => 'Month'
        ]);

        UserPlan::create([
            'plan_name' => 'Gold',
            'price' => '20.00',
            'interval' => 'Month'
        ]);

        UserPlan::create([
            'plan_name' => 'Silver',
            'price' => '0.00',
            'interval' => 'Month'
        ]);

        UserPlan::create([
            'plan_name'  => 'Diamond',
            'price' => '10.00',
            'interval' => 'Month'
        ]);

        sys_module::create([
            'module_key' => 'Landing Page Builder',
            'module_name' => 'Diamond'
        ]);

        sys_module::create([
            'module_key' => 'E-mail Integration ',
            'module_name' => 'Silver'
        ]);

        sys_module::create([
            'module_key' => 'Customer Tracking  ',
            'module_name' => 'Gold'
        ]);

        sys_module::create([
            'module_key' =>'Affiliate Manager ',
            'module_name' => 'Platinum'
        ]);

    }
}
