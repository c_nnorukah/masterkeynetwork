<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_blog', function (Blueprint $table) {
            $table->increments('user_blog_id');
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('user_email');
            $table->string('blogname');
            $table->string('blog_title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_blog', function (Blueprint $table) {
            //
        });
    }
}
