<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLevelHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_level_history', function (Blueprint $table) {
            $table->increments('payment_level_history_id');
            $table->string('payment_level_1');
            $table->string('payment_level_2');
            $table->string('payment_level_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smtp_setting');
    }
}
