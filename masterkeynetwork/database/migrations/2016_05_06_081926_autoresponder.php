<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Autoresponder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `auto_responders` DROP COLUMN `from_name`,DROP COLUMN `from_email`,ADD COLUMN `smtp_setting_id` INT(10) NULL AFTER `landingpage_id`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
