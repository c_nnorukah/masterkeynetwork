<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmailCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("DROP TABLE `email_campaign`;");
        \DB::statement("CREATE TABLE `email_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `subject` text,
  `body` text,
  `dynamic_param` text,
  `template_id` int(11) DEFAULT NULL,
  `send_on` datetime DEFAULT NULL,
  `is_sent` enum('0','1') DEFAULT '0',
  `contact_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ");
        \DB::statement("CREATE TABLE `email_campaign_contact`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email_campaign_id` INT(11),
  `list_id` INT(11),
  `contact_id` INT(11),
  `created_at` DATETIME,
  `updated_at` DATETIME,
  PRIMARY KEY (`id`)
);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
