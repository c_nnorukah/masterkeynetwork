<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenceKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("set foreign_key_checks=0;");
        \DB::statement("ALTER TABLE `assigned_roles` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `folders` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `images` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `libraries` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `payment_history` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `throttle` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_academic_positions` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_education_details` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_honors` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_portfolios` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `users_projects` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `users`  CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;");

        \DB::statement("ALTER TABLE assigned_roles ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE folders ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE images ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE libraries ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE payment_history ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE throttle ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE user_academic_positions ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE user_education_details ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE user_honors ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE user_portfolios ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE users_projects ENGINE=InnoDB;");
        \DB::statement("ALTER TABLE users ENGINE=InnoDB;");

        Schema::table('assigned_roles', function (Blueprint $table) {

            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "assigned_roles_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('assigned_roles_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('folders', function (Blueprint $table) {

            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "folders_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('folders_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('images', function (Blueprint $table) {

            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "images_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('images_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('libraries', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "libraries_user_id_foreign"'
                )
            );

            if(!empty($keyExists)) {
                $table->dropForeign('libraries_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('payment_history', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "payment_history_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('payment_history_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('throttle', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "throttle_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('throttle_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_academic_positions', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "user_academic_positions_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('user_academic_positions_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_education_details', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "user_education_details_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('user_education_details_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_honors', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "user_honors_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('user_honors_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_portfolios', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "user_portfolios_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('user_portfolios_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('users_projects', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "users_projects_ibfk_1"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('users_projects_ibfk_1');
            }

            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "users_projects_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('users_projects_user_id_foreign');
            }
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('projects', function (Blueprint $table) {
            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "projects_user_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('projects_user_id_foreign');
            }

            $keyExists = DB::select(
                DB::raw(
                    'SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS   WHERE CONSTRAINT_SCHEMA = DATABASE() AND CONSTRAINT_NAME = "projects_id_foreign"'
                )
            );
            if(!empty($keyExists)) {
                $table->dropForeign('projects_id_foreign');
            }
        });
        \DB::statement("set foreign_key_checks=1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
