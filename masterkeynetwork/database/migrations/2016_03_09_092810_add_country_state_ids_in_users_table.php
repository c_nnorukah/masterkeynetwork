<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryStateIdsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('plan_id')->unsigned();

            $table->foreign('country_id')->references('country_id')->on('country');
            $table->foreign('state_id')->references('state_id')->on('state');
            $table->foreign('plan_id')->references('plan_id')->on('user_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('country_id');
            $table->dropColumn('state_id');
            $table->dropColumn('plan_id');
        });
    }
}
