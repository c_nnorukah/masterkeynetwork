<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("set foreign_key_checks=0;");
        \DB::statement("ALTER TABLE `assigned_roles` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `folders` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `images` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `libraries` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `payment_history` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `throttle` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_academic_positions` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_education_details` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_honors` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `user_portfolios` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `users_projects` CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL;");
        \DB::statement("ALTER TABLE `users`  CHANGE `user_id` `user_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;");
        Schema::table('assigned_roles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('folders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('images', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('libraries', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('payment_history', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('throttle', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_academic_positions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_education_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_honors', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('user_portfolios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('users_projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreign('id')
                ->references('project_id')->on('users_projects')
                ->onDelete('cascade');
        });
        \DB::statement("set foreign_key_checks=1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
