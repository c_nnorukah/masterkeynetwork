<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserEducationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_education_details', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned()->nullable()->default(NULL)->index();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');

            $table->integer('passing_year');
            $table->string('degree');
            $table->string('title');
            $table->string('university');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_education_details', function (Blueprint $table) {
            //
        });
    }
}
