<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserAcademicPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_academic_positions', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned()->nullable()->default(NULL)->index();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');

            $table->integer('from_date');
            $table->integer('to_date');
            $table->string('title');
            $table->string('summary');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_academic_positions', function (Blueprint $table) {
            //
        });
    }
}
