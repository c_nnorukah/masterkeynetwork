<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserHonors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_honors', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned()->nullable()->default(NULL)->index();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');

            $table->date('date');
            $table->string('title');
            $table->string('details');
            $table->integer('image_id')->unsigned()->nullable()->default(NULL)->index();

            $table->foreign('image_id')->references('id')->on('user_medias')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_honors', function (Blueprint $table) {
            //
        });
    }
}
