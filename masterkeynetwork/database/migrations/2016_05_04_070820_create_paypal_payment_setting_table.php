<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalPaymentSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_paypal_setting', function (Blueprint $table) {
            $table->increments('paypal_payment_setting_id');
            $table->string('paypal_email');
            $table->string('payment_currency');
            $table->text('message');
            $table->string('paypal_IPN_URL');
            $table->string('paypal_API_username');
            $table->string('paypal_API_signature');
            $table->string('paypal_API_password', 60);
            $table->string('paypal_API_URL');
            $table->string('payment_level');
            $table->date('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_paypal_setting');
    }
}
