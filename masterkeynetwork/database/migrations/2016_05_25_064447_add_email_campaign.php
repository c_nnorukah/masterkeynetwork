<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("CREATE TABLE `email_campaign`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `to_email` VARCHAR(255),
  `subject` TEXT,
  `body` TEXT,
  `dynamic_param` TEXT,
  `template_id` INT(11),
  `send_on` DATETIME,
  `is_sent` ENUM('0','1') DEFAULT '0',
  `created_at` DATETIME,
  `updated_at` DATETIME,
  PRIMARY KEY (`id`)
);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
