<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminEmailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_email_template', function (Blueprint $table) {
            $table->increments('admin_email_template_id');
            $table->integer('smtp_setting_id');
            $table->string('mail_from');
            $table->string('mail_to');
            $table->string('mail_subject');
            $table->text('mail_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smtp_setting');
    }
}
