<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state', function (Blueprint $table) {
            $table->increments('state_id');
            $table->string('state_name', 32);
            $table->string('state_code', 16);
            $table->integer('country_id')->unsigned()->index();
            $table->foreign('country_id')->references('country_id')->on('country')->onDelete('cascade');
            $table->tinyInteger('zone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('state');
    }
}
