<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFtpDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_ftp_detail', function ($table) {
            $table->timestamp('created_at');
        });
        Schema::table('project_ftp_detail', function ($table) {
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_ftp_detail', function ($table) {
            $table->dropColumn('created_at');
        });
        Schema::table('project_ftp_detail', function ($table) {
            $table->dropColumn('updated_at');
        });
    }
}
