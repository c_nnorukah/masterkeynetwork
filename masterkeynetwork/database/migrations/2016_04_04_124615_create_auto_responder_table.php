<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoResponderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_responders', function (Blueprint $table) {
            $table->increments('auto_reposnders_id');
            $table->integer('landingpage_id')->unsigned();
            $table->foreign('landingpage_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('from_name');
            $table->string('from_email');
            $table->string('subject');
            $table->text('email_body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auto_responders'); //
    }
}
