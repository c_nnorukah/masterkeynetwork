<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPageResponseEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_page_response', function (Blueprint $table) {
            $table->increments('landing_page_response_id');
            $table->integer('landingpage_id')->unsigned();
            $table->foreign('landingpage_id')->references('id')->on('projects')->onDelete('cascade');
            $table->text('response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landing_page_response');
    }
}
