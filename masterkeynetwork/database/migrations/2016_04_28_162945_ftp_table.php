<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("CREATE TABLE `project_ftp_detail` (
  `project_ftp_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `host_name`
varchar(255) DEFAULT NULL,
  `protocol` varchar(255) DEFAULT NULL,
  `port_number` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password`
varchar(255) DEFAULT NULL,
  `remote_directory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_ftp_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        \DB::statement("ALTER TABLE `projects`
  ADD COLUMN `published_at` DATETIME NULL AFTER `updated_at`;");

        \DB::statement("UPDATE projects SET published_at = updated_at;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
