<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('bio');
            $table->string('contact_summary');
            $table->string('skype_id');
            $table->string('twitter_id');
            $table->string('phone_no');
            $table->string('linked_id');
            $table->string('user_cv');
            $table->integer('image_id')->unsigned()->nullable()->default(NULL)->index();

            $table->foreign('image_id')->references('id')->on('user_medias')->onDelete('cascade');
            $table->string('usertoken')->nullable()->default(NULL)->unique();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
