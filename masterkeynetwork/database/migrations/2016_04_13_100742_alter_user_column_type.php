<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `users` CHANGE `bio` `bio` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;");
       \DB::statement("ALTER TABLE `users` CHANGE `contact_summary` `contact_summary` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
