<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates_variables', function(Blueprint $table) {
            $table->integer('email_template_id')->unsigned()->index();
            $table->foreign('email_template_id')->references('email_template_id')->on('email_templates')->onDelete('cascade');
            $table->string('variable_name', 256);
            $table->string('variable_tag', 256);
            $table->string('variable_description', 256);
            $table->enum('type', ['0','1']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_templates_variables');
    }
}
