<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_medias', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('image_name');
            $table->string('image_path');
            $table->enum('type', ['1','2','3','4']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_medias', function (Blueprint $table) {
            //
        });
    }
}
