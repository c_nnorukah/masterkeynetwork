<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactsApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_api', function (Blueprint $table) {
            $table->increments('contacts_api_id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->string('mail_chimp');
            $table->string('get_response');
            $table->string('aweber');
            $table->string('constant_contact');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_api', function (Blueprint $table) {
            //
        });
    }
}
