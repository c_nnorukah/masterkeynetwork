<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPortfolioAddPk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('user_portfolios', 'id')){
            Schema::table('user_portfolios', function ($table) {
                $table->dropColumn(['id']);
            });
        }
        Schema::table('user_portfolios', function ($table) {
            $table->integer("id",11)->increment();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_portfolios', function ($table) {
            $table->dropColumn(['id']);
        });
    }
}
