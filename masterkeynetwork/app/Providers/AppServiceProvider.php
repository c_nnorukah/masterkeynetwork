<?php

namespace App\Providers;

use App\Interfaces\PaymentServiceInterface;
use App\Models\EmailTemplate;
use App\Services\Mailer\UseMailer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['db']->enableQueryLog();
        require_once app_path() . '/helpers.php';
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('useMailer', function ($app) {
            return new UseMailer(new EmailTemplate());
        });

        $this->app->bind('App\Interfaces\PaymentServiceInterface', function () {
            return new \App\Services\PaymentService();
        });
    }


}
