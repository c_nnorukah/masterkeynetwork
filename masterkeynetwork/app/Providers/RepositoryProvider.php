<?php

namespace App\Providers;

use App\Interfaces\ContactRepositoryInterface;
use App\Interfaces\CourseRepositoryInterface;
use App\Interfaces\UploadManagerRepositoryInterface;
use App\Interfaces\ProfileRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\AffiliateRepositoryInterface;
use App\Interfaces\CampaignRepositoryInterface;
use App\Models\Course;
use App\Models\Affiliate;
use App\Models\Campaign;
use App\Models\CourseCategory;
use App\Models\CourseMaterial;
use App\Repositories\ContactRepository;
use App\Repositories\CourseRepository;
use App\Repositories\UploadManagerRepository;
use App\Repositories\ProfileRepository;
use App\Repositories\UserRepository;
use App\Repositories\AffiliateRepository;
use App\Repositories\CampaignRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CourseRepositoryInterface::class, function ($app) {
            return new CourseRepository($app->make(Course::class),
                $app->make(CourseMaterial::class),
                $app->make(CourseCategory::class)
            );
        });
        $this->app->bind(ProfileRepositoryInterface::class, function(){
            return new ProfileRepository();
        });
        $this->app->bind(UserRepositoryInterface::class, function(){
            return new UserRepository();
        });
        $this->app->bind(UploadManagerRepositoryInterface::class, function(){
            return new UploadManagerRepository();
        });
        $this->app->bind(AffiliateRepositoryInterface::class, function(){
            return new AffiliateRepository();
        });
        $this->app->bind(CampaignRepositoryInterface::class, function(){
            return new CampaignRepository();
        });
        $this->app->bind(ContactRepositoryInterface::class, function(){
            return new ContactRepository();
        });

        /*
        $this->app->bind(CourseRepositoryInterface::class, function(){
            return new CourseRepository();
        });*/
    }
}
