<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/2/16
 * Time: 11:30 AM
 */

use App\Models\Course;

interface CourseRepositoryInterface
{
    public function store(Course $course);

    //public function storeMaterial($filename);

}