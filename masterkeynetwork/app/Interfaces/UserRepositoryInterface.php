<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/2/16
 * Time: 11:30 AM
 */


interface UserRepositoryInterface
{
    public function getList($sort_by,$sort_order,$limit,$criteria);
}