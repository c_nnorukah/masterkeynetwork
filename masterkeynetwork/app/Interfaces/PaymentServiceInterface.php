<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/2/16
 * Time: 11:30 AM
 */


interface PaymentServiceInterface
{
    public function userPayment($input_arr);

    public function cancelPayment($token);

    public function success($token);

}