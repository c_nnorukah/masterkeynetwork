<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/2/16
 * Time: 11:30 AM
 */


interface ProfileRepositoryInterface
{
    public function getAllUserData();
    public function saveAboutMe();
    public function savePortfolio();
    public function saveContactInfo();
    public function getUserByToken($id);
    public function saveAcademicPositions();
    public function saveEducation();
    public function saveHonors();
    public function getUserPortfolio();
    public function getPortfolioByID($id);
    public function getUserAcademic();
    public function getAcademicByID($id);
    public function getUserEducation();
    public function getEducationByID($id);
    public function getUserHonor();
    public function getHonorByID($id);

}