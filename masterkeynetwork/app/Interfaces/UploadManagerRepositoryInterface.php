<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/2/16
 * Time: 11:30 AM
 */


interface UploadManagerRepositoryInterface
{
    public function getLandingPages();
    public function getLandingPageDetail($id);
    public function saveSetting($id);

}