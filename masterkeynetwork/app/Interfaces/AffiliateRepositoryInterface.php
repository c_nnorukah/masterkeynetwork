<?php

namespace App\Interfaces;

use App\Models\User;

interface AffiliateRepositoryInterface
{
    public function saveAffiliate(User $user);
}