<?php

namespace App\Interfaces;

use App\Models\User;
use App\Models\Affiliate;
use App\Models\Campaign;

interface CampaignRepositoryInterface
{
    public function saveCampaign(Affiliate $affiliate, User $user);

    public function getdetailByCampaignCode($campaign_code);

    public function getCampaignData($user_id);
}