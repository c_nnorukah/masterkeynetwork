<?php namespace App\Services;

use App\Models\PaymentHistory;
use App\Models\User;
use Illuminate\Support\Facades\App;
use App\Payment;
use Illuminate\Session;

use Auth;

class PaymentService implements \App\Interfaces\PaymentServiceInterface
{
    protected $payment;
    public function __construct()
    {
        $this->payment = new \App\Payment\Payment();

    }

    public function userPayment($input_arr)
    {
            $express_checkout = $this->payment->setExpressCheckout($input_arr);
            $ack = strtoupper($express_checkout["ACK"]);

            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $this->payment->redirectToPayPal($express_checkout["TOKEN"]);
            } else {
                User::whereUserId($input_arr->user_id)->delete();
                $error_message = urldecode($express_checkout["L_SHORTMESSAGE0"]);
                Session::flash('error', $error_message);
                return false;
        }
        exit;
    }
    public function success($token)
    {

        $shipping_detail = $this->payment->getShippingDetails($token);
        $user = User::with(['plan'])->whereUserId($shipping_detail['CUSTOM'])->first();

        $shipping_detail['user_id'] = $shipping_detail['CUSTOM'];
        $shipping_detail['billing_period'] = trim($user->plan->interval);
        $shipping_detail['billing_frequency'] = trim($user->plan->days);
        $shipping_detail['desc'] = $user->plan->plan_name ." Plan Subscription-" . $user->plan->interval ."-". $user->plan->price ."/Month";

        $recurring_profile = $this->payment->createRecurringPaymentsProfile($shipping_detail);

        $res_ack = strtoupper($recurring_profile["ACK"]);
        if ($res_ack == "SUCCESS" || $res_ack == "SUCCESSWITHWARNING") {
            $payment_success = $this->payment->confirmPayment($shipping_detail);

            $user = User::whereUserId($user->user_id)->first();

            $payment_history = new PaymentHistory();
            $payment_history->profile_id = $recurring_profile['PROFILEID'];
            $payment_history->user_id = $user->user_id;
            $payment_history->plan_id = $user->plan_id;
            $payment_history->amount = $user->plan->price;
            //$payment_history->start_date = date("Y-m-d H:i:s");
            $payment_history->transaction_response = json_encode($payment_success);
            $payment_history->save();
            return $user;

        } else {
            return false;
        }
    }

    public function cancelPayment($token)
    {
        $shipping_detail = $this->payment->getShippingDetails($token);
        User::whereUserId($shipping_detail['CUSTOM'])->delete();
        return true;
    }
}