<?php namespace App\Services\Mailer;

interface Mailer
{
    public function SendEMail($mail_params_array, $template, $dynamic_data);
}