<?php namespace App\Services\Mailer\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade class to be called whenever the class Mailer is called
 */

/**
 * Class Access
 * @package App\Services\Access\Facades
 */
class Mailer extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'useMailer';
    }
}