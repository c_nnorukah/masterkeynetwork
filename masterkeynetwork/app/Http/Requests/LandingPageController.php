<?php
namespace App\Http\Controllers\Admin;

use App\Models\LandingPageTemplate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Mail;
use File;
use Illuminate\Support\Facades\Auth;

use App\Models\LandingPage;
use App\Http\Requests\LandingPageRequest;
use App\Repositories\LandingPageRepository;
use App\Http\Requests\InstallLandingPageRequest;
use Redirect;

class LandingPageController extends Controller
{

    protected $landingPage;
    protected $landingPageRepo;

    function __construct(LandingPage $landingPage, LandingPageRepository $repo)
    {
        $this->landingPage = $landingPage;
        $this->landingPageRepo = $repo;
    }

    /*
     * An Landing page dashboard
     */
    public function index()
    {
        $user_id = auth('admin')->user()->admin_id;
        return Redirect::to("http://masterkeynetwork.alfyopare.com/beta/landingpage/architect/?user=$user_id");

        // get all admin templates
        $adminTemplates = $this->landingPageRepo->getLandingPagesTemplates();

        $userLandingPages = $this->landingPageRepo->getUserLandingPagesByUser(auth('admin')->user()->admin_id);

        return view('admin.landing-pages.index', ['landingPage' => $adminTemplates, 'userLandingPages' => $userLandingPages->get()]);
    }

    /*
     * Post create new landing page data
     */
    public function createLandingPage(LandingPageRequest $request)
    {
        $landingPagePost = $request->except('_token');
        $landingPageData = $this->landingPageRepo->store($landingPagePost);

        if(!empty($landingPageData))
        {
            return redirect()->back()->withFlashSuccess(trans('message.landingPage.createSuccess'));
        } else {
            return redirect()->back()->withFlashDanger(trans('message.landingPage.createError'));
        }
    }

    /*
     * An edit landin page form for edit any landing page
     */
    public function getEditLandingPage($id = null)
    {
        if(!empty($id))
        {
            $data = $this->landingPageRepo->getUserLandingPageById($id);

            return view('admin.landing-pages.customize', ['content' => $data->content, 'template_name' => $data->title, 'template_id' => $data->user_landing_page_id]);
        }
    }

    /*
     * Post edit landing page data
     */
    public function editLandingPage(LandingPageRequest $request, $id = null)
    {
        $landingPagePost = $request->except('_token');
        $landingPageData = $this->landingPageRepo->update($landingPagePost, $id);

        if(!empty($landingPageData))
        {
            return redirect()->route('admin-list-landing-page')->withFlashSuccess(trans('message.landingPage.updateSuccess'));
        }
        else {
            return redirect()->back()->withFlashDanger(trans('message.landingPage.updateError'));
        }
    }

    /*
     * Delete Landing Page
     */
    public function deleteLandingPage($id = null)
    {
        if(!empty($id))
        {
            $response = $this->landingPageRepo->deleteUserLandingPageById($id);

            if($response)
            {
                return redirect()->back()->withFlashSuccess(trans('message.landingPage.deleteSuccess'));
            }
            else {
                return redirect()->back()->withFlashDanger(trans('message.landingPage.deleteError'));
            }
        }
    }

    /*
     * List all landing pages created by admin
     */
    public function landingPageList()
    {
        $data = $this->landingPageRepo->getUserLandingPages();
        $data = $data->get();

        return view('admin.landing-pages.list', compact('data'));
    }

    /*
     * get html content from landing page tables
     */
    public function getContentLandingPage($id = null)
    {
        if(!empty($id))
        {
            $data = $this->landingPageRepo->getUserLandingPageById($id);

            echo \Html::image($data->preview_image, 'preview', ['width' => '550']);
        }
    }

    /*
     * use this for search an landing page
     */
    public function searchlandingPage($tag = null)
    {
        if(!empty($tag))
        {
            // check an tag from title
            $data = $this->landingPageRepo->searchUserLandingPages($tag);
            $data = $data->get();

            return view('admin.landing-pages.list', compact('data'));
        }
    }

    /*
     * Use this for install an html page and customize it
     */
    public function installLandingPage()
    {
        return view('admin.landing-pages.install');
    }

    /*
     * An page for customize landing pages
     */
    public function customizeLandingPage(InstallLandingPageRequest $request, $action = null)
    {
        if(!empty($action)) {

            if($action == 'install')
            {
                $file = $request->file('uploadFile');

                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

                $name = $timestamp. '-' .$file->getClientOriginalName();

                $filePath = public_path() . '/admin/images/landing_pages/';

                $file->move($filePath, $name);

                $zip = new \ZipArchive();
                if ($zip->open(str_replace("//", "/", $filePath.''.$name)) === true) {
                    File::makeDirectory($filePath.$timestamp, 0775);
                    $zip->extractTo($filePath.$timestamp);
                    $zip->close();
                }

                // get content from file and delete it
                $getFiles = scandir($filePath.$timestamp);
                if(count($getFiles) <= 3) {
                    $fileData = pathinfo($getFiles[2]);
                    $content = file_get_contents($filePath.$timestamp.'/'.$getFiles[2]);
                    File::deleteDirectory($filePath.$timestamp);
                    File::delete($filePath.$name);
                    if(!isset($fileData['extension']) || $fileData['extension'] != 'html') {
                        return redirect()->back()->withFlashDanger(trans('message.landingPage.noHtmlFileError'));
                    }
                } else {
                    return redirect()->back()->withFlashDanger(trans('message.landingPage.multipleHtmlInZip'));
                }
            }

            \Session::set('htmlContent', base64_encode(urlencode($content)));

            return view('admin.landing-pages.customize', ['content' => $content]);
        }

    }

    public function createCustomizeLandingPage(Request $request)
    {
        // get admin template from
        $templateId = $request->get('template_id');
        $templateName = $request->get('template_name');
        if($templateId!= null) {
            $data = LandingPageTemplate::find($templateId);
            $content = $data->content;
        }
        else {
            $content = $request->get('content');
            if($content=='') {
                return redirect()->back()->withFlashDanger(trans('message.landingPage.noHtmlContentError'));
            }
        }

        \Session::set('htmlContent', base64_encode(urlencode($content)));

        return view('admin.landing-pages.customize', ['content' => $content, 'template_name' => $templateName]);
    }

    public function azexoComposerRequests(Request $request)
    {
        $action = $request->get('action');

        if($action == 'container_save') {

            $Inputs = [];
            $Inputs['title'] = $request->get('temp_name');
            $Inputs['content'] = base64_decode($request->get('shortcode'));
            $Inputs['content'] = urldecode($Inputs['content']);
            $Inputs['created_by'] = auth('admin')->user()->admin_id;

            $userLandingPageId = $request->get('shortcodeId');
            if ($userLandingPageId != '') {
                $landingPage = $this->landingPageRepo->updateUserLandingPage($Inputs, $userLandingPageId);
                $landingPageId = $userLandingPageId;
            } else {
                $landingPage = $this->landingPageRepo->saveUserTemplate($Inputs);
                $landingPageId = $landingPage->user_landing_page_id;
            }

            // Use phantomjs to convert html to image use that image from preview
            $timestamp = time();
            $imagePath = public_path() . '/admin/images/landing_pages/' . $timestamp . '.png';
            exec(env('PHP_PHANTOMJS_SOURCE_PATH') . 'phantomjs --ignore-ssl-errors=true --ssl-protocol=any --web-security=true ' . env('PHP_PHANTOMJS_RESTERIZE_JS_PATH') . 'rasterize.js ' . URL::route('print-dynamic-content-layout', $landingPageId) . ' ' . $imagePath);


            if (!File::exists(public_path() . '/admin/images/landing_pages/' . $timestamp . '.png')) {
                //echo json_encode(['code_id' => $landingPageId, 'msg' => 'Template saved successfully']);
            }

            // update the preview field
            $Inputs = [];
            $Inputs['preview_image'] = 'admin/images/landing_pages/'.$timestamp.'.png';
            $landingPage = $this->landingPageRepo->updateUserLandingPage($Inputs, $landingPageId);


            // save that html
            echo json_encode(['code_id' => $landingPageId, 'msg' => 'Template saved successfully']);
        }

        return;
    }

    public function printLandingPage(Request $request, $id = null)
    {
        if(!empty($id))
        {
            $data = $this->landingPageRepo->getUserLandingPageById($id);

            //dd($data->content);

            return view('admin.landing-pages.print', ['content' => $data->content]);
        }
    }

    public function printAdminLandingPage(Request $request, $id = null)
    {
        if(!empty($id))
        {
            $data = $this->landingPageRepo->getAdminLandingPageById($id);

            return view('admin.landing-pages.print', ['content' => $data->content]);
        }
    }

    public function getCustomizeLandingPage()
    {
        $content = \Session::get('htmlContent');
        if(!empty($content)) {
            $content = base64_decode($content);
            $content = urldecode($content);

            return view('admin.landing-pages.customizeFrame', ['content' => $content]);
        }
    }
}
