<?php namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'user_name'     => 'required|max:255|unique:users',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|min:6',
            'phone_number'  => 'required|max:20',
            'country_id'    => 'required|numeric',

        ];
    }
}
