<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaypalSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paypal_email' => 'required|email',
            'payment_currency' => 'required',
            'message' => 'required',
            'paypal_IPN_URL' => 'required',
            'paypal_API_username' => 'required',
            'paypal_API_signature' => 'required',
            'paypal_API_password' => 'required',
            'paypal_API_URL' => 'required',
            'payment_date' => 'required',
        ];
    }
}
