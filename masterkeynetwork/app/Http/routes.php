<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::get('confirmation', 'ExtendedAuth\ExtendedAuthController@confirmation')->name('confirmation');


Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => ['auth']], function () {

        //Route::get('/', 'Member\DashboardController@index')->name('member-
        Route::get('myblog', 'DashboardController@blog')->name('myblog');

        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::get('dashboard/topnavmenu', 'DashboardController@TopnavMenu')->name('topnavmenu');
        Route::get('dashboard/leftnavmenu', 'DashboardController@LeftnavMenu')->name('leftnavmenu');
        Route::get('dashboard/checkadmin', 'DashboardController@CheckAdmin')->name('checkadmin');
        Route::get('blog-search', 'DashboardController@SearchBlog')->name('blog-search');

        Route::get('user-profile', 'ProfileController@index')->name('user-profile');
        Route::post('user-profile/update-about-me', 'ProfileController@updateAboutMe');
        Route::post('user-profile/update-portfolio', 'ProfileController@updatePortfolio');
        Route::post('user-profile/update-cv', 'ProfileController@update_cv');
        Route::post('user-profile/update-contact-info', 'ProfileController@updateContactInfo');
        Route::post('user-profile/update-education', 'ProfileController@updateEducation');
        Route::post('user-profile/update-honors', 'ProfileController@updateHonors');
        Route::post('user-profile/update-academic_positions', 'ProfileController@updateAcademicPositions');
        Route::get('landing-page/responses', 'LeadCaptureController@responses')->name('lead-capture-responses');
        Route::post('landing-page/responses', 'LeadCaptureController@responses')->name('lead-capture-responses');
        Route::get('landing-page/response-details/{id}', 'LeadCaptureController@response_details')->name('lead-capture-response-details');

        Route::get('admin-setting', 'ApplicationSettingController@adminSetting')->name("admin-settings");
        Route::post('addplan', 'ApplicationSettingController@addplan');
        Route::get('editplan/{id}', 'ApplicationSettingController@editplan');
        Route::post('updateplan/{id}', 'ApplicationSettingController@updateplan');
        Route::get('social-posting','UserController@SocialPosting');
        Route::get('ftp-settings','UploadManagerController@index');
        Route::post('ftp-settings/{id}','UploadManagerController@saveSetting');
        Route::get('ftp-detail/{id}','UploadManagerController@detail');
        Route::get('paypal-test','UserController@paypal');
        Route::get('paypal-payment-settings', 'PaypalPaymentSettingController@index')->name('paypal-payment-settings');
        Route::post('downline-reports','DownlineController@reports');
        Route::get('downline-reports','DownlineController@reports');
        Route::get('downline-members','DownlineController@members');
        Route::post('downline-members','DownlineController@members');
        Route::get('get-downline-member','DownlineController@getMembers');

        Route::post('update-paypal-setting/{id}', 'PaypalPaymentSettingController@updatepaypalsetting');
        Route::post('add-paypal-setting', 'PaypalPaymentSettingController@addpaypalsetting');

        Route::post('update-paypal-level/{id}', 'PaypalPaymentSettingController@updatepaypallevel');
        Route::post('add-paypal-level', 'PaypalPaymentSettingController@addpaypallevel');

        Route::get('smtp-setting', 'SmtpSettingController@index')->name('smtp-setting');
        Route::post('add-smtp', 'SmtpSettingController@add_smtp');
        Route::get('delete-smtp/{id}', 'SmtpSettingController@delete_smtp');
        Route::post('update-smtp/{id}', 'SmtpSettingController@update_smtp');
        Route::post('add-email', 'AdminEmailTemplateController@addemail');
        Route::get('select-template/{id}', 'AdminEmailTemplateController@select_template');
        Route::post('update-email/{id}', 'AdminEmailTemplateController@update_email');
        Route::get('add-email-template', 'AdminEmailTemplateController@index');
        Route::post('delete-image','UserController@deleteImage');

        Route::group(['prefix' => 'course'], function () {
            Route::get('education', 'CourseController@index')->name('education');
            Route::post('save', 'CourseController@store')->name('store-course');
            Route::get('details/{id}', 'CourseController@details')->name('course-details');
            Route::get('show', 'CourseController@show');
            Route::get('add', 'CourseController@add')->name('add-course');
            Route::get('edit/{courseId}', 'CourseController@edit')->name('get-edit-course');
            Route::get('delete-material/{materialId}', 'CourseController@deleteMaterial')->name('delete-material');
            Route::get('delete/{courseId}', 'CourseController@delete')->name('delete-course');

            Route::post('video', 'CourseController@videopreview')->name('video');
            Route::post('check-url', 'CourseController@checkurl')->name('check-url');
            Route::post('get-category', 'CourseController@getcategory');
            Route::resource('download-file', 'CourseController@filedownload');
        });
        /*
        * --------------------------------------------------------------------------
        * Auto responder page routing section
        * --------------------------------------------------------------------------
        */
        Route::get('email-template/{id}', 'AutoResponderController@getEmailTemplate')->name('email-template');

        Route::get('auto-responder-detail/{id}', 'AutoResponderController@getResponderDetail')->name('auto-responder-detail');
        Route::post('save-auto-responder', 'AutoResponderController@saveAutoResponder')->name('save-auto-responder');
        Route::get('auto-responder', 'AutoResponderController@index')->name('auto-responder');


        Route::get('application-setting', 'ApplicationSettingController@index')->name('application-settings');
        Route::get('deleteplan/{id}', 'ApplicationSettingController@destroyplan')->name('deleteplan');

        Route::get('social/{type}', 'ApplicationSettingController@redirectToProviderAll');
        Route::get('social/{type}/callback', 'ApplicationSettingController@handleProviderCallbackAll');
        Route::get('twitter/login', 'ApplicationSettingController@twitterlogin');
        Route::get('twitter.error', function(){
            return redirect::to('application-setting');
        });
        Route::post('post-tweet','ApplicationSettingController@posttweet');
        Route::get('twitter/callback', 'ApplicationSettingController@callback')->name('twitter.callback');
        Route::get('fb/logout', 'ApplicationSettingController@fblogout');
        Route::post('fb/post', 'DashboardController@fbpost');
        Route::get('twitter/logout', 'ApplicationSettingController@twitterlogout');
        Route::match(['get', 'post'], 'user-list', 'UserController@index')->name('user-list');
        Route::match(['get', 'post'], 'user-list/{sort_by}/{sort_order}/{limit}', 'UserController@index')->name('user-list-options');
        Route::get('user-reset', 'UserController@reset')->name('user-reset');
        Route::get('user-detail/{id}', 'UserController@detail')->name('user-detail');
        Route::get('user-delete/{id}', 'UserController@delete')->name('user-delete');
        Route::get('plans', 'UserController@plans')->name('plans');
        Route::get('upgrade-plan/{id}', 'UserController@upgradePlan')->name('upgrade-plan');
        Route::match(['get', 'post'], 'contact-book', 'ContactController@contactBook')->name('contact-book');
        Route::match(['get', 'post'], 'contact-book/{sort_by}/{sort_order}/{limit}', 'ContactController@contactBook')->name('contact-book-options');
        Route::get('contact-book-reset', 'ContactController@reset')->name('contact-book-reset');
        Route::get('contact-delete/{id}', 'ContactController@delete')->name('contact-delete');

    });

    Route::get('login', 'ExtendedAuth\ExtendedAuthController@getLogin')->name('login');
    Route::get('auth/login', 'ExtendedAuth\ExtendedAuthController@getLogin')->name('get-login');
    Route::post('auth/login', 'ExtendedAuth\ExtendedAuthController@postLogin')->name('post-login');
    Route::get('auth/register', 'ExtendedAuth\ExtendedAuthController@getRegister')->name('get.register');
    Route::post('post-register', 'ExtendedAuth\ExtendedAuthController@postRegister');
    Route::get('reset-password', ['as' => 'auth.password.reset.request', 'uses' => 'ExtendedAuth\ExtendedPasswordController@getEmail']);
    Route::post('reset-password', ['as' => 'auth.password.reset.submit', 'uses' => 'ExtendedAuth\ExtendedPasswordController@postEmail']);
    Route::get('reset/{token}', ['as' => 'auth.password.reset', 'uses' => 'ExtendedAuth\ExtendedPasswordController@getReset']);
    Route::post('reset', ['as' => 'auth.reset.password', 'uses' => 'ExtendedAuth\ExtendedPasswordController@postReset']);
    Route::get('auth/logout', 'ExtendedAuth\ExtendedAuthController@getLogout')->name('logout');
    Route::get('ajax/get_state_list/{country_id}', 'ExtendedAuth\ExtendedAuthController@getStateList');
    Route::post('checkusername', 'ExtendedAuth\ExtendedAuthController@checkusername');
    Route::post('checkemail', 'ExtendedAuth\ExtendedAuthController@checkemail');
    Route::get('get-dynamic-layout/{id}', 'Admin\LandingPageController@printLandingPage')->name('print-dynamic-content-layout');
    Route::get('get-admin-landing-page/{id}', 'Admin\LandingPageController@printAdminLandingPage')->name('print-dynamic-content-layout1');

    Route::get('referral/{referral_code}',[
        'as'=>'referral',
        'uses' =>  'AffiliateController@invite'
    ]);
   // Route::get('referral', 'DashboardController@blog')->name('myblog');

    Route::get('register/verify/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'ExtendedAuth\ExtendedAuthController@confirm'
    ]);

    Route::get('success', 'ExtendedAuth\ExtendedAuthController@paymentSucess');
    Route::get('paypal/cancel', 'ExtendedAuth\ExtendedAuthController@paymentCancel');

    /*
 * --------------------------------------------------------------------------
 * Admin landing page routing section
 * --------------------------------------------------------------------------
 */
    Route::group(['prefix' => 'landing-page'], function () {
        Route::get('/', 'LandingPageController@index')->name('admin-landing-pages');
        Route::post('create', 'LandingPageController@createLandingPage')->name('admin-create-landing-page');
        Route::get('edit/{id}', 'LandingPageController@getEditLandingPage')->name('admin-get-edit-landing-page');
        Route::post('edit/{id}', 'LandingPageController@editLandingPage')->name('admin-edit-landing-page');
        Route::get('delete/{id}', 'LandingPageController@deleteLandingPage')->name('admin-delete-landing-page');
        Route::get('list', 'LandingPageController@landingPageList')->name('admin-list-landing-page');
        Route::get('get-content/{id}', 'LandingPageController@getContentLandingPage')->name('admin-get-landing-page-content');
        Route::get('search/{tag}', 'LandingPageController@searchlandingPage')->name('admin-search-landing-page');
        Route::get('install', 'LandingPageController@installLandingPage')->name('admin-install-landing-page');
        Route::post('customize/{tag}', 'LandingPageController@customizeLandingPage')->name('admin-customize-landing-page');
        Route::post('create-customize', 'LandingPageController@createCustomizeLandingPage')->name('admin-create-customize-landing-page');
        Route::get('get-customize', 'LandingPageController@getCustomizeLandingPage')->name('admin-get-customize-landing-page');

    });
// Use to handle all Azexo composer html builder requests
    Route::get('portfolio/{id}', 'ProfileController@getPortfolio');
    Route::get('academic/{id}', 'ProfileController@getAcademic');
    Route::get('education/{id}', 'ProfileController@getEducation');
    Route::get('award/{id}', 'ProfileController@getAward');
    Route::get('portfolio/delete/{id}', 'ProfileController@deletePortfolio');
    Route::get('academic/delete/{id}', 'ProfileController@deleteAcademic');
    Route::get('education/delete/{id}', 'ProfileController@deleteEducation');
    Route::get('award/delete/{id}', 'ProfileController@deleteAward');

    Route::get('contacts', 'ContactController@index');
    Route::get('contacts/create', 'ContactController@create');
    Route::post('contacts/importExcel', 'ContactController@importExcel');
    Route::post('contacts/importMailchimp', 'ContactController@importMailchimp');
    Route::post('contacts/importgetResponse', 'ContactController@importgetResponse');
    Route::post('contacts/importConstantContact', 'ContactController@importConstantContact');
    Route::get('contacts/importConstantContact', 'ContactController@importConstantContact');
    Route::get('contacts/addConstantContact', 'ContactController@addConstantContact');
    Route::post('contacts/storecp', 'ContactController@storecp');
    Route::post('contacts/importAweber', 'ContactController@importAweber');
    Route::get('contacts/add-aweber-contact', 'ContactController@addAweberContact');
    Route::get('contact-list', 'ContactListController@index');
    Route::post('contact-list', 'ContactListController@saveContactList');
    Route::get('email-campaign', 'EmailCampaignController@emailCampaign');
    Route::get('email-campaign/{id}', 'EmailCampaignController@emailCampaign');
    Route::get('email-campaign-delete/{id}', 'EmailCampaignController@delete');
    Route::post('send-campaign-mail', 'EmailCampaignController@sendEmails');
    Route::get('email-campaign-list', 'EmailCampaignController@index');
    Route::match(['get', 'post'], 'email-campaign-list/{sort_by}/{sort_order}/{limit}', 'EmailCampaignController@index')->name('email-campaign-list-options');
});
Route::get('user-profile/view/{id}', 'ProfileController@viewProfile')->name('view-user-profile');
Route::post('landingpage-data', 'LeadCaptureController@LandingpageData');
Route::get('update-plan', 'CronController@updatePlan');
Route::get('referral-payment', 'CronController@referralPayment');