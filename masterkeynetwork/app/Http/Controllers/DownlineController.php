<?php

namespace App\Http\Controllers;

use App\Models\ReferralPaymentHistory;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class DownlineController extends Controller
{
    protected $start_date = "";
    protected $end_date = "";

    public function reports()
    {
        $first_level = $second_level = $third_level = [];
        $user_id = Auth::user()->user_id;
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val == '1') {
                    $user_id = 0;
                }
            }
        }
        $today = date("m/d/Y");
        $user_id = Input::get("user_list") ? Input::get("user_list") : $user_id;
        $this->start_date = $start_date = Input::get("start_date") ? Input::get("start_date") : date('m/d/Y', strtotime("-30 days"));
        $this->end_date = $end_date = Input::get("end_date") ? Input::get("end_date") : $today;
        $this->start_date .= " 00:00:00";
        $this->end_date .= " 23:59:59";
        $users = User::with("assignedRoles")->where('referral_id', null);
        if ($start_date != "") {
            $users->where("created_at", ">=", format_date($start_date,"Y-m-d H:i:s"));
        }
        if ($end_date != "") {
            $users->where("created_at", "<=", format_date($end_date,"Y-m-d H:i:s"));
        }
        $users = $users->orderBy("first_name")->get();
        $user_list = User::with("assignedRoles")->get();
        $this->getReferrals($users, $first_level, $second_level, $third_level, $user_id, true);
        return view('downline.downline-reports', compact("first_level", "second_level", "third_level", "users", "user_id", "start_date", "end_date", "user_list"));
    }

    public function members()
    {
        $first_level = $second_level = $third_level = [];
        $user_id = Auth::user()->user_id;
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val == '1') {
                    $user_id = 0;
                }
            }
        }
        $user_id = Input::get("user_list") ? Input::get("user_list") : $user_id;
        $users = User::with("assignedRoles")->where('referral_id', null);
        $users = $users->orderBy("first_name")->get();
        $this->getReferrals($users, $first_level, $second_level, $third_level, $user_id, false);
        return view('downline.downline-members', compact("first_level", "second_level", "third_level", "users", "user_id"));
    }
    public function getMembers()
    {
        $first_level = $second_level = $third_level = [];
        $user_id = Auth::user()->user_id;
        $users = User::with("assignedRoles")->where('referral_id', null);
        $users = $users->orderBy("first_name")->get();
        $this->getReferrals($users, $first_level, $second_level, $third_level, $user_id, false,false);
        return view('dashboard.includes.chat', compact("first_level", "second_level", "third_level", "users", "user_id"));
    }

    function array_map_recursive($array, $user_id, &$referral_array)
    {
        foreach ($array as $key => $value) {
            if ($value["user_id"] == $user_id) {
                $referral_array = $value;
            }
            if (isset($value["subChild"]) && is_array($value["subChild"])) {
                $array[$key] = $this->array_map_recursive($value["subChild"], $user_id, $referral_array);
            }
        }
        return $array;
    }

    public function getReferrals(&$referrals, &$first_level, &$second_level, &$third_level, $user_id, $get_sale,$all_level = true)
    {
        $referrals = $this->addRelation($referrals);
        $referral_array = [];
        $this->array_map_recursive($referrals, $user_id, $referral_array);
        $level1 = $level2 = [];
        $referral_array = array($referral_array);
        if (!empty($referral_array)) {
            foreach ($referral_array as $k => $root) {
                if (!empty($root["subChild"])) {
                    foreach ($root["subChild"] as $k1 => $root1) {
                        $text = $root1['first_name'] . " " . $root1['last_name'];
                        $level1[$k1] = $first_level[$k1] = array("text" => $text);
                        if ($get_sale && $all_level) {
                            $sale = $this->getFirstLevelSale($user_id, $root1['user_id']);
                            $first_level[$k1]["tags"] = [format_price($sale)];
                        }
                        if (!empty($root1["subChild"])) {
                            foreach ($root1["subChild"] as $k2 => $root2) {
                                $text = $root2['first_name'] . " " . $root2['last_name'];
                                if (!isset($second_level[$k1])) {
                                    $second_level[$k1] = $level1[$k1];
                                }
                                $second_level[$k1]["nodes"][$k2] = $level1[$k1]["nodes"][$k2] = array("text" => $text);

                                if ($get_sale && $all_level) {
                                    $sale = $this->getSecondLevelSale($user_id, $root2['user_id']);
                                    $second_level[$k1]["nodes"][$k2]["tags"] = [format_price($sale)];
                                }
                                if (!empty($root2["subChild"])) {
                                    foreach ($root2["subChild"] as $k3 => $root3) {
                                        $text = $root3['first_name'] . " " . $root3['last_name'];
                                        if (!isset($third_level[$k1]["nodes"][$k2])) {
                                            $third_level[$k1] = $level1[$k1];
                                        }
                                        if(!isset($third_level[$k1]["nodes"][$k2]["nodes"])){
                                            $third_level[$k1]["nodes"][$k2]["nodes"] = [];
                                        }
                                        $third_level[$k1]["nodes"][$k2]["nodes"][$k3] = array("text" => $text);
                                        if ($get_sale) {
                                            $sale = $this->getThirdLevelSale($user_id, $root3['user_id']);
                                            $third_level[$k1]["nodes"][$k2]["nodes"][$k3]["tags"] = [format_price($sale)];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!$all_level && !empty($third_level)){
            $third_level = array("text"=>$referral_array[0]['first_name']." ".$referral_array[0]['last_name'],"nodes"=>$third_level);
        }
        $first_level = !isset($first_level[0]) && !empty($first_level) ? array($first_level[array_keys($first_level)[0]]) : $first_level;
        $second_level = !isset($second_level[0]) && !empty($second_level) ? array($second_level[array_keys($second_level)[0]]) : $second_level;
        $third_level = !isset($third_level[0]) && !empty($third_level) ? array($third_level) : $third_level;

        $first_level = json_encode($first_level);
        $second_level = json_encode($second_level);
        $third_level = json_encode($third_level);
    }

    protected function getFirstLevelSale($user_id, $downline_user)
    {
        $data = ReferralPaymentHistory::where("level1_userid", $user_id)
            ->where("user_id", $downline_user)
            ->where("created_at", ">=", format_date($this->start_date,"Y-m-d H:i:s"))
            ->where("created_at", "<=", format_date($this->end_date,"Y-m-d H:i:s"))
            ->select(DB::raw("sum(level1_payment) as level1_payment"))->first();
        return !empty($data->level1_payment) ? $data->level1_payment : 0;
    }

    protected function getSecondLevelSale($user_id, $downline_user)
    {
        $data = ReferralPaymentHistory::where("level2_userid", $user_id)
            ->where("user_id", $downline_user)
            ->where("created_at", ">=", format_date($this->start_date,"Y-m-d H:i:s"))
            ->where("created_at", "<=", format_date($this->end_date,"Y-m-d H:i:s"))
            ->select(DB::raw("sum(level2_payment) as level2_payment"))->first();
        return !empty($data->level2_payment) ? $data->level2_payment : 0;
    }

    protected function getThirdLevelSale($user_id, $downline_user)
    {
        $data = ReferralPaymentHistory::where("level3_userid", $user_id)
            ->where("user_id", $downline_user)
            ->where("created_at", ">=", format_date($this->start_date,"Y-m-d H:i:s"))
            ->where("created_at", "<=", format_date($this->end_date,"Y-m-d H:i:s"))
            ->select(DB::raw("sum(level3_payment) as level3_payment"))->first();
        return !empty($data->level3_payment) ? $data->level3_payment : 0;
    }

    protected function selectChild($id)
    {
        $referrals = User::where('referral_id', $id)->where("status",1)->get();
        $referrals = $this->addRelation($referrals);
        return $referrals;
    }

    protected function addRelation($referrals)
    {
        $referrals->map(function ($item, $key) {
            $sub = $this->selectChild($item->user_id);
            return $item = array_add($item, 'subChild', $sub->toArray());
        });
        return $referrals;
    }
}
