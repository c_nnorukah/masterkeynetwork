<?php

namespace App\Http\Controllers;

use App\Models\AutoResponder;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Landing_page_response;
use App\Models\User_project;
use App\Models\LandingPageResponse;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Mail;
//use App\Models\Admin;
use App\Models\Project;

class LeadCaptureController extends Controller
{
    public function responses(\Illuminate\Http\Request $request)
    {
        $user_id = Auth::user()->user_id;
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val == '1') {
                    $user_id = 0;
                }
            }
        }
        $this->start_date = $start_date = date('m/01/Y');
        $this->end_date = $end_date = date("m/d/Y");
         if (Input::get("search")) {
            $user_id = Input::get("user_list");
            $this->start_date = $start_date = Input::get("start_date");
            $this->end_date = $end_date = Input::get("end_date");
            $search_array = ["user_id" => $user_id, "start_date" => $this->start_date, "end_date" => $this->end_date];
            session(["lead_search_data" => json_encode($search_array)]);
        } else if ($request->session()->has('lead_search_data')) {
            $post_data = json_decode(session("lead_search_data"), true);
            $user_id = $post_data["user_id"];
            $this->start_date = $start_date = $post_data["start_date"];
            $this->end_date = $end_date = $post_data["end_date"];
        }
        $this->start_date .= " 00:00:00";
        $this->end_date .= " 23:59:59";
        $user_list = User::with("assignedRoles")->orderBy("first_name")->get();
        $projects = User_project::with("projects");
        if (!empty($user_id)) {
            $projects = $projects->where('user_id', $user_id);
        }
        if ($start_date != "" || $end_date != "") {
            $projects->whereHas('projects', function ($q) use ($start_date, $end_date) {
                if ($start_date != "")
                    $q->where("created_at", ">=", format_date($this->start_date, "Y-m-d H:i:s"));

                if ($end_date != "")
                    $q->where("created_at", "<=", format_date($this->end_date, "Y-m-d H:i:s"));
            });
        }
        $projects = $projects->paginate(10);
        //   dd(\DB::getQueryLog());
        $response = array();
        if (!$projects->isEmpty()) {
            foreach ($projects as $key => $val) {
                $response[$val['projects']['id']] = Landing_page_response::where("landingpage_id", $val['projects']['id'])->get();
            }
        }
        return view('landing-page-response', compact('projects', 'response', 'user_list', "user_id", "start_date", "end_date"));
    }

    public function response_details($id)
    {
        $data = Landing_page_response::with("projects")->where("landingpage_id", $id)->simplePaginate(10);
        return view("response_details")->with('data', $data);
    }

    public function LandingpageData(Request $request)
    {
        $_REQUEST['SERVER_ADDR'] = $_SERVER['SERVER_ADDR'];
        $_REQUEST['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
        $landingpageResponse = new LandingPageResponse();
        if (!empty($request->landing_page_id)) {
            $landingpageResponse->landingpage_id = $request->landing_page_id;
        }
        $landingpageResponse->response = json_encode($_REQUEST);
        $landingpageResponse->save();

        $responder = AutoResponder::with('smtp_setting')->where('landingpage_id', $request->landing_page_id)->first()->toArray();
//print_r($responder);
        $SmtpHostname = $responder['smtp_setting']['domain'];
        $SmtpPortNumber = $responder['smtp_setting']['port'];
        $EmailLogin = $responder['smtp_setting']['email_for_smtp'];
        $EmailPWD = $responder['smtp_setting']['password'];
        $contactName = $responder['smtp_setting']['user_name'];
        \Config::set('mail.host', $SmtpHostname);
        \Config::set('mail.port', $SmtpPortNumber);
        \Config::set('mail.username', $EmailLogin);
        \Config::set('mail.password', $EmailPWD);
//        \Config::set('mail.from', ['address' =>$EmailLogin , 'name' => $EmailLogin]);
        if ($SmtpHostname == "smtp.gmail.com") {
            \Config::set('mail.encryption', 'ssl');
        }
        /// print_r(Config('mail'));
        $data = array('name' => $contactName, 'email' => $EmailLogin, 'content' => $responder['email_body']);
        \Mail::send('emails.email', $data, function ($m) use ($contactName, $EmailLogin, $responder) {
            $m->from($EmailLogin, $contactName);
            $m->to($_REQUEST['email'], $_REQUEST['email'])->subject($responder['subject']);
        });

        return json_encode('You have been successfully subscribe to us.');
    }

}