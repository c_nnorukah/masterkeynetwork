<?php

namespace App\Http\Controllers;

use App\Interfaces\UserRepositoryInterface;
use App\Http\Requests;
use App\Models\PaymentHistory;
use App\Models\Role;
use App\Models\User;
use App\Models\UserPlan;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use App\Models\UserSocialMedia;
use Auth;
use Facebook\Facebook;
use PayPal\Api\Agreement;
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\Payer;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;
use PayPal\Common\PayPalModel;
use Twitter;

class UserController extends Controller
{
    protected $user_repository;
    protected $payment;
    protected $fb;

    public function __construct(UserRepositoryInterface $user_repository)
    {
        $this->user_repository = $user_repository;
        $this->payment = new \App\Payment\Payment();
        $this->fb = new Facebook(array(
            'appId' => '1064859340253171', // my app id
            'secret' => 'eaf43f7e529a5cf01f6eb8bbb80bbb68', // my secret i
            'fileUpload' => true // this for file upload
        ));
    }

    public function index(\Illuminate\Http\Request $request)
    {
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val != '1') {
                    return redirect("error");
                }
            }
        }
        $sort_by = !empty(Request::segment(2)) ? Request::segment(2) : "user_id";
        $sort_by = str_replace("-","_",$sort_by);
        $sort_order = !empty(Request::segment(3)) ? Request::segment(3) : "desc";
        $need_to_sort = $sort_order == "desc" ? "asc" : "desc";
        $limit = !empty(Request::segment(4)) ? Request::segment(4) : 15;
        $post_data = Input::get();
        if (!isset($post_data["search_criteria"]) && $request->session()->has('user_search_data')) {
            $post_data = json_decode(session("user_search_data"), true);

        }
        if (isset($post_data["search_criteria"])) {
            session(["user_search_data" => json_encode($post_data)]);
        }
        $selected_criteria = isset($post_data["search_criteria"]) ? $post_data["search_criteria"] : "";

        $user_data = $this->user_repository->getList($sort_by, $sort_order, $limit, $post_data);
        $user_data->setPath(URL("user-list/$sort_by/$sort_order/$limit"));
        $role = Role::get();
        $plan = UserPlan::get();
        return view("user.index")->with(compact('user_data', 'sort_by', 'sort_order', 'limit', 'need_to_sort', 'role', 'plan', 'selected_criteria', 'post_data'));
    }

    public function reset(\Illuminate\Http\Request $request)
    {
        $request->session()->forget('user_search_data');
        return $this->index($request);
    }

    public function delete($id, \Illuminate\Http\Request $request)
    {
        $this->user_repository->delete($id);
    }

    public function detail($id)
    {
        $limit = 15;
        $user_data = $this->user_repository->getDetailById($id, $limit);
        $min_date = $this->user_repository->getMinTransactionDate($id);
        $next_date = $this->user_repository->getMaxTransactionDate($id);
        if (!empty($next_date)) {
            $next_date = date_create($next_date);
            if (!empty($user_data[0]->plan)) {
                date_add($next_date, date_interval_create_from_date_string($user_data[0]->plan->days . " " . $user_data[0]->plan->interval));
            }
        }
        $transaction_history = $this->user_repository->getTransactionHistory($id, $limit);
        if (!empty($user_data)) {
            return view("user.detail", compact("user_data", "limit", "transaction_history", "min_date", "next_date"));
        } else {
            redirect("error");
        }

    }

    public function plans()
    {
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val != '2') {
                    return redirect("error");
                }
            }
        }
        $id = Auth::user()->user_id;
        $user_plan_id = Auth::user()->plan_id;
        $user_data = $this->user_repository->getDetailById($id);
        $min_date = $this->user_repository->getMinTransactionDate($id);
        $next_date = $this->user_repository->getMaxTransactionDate($id);
        $today = date("Y-m-d H:i:s");
        $upgraded_plan = PaymentHistory::where("user_id", "=", $id)->where("plan_id", "<>", $user_plan_id)->where("start_date", ">", $today)->with("plan")->get();
        if (!empty($next_date)) {
            $next_date = date_create($next_date);
            if (!empty($user_data[0]->plan)) {
                date_add($next_date, date_interval_create_from_date_string($user_data[0]->plan->days . " " . $user_data[0]->plan->interval));
            }
        }

        $user_plan = UserPlan::get();
        return view('user.plan', compact("user_data", "limit", "transaction_history", "min_date", "next_date", 'user_plan', 'upgraded_plan'));
    }

    public function upgradePlan($id)
    {
        $plan_data = UserPlan::find($id);
        $user_id = Auth::user()->user_id;
        $profile_id = PaymentHistory::where("user_id", $user_id)->orderBy("created_at")->first();
        if (!isset($profile_id->profile_id)) {
            Session::flash('flash_danger', 'Profile is not generated for this user.');
            return redirect("plans");
        }

        $profile_id = $profile_id->profile_id;
//        dd( $this->payment->getRecurringPaymentsProfileDetails($profile_id));
        $shipping_detail = [];
        $shipping_detail['profileid'] = trim($profile_id);
        $shipping_detail['billing_period'] = trim($plan_data->interval);
        $shipping_detail['billing_frequency'] = trim($plan_data->days);
        $shipping_detail['amt'] = 1000;
        $shipping_detail['desc'] = "Recurring Payment($" . $plan_data->days . $plan_data->interval . ")";
        $recurring_profile = $this->payment->updateRecurringPaymentsProfile($shipping_detail);
        $res_ack = strtoupper($recurring_profile["ACK"]);
        if ($res_ack == "SUCCESS" || $res_ack == "SUCCESSWITHWARNING") {
            $user_id = Auth::user()->user_id;
            $user_plan_id = Auth::user()->plan_id;
            $today = date("Y-m-d H:i:s");
            $next_date = $this->user_repository->getMaxTransactionDate($user_id);
            $current_plan_data = UserPlan::find($user_plan_id);
            if (!empty($next_date)) {
                $next_date = date_create($next_date);
                if (!empty($plan_data)) {
                    date_add($next_date, date_interval_create_from_date_string($current_plan_data->days . " " . $current_plan_data->interval));
                }
            } else {
                $next_date = date("Y-m-d H:i:s");
            }
            //     \DB::enableQueryLog();
            PaymentHistory::where("user_id", "=", $user_id)->where("plan_id", "<>", $user_plan_id)->where("start_date", ">", $today)->delete();
            //    dd(\DB::getQueryLog());
            $payment_history = new PaymentHistory();
            $payment_history->profile_id = $recurring_profile['PROFILEID'];
            $payment_history->user_id = $user_id;
            $payment_history->plan_id = $plan_data->plan_id;
            $payment_history->amount = $plan_data->price;
            $payment_history->transaction_response = json_encode($recurring_profile);
            $payment_history->start_date = $next_date;
            $payment_history->save();
            Session::flash('flash_success', 'Your plan is changed successfully. New plan will be activated after your current plan expire.');
        } else {
            $msg = $recurring_profile["L_LONGMESSAGE0"] ? $recurring_profile["L_LONGMESSAGE0"] : 'Something went wrong.Please try again later.';
            Session::flash('flash_danger', $msg);
        }
        return redirect("plans");
    }

    public function SocialPosting()
    {
        $twitter_accesstoken = UserSocialMedia::select('access_token')->where('type_id', '=', '')->where('user_id', Auth::user()->user_id)->first();
        $type_id = UserSocialMedia::select('type_id')->where('type_id', '!=', '')->where('user_id', Auth::user()->user_id)->first();
        $facebook_accesstoken = UserSocialMedia::select('access_token')->where('type_id', $type_id['type_id'])->first();

        if (isset($facebook_accesstoken['access_token'])) {

            try {
               // $dp = $this->fb->get('/me?field=ProfilePicture,likes,comments',$facebook_accesstoken['access_token']);
                $post = $this->fb->get('/me/posts?fields=picture,message,created_time,likes,comments', $facebook_accesstoken['access_token'])->getGraphEdge()->asArray();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $data['fbpost'] = $post;
            //$data['fbpics'] = $photos;
        }
        if (isset($twitter_accesstoken['access_token'])) {
            $data['tweet'] = json_decode(Twitter::getHomeTimeline(['format' => 'json']));
        }
        return view('socialPosting', compact('data'));
    }

    function deleteImage()
    {
        $image = Input::get("image");
        unlink($image);
    }
}
