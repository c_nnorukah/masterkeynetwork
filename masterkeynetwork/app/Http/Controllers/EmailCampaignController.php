<?php

namespace App\Http\Controllers;


use App\Interfaces\ContactRepositoryInterface;
use App\Models\Email_campaign;
use App\Models\Email_campaign_contact;
use App\Models\EmailTemplate;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Input;
use Excel;
use Auth;
use Session;
use Validator;
use App\Models\Contacts;

class EmailCampaignController extends Controller
{
    protected $contact_repository;

    public function __construct(ContactRepositoryInterface $contact_repository)
    {
        $this->contact_repository = $contact_repository;
    }

    public function index()
    {
        $sort_by = !empty(\Illuminate\Support\Facades\Request::segment(2)) ? \Illuminate\Support\Facades\Request::segment(2) : "id";
        $sort_by = $sort_by == "template" ? "email_templates.subject" : $sort_by;
        $sort_by = str_replace("-", "_", $sort_by);
        $sort_order = !empty(\Illuminate\Support\Facades\Request::segment(3)) ? \Illuminate\Support\Facades\Request::segment(3) : "desc";
        $need_to_sort = $sort_order == "desc" ? "asc" : "desc";
        $limit = !empty(\Illuminate\Support\Facades\Request::segment(4)) ? \Illuminate\Support\Facades\Request::segment(4) : 15;
        $user_id = Auth::user()->user_id;
        $email_campaign = Email_campaign::query();
        $email_campaign = $email_campaign->select("email_campaign.*");
        $email_campaign->leftjoin("email_templates", function ($join) {
            $join->on("email_templates.email_template_id", "=", "email_campaign.template_id");
        });
        $email_campaign = $email_campaign->where("user_id", $user_id)->where("is_sent", "0")->with("template")->orderBy($sort_by, $sort_order)->paginate($limit);
        $email_campaign->setPath(URL("email-campaign-list/$sort_by/$sort_order/$limit"));
        session(["email_campaign_operation"=>json_encode(["sort_by"=>$sort_by,"sort_order"=>$sort_order,"limit"=>$limit,"current_page"=>$email_campaign->currentPage()])]);
        return view("contacts.email-campaign-list")->with(compact('email_campaign', 'user_id', 'sort_by', 'sort_order', 'limit', 'need_to_sort'));

    }

    public function emailCampaign($id = 0)
    {
        $user_id = Auth::user()->user_id;
        $landingpage_list = $this->contact_repository->getLandingpageList();
        $list_ids = array_unique(array_keys($landingpage_list));
        $contact_list = $this->contact_repository->getContactList($list_ids);
        $EmailTemplate = EmailTemplate::where('created_by', $user_id)->orderBy("subject")->get();
        if (!empty($id)) {
            $emailCampaign = Email_campaign::find($id);
            if ($emailCampaign->contact_type == "list") {
                $list_array = Email_campaign_contact::where("email_campaign_id", $emailCampaign->id)->select(DB::raw("group_concat(list_id) as list_id"))->first();
                $list_array = explode(",",$list_array->list_id);
           } else {
                $contact = Email_campaign_contact::where("email_campaign_id", $emailCampaign->id)->select(DB::raw("group_concat(contact_id) as contact_id"))->first();
                $contact = explode(",",$contact->contact_id);
            }
        }
        return view("contacts.email-campaign", compact("landingpage_list", "list_ids", "contact_list", "EmailTemplate", "emailCampaign","list_array","contact"));
    }

    public function sendEmails(\Illuminate\Http\Request $request)
    {
        $mail_params = [];
        $body = Input::get("email_body");
        $id = Input::get("email_campaign_id");
        $body .= '<img src="http://www.google-analytics.com/collect?v=1&tid=' . env('ANALYTICS_TRACKING_ID') . '&cid=' . rand() . '&t=event&ec=email&ea=open&el=' . rand() . '&cs=newsletter&cm=email&cn=062413&cm1=1"/>';
        $send_on = Input::get("send_on");
        $contact_type = Input::get("contact_type");
        $insert = Email_campaign::firstOrNew(array("id" =>$id));
        $insert->subject = $mail_params['subject'] = Input::get("subject");
        $insert->body = $body;
        $insert->template_id = Input::get("email_template_id");
        $insert->send_on = ($send_on == "now") ? null : format_date(Input::get("send_date"), "Y-m-d H:i:s");
        $insert->is_sent = '0';
        $insert->user_id = Auth::user()->user_id;
        $insert->contact_type = $contact_type;
        $insert->save();
        $campaignId = $insert->id;
        if(!empty($id)){
            Email_campaign_contact::where("email_campaign_id",$id)->delete();
        }
        if ($contact_type == "list") {
            $list_ids = Input::get("list");
            foreach ($list_ids as $list) {
                $contact = new Email_campaign_contact();
                $contact->email_campaign_id = $campaignId;
                $contact->list_id = $list;
                $contact->contact_id = null;
                $contact->save();
            }
        } else if ($contact_type == "individual") {
            $contacts = Input::get("contacts");
            foreach ($contacts as $val) {
                $contact = new Email_campaign_contact();
                $contact->email_campaign_id = $campaignId;
                $contact->list_id = null;
                $contact->contact_id = $val;
                $contact->save();
            }
        }
        //send mail
        if($send_on == "now"){
            $this->contact_repository->sendEmail($campaignId);
        }
        if(!empty($id)){
            Session::flash('flash_success', "Email campaign updated successfully.");
            $url = "email-campaign-list";
            if($request->session()->has('email_campaign_operation')){
                $data = json_decode(session("email_campaign_operation"),true);
                $url .= "/" . $data["sort_by"] . "/" . $data["sort_order"] . "/" . $data["limit"] ."?page=".$data["current_page"];
            }
            return redirect($url);
        }else{
            Session::flash('flash_success', "Email campaign added successfully.");
        }
        return redirect("email-campaign");
    }
    public function delete($id,\Illuminate\Http\Request $request){
        Email_campaign::find($id)->delete();
        Email_campaign_contact::where("email_campaign_id",$id)->delete();
        Session::flash('flash_success', "Email campaign deleted successfully.");
        Session::flash('flash_success', "Email campaign updated successfully.");
        $url = "email-campaign-list";
//        if($request->session()->has('email_campaign_operation')){
//            $data = json_decode(session("email_campaign_operation"),true);
//            $url .= "/" . $data["sort_by"] . "/" . $data["sort_order"] . "/" . $data["limit"] ."?page=".$data["current_page"];
//        }
        return redirect($url);

    }
}
