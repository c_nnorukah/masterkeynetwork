<?php
namespace App\Http\Controllers;

use App\Interfaces\CampaignRepositoryInterface;
use App\Models\Campaign;
use App\Models\Affiliate;
use Auth;
use Redirect;
use Request;
use Response;
use App\Models\User;

class AffiliateController extends Controller
{
    protected $campaign_repository;

    public function __construct(CampaignRepositoryInterface $campaign_repository)
    {
        $this->campaign_repository = $campaign_repository;
    }

    public function generateLink()
    {
        $user = Auth::user();
        $username = trim(clean_url_values($user->first_name)) . trim(clean_url_values($user->last_name));
        $code = $username . format_date($user->birth_date, "m") . format_date($user->birth_date, "d");
        $campaign_code = Campaign::whereCampaignCode($code)->first();
        if (!empty($campaign_code)) {
            $code .= "-" . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
        }
        //TODO: We can move this affiliate data save logic to models
        $affiliate_data = array_only($user->toArray(), ['first_name', 'last_name', 'email_address']);
        $affiliate = new Affiliate($affiliate_data);
        $affiliate->affiliate_type = "user";
        $affiliate->ip = Request::getClientIp();
        $user->affiliates()->save($affiliate);
        //TODO: we can move this campaign save logic to Models
        $campaign = new Campaign();
        $campaign->affiliate_id = $affiliate->affiliate_id;
        $campaign->campaign_code = $code;
        $campaign_data = ["campaign_name"   => 'user',
            "campaign_source" => $user->first_name . "-" . $code,
            "campaign_medium" => 'user-link'];
        $campaign->campaign_data = json_encode($campaign_data);
        $campaign->campaign_type = "user";
        $campaign->cookie_expire_days = USER_REFERRAL_COOKIE_EXPIRE_DAYS;
        $success = $campaign->save();
        $response_message = $success ?
            "Congratulations! Your Referral Link is: " : "OOPS! There was issue generating your reference link.";
        return Response::json(["success"       => true,
            "message"       => $response_message,
            "response_data" => ["code" => $code]]);
    }

    /**
     * Retrive affiliate details and set it for google analytics tracking
     *
     * @param $campaign_code
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function affiliate($campaign_code)
    {
        if ($campaign_code != "") {
            $campaign_data = $this->campaign_repository->getdetailByCampaignCode($campaign_code);
            $campaign_detail = json_decode($campaign_data->campaign_data, true);
            $campaign_params = [];
            if (!empty($campaign_detail)) {
                $campaign_params[] = "utm_source=" . $campaign_detail['campaign_source'];
                $campaign_params[] = "utm_medium=" . $campaign_detail['campaign_medium'];
                $campaign_params[] = "utm_campaign=" . $campaign_detail['campaign_name'];
                // $this->guzzle_service->post(url(), ['query' => $campaign_params]);
                $campaign_start = strtotime($campaign_data->campaign_from);
                $campaign_end = strtotime($campaign_data->campaign_to);
                $expire = ($campaign_end - $campaign_start) / 60; //Get minutes out of seconds
            }
            return Redirect::to('register')->withCookie(cookie('campaign_code', $campaign_code, $expire));
        }
        return Redirect::to('register');
    }

    public function invite($referral_code)
    {
        return redirect('http://masterkeynetwork.com/?referral_code='.Auth::user()->referral_code);

        if ($referral_code != "") {
            $referral_data = User::whereReferralCode($referral_code)->first();
            if (!empty($referral_data)) {
                $expire = config('constants.Constants_Value.COOKIE_EXPIRE_DAYS')* (24 * 60 * 60);
                //$expire = time() + 3600;
                return Redirect::to('auth/register')->withCookie(cookie('referral_code', $referral_code, $expire));
            }
        }
        return Redirect::to('auth/register');
    }
}
