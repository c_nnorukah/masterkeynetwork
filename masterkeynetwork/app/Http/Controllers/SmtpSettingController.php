<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\SmtpRequest;
use Session;
use App\Http\Requests;
use App\Models\SmtpSetting;
use DB;
use Input;
use EmailVerifier;
use App\Models\EmailTemplate;
use Auth;

class SmtpSettingController extends Controller
{
	public function index()
	{
		$data = SmtpSetting::select('*')->get();
		return view('smtp-setting.SmtpSetting', compact('data'));
	}

	public function add_smtp(smtpRequest $request)
	{
		$request_data = $request->all();
		$request_data['user_id'] = Auth::user()->user_id;
		$email= \EmailValidator::verify($request_data['email_for_smtp'])->isValid();
		if($email[0] == true)
		{
			SmtpSetting::create($request_data);
			Session::flash('messege', 'Successfully added');
			return redirect::to('smtp-setting');
		}
		else{
			Session::flash('messege', 'please enter valid email address');
			return redirect::to('smtp-setting');
		}
	}

	public function delete_smtp($id)
	{
		SmtpSetting::findOrFail($id)->delete();
		Session::flash('messege', 'Successfully deleted');
		return redirect::to('smtp-setting');
	}

	public function update_smtp($id)
	{
		$update      = SmtpSetting::find($id);
		$update->title = Input::get('title');
		$update->email_for_smtp = Input::get('email');
		$update->port           = Input::get('port');
		$update->domain         = Input::get('domain');
		$update->user_name      = Input::get('user_name');

		if(!empty($update->email_for_smtp))
		{
			$email= \EmailValidator::verify($update->email_for_smtp)->isValid();
		}
		else{
			//return false;
			Session::flash('messege', 'Email address is empty');
			return Redirect::to('smtp-setting');
		}

		if($email[0] == true ){
			$data=$update->save();

			//return response()->json($data);
			Session::flash('messege', 'Successfully updated');
			return Redirect::to('smtp-setting');
		}
		else{
			return false;
			Session::flash('messege', 'Please enter valid Email address');
			return Redirect::to('smtp-setting');
		}
	}
}
