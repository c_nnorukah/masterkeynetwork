<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\PaymentHistory;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CronController extends Controller
{
    public function updatePlan()
    {
        $today = date("Y-m-d");
        $start = $today . " 00:00:00";
        $end = $today . " 23:59:59";
        $updated_array = [];
        //\DB::enableQueryLog();
        $user_data = PaymentHistory::where("start_date", ">=", $start)->where("start_date", "<=", $end)->get();
        //dd(\DB::getQueryLog());
        foreach ($user_data as $user) {
            $response = json_decode($user->transaction_response, true);
            $res_ack = strtoupper($response["ACK"]);
            if ($res_ack == "SUCCESS" || $res_ack == "SUCCESSWITHWARNING") {
                $update_rec = User::find($user->user_id);
                if (!empty($update_rec)) {
                    $update_rec->plan_id = $user->plan_id;
                    $update_rec->save();
                    $updated_array[] = $update_rec->first_name . " " . $update_rec->last_name . "[" . $update_rec->email . "] [" . $update_rec->user_id . "]";
                }
            }
        }
        echo "Total ", count($updated_array) . " records updated successfully.";
        echo "<pre>";
        print_r($updated_array);
        echo "</pre>";
    }

    public function referralPayment()
    {
        $latest_payment_date = PaypalPaymentSettings::orderBy("created_at", "desc")->first();
        if (empty($latest_payment_date)) {
            die("No payment date set");
        }
        $payment_level = PaymentLevelHistory::where("created_at", ">=", $latest_payment_date->created_at)->orderBy("created_at", "asc")->first();
        if (empty($payment_level)) {
            $payment_level = PaymentLevelHistory::where("created_at", "<=", $latest_payment_date->created_at)->orderBy("created_at", "desc")->first();
        }
        if (empty($payment_level)) {
            die("Value not set for payment levels");
        }
        $level1_payment = $payment_level->payment_level_1;
        $level2_payment = $payment_level->payment_level_2;
        $level3_payment = $payment_level->payment_level_3;
        $previous_month = date('Y-m-d H:i:s', strtotime("-30 days"));
        $user_history = ReferralPaymentHistory::select(\DB::raw("group_concat(user_id) as user_id"))->get();
        $user_history = explode(",", $user_history[0]->user_id);
        \DB::enableQueryLog();
        $users = User::where("created_at", ">=", $previous_month)->whereNotNull("referral_id");
        if (!empty($user_history))
            $users = $users->whereNotIn("user_id", $user_history);
        $users = $users->get();
        //dd(\DB::getQueryLog());
        $payment = [];
        foreach ($users as $user) {
            $level1_userid = $user->referral_id;
            $level2_userid = User::where("user_id", $level1_userid)->select("referral_id")->first();
            if (!empty($level2_userid)) {
                $level2_userid = $level2_userid->referral_id;
            } else {
                $level2_userid = null;
            }
            $level3_userid = User::where("user_id", $level2_userid)->select("referral_id")->first();
            if (!empty($level3_userid)) {
                $level3_userid = $level3_userid->referral_id;
            } else {
                $level3_userid = null;
            }
            $referral_payment = New ReferralPaymentHistory();
            $referral_payment->user_id = $user->user_id;
            $referral_payment->level1_userid = $level1_userid;
            $referral_payment->level1_payment = $level1_payment;
            $referral_payment->level2_userid = $level2_userid;
            $referral_payment->level2_payment = empty($level2_userid) ? null : $level2_payment;
            $referral_payment->level3_userid = $level3_userid;
            $referral_payment->level3_payment = empty($level3_userid) ? null : $level3_payment;
            $referral_payment->save();
            $payment[] = $referral_payment->toArray();
        }
        echo "<pre>";
        print_r($payment);
        echo "</pre>";
    }
}

///
