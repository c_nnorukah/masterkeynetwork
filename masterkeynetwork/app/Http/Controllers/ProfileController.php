<?php

namespace App\Http\Controllers;

use App\Interfaces\ProfileRepositoryInterface;
use App\Models\Image;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\File;
use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Session;


class ProfileController extends Controller
{
    protected $profile_repository;

    public function __construct(ProfileRepositoryInterface $profile_repository)
    {
        $this->profile_repository = $profile_repository;
    }

    public function index()
    {
        $user_data = $this->profile_repository->getAllUserData();
        return view("profile.update-profile")->with('portfolio_data', $user_data);
    }

    public function updateAboutMe()
    {
        if (Input::get('textareaBio') == "") {
            echo "field is blank please fill required details ";
            exit;
        }
        if (Input::file('txtPP') == null && Input::get('image_id') == "") {
            echo "Please select Profile picture first ";
            exit;
        }

        if (Input::file('txtCv') == null && Input::get('user_cv') == "") {
            echo "Please select CV file first ";
            exit;
        }
        $this->profile_repository->saveAboutMe();
        echo "About me section updated successfully";

    }

    public function updatePortfolio()
    {
        $this->profile_repository->savePortfolio();
        echo "Your portfolio updated successfully";
    }

    public function updateContactInfo()
    {
        $this->profile_repository->saveContactInfo();
        echo "Contact Info Added successfully";
    }

    public function viewProfile($id)
    {
        if (!empty($id)) {
            $user_data = $this->profile_repository->getUserByToken($id);
            return view('profile.viewprofile')->with('user_data', $user_data);
        } else {
            return view('errors.503');
        }
    }

    public function updateAcademicPositions()
    {
        $this->profile_repository->saveAcademicPositions();
        echo "Your academic positions updated successfully";
    }

    public function updateEducation()
    {
        $this->profile_repository->saveEducation();
        echo "Your education details updated successfully";

    }

    public function updateHonors()
    {
        $this->profile_repository->saveHonors();
        echo "Your Honors, Awards and Grants details updated successfully";
    }

    function getPortfolio($id)
    {
        $portfolio = array();
        $portfolio_data = $this->profile_repository->getUserPortfolio();
        if ($id != 0) {
            $portfolio = $this->profile_repository->getPortfolioByID($id);
        }
        return view("profile.my-portfolio", compact("portfolio", "portfolio_data", "id"));
    }

    function getAcademic($id)
    {
        $academic = array();
        $portfolio_data = $this->profile_repository->getUserAcademic();
        if ($id != 0) {
            $academic = $this->profile_repository->getAcademicByID($id);
        }
        return view("profile.academic-position", compact("academic", "portfolio_data", "id"));
    }

    function getEducation($id)
    {
        $education = array();
        $portfolio_data = $this->profile_repository->getUserEducation();
        if ($id != 0) {
            $education = $this->profile_repository->getEducationByID($id);
        }
        return view("profile.education", compact("education", "portfolio_data", "id"));
    }

    function getAward($id)
    {

        $honor = array();
        $portfolio_data = $this->profile_repository->getUserHonor();
        if ($id != 0) {
            $honor = $this->profile_repository->getHonorByID($id);
        }
        return view("profile.award", compact("honor", "portfolio_data", "id"));
    }
    public function deletePortfolio($id)
    {
        $this->profile_repository->deletePortfolio($id);
        echo "Your portfolio deleted successfully";
    }
    public function deleteAcademic($id)
    {
        $this->profile_repository->deleteAcademic($id);
        echo "Your academic position deleted successfully";
    }
    public function deleteEducation($id)
    {
        $this->profile_repository->deleteEducation($id);
        echo "Your education detail deleted successfully";
    }
    public function deleteAward($id)
    {
        $this->profile_repository->deleteAward($id);
        echo "Your Honors, Awards and Grants details deleted successfully";
    }

}
