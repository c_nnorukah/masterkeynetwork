<?php

namespace App\Http\Controllers;

use App\Interfaces\ContactRepositoryInterface;
use App\Models\User;
use Ctct\ConstantContact;
use Illuminate\Http\Request;
use App\Http\Requests;
use Laravel\Socialite\Facades\Socialite;
use ZfrMailChimp\Client\MailChimpClient;
use Input;
use Excel;
use Auth;
use Session;
use Validator;
use App\Models\Contacts;
use App\Models\ContactApi;
use App\Services\jsonRPCClient;

class ContactController extends Controller
{
    protected $contact_repository;

    public function __construct(ContactRepositoryInterface $contact_repository)
    {
        $this->contact_repository = $contact_repository;
    }

    public function index()
    {
        $contacts = Contacts::where("user_id", Auth::user()->user_id)->simplePaginate(10);
        return view('contacts.index', compact('contacts'));
    }

    public function create()
    {
        $contactKey = ContactApi::where('user_id', Auth::user()->user_id)->first();
        $landingpage_list = $this->contact_repository->getLandingpageList();
        return view('contacts.create', compact('contactKey', 'landingpage_list'));
    }

    public function storecp(Request $request)
    {

        $rules = array(
            'import_cp' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        // process the form

        if ($validator->fails()) {
            \Session::flash('flash_danger', 'Field is required');
            return redirect('contacts/create')->withErrors($validator);
        } else {

            $lines = explode(PHP_EOL, $request->import_cp);
            $result = array();
            $result1 = array();
            foreach (array_filter($lines) as $key => $value) {
                $splittedstring = explode(",", $value);
                foreach ($splittedstring as $key1 => $value1) {
                    if ($key1 == 0) {
                        $full_name = explode(" ", $value1);
                        @$result['f_name'] = $full_name[0];
                        @$result['l_name'] = $full_name[1];
                    }
                    if ($key1 == 1) {
                        @$result['email'] = $value1;
                    }
                }
                $result1[] = $result;
            }

            $result1[] = $result;
        }
        if (!empty($result1) && (count($result1 > 0))) {
            foreach ($result1 as $key => $value) {
                if (!empty($result1) && (count($result1 > 0))) {
                    foreach ($result1 as $key => $value) {
                        if (!array_key_exists('email', $value) || empty($value['l_name']) || empty($value['f_name'])) {
                            \Session::flash('flash_danger', 'Enter data in proper format.');
                            return redirect('contacts/create')->withErrors($validator);
                        }
                        $user = Contacts::firstOrNew(array('email' => $value['email'], "source_app_name" => "Copy & Paste", "list_id" => Input::get("list_id")));
                        $user->first_name = $value['f_name'];
                        $user->last_name = $value['l_name'];
                        $user->email = @$value['email'];
                        $user->user_id = Auth::user()->user_id;
                        $user->source_app_name = "Copy & Paste";
                        $user->list_id = Input::get("list_id");
                        $user->save();
                    }
                    Session::flash('messege', 'Successfully added records.');
                }
                return redirect('contacts/create');

            }

        }
    }

    public function importExcel()
    {
        $rules = array(
            'import_file' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        // process the form

        if ($validator->fails()) {
            \Session::flash('flash_danger', 'File is required');
            return redirect('contacts/create')->withErrors($validator);

        } else {
            Excel::load(Input::file('import_file'), function ($reader) {
                $filed_array = array('id' => '', 'firstname' => '', 'lastname' => '', 'email' => '');
                foreach ($reader->toArray() as $key => $row) {

                    if (!empty(array_diff_key($filed_array, $row))) {
                        $valid1 = 'no';
                    } else {
                        $valid1 = 'yes';
                        $user = Contacts::firstOrNew(array('email' => $row['email'], "source_app_name" => "File Import", "list_id" => Input::get("list_id")));
                        $user->first_name = $row['firstname'];
                        $user->last_name = $row['lastname'];
                        $user->email = $row['email'];
                        $user->user_id = Auth::user()->user_id;
                        $user->source_app_name = "File Import";
                        $user->list_id = Input::get("list_id");
                        $user->save();

                    }
                }
                if (!empty($valid1) && $valid1 != 'no') {
                    \Session::flash('messege', 'Contacts uploaded successfully.');
                } else {
                    \Session::flash('flash_danger', "Fields are Mismatch.");
                }


            });
            return redirect('contacts/create');


        }

    }

    public function importMailchimp(Request $request)
    {

        $API_KEY = $request->mailchimp_service;

        $client = new MailChimpClient($API_KEY);

        if (!empty($request->mailchimp_list_id) && isset($request->mailchimp_list_id)) {
            $activity = $client->getListMembers(array(
                'id' => $request->mailchimp_list_id
            ));

            if (!empty($activity['data']) && (count($activity['data'] > 0))) {

                foreach ($activity['data'] as $key => $value) {
                    $user = Contacts::firstOrNew(array('email' => $value['merges']['EMAIL'], "source_app_name" => "Mailchimp", "list_id" => Input::get("list_id")));
                    $user->first_name = $value['merges']['FNAME'];
                    $user->last_name = $value['merges']['LNAME'];
                    $user->email = $value['merges']['EMAIL'];
                    $user->user_id = Auth::user()->user_id;
                    $user->source_app_name = "Mailchimp";
                    $user->list_id = Input::get("list_id");
                    $user->save();
                }

                $ContactApi = ContactApi::firstOrNew(array('user_id' => Auth::user()->user_id));
                $ContactApi->mail_chimp = $API_KEY;
                $ContactApi->save();

                //Session::flash('messege', 'Successfully added new records');

                Session::flash('flash_success', 'Total ' . count($activity['data']) . ' contacts imported successfully.');

                return "yes";
            } else {
                Session::flash('flash_danger', 'There is no contact present for import.');
                return "yes";
            }


        } else {

            $mailchimp_list_id = $client->getLists();
            return $mailchimp_list_id;
        }
    }

    public function importConstantContact()
    {
        return (Socialite::with('constantcontact')->redirect());
    }

    public function importgetResponse(Request $request)
    {

        $api_key = $request->get_response_service;
        $api_url = 'http://api2.getresponse.com';
        $client = new jsonRPCClient($api_url);
        $campaigns = $client->get_contacts(
            $api_key
        );
        if (!empty($campaigns)) {
            foreach ($campaigns as $key => $value) {
                $user = Contacts::firstOrNew(array('email' => $value['email'], "source_app_name" => "Get Response", "list_id" => Input::get("list_id")));
                $nameArray = explode(' ', $value['name'], 2);
                $firstname = !empty($nameArray[0]) ? $nameArray[0] : '';
                $lastname = !empty($nameArray[1]) ? $nameArray[1] : '';
                $user->first_name = $firstname;
                $user->last_name = $lastname;
                $user->email = $value['email'];
                $user->user_id = Auth::user()->user_id;
                $user->source_app_name = "Get Response";
                $user->list_id = Input::get("list_id");
                $user->save();

            }

            $ContactApi = ContactApi::firstOrNew(array('user_id' => Auth::user()->user_id));
            $ContactApi->get_response = $api_key;
            $ContactApi->save();
            // Session::flash('messege', 'Successfully added new records');
            Session::flash('flash_success', 'Total ' . count($campaigns) . ' contacts imported successfully.');
            return "yes";
        } else {
            Session::flash('flash_danger', 'There is no contact present for import.');
            return "yes";
        }
    }

    public function addConstantContact()
    {
        try {
            $token = Socialite::driver('constantcontact')->user()->token;
            $cc = new ConstantContact(env('CONSTANTCONTACT_KEY'));

            $contacts = $cc->contactService->getContacts($token);
            if (!empty($contacts->results)) {
                foreach ($contacts->results as $key => $value) {
                    if (!empty($value->email_addresses)) {
                        $user = Contacts::firstOrNew(array('email' => $value->email_addresses[0]->email_address, "source_app_name" => "Constant Contact", "list_id" => Input::get("list_id")));
                    } else {

                        $user = new Contacts();
                    }

                    $user->first_name = $value->first_name;
                    $user->last_name = $value->last_name;
                    $user->email = !empty($value->email_addresses) ? $value->email_addresses[0]->email_address : "";
                    $user->user_id = Auth::user()->user_id;
                    $user->source_app_name = "Constant Contact";
                    $user->list_id = Input::get("list_id");
                    $user->save();

                    //  Session::flash('flash_success', 'Contacts imported successfully.');

                    Session::flash('flash_success', 'Total ' . count($contacts->results) . ' contacts imported successfully.');
                }
            } else {
                Session::flash('flash_danger', 'There is no contact present for import.');

            }
        } catch
        (Exception $e) {
            Session::flash('flash_error', $e->getMessage());
        }
        return redirect("contacts/create");

    }

    public function importAweber()
    {
        $consumerKey = env('AWEBER_CONSUMER_KEY');
        $consumerSecret = env('AWEBER_SECRET_KEY');


        $aweber = new \AWeberAPI($consumerKey, $consumerSecret);
        // Put the callback URL of your app below or set to 'oob' if your app isnt
// a web based application.
        $callbackURL = env('AWEBER_REDIRECT_URI');

// get a request token
        list($key, $secret) = $aweber->getRequestToken($callbackURL);
        $authorizationURL = $aweber->getAuthorizeUrl();

// store the request token secret
        setcookie('secret', $secret);

// redirect user to authorization URL
        return redirect($authorizationURL);

    }

    public function addAweberContact()
    {
        $consumerKey = env('AWEBER_CONSUMER_KEY');
        $consumerSecret = env('AWEBER_SECRET_KEY');

        $aweber = new \AWeberAPI($consumerKey, $consumerSecret);
        // Pull the request token key and verifier code from the URL
        $aweber->user->requestToken = $_GET['oauth_token'];
        $aweber->user->verifier = $_GET['oauth_verifier'];

// retrieve the stored request token secret
        $aweber->user->tokenSecret = $_COOKIE['secret'];

// Exchange a request token with a verifier code for an access token.
        list($accessTokenKey, $accessTokenSecret) = $aweber->getAccessToken();
        $account = $aweber->getAccount($accessTokenKey, $accessTokenSecret);

        $params = array();
        $found_subscribers = $account->findSubscribers($params);
        if (!empty($found_subscribers)) {
            foreach ($found_subscribers as $subscriber) {
                $subscriber = $subscriber->data;
                if (!empty($subscriber['email'])) {
                    $user = Contacts::firstOrNew(array('email' => $subscriber['email'], "source_app_name" => "Aweber", "list_id" => Input::get("list_id")));
                } else {
                    $user = new Contacts();
                }

                $user->first_name = $subscriber['name'];
                $user->email = !empty($subscriber['email']) ? $subscriber['email'] : "";
                $user->user_id = Auth::user()->user_id;
                $user->source_app_name = "Aweber";
                $user->list_id = Input::get("list_id");
                $user->save();
            }
            Session::flash('flash_success', 'Total ' . count($found_subscribers) . ' contacts imported successfully.');
        } else {
            Session::flash('flash_danger', 'There is no contact present for import.');
        }
        return redirect("contacts/create");
    }

    public function contactBook(Request $request)
    {
        $sort_by = !empty(\Illuminate\Support\Facades\Request::segment(2)) ? \Illuminate\Support\Facades\Request::segment(2) : "contacts_id";
        $sort_by = $sort_by == "user" ? "users.first_name" : $sort_by;
        $sort_by = $sort_by == "list" ? "landingpage_list.name" : $sort_by;
        $sort_by = str_replace("-", "_", $sort_by);
        $sort_order = !empty(\Illuminate\Support\Facades\Request::segment(3)) ? \Illuminate\Support\Facades\Request::segment(3) : "desc";
        $need_to_sort = $sort_order == "desc" ? "asc" : "desc";
        $limit = !empty(\Illuminate\Support\Facades\Request::segment(4)) ? \Illuminate\Support\Facades\Request::segment(4) : 15;
        $post_data = Input::get();

        if (!isset($post_data["search_criteria"]) && $request->session()->has('contact_search_data')) {
            $post_data = json_decode(session("contact_search_data"), true);
        }
        if (isset($post_data["search_criteria"])) {
            session(["contact_search_data" => json_encode($post_data)]);
        }
        $selected_criteria = isset($post_data["search_criteria"]) ? $post_data["search_criteria"] : "";
        $user_id = Auth::user()->user_id;
        if (Session::has('role')) {
            foreach (Session::get('role') as $val) {
                if ($val == '1') {
                    $user_id = 0;
                }
            }
        }
        $user_id = isset($post_data["user_id"]) ? $post_data["user_id"] : $user_id;
        $user_data = $this->contact_repository->getList($sort_by, $sort_order, $limit, $post_data, $user_id);
        $user_data->setPath(URL("contact-book/$sort_by/$sort_order/$limit"));
        $source_app = ["Copy & Paste", "File Import", "Mailchimp", "Get Response", "Constant Contact", "Aweber"];
        $users = User::with("assignedRoles")->get();
        $landingpage_list = $this->contact_repository->getLandingpageList();
        return view("contacts.list")->with(compact('landingpage_list', 'user_data', 'user_id', 'users', 'sort_by', 'sort_order', 'limit', 'need_to_sort', 'selected_criteria', 'post_data', 'source_app'));
    }

    public function reset(\Illuminate\Http\Request $request)
    {
        $request->session()->forget('contact_search_data');
        return $this->contactBook($request);
    }

    public function delete($id, \Illuminate\Http\Request $request)
    {
        $this->contact_repository->delete($id);
    }
}
