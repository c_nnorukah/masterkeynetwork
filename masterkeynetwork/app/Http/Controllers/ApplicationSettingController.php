<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\addplanRequest;
use App\Models\User;
use App\Models\UserSocialMedia;
use App\Models\UserPlan;
use DB;
use Facebook\Facebook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Mockery\CountValidator\Exception;
use Session;
use Socialite;
use Illuminate\Support\Facades\Input;
use Twitter;


class applicationSettingController extends Controller
{
	protected $fb;

	public function __construct()
	{
		$this->fb = new Facebook(array(
			'appId'      => '1064859340253171', // my app id
			'secret'     => 'eaf43f7e529a5cf01f6eb8bbb80bbb68', // my secret i
			'fileUpload' => true // this for file upload
		));
		//for fetching page list

		if (Session::has('token_fb') || Session::has('fbtoken')) {
			if(Session::has('token_fb'))
			{
				$token = Session::get('token_fb');
			}
			else
			{
				$token = Session::get('fbtoken');
			}

			try {

				$getallpage = $this->fb->get('/me/accounts/pages', $token)->getGraphEdge()->asArray();
				//dd($getallpage);
			} catch (Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error:' . $e->getMessage();
				exit;
			}
			Session::put('getallpage', $getallpage);
		}
	}

	public function index()
	{
		return view('dashboard.applicationSetting');
	}

	public function adminSetting()
	{
		$data = DB::table('user_plans')->get();
		return view('dashboard.AdminSetting', compact('data'));
	}

	public function addplan(addplanRequest $request)
	{
		UserPlan::create($request->all());
		Session::flash('messege', 'Successfully added new plan');
		return redirect::to('admin-setting');
	}

	public function redirectToProviderAll($type)
	{
		if ($type == 'facebook')
			return Socialite::driver($type)->scopes([
				'email', 'user_birthday', 'user_photos', 'user_likes', 'user_posts', 'pages_show_list', 'manage_pages','publish_actions','publish_pages'])->redirect();
		else {
			return Socialite::with($type)->redirect();
		}
	}

	public function handleProviderCallbackAll($type)
	{
		try {

			$user_social_info = Socialite::driver($type)->user();
			if ($type == "facebook") {
				Session::put('token_fb', $user_social_info->token);
				Session::put('type_id', $user_social_info->id);
			} else {
				Session::put('token_tw', $user_social_info->token);
			}
			Session::put('profilepic',$user_social_info->avatar);
			Session::put('username',$user_social_info->name);


		} catch (Exception $e) {
			return redirect('auth/' . $type);
		}

		$authUser = $this->findOrCreateUserAll($user_social_info);

		//Auth::login($authUser, true);
		Session::flash('messege', 'Successfully login');

		return redirect::to('application-setting');
	}

	private function findOrCreateUserAll($user_social_info)
	{
		$authUser = UserSocialMedia::where('type_id', $user_social_info->id)->first();
		$user_id  = Auth::user()->user_id;
		if ($authUser) {
			return $authUser;
		} else {
			return UserSocialMedia::create([
				'type_id'      => $user_social_info->id,
				'access_token' => $user_social_info->token,
				'user_id'      => $user_id,
				'user_name' => $user_social_info->name,
				'user_dp' => $user_social_info->avatar
			]);
		}
	}

	public function fblogout()
	{

		Session::forget('token_fb');
		Session::forget('fbtoken');
		Session::forget('getallpage');
		$type_id = UserSocialMedia::select('id')->where('type_id', '!=', '')->where('user_id', Auth::user()->user_id)->first();

		UserSocialMedia::whereid($type_id['id'])->delete();
		Session::flash('messege', 'Successfully logout');
		return redirect::to('application-setting');
	}


	public function twitterlogout()
	{
		Session::forget('access_token');
		Session::forget('twtoken');
		Session::flash('messege', 'Successfully logout');
		$twitter_accesstoken = UserSocialMedia::select('id')->where('type_id', '=', '')->where('user_id', Auth::user()->user_id)->first();
		UserSocialMedia::whereid($twitter_accesstoken['id'])->delete();
		return redirect::to('application-setting')->with('messege', "Successfully logout");
		//return redirect("https://twitter.com/logout?next=http://laravel.masterkey.com/applicationSetting/&access_token=" . $a);
	}


	public function destroyplan($id)
	{
		try {
			$plan = UserPlan::findOrFail($id)->delete();

		} catch (\Exception $e) {

			Session::flash('messege', 'This plan is already assigned to user can not delete');
			return redirect::to('admin-setting');
		}

		Session::flash('messege', 'Successfully deleted plan');
		return redirect::to('admin-setting');
	}

	public function editplan($id)
	{
		$edit = UserPlan::findOrFail($id)->select('*')->where('plan_id', $id)->first();

		$data = DB::table('user_plans')->get();

		return view('dashboard.AdminSetting')->with('data', $data)->with('edit', $edit);
	}

	public function updateplan($id)
	{
		$update              = UserPlan::find($id);
		$update->plan_name   = Input::get('plan_name');
		$update->description = Input::get('description');
		$update->price       = Input::get('price');
		$update->interval    = Input::get('interval');
		$update->days        = Input::get('days');
		$update->save();

		// redirect
		Session::flash('messege', 'Successfully updated plan');
		return Redirect::to('admin-setting');
	}

	public function twitterlogin()
	{
		// your SIGN IN WITH TWITTER  button should point to this route
		$sign_in_twitter = true;
		$force_login     = false;

		// Make sure we make this request w/o tokens, overwrite the default values in case of login.
		Twitter::reconfig(['token' => '', 'secret' => '']);
		$token = Twitter::getRequestToken(route('twitter.callback'));

		if (isset($token['oauth_token_secret'])) {
			$url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

			Session::put('oauth_state', 'start');
			Session::put('oauth_request_token', $token['oauth_token']);
			Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

			return Redirect::to($url);
		}

		return Redirect::route('twitter.error');
	}

	public function callback()
	{
		// You should set this route on your Twitter Application settings as the callback
		// https://apps.twitter.com/app/YOUR-APP-ID/settings
		if (Session::has('oauth_request_token')) {
			$request_token = [
				'token'  => Session::get('oauth_request_token'),
				'secret' => Session::get('oauth_request_token_secret'),
			];

			Twitter::reconfig($request_token);

			$oauth_verifier = false;

			if (Input::has('oauth_verifier')) {
				$oauth_verifier = Input::get('oauth_verifier');
			}

			// getAccessToken() will reset the token for you
			$token = Twitter::getAccessToken($oauth_verifier);
			if (!isset($token['oauth_token_secret'])) {
				return Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
			}

			$credentials = Twitter::getCredentials();
			Session::put('twittername',$credentials->name);
			Session::put('twitterprofilepic',$credentials->profile_image_url);

			if (is_object($credentials) && !isset($credentials->error)) {
				Session::put('access_token', $token);
				$this->findOrCreatetwiteerUserAll();
				Session::flash('messege', 'Successfully login');
				return Redirect::to('application-setting');
			}
			return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
		}
	}

	private function findOrCreatetwiteerUserAll()
	{
		$access_token = Session::get('access_token');

		$authUser = UserSocialMedia::where('access_token', $access_token['oauth_token'])->first();
		$user_id  = Auth::user()->user_id;

		if ($authUser) {
			return $authUser;
		} else {
			return UserSocialMedia::create([
				'access_token' => $access_token['oauth_token'],
				'user_id'      => $user_id,
				'user_name' => Session::get('twittername'),
				'user_dp' => Session::get('twitterprofilepic'),
				'screen_name' => $access_token['screen_name']
			]);
		}
	}

	public function posttweet(){
		$comment = Input::get('tweet');
		Twitter::postTweet(['status' => $comment, 'format' => 'json']);
		Session::flash('messege','Tweet posted successfully');
		return redirect::to('application-setting');
	}

}
