<?php

namespace App\Http\Controllers;
use App\Http\Requests\PaymentLevelRequest;
use App\Models\PaymentLevelHistory;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use App\Models\PaypalPaymentSettings;
use App\Http\Requests\PaypalSettingRequest;
use Session;
use Input;
use App\Models\ReferralPaymentHistory;
class PaypalPaymentSettingController extends Controller
{
    public function index()
    {
        $data_paypal = PaypalPaymentSettings::first();
        $data_level = PaymentLevelHistory::first();
        $data_affilate = ReferralPaymentHistory::select('user_id')->get();
        foreach($data_affilate as $affilate){
            $affilate_user[] = User::select('user_name','user_id')->where('user_id',$affilate['user_id'])->get();
        }

        $currency = ["USD", "EURO", "Pound"];
        return view('paypal-payment-settings.PaypalPaymentSettings', compact('data_paypal', 'data_level','currency','affilate_user'));
    }


    public function addpaypalsetting(PaypalSettingRequest $request)
    {
            PaypalPaymentSettings::create($request->all());
            Session::flash('messege', 'Successfully added');
            return redirect::to('paypal-payment-settings');
    }

    public function updatepaypalsetting($id,PaypalSettingRequest $request)
    {
        $paypal = PaypalPaymentSettings::find($id);
        $input = $request->all();
        $paypal->fill($input)->save();
        Session::flash('messege', 'Successfully updated');
        return redirect::to('paypal-payment-settings');
    }


    public function addpaypallevel(PaymentLevelRequest $request)
    {
            PaymentLevelHistory::create($request->all());
            Session::flash('messege', 'Successfully added');
            return redirect::to('paypal-payment-settings');
    }

    public function updatepaypallevel($id)
    {
        $update              = PaymentLevelHistory::find($id);
        $update->payment_level_1   = Input::get('payment_level_1');
        $update->payment_level_2 = Input::get('payment_level_2');
        $update->payment_level_3 = Input::get('payment_level_3');
        $update->save();
        Session::flash('messege', 'Successfully updated');
        return redirect::to('paypal-payment-settings');
    }

}
