<?php

namespace App\Http\Controllers;
use App\Models\AssignedRole;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EmailTemplateRequest;
use App\Http\Requests;
use Session;
use DB;
use Auth;
use Input;
use App\Models\SmtpSetting;

class AdminEmailTemplateController extends Controller
{
	public function index()
	{
		$template  =EmailTemplate::select('*')->where('created_by',Auth::user()->user_id)->get();
		  $default_template  =EmailTemplate::select('*')->where('template_type','=','html')->get();
		  return view('admin-email-template.AddEmailTemplate', compact('template','default_template'));
	}


	public function addemail(EmailTemplateRequest $request)
	{
		$post               = $request->all();
		$post['created_by'] = Auth::user()->user_id;
		EmailTemplate::create($post);
		Session::flash('messege', 'Successfully added');
		return redirect::to('add-email-template');
	}

	public function select_template($id)
	{
		$edit      = EmailTemplate::findOrFail($id)->select('*')->where('email_template_id', $id)->first();
		echo json_encode($edit);
		exit;
	}

	public function update_email($id)
	{
		$update                    = EmailTemplate::find($id);
		$update->subject           = Input::get('subject');
		$update->content           = Input::get('content');
		$update->save();
		Session::flash('messege', 'Successfully updated');
		return Redirect::to('add-email-template');
	}
}
