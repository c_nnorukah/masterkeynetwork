<?php

namespace App\Http\Controllers;


use App\Interfaces\UploadManagerRepositoryInterface;

class uploadManagerController extends Controller
{

    protected $ftp_repository;

    public function __construct(UploadManagerRepositoryInterface $ftp_repository)
    {
        $this->ftp_repository = $ftp_repository;
    }

    public function index()
    {
        $landing_page = $this->ftp_repository->getLandingPages();
        $id = "";
        return view('dashboard.ftpSetting', compact("landing_page","id"));
    }

    public function saveSetting($id)
    {
        $landing_page = $this->ftp_repository->saveSetting($id);
        echo "Your project published successfully.";
    }

    public function detail($id)
    {
        $landing_page_detail = $this->ftp_repository->getLandingPageDetail($id);
        $landing_page_detail = isset($landing_page_detail[0]) ? $landing_page_detail[0] : [];
        return view('dashboard.ftpSetting', compact("landing_page_detail","id"));
    }
}
