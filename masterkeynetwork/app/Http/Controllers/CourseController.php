<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use App\Http\Requests\CourseRequest;
use App\Interfaces\CourseRepositoryInterface;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseMaterial;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Njasm\Laravel\Soundcloud\Facades\Soundcloud;
use Config;

class CourseController extends Controller
{
    protected $course_repository;

    public function __construct(CourseRepositoryInterface $course_repository)
    {
        $this->course_repository = $course_repository;
        $this->course_youtube = new \App\course\course_material();
    }


    public function index()
    {
        $data = $this->course_repository->All();
        $category = CourseCategory::orderBy("category_name")->get();

        return view('course.education', compact('data', 'category'));
    }

    public function details($id)
    {
        $data = $this->course_repository->CourseDetail($id);

        if (!empty($data))
            return view('course.index', compact('data'));
        else
            return view('errors.503')->with('flash_danger', trans('message.error_msg'));
    }

    public function store(CourseRequest $request)
    {

        $input = $request->except('_token');
        if (!empty($input['course_title'] && $input['course_description'] && $input['category_name'])) {
            $category_id = '';
            if ($request["category_name"] && $request["category_name"] != '') {
                $category_id_obj = CourseCategory::where('category_name', "=", $request["category_name"])->first();
                if (count($category_id_obj) > 0) {
                    $category_id = $category_id_obj->category_id;
                } else {
                    $data_category = CourseCategory::create($request->all());
                    $category_id = $data_category->category_id;
                }
            }
            if (!empty($request['course_id'])) {
                $id = $request['course_id'];
            }
            if (!empty($id)) {
                $course = Course::findOrFail($id);
                $course->course_title = $input['course_title'];
                $course->course_description = $input['course_description'];
                $course->category_id = $category_id;
            } else {
                $input['category_id'] = $category_id;
                $input['created_by'] = Auth::user()->user_id;
                $course = new Course($input);
            }
            $course = $this->course_repository->store($course);
            $course_id = $course->course_id;
            $fileInput = [];
            $timestamp = time();
            if (isset($request['course_id']) && $request['course_id'] > 0) {
                $update_material_id_default = CourseMaterial::where('is_default', 1)->where('course_id', $request['course_id'])->first();
                if (count($update_material_id_default) > 0) {
                    $update_material_id_default->is_default = '0';
                    $update_material_id_default->save();
                }
            }
            if (Input::hasFile('images')) {
                foreach (Input::file('images') as $k => $i) {
                    $destination_path = 'uploads/course/images/';
                    $file['course_id'] = $course_id;
                    $file['material_name'] = $i->getClientOriginalName();
                    $fileName = $timestamp . '_img' . $k . '_' . $i->getClientOriginalName();
                    $file['material_url'] = $destination_path . $fileName;
                    $file['type'] = '0';
                    if (!empty($request['feature_image'])) {
                        $feature_image = explode("\\", $request['feature_image']);
                        $default_image = isset($feature_image[2]) ? $feature_image[2] : "";
                        if ($file['material_name'] == $default_image) {
                            $file['is_default'] = '1';
                        } else {
                            $file['is_default'] = '0';
                        }
                    } else {
                        $file['is_default'] = '0';
                    }
                    $fileInput[] = $file;
                    $i->move($destination_path, $fileName);
                }
            }
            if (isset($request['material_id_hidden']) && $request['material_id_hidden'] > 0) {
                $material_id_default = CourseMaterial::find($request['material_id_hidden']);
                if (count($material_id_default) > 0) {
                    $material_id_default->is_default = '1';
                    $material_id_default->save();
                }
            }
            if (Input::hasFile('docs')) {
                foreach (Input::file('docs') as $k => $i) {

                    $destination_path = 'uploads/course/docs/';
                    $file['course_id'] = $course_id;
                    $fileName = $timestamp . '_doc' . $k . '_' . $i->getClientOriginalName();
                    $file['material_name'] = $fileName;
                    $file['material_url'] = $destination_path . $fileName;
                    $file['type'] = '1';
                    $file['is_default'] = '0';
                    $fileInput[] = $file;
                    $i->move($destination_path, $fileName);
                }
            };
            foreach ($input['material_video'] as $k => $i) {
                if ($i != '') {
                    if (strpos($i, 'youtube') > 0) {
                        $file['material_key'] = $this->course_youtube->parse_youtube($i);
                    } elseif (strpos($i, 'vimeo') > 0) {
                        $file['material_key'] = $this->course_youtube->parse_vimeo($i);
                    } elseif (strpos($i, 'soundcloud') > 0) {
                        $file['material_key'] = $this->course_youtube->parse_soundcloud($i);
                    }
                    $file['course_id'] = $course_id;
                    $file['material_name'] = $i;
                    $file['material_url'] = $i;
                    $file['type'] = '2';
                    $file['is_default'] = '0';
                    $file['material_video_src'] = (isset($input['material_video_src'][$k])) ? $input['material_video_src'][$k] : "";
                    $fileInput[] = $file;
                }
            }
            if (!empty($fileInput)) {
                $this->course_repository->storeMaterial($fileInput);
            }
            if (isset($id)) {
                return redirect()->back()->withFlashSuccess(trans('message.courseUpdated'));
            } else {
                return redirect()->back()->withFlashSuccess(trans('message.courseAdded'));
            }
        } else {
            return redirect()->back();
        }

    }

    public function show()
    {
        $data = $this->course_repository->All();

        return view('course.education', compact('data'));
    }

    public function add()
    {
        $view = View::make('include.courseDialogBox');
        $contents = $view->render();

        return $contents;
    }

    public function edit($courseId = null)
    {
        if (!empty($courseId)) {
            $courseData = $this->course_repository->CourseDetail($courseId);

            return response()->json($courseData);

        }
    }

    public function delete($id)
    {
        $this->course_repository->DeleteCourse($id);

        return Redirect::back()->withFlashSuccess(trans('message.courseDeleted'));
    }

    public function deleteMaterial($id)
    {
        if ($this->course_repository->DeleteMaterial($id))
            return "true";
        else
            return "false";
    }

    public function checkurl()
    {
        if (strpos(Input::get('vid'), 'youtube') > 0) {
            $data = $this->course_youtube->parse_youtube(Input::get('vid'));
            if ($data === 0) {
                $data = "false";

                return $data;
            } else {
                $source = $this->course_youtube->save_video_url(Input::get('vid'));
                if (is_array($source['new_thumb'])) {
                    $data = $source['new_thumb'];
                } else {
                    $data = explode("\\", $source['new_thumb']);
                }
                return $data;
            }
        } else if (strpos(Input::get('vid'), 'vimeo') > 0) {
            $data = $this->course_youtube->parse_vimeo(Input::get('vid'));
            if ($data == 0) {
                $data = "false";

                return $data;
            } else {
                $source = $this->course_youtube->save_video_url(Input::get('vid'));
                $data = explode("\\", $source['new_thumb']);

                return $data;
            }
        } else if (strpos(Input::get('vid'), 'soundcloud') > 0) {
            $data = $this->course_youtube->parse_soundcloud(Input::get('vid'));
            if ($data == 0) {
                $data = "false";

                return $data;
            } else {
                $source = $this->course_youtube->sound_cloud_data(Input::get('vid'));
                $data = isset($source['user']) ? $source['user']['avatar_url'] : "";
                return $data;
            }
        } else {
            $data = "false";

            return $data;
        }
    }

    public function getcategory()
    {
        $term = Input::get('category_name');
        $data = $this->course_repository->GetCategory($term);
        return response()->json($data);
    }

    public function filedownload()
    {
        $file = Input::get('file_url');
        $name = Input::get('file_name');
        $filepath = public_path() . "/" . $file;
        if (\File::exists($filepath)) {
            $mimetype = \File::mimeType($filepath);
            $headers = array(
                'Content-Type: ' . $mimetype,
            );

            return response()->download($filepath, $name, $headers);
        } else {
            return redirect()->back()->with('flash_danger', trans('message.filenotfound'));
        }
    }

}