<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Interfaces\CourseRepositoryInterface;
use App\Models\UserSocialMedia;
use Auth;
use DB;
use Facebook\Facebook;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Redirect;
use Twitter;
use Cookie;

class DashboardController extends Controller
{
    protected $fb;
    protected $course_repository;
    public function __construct(CourseRepositoryInterface $course_repository)
    {
        header('Access-Control-Allow-Origin: *');

        $this->course_repository = $course_repository;

        // $user_id = Auth::user()->user_id;
        // $role = DB::table('assigned_roles')->select('role_id')->where('user_id', $user_id)->first();
        // Session::put('role', $role);

        $this->fb = new Facebook(array(
            'appId' => '1064859340253171', // my app id
            'secret' => 'eaf43f7e529a5cf01f6eb8bbb80bbb68', // my secret i
            'fileUpload' => true // this for file upload
        ));
    }

    /*
     * An admin panel dashboard
     */
    public function index()
    {
        $twitter_accesstoken = UserSocialMedia::select('access_token', 'user_name', 'user_dp','screen_name')->where('type_id', '=', '')->where('user_id', Auth::user()->user_id)->first();
        $type_id = UserSocialMedia::select('type_id')->where('type_id', '!=', '')->where('user_id', Auth::user()->user_id)->first();
        $facebook_accesstoken = UserSocialMedia::select('access_token', 'user_name', 'user_dp')->where('type_id', $type_id['type_id'])->first();
        if (!empty($twitter_accesstoken['access_token'])) {
            $twtoken = '';
            $twtoken = $twitter_accesstoken['access_token'];
            Session::put('twtoken', $twtoken);
            Session::put('tw_name', $twitter_accesstoken['user_name']);
            Session::put('tw_dp', $twitter_accesstoken['user_dp']);
        }
        if (!empty($facebook_accesstoken['access_token'])) {
            $fbtoken = '';
            $fbtoken = $facebook_accesstoken['access_token'];
            Session::put('fbtoken', $fbtoken);
            Session::put('fb_name', $facebook_accesstoken['user_name']);
            Session::put('fb_dp', $facebook_accesstoken['user_dp']);

        }

        if (isset($facebook_accesstoken['access_token'])) {

            try {
                $post = $this->fb->get('/me/posts', $facebook_accesstoken['access_token'])->getGraphEdge()->asArray();

            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $data['fbpost'] = $post;
        }
        if (isset($twitter_accesstoken['access_token'])) {
            Session::put('twitter_data',$twitter_accesstoken['access_token']);
            $data['tweet'] = json_decode(Twitter::getUserTimeline(['count' => 4, 'format' => 'json', 'screen_name' => $twitter_accesstoken['screen_name']]));
        }


        $course = $this->course_repository->All();
        $data['course']=$course;
        //dd($data['course']);

//        $search_post = "test";
        //$sql=" SELECT * FROM users WHERE post_title like '%".$name."%' OR post_content like '%".$email."%'";
//        $blog_post = DB::connection('mysql2')->select('p.post_title like ' % ".$search_post." % '"');
//        echo $blog_post;
//        exit;
//        echo "<pre>";
//        print_r($blog_post);
//        echo "</pre>";
//        exit;

        //////////////////////////////////////////
        $blog_ids = array();
        $blog_ids = DB::connection('mysql2')->select('select DISTINCT(blog_id) from wp_registration_log ');
        $post_array = array();
        $data['blog_post'] = array();
        if (!empty($blog_ids)) {
            static $i = 0;
            foreach ($blog_ids as $val) {
                $val->blog_id;
                if ($val->blog_id > 2) {
                    $posts = DB::connection('mysql2')->select('
select p.*,c.comment_content,tr.term_taxonomy_id, t.name from wp_' . $val->blog_id . '_posts as p
 left join wp_' . $val->blog_id . '_comments as c on c.comment_post_ID = p.ID and c.comment_approved=1
 left join wp_' . $val->blog_id . '_term_relationships as tr on tr.object_id = p.ID
 left join wp_' . $val->blog_id . '_terms as t on tr.term_taxonomy_id = t.term_id
where p.post_status="publish"');

                    //$thubmali = DB::connection('mysql2')->select('select * from wp_' . $val->blog_id . '_postmeta where post_id=""');
                    foreach ($posts as $k => $v) {

                        $data['blog_post'][$val->blog_id][$v->ID]['ID'] = $v->ID;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_author'] = $v->post_author;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_date'] = $v->post_date;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_author'] = $v->post_author;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_date_gmt'] = $v->post_date_gmt;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_content'] = $v->post_content;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_title'] = $v->post_title;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_excerpt'] = $v->post_excerpt;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_status'] = $v->post_status;
                        $data['blog_post'][$val->blog_id][$v->ID]['comment_status'] = $v->comment_status;
                        $data['blog_post'][$val->blog_id][$v->ID]['ping_status'] = $v->ping_status;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_password'] = $v->post_password;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_name'] = $v->post_name;
                        $data['blog_post'][$val->blog_id][$v->ID]['to_ping'] = $v->to_ping;
                        $data['blog_post'][$val->blog_id][$v->ID]['pinged'] = $v->pinged;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_modified'] = $v->post_modified;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_modified_gmt'] = $v->post_modified_gmt;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_content_filtered'] = $v->post_content_filtered;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_parent'] = $v->post_parent;
                        $data['blog_post'][$val->blog_id][$v->ID]['guid'] = $v->guid;
                        $data['blog_post'][$val->blog_id][$v->ID]['menu_order'] = $v->menu_order;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_type'] = $v->post_type;
                        $data['blog_post'][$val->blog_id][$v->ID]['post_mime_type'] = $v->post_mime_type;

                        $data['blog_post'][$val->blog_id][$v->ID]['comment_count'] = $v->comment_count;
                        if ($v->comment_content != '') {
                            $data['blog_post'][$val->blog_id][$v->ID]['comment'][] = $v->comment_content;
                        }
                        if ($v->name != '') {
                            $data['blog_post'][$val->blog_id][$v->ID]['terms'][] = $v->name;
                        }
//						$post_images = DB::connection('mysql2')->select('select * from wp_' . $val->blog_id . '_postmeta where post_id=' . $v->ID . ' and meta_key="_wp_attachment_metadata"');
//						if(!empty($post_images)) {
//							print_r($post_images[0]->meta_value);exit;
//							$data['blog_post'][$val->blog_id][$v->ID]['comment_img'] = $v->guid;
//						}
                        $i++;
                    }
                }
            }
            /*echo "<pre>";
            print_r($data['blog_post']);
            echo "</pre>";exit;*/
            //=$posts;
            $id = Auth::user()->user_id;
            return view('dashboard.index', compact('data'))->withCookie(cookie('user_profile', $id));
        } else {
            $id = Auth::user()->user_id;
            return view('dashboard.index', compact('data'))->withCookie(cookie('user_profile', $id));
        }
    }

    public function TopnavMenu()
    {
        return view('includes.topnav');
    }

    public function LeftnavMenu()
    {
        return view('includes.leftsidebar');
    }

    public function CheckAdmin()
    {
        $user_id = Auth::user()->user_id;
        $CheckAdmin = DB::table('assigned_roles')->where('user_id', $user_id)->first();
        $CheckAdmin = ($CheckAdmin->role_id == "1") ? 'true' : 'false';

        return $CheckAdmin;

    }

    //for posting commnet in facebook timeline but its only working for api account not for other
    public function fbpost()
    {
        if (Session::has('token_fb') || Session::has('fbtoken')) {

            if (Session::has('token_fb')) {
                $token = Session::get('token_fb');
            } else {
                $token = Session::get('fbtoken');
            }

            $cmt = Input::get('comment');
            try {
                $id = Input::get('pagename');
                $data_page = explode(',', $id);
                $post = $this->fb->post('/' . $data_page[0] . '/feed', array('message' => $cmt), $data_page[1]);
                //	$publish = $this->fb->post('https://graph.facebook.com/feed' ,['message' => $cmt], $token);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error:' . $e->getMessage();
                exit;
            }
            Session::flash('messege', 'Successfully comment posted facebook page');
            return redirect::to('application-setting');
        } else {
            Session::flash('messege', 'Login first to post comment');
            return redirect::to('application-setting');
        }
    }

    public function blog()
    {
        $user_id = Auth::user()->user_id;
        return redirect('http://masterkeynetwork.com/course/wp-signup.php?user_id_masterkey=' . $user_id);
    }

    public function SearchBlog()
    {
        //$blog_ids = DB::connection('mysql2')->select('select DISTINCT(blog_id) from wp_registration_log ');
        $sql = "";
        if (isset($_GET['search_post']) && $_GET['search_post'] != '') {
            $search_post = $_GET['search_post'];
            $blog_ids = DB::connection('mysql2')->select('select DISTINCT(blog_id) from wp_registration_log ');
            $post_array = array();
            if (!empty($blog_ids)) {
                $data['blog_post'] = array();
                foreach ($blog_ids as $val) {
                    $val->blog_id;
                    if ($val->blog_id > 2) {
                        $query = '';
                        $query .= 'select p.*,c.comment_content,tr.term_taxonomy_id, t.name from wp_' . $val->blog_id . '_posts as p
 left join wp_' . $val->blog_id . '_comments as c on c.comment_post_ID = p.ID and c.comment_approved=1
 left join wp_' . $val->blog_id . '_term_relationships as tr on tr.object_id = p.ID
 left join wp_' . $val->blog_id . '_terms as t on tr.term_taxonomy_id = t.term_id
where p.post_status="publish"';
                        if($search_post!=''){
                            $query .= " AND (p.post_title like '%".$search_post."%' OR p.post_content like '%".$search_post."%')";
                        }
                        $posts = DB::connection('mysql2')->select($query);
                        foreach ($posts as $k => $v) {

                            $data['blog_post'][$val->blog_id][$v->ID]['ID'] = $v->ID;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_author'] = $v->post_author;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_date'] = $v->post_date;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_author'] = $v->post_author;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_date_gmt'] = $v->post_date_gmt;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_content'] = $v->post_content;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_title'] = $v->post_title;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_excerpt'] = $v->post_excerpt;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_status'] = $v->post_status;
                            $data['blog_post'][$val->blog_id][$v->ID]['comment_status'] = $v->comment_status;
                            $data['blog_post'][$val->blog_id][$v->ID]['ping_status'] = $v->ping_status;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_password'] = $v->post_password;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_name'] = $v->post_name;
                            $data['blog_post'][$val->blog_id][$v->ID]['to_ping'] = $v->to_ping;
                            $data['blog_post'][$val->blog_id][$v->ID]['pinged'] = $v->pinged;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_modified'] = $v->post_modified;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_modified_gmt'] = $v->post_modified_gmt;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_content_filtered'] = $v->post_content_filtered;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_parent'] = $v->post_parent;
                            $data['blog_post'][$val->blog_id][$v->ID]['guid'] = $v->guid;
                            $data['blog_post'][$val->blog_id][$v->ID]['menu_order'] = $v->menu_order;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_type'] = $v->post_type;
                            $data['blog_post'][$val->blog_id][$v->ID]['post_mime_type'] = $v->post_mime_type;

                            $data['blog_post'][$val->blog_id][$v->ID]['comment_count'] = $v->comment_count;
                            if ($v->comment_content != '') {
                                $data['blog_post'][$val->blog_id][$v->ID]['comment'][] = $v->comment_content;
                            }
                            if ($v->name != '') {
                                $data['blog_post'][$val->blog_id][$v->ID]['terms'][] = $v->name;
                            }
                        }
                    }
                }
               /* echo "<pre>";
                print_r($data['blog_post']);
                echo "</pre>";
                exit;*/
                return view('dashboard.BlogSearch', compact('data'));
            }
        } else {
            return view('dashboard.index', compact('data'));
        }
    }
}
