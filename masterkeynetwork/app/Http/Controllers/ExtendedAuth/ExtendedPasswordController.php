<?php
namespace App\Http\Controllers\ExtendedAuth;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests;

use Illuminate\Auth\Passwords\PasswordBroker;
use Password;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;
use Illuminate\Http\Request;
use App\Services\Mailer\UseMailer;
use App\Models\PasswordResets;
use Redirect;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ExtendedPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    protected $tokens;
    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct(PasswordBroker $passwords)
    {
        $this->passwords = $passwords;
        $this->middleware('guest');
    }

    public function getEmail()
    {
        return view('auth.forgot-password');
    }

    /**
     * @param PasswordRecoveryRequest $request
     * @param UserMailer $mailer
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEmail(Request $request)
    {
        $email = Input::get('email');
        $token = $this->createTokenCustom($email);
        if (!$token) {
            return Redirect::to('reset-password')->with('flash_danger', trans('message.user_not_exist'));
        }
        $mail_params = [];
        $mail_params['from'] = env('MAIL_FROM');
        $mail_params['to'] = $email;
        $mail_params['subject'] = 'Password Reset';
        $template = 'password_reset';
        $url = url('reset', $token);
        $url = '<a href="'.$url.'"> here</a>';
        $dynamicData = ['#URL#' => $url, '#EMAIL#' => $email,'#TOKEN#'=>$url];
        \EmailService::SendEMail($mail_params, $template, $dynamicData);
        return Redirect::to('login')->withFlashSuccess(trans('message.mail_sent'));
    }

    /**

     * @param PasswordRecoveryResetRequest $request
     * @param null $token
     * @return $this
     * @throws NotFoundHttpException
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('auth.passwords.reset')->with('token', $token);
    }

    /**
     * @param PasswordRecoveryUpdateRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postReset()
    {
        $credentials = Input::only('email', 'password', 'password_confirmation', 'token');
        $credentials['email'] = (Input::get('email'));
        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        switch ($response) {
            case PasswordBroker::INVALID_PASSWORD:
            case PasswordBroker::INVALID_TOKEN:
            case PasswordBroker::INVALID_USER:
                Session::flash('flash_type', 'danger');
                Session::flash('flash_message', 'Something went wrong.');
                return Redirect::to('reset-password');
            case PasswordBroker::PASSWORD_RESET:
                Session::flash('flash_type', 'success');
                Session::flash('flash_message', 'Mail sent successfully.');
                return Redirect::to('login')->withFlashSuccess(trans('message.reset'));
        }
        return Redirect::to('login');
    }


    public function createTokenCustom($email)
    {
        $user = $this->passwords->getUser(['email' => $email]);
        if (count($user) > 0) {
            $this->deleteExisting($email);
            $token = $this->createNewToken();
            PasswordResets::insert($this->getPayload($email, $token));
            return $token;
        } else {
            return false;
        }
    }

    public function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), '');
    }

    public function getPayload($email, $token)
    {
        return ['email' => $email, 'token' => $token, 'created_at' => new Carbon];
    }

    public function deleteExisting($email)
    {
        return PasswordResets::where('email', $email)->delete();
    }

}