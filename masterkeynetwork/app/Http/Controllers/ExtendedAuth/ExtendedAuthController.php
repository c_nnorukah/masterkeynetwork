<?php

namespace App\Http\Controllers\ExtendedAuth;
use App\Interfaces;
use Illuminate\Support\Facades\Event;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\AssignedRole;
use App\Models\State;
use App\Models\UserPlan;
use App\Repositories\Auth\UserRepository;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Country;
use Illuminate\Support\Facades\Input;
use Mail;
use Auth;
use App\Repositories\AffiliateRepository;
use App\Models\sys_module;
use App\Events\UserWasRegistered;
use App\Services\PaymentService;
use App\User\ReferralLinkGenerator;
use Illuminate\Support\Facades\Cookie;


class ExtendedAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $affiliate_repository;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Interfaces\PaymentServiceInterface $paymentServiceInterface ,AffiliateRepository $affiliate_repository)
    {

        $this->middleware('guest', ['except' => 'getLogout']);
        $this->payment_service = $paymentServiceInterface;
        $this->affiliate_repository = $affiliate_repository;

    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function getRegister()
    {

        $countries = Country::orderByRaw("(CASE WHEN country_name = 'United States' THEN 0 ELSE 1 END) ASC, country_name asc")
            ->with('states')
            ->get();

        $plans = UserPlan::all();
        $sys_manage = sys_module::all();
        return view('auth.register', compact('countries', 'plans', 'sys_manage'));

    }

    public function postLogin(LoginRequest $request, UserRepository $userRepository, Guard $auth)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];
        Session::put('email', $_POST['email']);

        if (!$userRepository->checkLogin($credentials, $auth)) {
            return redirect()->back()->with('flash_danger', trans('message.password'));
        }
        if (!$userRepository->checkUserStatus($credentials)) {
            return redirect()->back()->with('flash_danger', trans('message.userNotActive'));
        }
        $user = $auth->getLastAttempted();
        $auth->login($user, $request->has('memory'));

        if ($request->session()->has('user_id')) {
            $request->session()->forget('user_id');
        }
        if ($request->get('redirect_to') != '') {
            return Redirect::to($request->get('redirect_to'))->withFlashSuccess(trans('message.loggedIn'));
        }
        if ($redirect = Session::get('redirect')) {
            Session::forget('redirect');
            return Redirect::to($redirect)->withFlashSuccess(trans('message.loggedIn'));
        }
        return redirect()->route('dashboard')->withFlashSuccess(trans('message.loggedIn'));

    }

    public function getLogout(Guard $auth)
    {
        $auth = auth()->guard('web');
        $auth->logout();
        Session::flush();
        return redirect()->route('dashboard')->withFlashSuccess(trans('message.loggedOut'));
    }

    /*
     *  Handle an register user for an application
     */

    public function postRegister(RegisterRequest $request, UserRepository $repo, Guard $auth)
    {
        $input               = $request->except('_token');
        if(!empty($input['referral_code'])) {
            $referral_code = $input['referral_code'];
        }
        $input['plan_id'] = Input::get('plan_id');
        //$input = $input_data->except('_token');

        if(!empty($input['plan_id'])){
            $referral_id=null;
            if(!empty($referral_code)){

                $affiliate_data = $this->affiliate_repository->getAffiliaterUserDetails($referral_code);
                $referral_id = (!empty($affiliate_data)) ? $affiliate_data->user_id : '';

            }
            $user_input=array_merge($input,['referral_id'=>$referral_id]);
            $user = $repo->store($user_input);
            if($user && !empty($referral_code)){
                $affiliate_data = $this->affiliate_repository->getAffiliaterUserDetails($referral_code);

                $user['plan'] = UserPlan::where('plan_id', $user->plan_id)->get()->toArray();
                $mail_params = [];
                $mail_params['to'] = $affiliate_data->email;
                $mail_params['subject'] = 'Lead Genrated';
                $template = 'affiliate';

                //$confirmation_link = $confirmation_pre_link;
                $dynamicData = ['#USER_NAME#' => $user->user_name, '#AFF_NAME#' =>$affiliate_data->user_name, '#PLAN_NAME#' => isset($user['plan'][0]['plan_name']) ? $user['plan'][0]['plan_name'] : 'No Plan selected'];
                \EmailService::SendEMail($mail_params, $template, $dynamicData);
            }
            //Event::fire(new UserWasRegistered($user));
            if ($redirect = Session::get('redirect')) {
                Session::forget('redirect');
                return Redirect::to($redirect)->withFlashSuccess(trans('message.loggedOut'));
            }
            $credentials = [
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ];
            if (!empty($user)) {

                $users = [];
                $users['plan'] = UserPlan::where('plan_id', $user->plan_id)->get()->toArray();
                // Assign role
                $data = AssignedRole::create(['user_id' => $user->user_id, 'role_id' => 2]);
            }

            $billing_freq = UserPlan::wherePlanId($user->plan_id)->first();
            if(!empty($billing_freq)){
                if ($billing_freq->price <= 0) {
                    if($this->mail($user)){
                        $this->affiliate_repository->addReferralSale($user->user_id);

                        /*if($_COOKIE['referral_code']){
                            if (isset($_SERVER['HTTP_COOKIE'])) {
                                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                                foreach($cookies as $cookie) {
                                    $parts = explode('=', $cookie);
                                    $name = trim($parts[0]);
                                    setcookie($name, '', time()-1000);
                                    setcookie($name, '', time()-1000, '/');
                                }
                            }
                           // unset($_COOKIE['referral_code'] );
                          //  setcookie('referral_code', '', time() - ( 15 * 60 ) );
                            //setcookie("referral_code",'',time() - ( 15 * 60 ),"/");
                            return redirect('confirmation');
        //                    return redirect('confirmation')->withCookie(cookie('referral_code', '', 45000));
                        }else{
                            return redirect('confirmation');
                        }*/
                    }
                }
            }else{
                return redirect('auth/register')->withFlashDanger(trans('message.planNotSelected'));

            }
            if(!empty($billing_freq)) {

                if ($billing_freq->price > 0) {
                    $price = $billing_freq->price;
                    $plan_name = $billing_freq->plan_name;
                    $plan_description = $plan_name . " Plan Subscription-" . $billing_freq->interval . "-" . $billing_freq->price . "/Month";

                    $input_arr = [
                        'amount' => $price,
                        'description' => $plan_description,
                        'user_id' => $user->user_id
                    ];
                    if (!$this->payment_service->userPayment($input_arr)) {
                        dd("in");

                        if ($this->mail($user)) {

                            $this->affiliate_repository->addReferralSale($user->user_id);

                            $cookie_referral_code=cookie('referral_code');
                            if(isset($_COOKIE['referral_code'])){

                                unset($_COOKIE['referral_code'] );
                                //  dd('in');
                                return redirect('confirmation')->withCookie(Cookie::forget('referral_code'));

                                //  return redirect('confirmation');
                            }else{
                                return redirect('confirmation');
                            }
                        }
                    }
                }
            }else{
                return redirect('auth/register')->withFlashDanger(trans('message.planNotSelected'));

            }
        }
        else{
            return redirect('auth/register')->withFlashDanger(trans('message.planNotSelected'));

        }
    }

    public function mail($user)
    {
        if (!empty($user)) {
            $users = [];
            $users['plan'] = UserPlan::where('plan_id', $user->plan_id)->get()->toArray();
            $mail_params = [];
            $mail_params['to'] = $user->email;
            $mail_params['subject'] = 'User Registration';
            $template = 'user_registration';
            $confirmation_code = $user->confirmation_code;
            $confirmation_pre_link = url('register/verify', $confirmation_code);
            $confirmation_link = $confirmation_pre_link;
            $dynamicData = ['#USER_NAME#' => $user->user_name, '#EMAIL#' => $user->email, '#PLAN_NAME#' => isset($users['plan'][0]['plan_name']) ? $users['plan'][0]['plan_name'] : 'No Plan selected', '#CONFIRMATION_LINK#' => $confirmation_link,'#PLAN_AMOUNT#'=> $users['plan'][0]['price']];
            \EmailService::SendEMail($mail_params, $template, $dynamicData);
            return true;
        }
    }

    public function getForgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function postForgotPassword(Request $request)
    {
        $response = $this->passwords->reset($request->only('email', 'password', 'token'), function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case config('config.password-reset'):
                return redirect(route('account.alert-threshold.index'))->with('status', tfrans($response));

            default:
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
        }
    }

    public function getStateList($country_id)
    {
        $state = State::where('country_id', $country_id)->get();
        $state_list = $state->lists('state_name', 'state_id');
        return view('auth.state', compact('state_list'));
        //echo json_encode($state_list);
        exit;
    }

    public function checkemail()
    {
        $mail = Input::get('emails');
        $model = User::where('email', '=', $mail)->get();
        if ($model->count() >= 1) {
            echo  "false";
            exit;
        }
        echo  "true";
        exit;
    }

    public function checkusername()
    {
        $username = Input::get('username');
        $model = User::where('user_name', '=', $username)->get();
        if ($model->count() >= 1) {
            return "false";
        }
        return "true";
    }

    public function confirmation()
    {
        return view('auth.confirmation');

    }

    public function confirm($confirmation_code)
    {
        if (isset($confirmation_code)) {
            $user = User::whereConfirmationCode($confirmation_code)->first();
            if (!$user) {
                return redirect('login')->with('flash_danger',trans('message.invalid_confirmation_code'));
            }
            $user->status = 1;
            $user->activated= 1;
            $user->confirmation_code = null;
            $user->activation_date = date("Y-m-d H:i:s");
            $user->save();

            return Redirect::route('login')->with('flash_success',trans('message.verified'));
        } else {

            return Redirect::route('login')->with('flash_danger',trans("message.invalid_confirmation_code"));
        }
    }

    public function paymentSucess()
    {

        $token = Input::get('token');
        $user=$this->payment_service->success($token);
        if (!empty($user)) {
            $this->affiliate_repository->addReferralSale($user->user_id);
            if($this->mail($user))
            {
                $this->affiliate_repository->addReferralSale($user->user_id);
                return redirect('confirmation')->withFlashSuccess(trans('message.paymentSucess'));
            }
        } else {
            return redirect('auth/register')->withFlashDanger(trans('message.paymentCancle'));

        }

    }

    public function paymentCancel()
    {
        $token = Input::get('token');
        if ($this->payment_service->cancelPayment($token)) {

            return redirect::to('auth/register');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
//        ]);
    }
}