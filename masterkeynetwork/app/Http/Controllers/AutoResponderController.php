<?php

namespace App\Http\Controllers;

use App\Models\AutoResponder;
use App\Models\EmailTemplate;
use App\Models\Project;
use App\Models\SmtpSetting;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class AutoResponderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->user_id;
        $LandingPageitems = Project::with("user_projects")->where('published', '1')->orderBy('name')->get();
        $EmailTemplate = EmailTemplate::where('created_by', $user_id)->orderBy("subject")->get();
        $smtp = SmtpSetting::orderBy("domain")->get();

        return view('autoresponder.index', compact('LandingPageitems', 'EmailTemplate', 'smtp','user_id'));

    }

    public function getResponderDetail($id)
    {
        $responder_detail = AutoResponder::where("landingpage_id", $id)->get();
        $responder_detail = isset($responder_detail[0]) ? $responder_detail[0] :$responder_detail;
        echo json_encode($responder_detail);
        exit;
    }
    public function getEmailTemplate($id)
    {
        $template_detail = EmailTemplate::find($id);
        $template_detail = isset($template_detail[0]) ? $template_detail[0] : $template_detail;
        echo json_encode($template_detail);
        exit;
    }

    public function saveAutoResponder()
    {
        $id = Input::get("landingpage_id");
      //  echo $id;
        $data = AutoResponder::where("landingpage_id", $id)->get();
       // dd($data);
        if (empty($data)) {
            $data = new AutoResponder();
            $data->landingpage_id = Input::get("landingpage_id");
            $data->smtp_setting_id = Input::get("smtp_setting");
            $data->subject = Input::get("subject");
            $data->email_body = Input::get("email_body");
            $data->save();
        } else {
            $data = [];
            $data['smtp_setting_id'] = Input::get("smtp_setting");
            $data['subject'] = Input::get("subject");
            $data['email_body'] = Input::get("email_body");
            AutoResponder::where("landingpage_id", $id)->update($data);
        }
       echo "Autoresponder data saved successfully";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
