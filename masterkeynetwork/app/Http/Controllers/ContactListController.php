<?php

namespace App\Http\Controllers;


use App\Interfaces\ContactRepositoryInterface;
use App\Models\Landingpage_list;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class ContactListController extends Controller
{
    protected $contact_repository;

    public function __construct(ContactRepositoryInterface $contact_repository)
    {
        $this->contact_repository = $contact_repository;
    }
    public function index()
    {
        $contact_list = $this->contact_repository->getLandingpageList();
        return view('contacts.contact-list', compact('contact_list'));
    }
    public function saveContactList(){
        $id = Input::get("list_id");
        $name = Input::get("name");
        $list = Landingpage_list::firstOrNew(array("list_id"=>$id));
        $list->name = $name;
        $list->user_id = Auth::user()->user_id;
        $list->save();
        Session::flash('flash_success', 'Contact List added successfully');
        return redirect("contact-list");
    }
}
