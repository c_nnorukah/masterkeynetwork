<?php

namespace App\Http\Controllers;

use App\Models\AssignedRole;
use App\Models\PaymentHistory;
use App\Models\UserPlan;
use App\Repositories\Auth\UserRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\Mailer\UseMailer as EmailService;
use DB;
use Auth;
use App\Models\User;
use Mail;

class PaymentReccuringController extends Controller
{
    protected $payment;
    protected $recurring;
    protected $userDetail;

    public function __construct()
    {
        $this->payment = new \App\Payment\Payment();
    }

    public function payment(RegisterRequest $request)
    {
        $billing_freq = UserPlan::wherePlanId($request->plan_id)->first();
        $userRepo = new UserRepository(new User());

        $user = $userRepo->store($request->except('_token'));

        if (!empty($user)) {
            $users = [];
            $users['plan'] = UserPlan::where('plan_id', $user->plan_id)->get()->toArray();

            // Assign role
            $data = AssignedRole::create(['user_id' => $user->user_id, 'role_id' => 2]);

            $greeting_text = "Dear " . $user->first_name . ' ' . $user->last_name . ",";

            $mail_params = [];
            $mail_params['to'] = $user->email;
            $mail_params['subject'] = 'User Registration';
            $template = 'user_registration';
            $confirmation_code=$user->confirmation_code;
            $confirmation_pre_link= url('register/verify',$confirmation_code);
            $confirmation_link='<a href="'.$confirmation_pre_link.'">Click Here</a>';
            $dynamicData = ['#USER_NAME#' => $user->user_name, '#EMAIL#' => $user->email, '#PLAN_NAME#' => isset($users['plan'][0]['plan_name']) ? $users['plan'][0]['plan_name'] : 'No Plan selected','#CONFIRMATION_LINK#'=>$confirmation_link];
            \EmailService::SendEMail($mail_params, $template, $dynamicData);
        }
        if ($billing_freq->price <= 0.00) {
            //auth()->login($user, $request->has('memory'));
            return redirect('confirmation');
        }
        if ($billing_freq->price > 0) {
            $price = $billing_freq->price;
            $input_arr = [
                'amount' => $price,
                'description' => "Recurring Payment($" . $price . $request->get('interval') . ")",
                'user_id' => $user->user_id
            ];
            $express_checkout = $this->payment->setExpressCheckout($input_arr);
            $ack = strtoupper($express_checkout["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $this->payment->redirectToPayPal($express_checkout["TOKEN"]);
            } else {
                User::whereUserId($user->user_id)->delete();
                $error_message = urldecode($express_checkout["L_SHORTMESSAGE0"]);
                Session::flash('error', $error_message);
                return redirect('auth/register');
            }
        }
        exit;
    }

    public function success()
    {
        $token = Input::get('token');
        $shipping_detail = $this->payment->getShippingDetails($token);
        $user = User::with(['plan'])->whereUserId($shipping_detail['CUSTOM'])->first();

        $shipping_detail['user_id'] = $shipping_detail['CUSTOM'];
        $shipping_detail['billing_period'] = trim($user->plan->interval);
        $shipping_detail['billing_frequency'] = trim($user->plan->days);
        $shipping_detail['desc'] = "Recurring Payment($" . $user->plan->price . $user->plan->interval . ")";
        $recurring_profile = $this->payment->createRecurringPaymentsProfile($shipping_detail);

        $res_ack = strtoupper($recurring_profile["ACK"]);

        if ($res_ack == "SUCCESS" || $res_ack == "SUCCESSWITHWARNING") {
            $payment_success = $this->payment->confirmPayment($shipping_detail);

            $user = User::whereUserId($user->user_id)->first();
            //auth()->login($user);

            $payment_history = new PaymentHistory();
            $payment_history->profile_id = $recurring_profile['PROFILEID'];
            $payment_history->user_id = $user->user_id;
            $payment_history->plan_id = $user->plan_id;
            $payment_history->amount = $user->plan->price;
            $payment_history->transaction_response = json_encode($payment_success);
            $payment_history->start_date = date("Y-m-d H:i:s");
            $payment_history->save();

            //Session::flash('flash_success', 'You are successfully subscribed for the site with payment successfully.');
            // return redirect('dashboard');
            return redirect('auth.confirmation');

        } else {
            User::whereUserId($user->user_id)->delete();
            Session::flash('flash_danger', 'Something went wrong !');
            return redirect('auth/register');
        }
    }

    public function cancelPayment()
    {
        $token = Input::get('token');
        $shipping_detail = $this->payment->getShippingDetails($token);
        User::whereUserId($shipping_detail['CUSTOM'])->delete();
        return redirect::to('auth/register');
    }
}
