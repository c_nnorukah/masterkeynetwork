<?php
namespace App\Payment;

class Payment
{

    protected $description = '';

    function setExpressCheckout($input_arr)
    {
        $currency_code_type = config('payment.CURRENCY_CODE_TYPE');
        $payment_type = config('payment.PAYMENT_TYPE');
        $return_url = url('/') . '/success';
        $cancel_url = url('/') . '/paypal/cancel';
        $payment_amount = $input_arr['amount'];
        $nvpstr = "&AMT=" . $payment_amount;
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $payment_type;
        $nvpstr = $nvpstr . "&BILLINGAGREEMENTDESCRIPTION=" . urlencode($input_arr['description'] );
        //urlencode($input_arr['plan_name'];
        $nvpstr = $nvpstr . "&BILLINGTYPE=RecurringPayments";
        $nvpstr = $nvpstr . "&RETURNURL=" . $return_url;
        $nvpstr = $nvpstr . "&CANCELURL=" . $cancel_url;
        $nvpstr = $nvpstr . "&CURRENCYCODE=" . $currency_code_type;
        $nvpstr = $nvpstr . "&CUSTOM=" . $input_arr['user_id'];

        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            $token = urldecode($resArray["TOKEN"]);
            session(['TOKEN' => $token]);
        }
        return $resArray;
    }

    function getShippingDetails($token)
    {
        session(['TOKEN' => $token]);
        $nvpstr = "&TOKEN=" . $token;
        $resArray = $this->hash_call("GetExpressCheckoutDetails", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            return $resArray;
        }
        return $resArray;
    }

    function confirmPayment($data)
    {
        /* Gather the information to make the final call to
           finalize the PayPal payment.  The variable nvpstr
           holds the name value pairs
           */

        //Format the other parameters that were stored in the session from the previous calls
        $token = urlencode($data['TOKEN']);
        $payerID = urlencode($data['PAYERID']);
        $currency_code_type = config('payment.CURRENCY_CODE_TYPE');
        $payment_type = config('payment.PAYMENT_TYPE');
        $serverName = urlencode($_SERVER['REMOTE_ADDR']);

        $nvpstr = '&TOKEN=' . $token . '&PAYERID=' . $payerID . '&PAYMENTACTION=' . $payment_type . '&AMT=' . $data['AMT'];
        $nvpstr .= '&CURRENCYCODE=' . $currency_code_type . '&IPADDRESS=' . $serverName;


        /* Make the call to PayPal to finalize payment
           If an error occured, show the resulting errors
           */
        $resArray = $this->hash_call("DoExpressCheckoutPayment", $nvpstr);

        /* Display the API response back to the browser.
           If the response from PayPal was a success, display the response parameters'
           If the response was an error, display the errors received using APIError.php.
           */
        $ack = strtoupper($resArray["ACK"]);

        return $resArray;
    }

    function createRecurringPaymentsProfile($data)
    {
        $currency_code_type = config('payment.CURRENCY_CODE_TYPE');

        $nvpstr = "&TOKEN=" . urlencode($data['TOKEN']);
        $nvpstr .= "&SHIPTONAME=" . urlencode($data['SHIPTONAME']);
        $nvpstr .= "&SHIPTOSTREET=" . urlencode($data['SHIPTOSTREET']);
        $nvpstr .= "&SHIPTOCITY=" . urlencode($data['SHIPTOCITY']);
        $nvpstr .= "&SHIPTOSTATE=" . urlencode($data['SHIPTOSTATE']);
        $nvpstr .= "&SHIPTOZIP=" . urlencode($data['SHIPTOZIP']);
        $nvpstr .= "&SHIPTOCOUNTRY=" . urlencode($data['SHIPTOCOUNTRYCODE']);
        $nvpstr .= "&PROFILESTARTDATE=" . urlencode(date('Y-m-d\TH:i:s'));

        $nvpstr.=(isset($data['desc']) && $data['desc']!='') ? "&DESC=".urlencode($data['desc']) : "&DESC=";
        $nvpstr.="&BILLINGPERIOD=".$data['billing_period'];
        $nvpstr.="&BILLINGFREQUENCY=".$data['billing_frequency'];
        $nvpstr .= "&AMT=". $data['AMT'];
        $nvpstr .= "&CURRENCYCODE=".$currency_code_type;
        $nvpstr .= "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        //'---------------------------------------------------------------------------
        //' Make the API call and store the results in an array.
        //'	If the call was a success, show the authorization details, and provide
        //' 	an action to complete the payment.
        //'	If failed, show the error
        //'---------------------------------------------------------------------------

        $resArray = $this->hash_call("CreateRecurringPaymentsProfile", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        return $resArray;
    }

    function updateRecurringPaymentsProfile($data)
    {
        $currency_code_type = config('payment.CURRENCY_CODE_TYPE');

        $nvpstr = "&DESC=" . urlencode($data['desc']);
        $nvpstr .= "&PROFILESTARTDATE=" . urlencode(date('Y-m-d\TH:i:s'));
        $nvpstr .= "&BILLINGPERIOD=" . $data['billing_period'];
        $nvpstr .= "&BILLINGFREQUENCY=" . $data['billing_frequency'];
        $nvpstr .= "&AMT=" . $data['amt'];
        $nvpstr .= "&PROFILEID=" . $data['profileid'];
        $nvpstr .= "&CURRENCYCODE=" . $currency_code_type;
        $nvpstr .= "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        //'---------------------------------------------------------------------------
        //' Make the API call and store the results in an array.
        //'	If the call was a success, show the authorization details, and provide
        //' 	an action to complete the payment.
        //'	If failed, show the error
        //'---------------------------------------------------------------------------

        $resArray = $this->hash_call("UpdateRecurringPaymentsProfile", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        return $resArray;
    }

    function getRecurringPaymentsProfileDetails($profile_id)
    {
        $currency_code_type = config('payment.CURRENCY_CODE_TYPE');

        $nvpstr = "&PROFILEID=" . $profile_id;
        $nvpstr .= "&CURRENCYCODE=" . $currency_code_type;
        $nvpstr .= "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        //'---------------------------------------------------------------------------
        //' Make the API call and store the results in an array.
        //'	If the call was a success, show the authorization details, and provide
        //' 	an action to complete the payment.
        //'	If failed, show the error
        //'---------------------------------------------------------------------------

        $resArray = $this->hash_call("GetRecurringPaymentsProfileDetails", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        return $resArray;
    }

    /**
     * '-------------------------------------------------------------------------------------------------------------------------------------------
     * hash_call: Function to perform the API call to PayPal using API signature
     *
     * @methodName is name of API  method.
     * @nvpStr     is nvp string.
     *             returns an associtive array containing the response from the server.
     *             '-------------------------------------------------------------------------------------------------------------------------------------------
     */
    function hash_call($methodName, $nvpStr)
    {
        $user_name = config('payment.USERNAME');
        $password = config('payment.PASSWORD');
        $signature = config('payment.SIGNATURE');
        $sbn_code = config('payment.BNCODE');
        $sandbox_url = config('payment.SANDBOX_URL');
        $version = config('payment.VERSION');

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sandbox_url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);


        //NVPRequest for submitting to server
        $nvpreq = "METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($version) . "&PWD=" . urlencode($password) . "&USER=" . urlencode($user_name) . "&SIGNATURE=" . urlencode($signature) . $nvpStr . "&BUTTONSOURCE=" . urlencode($sbn_code);

        //setting the nvpreq as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        //getting response from server
        $response = curl_exec($ch);

        //convrting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);
        $nvpReqArray = $this->deformatNVP($nvpreq);
        session(['nvpReqArray' => $nvpReqArray]);

        if (curl_errno($ch)) {
            // moving to display page to display curl errors
            session(['curl_error_no' => curl_errno($ch)]);
            session(['curl_error_msg' => curl_error($ch)]);

            //Execute the Error handling module to display errors.
        } else {
            //closing the curl
            curl_close($ch);
        }

        return $nvpResArray;
    }

    /*'----------------------------------------------------------------------------------
     Purpose: Redirects to PayPal.com site.
     Inputs:  NVP string.
     Returns:
    ----------------------------------------------------------------------------------
    */
    function redirectToPayPal($token)
    {
        $paypal_url = config('payment.PAYPAL_URL') . $token;
        header("Location: " . $paypal_url);
    }


    /*'----------------------------------------------------------------------------------
     * This function will take NVPString and convert it to an Associative Array and it will decode the response.
      * It is usefull to search for a particular key and displaying arrays.
      * @nvpstr is NVPString.
      * @nvpArray is Associative Array.
       ----------------------------------------------------------------------------------
      */
    function deformatNVP($nvpstr)
    {
        $intial = 0;
        $nvpArray = array();

        while (strlen($nvpstr)) {
            //postion of Key
            $keypos = strpos($nvpstr, '=');
            //position of value
            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

            /*getting the Key and Value values and storing in a Associative Array*/
            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
            //decoding the respose
            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }

}