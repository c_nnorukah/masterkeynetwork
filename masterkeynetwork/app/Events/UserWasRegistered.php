<?php

namespace App\Events;

use App\Models\User;

class UserWasRegistered extends Event
{

    public $user;


    /**
     * Create a new event instance.
     *
     * @param  Customer $customer
     * @param  $password_needs_to_set
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }
}