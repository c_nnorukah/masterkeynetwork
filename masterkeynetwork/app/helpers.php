<?php
/**
 * Global helpers file with misc functions
 **/
if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}
function format_date($date, $format = "m/d/Y")
{
    return !empty($date) && $date != "0000-00-00" && $date != "0000-00-00 00:00:00" ? date($format, strtotime($date)) : "";
}
function format_price($price)
{
    return "$ ".number_format ( $price,2 );
}