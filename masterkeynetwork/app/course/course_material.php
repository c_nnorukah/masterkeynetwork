<?php
namespace App\course;

/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 3/23/2016
 * Time: 2:50 PM
 */
class course_material
{

    function parse_youtube($link)
    {

        $regexstr = '~
            # Match Youtube link and embed code
            (?:                             # Group to match embed codes
                (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                |(?:                        # Group to match if older embed
                    (?:<object .*>)?      # Match opening Object tag
                    (?:<param .*</param>)*  # Match all param tags
                    (?:<embed [^>]*src=")?  # Match embed tag to the first quote of src
                )?                          # End older embed code group
            )?                              # End embed code groups
            (?:                             # Group youtube url
                https?:\/\/                 # Either http or https
                (?:[\w]+\.)*                # Optional subdomains
                (?:                         # Group host alternatives.
                youtu\.be/                  # Either youtu.be,
                | youtube\.com              # or youtube.com
                | youtube-nocookie\.com     # or youtube-nocookie.com
                )                           # End Host Group
                (?: /watch\?v= | \S*[^\w\-\s])?           # Extra stuff up to VIDEO_ID
                ([\w\-]{11})                # $1: VIDEO_ID is numeric
                [^\s]*                      # Not a space
            )                               # End group
            "?                              # Match end quote if part of src
            (?:[^>]*>)?                       # Match any extra stuff up to close brace
            (?:                             # Group to match last embed code
                </iframe>                 # Match the end of the iframe
                |</embed></object>          # or Match the end of the older embed
            )?                              # End Group of last bit of embed code
            ~ix';

        preg_match($regexstr, $link, $matches);
        if (isset($matches[1]))
            return $matches[1];
        return 0;

    }

    function parse_vimeo($link)
    {

        $regexstr = '~
            # Match Vimeo link and embed code
            (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
            (?:                         # Group vimeo url
                https?:\/\/             # Either http or https
                (?:[\w]+\.)*            # Optional subdomains
                vimeo\.com              # Match vimeo.com
                (?:[\/\w]*\/?)?   # Optional video sub directory this handles groups links also
                \/                      # Slash before Id
                ([0-9]+)                # $1: VIDEO_ID is numeric
                [^\s]*                  # Not a space
            )                           # End group
            "?                          # Match end quote if part of src
            (?:[^>]*></iframe>)?        # Match the end of the iframe
            (?:<p>.*</p>)?              # Match any title information stuff
            ~ix';

        preg_match($regexstr, $link, $matches);

        if (isset($matches[1]))
            return $matches[1];
        return 0;

    }

    public function get_youtube_video_info($output, $info = '')
    {
        if (isset($output['items'][0])) {
            $path = &$output['items'][0];
            switch ($info) {
                case 'title':
                    $output = $path['snippet']['title'];
                    break;
                case 'description':
                    $output = $path['snippet']['description'];
                    break;
                case 'thumbnail_small':
                    $output = $path['snippet']['thumbnails']['default']['url'];
                    break;
                case 'thumbnail_medium':
                    $output = $path['snippet']['thumbnails']['medium']['url'];
                    break;
                case 'thumbnail_large':
                    $output = $path['snippet']['thumbnails']['high']['url'];
                    break;
                default:
                    $output = '';
                    break;
            }
        }
        return $output;
    }

    function get_video($vid, $type = 'youtube')
    {
        $api_key = env('GOOGLE_API_KEY');
        //$url = "http://gdata.youtube.com/feeds/api/videos/$vid?v=2&alt=json&feature=related";
        $url = "https://www.googleapis.com/youtube/v3/videos?id=$vid&key=$api_key&part=snippet";
        if ($type == 'vimeo') {
            $url = "http://vimeo.com/api/v2/video/$vid.json";
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output, $assoc = true);
        return $output;
    }

    function save_video_url($video_url)
    {
        //$video_url = $this->input->post("video_url");
        //$video_id = $video_thumb = $video_embed = "";
        $image_type = "video";
        if (strstr($video_url, 'youtube.com', true) || strstr($video_url, 'youtu.be', true)) {
            $video_id = $this->parse_youtube($video_url);
            if ($video_id) {
                $video_data = $this->get_video($video_id, 'youtube');

                if (!empty($video_data)) {
                    $video_thumb = $this->get_youtube_video_info($video_data, 'thumbnail_small');
                    //dd($video_data);
                    //dd($video_thumb);
                    $video_embed = "https://www.youtube.com/v/" . $video_id;
                    $image_type = "youtube_video";
                } else {
                    $video_id = 0;
                }
            }
        } else if (strstr($video_url, 'vimeo.com', true)) {
            $video_id = $this->parse_vimeo($video_url);
            if ($video_id) {
                $video_data = $this->get_video($video_id, 'vimeo');
                if (!empty($video_data)) {
                    $video_thumb = $video_data[0]['thumbnail_small'];
                    $video_embed = "https://player.vimeo.com/video/" . $video_id . "?portrait=0";
                    $image_type = "vimeo_video";
                } else {
                    $video_id = 0;
                }
            }
        }

        $image_name = $video_id;
        $image_path = $video_url;
        $suc = true;
        $msg = 'success';
        $thumb_img = $video_thumb;

        $res = array('name' => $image_name, 'type' => $image_type, 'new_thumb' => $thumb_img, 'success' => $suc, 'msg' => $msg);
        //echo json_encode($res);
        return $res;
        exit;
    }

    function parse_soundcloud($video_url)
    {
        $client_id = env('SOUNDCLOUD_CLIENT_ID');
        $url = "http://api.soundcloud.com/resolve?url=$video_url&client_id=$client_id";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output, $assoc = true);
        $id = explode("/", $output['location']);
        $video_id = explode("?", $id[4]);
        if (isset($video_id[0]))
            return $video_id[0];
        return 0;

    }

    function sound_cloud_data($video_url)
    {
        $client_id = env('SOUNDCLOUD_CLIENT_ID');
        $video_id = $this->parse_soundcloud($video_url);
        $data = "http://api.soundcloud.com/tracks/$video_id[0]?client_id=$client_id";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $data);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $op = curl_exec($ch);
        curl_close($ch);
        $op = json_decode($op, $assoc = true);
        return $op;
        exit;
    }


}