<?php

namespace App\User;

use App\Interfaces\AffiliateRepositoryInterface;
use App\Events\CustomerWasRegistered;
use App\Interfaces\CampaignRepositoryInterface;

class ReferralLinkGenerator
{
    protected $affiliate_repository;
    protected $campaign_repository;

    public function __construct(AffiliateRepositoryInterface $affiliate_repository, CampaignRepositoryInterface $campaign_repository)
    {
        $this->affiliate_repository = $affiliate_repository;
        $this->campaign_repository = $campaign_repository;
    }

    /**
     * Handle the event.
     *
     * @param  CustomerWasRegistered $event
     * @return void
     */
    public function handle(CustomerWasRegistered $event)
    {
        $affiliate = $this->affiliate_repository->saveAffiliate($event->user);
        $this->campaign_repository->saveCampaign($affiliate, $event->user);
    }
}