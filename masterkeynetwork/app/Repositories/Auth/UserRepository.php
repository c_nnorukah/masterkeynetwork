<?php namespace App\Repositories\Auth;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class UserRepository
{

	protected $users;

	function __construct(User $user)
	{
		$this->model = $user;
	}

	public function checkLogin($Inputs, $auth)
	{
		if(!$auth->validate($Inputs))
		{
			return false;
		}
		else {
			return true;
		}
	}
	public function checkUserStatus($Inputs)
	{
		$user = User::where('email', $Inputs['email'])->first();
		if (count($user) > 0 && $user->status == 0) {
			return false;
		} else if (count($user) > 0 && $user->status == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function store($user_input)
	{
		$user = new User($user_input);
		/*$var=cookie('referral_code');
		if(!empty($var)){
			//dd($var);
			if(cookie::forget('referral_code')){
				//dd("in");
			}
		}*/

		$user->confirmation_code= str_random(30);
		$user->referral_code=trim($user->first_name).trim($user->last_name).str_random(5);
		$user->save();

		return $user;
	}

	public function setRole($Inputs)
	{

	}
}