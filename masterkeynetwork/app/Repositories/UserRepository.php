<?php namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\PaymentHistory;
use App\Models\User;


class UserRepository implements UserRepositoryInterface
{
    public function getList($sort_by, $sort_order, $limit, $criteria)
    {
        $user = User::query();
        $user->leftjoin("user_plans", function ($join) {
            $join->on("users.plan_id", "=", "user_plans.plan_id");
        });
        $user->leftjoin("assigned_roles", function ($join) {
            $join->on("assigned_roles.user_id", "=", "users.user_id");
        });
        $user->leftjoin("roles", function ($join) {
            $join->on("assigned_roles.role_id", "=", "roles.role_id");
        });
        $sort_by = $sort_by == "user_id" ? "users.user_id" : $sort_by;
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "name") {
            $user->where(function ($query) use ($criteria) {
                $query->where('first_name', 'like', "%" . trim($criteria["name"]) . "%")
                    ->orWhere('last_name', 'like', "%" . trim($criteria["name"]) . "%");
            });
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "email") {
            $user->where("email", "like", "%" . trim($criteria["email"]) . "%");
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "role") {
            $user->where("assigned_roles.role_id", "=", $criteria["role"]);
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "plan") {
            $user->where("users.plan_id", "=", $criteria["plan"]);
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "status") {
            $user->where("status", "=", $criteria["status"]);
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "signup_date") {
            $user->where("users.created_at", ">=", format_date($criteria["from_signup"],"Y-m-d H:i:s"));
            $user->where("users.created_at", "<=", format_date($criteria["to_signup"],"Y-m-d H:i:s"));
        }

        $user->orderBy($sort_by, $sort_order);
        $user->groupBy('users.user_id');
        return $user->orderBy($sort_by, $sort_order)->paginate($limit);
        // dd(\DB::getQueryLog());
    }

    public function delete($id)
    {
        return User::find($id)->delete();
    }

    public function getDetailById($id)
    {
        return User::where("user_id", $id)->with(["plan"])->get();
    }
    public function getTransactionHistory($id,$limit)
    {
        return PaymentHistory::where("user_id", $id)->paginate($limit);
    }
    public function getMinTransactionDate($id)
    {
        return PaymentHistory::where("user_id", $id)->min("created_at");
    }
    public function getMaxTransactionDate($id)
    {
        return PaymentHistory::where("user_id", $id)->max("created_at");
    }
}