<?php namespace App\Repositories;

use Anchu\Ftp\Facades\Ftp;
use App\Interfaces\UploadManagerRepositoryInterface;
use App\Models\Project;
use App\Models\Project_ftp_detail;
use App\Models\User_project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class UploadManagerRepository implements UploadManagerRepositoryInterface
{
    public function getLandingPages()
    {
        $user_id = Auth::user()->user_id;
        return User_project::where("user_id", $user_id)->with("projects")->get();
    }

    public function getLandingPageDetail($id)
    {
        return Project_ftp_detail::where("project_id", $id)->get();
    }

    public function saveSetting($id)
    {
        $ftp_detail = Project_ftp_detail::where("project_id", $id)->get();
        if (empty($ftp_detail[0])) {
            $ftp_detail = New Project_ftp_detail();
            $ftp_detail->project_id = $id;
            $ftp_detail->host_name = Input::get("host_name");
            $ftp_detail->protocol = Input::get("protocol");
            $ftp_detail->port_number = Input::get("port_number");
            $ftp_detail->username = Input::get("username");
            $ftp_detail->password = Input::get("password");
            $ftp_detail->remote_directory = Input::get("remote_location");
            $ftp_detail->save();
        } else {
            $ftp_detail = [];
            $ftp_detail['host_name'] = Input::get("host_name");
            $ftp_detail['protocol'] = Input::get("protocol");
            $ftp_detail['port_number'] = Input::get("port_number");
            $ftp_detail['username'] = Input::get("username");
            $ftp_detail['password'] = Input::get("password");
            $ftp_detail['remote_directory'] = Input::get("remote_location");
            Project_ftp_detail::where('project_id', $id)->update($ftp_detail);
        }
        $project = Project::find($id);
        $from_path = base_path("landingpage/architect/storage/exports/" . $project->id .
            "-" . $project->name);
        if (!file_exists($from_path)) {
            echo "No files exist for selected project.";
            exit;
        }
        if (Input::get("protocol") == "ftp") {
            $this->ftpUpload($from_path);
        } else if (Input::get("protocol") == "sftp") {
            $this->sftpUpload($from_path);
        }
        Project::where('id', $id)->update(array("published_at" => date("Y-m-d H:i:s")));
    }

    public function reformat_name($file)
    {
        $file = str_replace('"', "", $file);
        $file = str_replace("'", "", $file);
        return $file;
    }

    public function ftpUpload($fileFrom)
    {
        try {
            ini_set('max_execution_time', 0);
            $ftp_config = array(
                'host' => Input::get("host_name"),
                'username' => Input::get("username"),
                'password' => Input::get("password"),
                'passive' => true,
            );
            Config::set('ftp.connections.setting', $ftp_config);
            $remote_location = Input::get("remote_location");


            if (empty(FTP::connection("setting")->getDirListingDetailed
            ($remote_location))
            ) {
                echo "Destination directory not found";
                exit;
            }
            $files = File::allFiles($fileFrom);
            $exist_directory = [];

            if (!empty($files)) {
                $files = File::allFiles($fileFrom);
                $file = explode("exports/", (string)$files[0]);
                $file = str_replace("\\", "/", $file[1]);
                $basedir = $this->reformat_name(explode("/", $file)[0]);
                FTP::connection("setting")->delete($remote_location . "/" . $basedir);
                $mkdir = $remote_location . "/" . $basedir;
                FTP::connection("setting")->makeDir($mkdir);
                $exist_directory[] = $mkdir;
                foreach ($files as $file) {
                    $localfile = $file = (string)$file;
                    $file = explode("exports/", $file);
                    $file = str_replace("\\", "/", $file[1]);
                    $mkdir = dirname($remote_location . "/" . $file);
                    if (in_array($mkdir, $exist_directory) == false) {
                        FTP::connection("setting")->makeDir($mkdir);
                    }
                    $exist_directory[] = $mkdir;
                    if (File::isFile($localfile)) {
                        $dest_file = $remote_location . "/" . $file;
                        FTP::connection("setting")->changeDir($mkdir);
                        FTP::connection("setting")->uploadFile($localfile, $dest_file);
                    }
                }
            } else {
                echo "No files exist for selected project.";
                exit;
            }
        } catch (\RuntimeException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function sftpUpload($fileFrom)
    {
        try {
            ini_set('max_execution_time', 0);
            $sftp_config = array(
                'host' => Input::get("host_name") . ":" . Input::get("port_number"),
                'username' => Input::get("username"),
                'password' => Input::get("password"),
                'passive' => true,
            );
            $remote_location = Input::get("remote_location");
            Config::set('remote.connections.production', $sftp_config);
            \Collective\Remote\RemoteFacade::run("cd '" . $remote_location . "'", function ($line) {
                if (strpos($line, "not") !== false || strpos($line, "can't") !== false) {
                    echo "Destination directory not found.";
                    exit;
                }
            });
            $files = File::allFiles($fileFrom);
            $cmd = [];
            $exist_directory = [];
            $file = explode("exports/", (string)$files[0]);
            $file = str_replace("\\", "/", $file[1]);
            $basedir = $this->reformat_name(explode("/", $file)[0]);
            $cmd[] = 'rm -rf "' . $remote_location . "/" . $basedir . '"';
            if (!empty($files)) {
                $mkdir = 'mkdir  "' . $remote_location . "/" . $basedir . '"';
                $cmd[] = $mkdir;
                $exist_directory[] = $mkdir;
                foreach ($files as $file) {
                    $localfile = $file = (string)$file;
                    $file = explode("exports/", $file);
                    $file = str_replace("\\", "/", $file[1]);
                    $file = $this->reformat_name($file);
                    $mkdir = 'mkdir  "' . dirname($remote_location . "/" . $file) . '"';
                    if (in_array($mkdir, $exist_directory) == false) {
                        $cmd[] = $mkdir;
                    }
                    $exist_directory[] = $mkdir;
                    if (File::isFile($localfile)) {
                        $cmd[] = 'cd "' . dirname($remote_location . '/' . $file) . '"';
                        $cmd[] = 'dir > "' . basename($remote_location . '/' . $file) . '"';
                    }
                }
                $a = "";
                \Collective\Remote\RemoteFacade::into('production')->run($cmd, function ($line) {
                    $a = $line;
                });
                foreach ($files as $file) {
                    $localfile = $file = (string)$file;
                    $file = explode("exports/", $file);
                    $file = str_replace("\\", "/", $file[1]);
                    \Collective\Remote\RemoteFacade::into('production')->put($localfile,
                        $remote_location . "/$file", 1);
                }
            } else {
                echo "No files exist for selected project.";
                exit;
            }
        } catch (\RuntimeException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

}