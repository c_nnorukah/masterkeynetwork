<?php namespace App\Repositories;

use App\Interfaces\ProfileRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\User_academic_positions;
use App\Models\User_education_details;
use App\Models\User_honors;
use App\Models\User_Portfolio;
use App\Models\User_media;


class ProfileRepository implements ProfileRepositoryInterface
{
    public function getAllUserData()
    {
        return User::with(["user_portfolios", "user_portfolios.user_medias", 'user_medias', "User_academic_positions", "User_education_details", "User_honors", "User_honors.user_medias"])->where('user_id', Auth::user()->user_id)->get();
    }

    Public function saveAboutMe()
    {
        $file_name_cv = Input::get('user_cv');
        if (Input::get('user_cv') == "") {
            $file_name_cv = time() . Input::file('txtCv')->getClientOriginalName();
            $file_extension_cv = Input::file('txtCv')->getClientOriginalExtension();

            if ($file_extension_cv == 'doc' || $file_extension_cv == 'docx' || $file_extension_cv == 'pdf') {
                Input::file('txtCv')->move("profile/cv/", $file_name_cv);
            } else {
                echo "File extension is not valid.Please upload only pdf and doc file.";
                exit;
            }
        }
        $image_id = Input::get('image_id');
        if (Input::get('image_id') == "") {
            $file_name = time() . Input::file('txtPP')->getClientOriginalName();
            $file_extension = Input::file('txtPP')->getClientOriginalExtension();

            if ($file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'gif') {
                Input::file('txtPP')->move("profile/Profile_images/", $file_name);

            } else {
                echo "File extension is not valid. Please upload only jpg,png and gif file.";
                exit;
            }

            $img = new User_media();
            $img->image_name = $file_name;
            $img->image_path = "profile/Profile_images/" . $file_name;
            $img->type = 1;
            $img->save();
            $image_id = $img->id;
        }

        $user = User::find(Auth::user()->user_id);
        $user->user_cv = $file_name_cv;
        $user->bio = Input::get('textareaBio');
        $user->first_name = Input::get('txtName');
        $user->image_id = $image_id;
        $user->save();
    }

    Public function savePortfolio()
    {
        $image_id = Input::get('image_id');
        if (Input::get('image_id') == "") {
            $file_name = time() . Input::file('image')->getClientOriginalName();
            $file_extension = Input::file('image')->getClientOriginalExtension();

            if ($file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'gif') {
                Input::file('image')->move("profile/portfolio_images/", $file_name);

            } else {
                echo "file extension is not valid";
                exit;
            }
            $img = new User_media();
            $img->image_name = $file_name;
            $img->image_path = "profile/portfolio_images/" . $file_name;
            $img->type = 1;
            $img->save();
            $image_id = $img->id;
        }

        $id = Input::get("id");
        if ($id != 0) {
            $portfolio = User_Portfolio::find($id);
        } else {
            $portfolio = New User_Portfolio();
        }

        $portfolio->title = Input::get('title');
        $portfolio->summary = Input::get('summary');
        $portfolio->user_id = Auth::user()->user_id;
        $portfolio->image_id = $image_id;
        $portfolio->save();
    }

    public function saveContactInfo()
    {
        if (Input::get('textareaContact') == "" || Input::get('numPhn') == "" || Input::get('txtMail') == ""
            || Input::get('txtSkype') == "" || Input::get('txtLinkedin') == "" || Input::get('txtTwitter') == ""
        ) {
            echo "*Field is blank , Please Fill all Required Details in Contact Info.";
            exit;
        }

        $user = User::find(Auth::user()->user_id);
        $user->first_name = Input::get('txtName');
        $user->contact_summary = Input::get('textareaContact');
        $user->phone_no = Input::get('numPhn');
        $user->user_name = Input::get('txtMail');
        $user->skype_id = Input::get('txtSkype');
        $user->linked_id = Input::get('txtLinkedin');
        $user->twitter_id = Input::get('txtTwitter');
        $user->save();
    }

    public function getUserByToken($id)
    {
        return User::with(["user_portfolios", "user_portfolios.user_medias", "user_medias", "User_academic_positions", "User_education_details", "User_honors", "User_honors.user_medias"])->where('usertoken', $id)->get();
    }

    public function saveAcademicPositions()
    {
        $id = Input::get("id");
        if ($id != 0) {
            $academic = User_academic_positions::find($id);
        } else {
            $academic = New User_academic_positions();
        }

        $academic->title = Input::get('position');
        $academic->summary = Input::get('details');
        $academic->from_date = Input::get('from_date');
        $academic->to_date = Input::get('to_date');
        $academic->user_id = Auth::user()->user_id;
        $academic->save();
    }

    public function saveEducation()
    {
        $id = Input::get("id");
        if ($id != 0) {
            $education = User_education_details::find($id);
        } else {
            $education = New User_education_details();
        }

        $education->degree = Input::get('degree');
        $education->title = Input::get('title');
        $education->university = Input::get('university');
        $education->passing_year = Input::get('passing_year');
        $education->user_id = Auth::user()->user_id;
        $education->save();
    }

    public function saveHonors()
    {
        $image_id = Input::get('image_id');
        if (Input::get('image_id') == "") {

            $file_name = time() . Input::file('honor_image')->getClientOriginalName();
            $file_extension = Input::file('honor_image')->getClientOriginalExtension();

            if ($file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'gif') {
                Input::file('honor_image')->move("profile/honor_images/", $file_name);

            } else {
                echo "file extension is not valid";
                exit;
            }
            $img = new User_media();
            $img->image_name = $file_name;
            $img->image_path = "profile/honor_images/" . $file_name;
            $img->type = 4;
            $img->save();
            $image_id = $img->id;
        }

        $id = Input::get("id");
        if ($id != 0) {
            $honor = User_honors::find($id);
        } else {
            $honor = New User_honors();
        }

        $honor->title = Input::get('name_of_honor');
        $honor->details = Input::get('honor_details');
        $honor->date = Input::get('date');
        $honor->image_id = $image_id;
        $honor->user_id = Auth::user()->user_id;
        $honor->save();
    }

    public function getUserPortfolio()
    {
        return User::with(["user_portfolios"])->where('user_id', Auth::user()->user_id)->get();
    }

    public function getPortfolioByID($id)
    {
        return User_Portfolio::with("user_medias")->find($id);
    }

    public function getUserAcademic()
    {
        return User::with(["User_academic_positions"])->where('user_id', Auth::user()->user_id)->get();
    }

    public function getAcademicByID($id)
    {
        return User_academic_positions::find($id);
    }

    public function getUserEducation()
    {
        return User::with(["User_education_details"])->where('user_id', Auth::user()->user_id)->get();
    }

    public function getEducationByID($id)
    {
        return User_education_details::find($id);
    }

    public function getUserHonor()
    {
        return User::with(["User_honors"])->where('user_id', Auth::user()->user_id)->get();
    }

    public function getHonorByID($id)
    {
        return User_honors::with("user_medias")->find($id);
    }
    public function deletePortfolio($id)
    {
        return User_Portfolio::find($id)->delete();
    }
    public function deleteAcademic($id)
    {
        return User_academic_positions::find($id)->delete();
    }
    public function deleteEducation($id)
    {
        return User_education_details::find($id)->delete();
    }
    public function deleteAward($id)
    {
        return User_honors::find($id)->delete();
    }
}