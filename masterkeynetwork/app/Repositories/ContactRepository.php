<?php namespace App\Repositories;

use App\Interfaces\ContactRepositoryInterface;
use App\Models\Contacts;
use App\Models\Email_campaign;
use App\Models\Email_campaign_contact;
use App\Models\Landingpage_list;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ContactRepository implements ContactRepositoryInterface
{
    public function getList($sort_by, $sort_order, $limit, $criteria, $user_id)
    {
        $user = Contacts::query()->with("contact_list");
        $user = $user->select("contacts.*");
        $user->leftjoin("users", function ($join) {
            $join->on("users.user_id", "=", "contacts.user_id");
        });
        $user->leftjoin("landingpage_list", function ($join) {
            $join->on("landingpage_list.list_id", "=", "contacts.list_id");
        });

        $sort_by = $sort_by == "user_id" ? "users.user_id" : $sort_by;
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "name") {
            $user->where(function ($query) use ($criteria) {
                $query->where('contacts.first_name', 'like', "%" . trim($criteria["name"]) . "%")
                    ->orWhere('contacts.last_name', 'like', "%" . trim($criteria["name"]) . "%");
            });
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "email") {
            $user->where("contacts.email", "like", "%" . trim($criteria["email"]) . "%");
        }
        if (!empty($user_id)) {
            $user->where("contacts.user_id", "=", $user_id);
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "source_app_name") {
            $user->where("source_app_name", "like", "%" . trim($criteria["source_app_name"]) . "%");
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "list") {
            $user->where("contacts.list_id", "=", $criteria["list"]);
        }
        if (isset($criteria["search_criteria"]) && $criteria["search_criteria"] == "signup_date") {
            $user->where("users.created_at", ">=", format_date($criteria["from_signup"], "Y-m-d H:i:s"));
            $user->where("users.created_at", "<=", format_date($criteria["to_signup"], "Y-m-d H:i:s"));
        }

        $user->orderBy($sort_by, $sort_order);
        $user->groupBy('contacts_id');
        return $user->orderBy($sort_by, $sort_order)->paginate($limit);
        // dd(\DB::getQueryLog());
    }

    public function delete($id)
    {
        return Contacts::find($id)->delete();
    }

    public function getLandingpageList()
    {
        $list = Landingpage_list::with("project")->get();
        $user_id = Auth::user()->user_id;
        $return = [];
        if (!empty($list)) {
            foreach ($list as $val) {
                if (isset($val->project->project_id)) {
                    $return[$val->list_id] = $val->name;
                }
            }
        }
        $list = Landingpage_list::whereNull("project_id")->where("user_id", $user_id)->get();
        if (!empty($list)) {
            foreach ($list as $val) {
                $return[$val->list_id] = $val->name;
            }
        }
        asort($return);
        return $return;
    }

    public function getContactList($list_id)
    {
        return Contacts::whereIn("list_id", $list_id)->groupBy("email")->get();
    }

    public function sendEmail($id = 0)
    {
        if(empty($id)){
            $email_campaign = Email_campaign::with("template")->where("send_on", "<=", date("Y-m-d H:i:s"))->where("is_sent", "0")->get();
        }else{
            $email_campaign = Email_campaign::with("template")->where("id",$id)->where("is_sent", "0")->get();
        }

        foreach ($email_campaign as $email) {

            if ($email->contact_type == "list") {
                $list = Email_campaign_contact::where("email_campaign_id", $email->id)->select(DB::raw("group_concat(list_id) as list_id"))->first();
                if (!empty($list)) {
                    $contacts = Contacts::whereIn("list_id", explode(",",$list->list_id))->get();
                }
            } else {
                $contact = Email_campaign_contact::where("email_campaign_id", $email->id)->select(DB::raw("group_concat(contact_id) as contact_id"))->first();
                if (!empty($contact)) {
                    $contacts = Contacts::whereIn("contacts_id", explode(",",$contact->contact_id))->get();
                }
            }
            $mail_params = [];
            $mail_params['subject'] = $email->subject;

            $template = $email->template->context;
            $body = $email->body;

            if (!empty($contacts)) {
                foreach ($contacts as $val) {
                    $mail_params['to'] = $val->email;
                    $dynamicData = ['#NaME#' => $val->first_name ." ".$val->last_name,'#EMAIL#'=>$val->email];
                    \EmailService::SendEMail($mail_params, $template, $dynamicData, $body);
                }
                $update_email = Email_campaign::firstOrNew(array("id" => $email->id));
                $update_email->is_sent = 1;
                $update_email->save();
            }
        }
    }
}