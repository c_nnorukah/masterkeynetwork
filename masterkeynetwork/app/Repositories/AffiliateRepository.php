<?php

namespace App\Repositories;

use App\Interfaces\AffiliateRepositoryInterface;
use App\Models\User;
use Request;
use App\Models\Campaign;
use App\Models\Affiliate;
use App\Models\PaymentLevelHistory;
use App\Models\PaypalPaymentSettings;
use App\Models\ReferralPaymentHistory;

class AffiliateRepository implements AffiliateRepositoryInterface
{
    public function __construct()
    {

    }

    public function saveAffiliate(User $user)
    {
        /* $affiliate_data = array_only($user->toArray(), ['first_name', 'last_name', 'email_address']);
         $affiliate = new Affiliate($affiliate_data);
         $affiliate->affiliate_type = "user";
         $affiliate->ip = Request::getClientIp();
         $user->affiliates()->save($affiliate);
         return $affiliate;*/
        //$referral_id;
        // $referral_id = User::whereReferralCode($referral_code)->first();
    }

    /**
     * @param integer $campaign_code
     * @return array
     */
    public function getAffiliaterUserDetails($referral_code)
    {
        return User::whereReferralCode($referral_code)->first();
        /*  return Campaign::with(['affiliate', 'affiliate.user'])
              ->whereCampaignCode($campaign_code)
              ->first();*/
    }

    public function addReferralSale($user_id)
    {
        $payment_level = PaymentLevelHistory::orderBy("created_at", "desc")->first();
        if (empty($payment_level)) {
            die("Value not set for payment levels");
        }
        $level1_payment = $payment_level->payment_level_1;
        $level2_payment = $payment_level->payment_level_2;
        $level3_payment = $payment_level->payment_level_3;
        $user_history = ReferralPaymentHistory::select(\DB::raw("group_concat(user_id) as user_id"))->get();
        $user_history = explode(",", $user_history[0]->user_id);
        $users = User::where("user_id", $user_id)->whereNotNull("referral_id");
        if (!empty($user_history))
            $users = $users->whereNotIn("user_id", $user_history);
        $user = $users->first();
        if(!empty($user)) {
            $level1_userid = $user->referral_id;
            $level2_userid = User::where("user_id", $level1_userid)->select("referral_id")->first();
            if (!empty($level2_userid)) {
                $level2_userid = $level2_userid->referral_id;
            } else {
                $level2_userid = null;
            }
            $level3_userid = User::where("user_id", $level2_userid)->select("referral_id")->first();
            if (!empty($level3_userid)) {
                $level3_userid = $level3_userid->referral_id;
            } else {
                $level3_userid = null;
            }
            $referral_payment = New ReferralPaymentHistory();
            $referral_payment->user_id = $user->user_id;
            $referral_payment->level1_userid = $level1_userid;
            $referral_payment->level1_payment = $level1_payment;
            $referral_payment->level2_userid = $level2_userid;
            $referral_payment->level2_payment = empty($level2_userid) ? null : $level2_payment;
            $referral_payment->level3_userid = $level3_userid;
            $referral_payment->level3_payment = empty($level3_userid) ? null : $level3_payment;
            $referral_payment->save();
        }
    }

    public function referralPayment($user_id)
    {
        $latest_payment_date = PaypalPaymentSettings::orderBy("created_at", "desc")->first();
        if (empty($latest_payment_date)) {
            die("No payment date set");
        }
        $payment_level = PaymentLevelHistory::where("created_at", ">=", $latest_payment_date->created_at)->orderBy("created_at", "asc")->first();
        if (empty($payment_level)) {
            $payment_level = PaymentLevelHistory::where("created_at", "<=", $latest_payment_date->created_at)->orderBy("created_at", "desc")->first();
        }
        if (empty($payment_level)) {
            die("Value not set for payment levels");
        }
        $level1_payment = $payment_level->payment_level_1;
        $level2_payment = $payment_level->payment_level_2;
        $level3_payment = $payment_level->payment_level_3;
        $previous_month = date('Y-m-d H:i:s', strtotime("-30 days"));
        $user_history = ReferralPaymentHistory::select(\DB::raw("group_concat(user_id) as user_id"))->get();
        $user_history = explode(",", $user_history[0]->user_id);
        \DB::enableQueryLog();
        $users = User::where("created_at", ">=", $previous_month)->whereNotNull("referral_id")->where("user_id", $user_id);
        if (!empty($user_history))
            $users = $users->whereNotIn("user_id", $user_history);
        $users = $users->get();
        $payment = [];
        foreach ($users as $user) {
            $level1_userid = $user->referral_id;
            $payment_array = [];
            $payment_array['amount'] = $level1_payment;
            $payment_array['downline_member'] = $user->first_name . " " . $user->last_name;
            $payment_array['user_id'] = $user->referral_id;

            $level2_userid = User::where("user_id", $level1_userid)->select("referral_id")->first();
            $level1_payment_response = $this->send_payout($payment_array);
            $level2_payment_response = $level3_payment_response = null;
            if (!empty($level2_userid)) {
                $level2_userid = $level2_userid->referral_id;
                $payment_array['amount'] = $level2_payment;
                $payment_array['user_id'] = $level2_userid->referral_id;
                $level2_payment_response = $this->send_payout($payment_array);
            } else {
                $level2_userid = null;
            }
            $level3_userid = User::where("user_id", $level2_userid)->select("referral_id")->first();
            if (!empty($level3_userid)) {
                $payment_array['amount'] = $level3_payment;
                $payment_array['user_id'] = $level3_userid->referral_id;
                $level3_userid = $level3_userid->referral_id;
                $level3_payment_response = $this->send_payout($payment_array);
            } else {
                $level3_userid = null;
            }
            $referral_payment = New ReferralPaymentHistory();
            $referral_payment->user_id = $user->user_id;
            $referral_payment->level1_userid = $level1_userid;
            $referral_payment->level1_payment = $level1_payment;
            $referral_payment->level1_payment_response = $level1_payment_response;
            $referral_payment->level2_userid = $level2_userid;
            $referral_payment->level2_payment = empty($level2_userid) ? null : $level2_payment;
            $referral_payment->level_payment_response = $level2_payment_response;
            $referral_payment->level3_userid = $level3_userid;
            $referral_payment->level3_payment = empty($level3_userid) ? null : $level3_payment;
            $referral_payment->level3_payment_response = $level3_payment_response;
            $referral_payment->save();
            $payment[] = $referral_payment->toArray();
        }
    }

    public function send_payout($user_array)
    {

        $payment_array['body'] = "You received the $" . $user_array['amount'] . " payment for your downline member " . $user_array['name'] . ".";
        $payment_array['email'] = User::where("user_id", $user_array['user_id'])->select("email")->first();
        $payment_array['email'] = !empty($payment_array['email']) ? $payment_array['email']["email"] : "";
        if (empty($payment_array['email'])) {
            return "No paypal email address found";
        }
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                config('payment.CLIENT_ID'),     // ClientID
                config('payment.SECRET_KEY')      // ClientSecret
            )
        );
        $payouts = new \PayPal\Api\Payout();
        $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
            ->setEmailSubject("You have a Affiliate Payment!");
        $senderItem = new \PayPal\Api\PayoutItem();
        $senderItem->setRecipientType('email')
            ->setNote($payment_array['body'])
            ->setReceiver($payment_array['email'])
            ->setAmount(new \PayPal\Api\Currency('{
                        "value":"' . $payment_array['amount'] . '",
                        "currency":"' . config('payment.CURRENCY_CODE_TYPE') . '"
                    }'));

        $payouts->setSenderBatchHeader($senderBatchHeader)
            ->addItem($senderItem);
        $request = clone $payouts;
        try {
            $output = $payouts->createSynchronous($apiContext);
            return $output;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
}
