<?php

namespace App\Repositories;

use App\Interfaces\CampaignRepositoryInterface;
use App\Models\Affiliate;
use App\Models\Campaign;
use App\Models\User;
use Cookie;

class CampaignRepository implements CampaignRepositoryInterface
{
    public function __construct()
    {

    }

    public function saveCampaign(Affiliate $affiliate, User $user)
    {
        $username = trim(clean_url_values($user->first_name)) . trim(clean_url_values($user->last_name));
        $code = $username . format_date($user->birth_date, "m") . format_date($user->birth_date, "d");
        $campaign_code = Campaign::whereCampaignCode($code)->first();
        if (!empty($campaign_code)) {
            $code .= "-" . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
        }
        $campaign = new Campaign();
        $campaign->affiliate_id = $affiliate->affiliate_id;
        $campaign->campaign_code = $code;
        $campaign_data = ["campaign_name" => 'user',
            "campaign_source" => $user->first_name . "-" . $code,
            "campaign_medium" => 'user-link'];
        $campaign->campaign_data = json_encode($campaign_data);
        $campaign->campaign_type = "user";
        $campaign->cookie_expire_days = USER_REFERRAL_COOKIE_EXPIRE_DAYS;
        $campaign->save();
        return $campaign;
    }

    public function getdetailByCampaignCode($campaign_code)
    {
        return Campaign::whereCampaignCode($campaign_code)->first();
    }

    public function getCampaignData($user_id)
    {

        $campaign_data = Campaign::whereHas('affiliate', function ($query) use ($user_id) {
            $query->where('affiliate.affiliate_type', '=', Affiliate::USER_TYPE_AFFILIATE)
                ->where('affiliate.user_id', '=', $user_id);
        })
            ->remember(cacheTimeOut(CAMPAIGN_CACHE_TIMEOUT))
            ->cacheTags(TAG_CAMPAIGN)
            ->first();

        return $campaign_data;

    }


}
