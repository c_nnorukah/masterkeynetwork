<?php namespace App\Repositories;

use App\Models\LandingPage;
use App\Models\LandingPageTemplate;
use App\Models\UserLandingPageTemplate;
use Illuminate\Database\Eloquent\Model;

class LandingPageRepository
{

	function __construct(LandingPage $landingPage)
	{
		$this->model = $landingPage;
	}

	public function store($Inputs)
	{
		$landingPage = new LandingPage($Inputs);
		$landingPage->save();
		return $landingPage;
	}

	public function update($Inputs, $Id)
	{
		$landingPage = $this->model->find($Id)->update($Inputs);
		return $landingPage;
	}

	public function getLandingPages()
	{
		return $this->model->all();
	}

	public function getLandingPagesTemplates()
	{
		return LandingPageTemplate::all();
	}

	public function getUserLandingPages()
	{
		return UserLandingPageTemplate::with('user');
	}

	public function getUserLandingPagesByUser($user_id)
	{
		return UserLandingPageTemplate::where('created_by', $user_id);
	}

	public function getLandingPageById($id)
	{
		return $this->model->find($id);
	}

	public function getAdminLandingPageById($id)
	{
		return LandingPageTemplate::find($id);
	}

	public function getUserLandingPageById($id)
	{
		return UserLandingPageTemplate::find($id);
	}

	public function deleteLandingPageById($id)
	{
		return $this->model->find($id)->delete();
	}

	public function deleteUserLandingPageById($id)
	{
		return UserLandingPageTemplate::find($id)->delete();
	}

	public function searchLandingPages($Input)
	{
		$data = $this->model->where('title', 'LIKE', $Input.'%')->get();
		return $data;
	}

	public function searchUserLandingPages($Input)
	{
		$data = UserLandingPageTemplate::with('user')->where('title', 'LIKE', $Input.'%');
		return $data;
	}

	public function saveUserTemplate($Inputs)
	{
		$landingPage = new UserLandingPageTemplate($Inputs);
		$landingPage->save();
		return $landingPage;
	}

	public function updateUserLandingPage($Inputs, $Id)
	{
		$landingPage = UserLandingPageTemplate::find($Id)->update($Inputs);
		return $landingPage;
	}
}