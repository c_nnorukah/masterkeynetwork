<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseMaterial;
use App\Interfaces\CourseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Config;

class CourseRepository implements CourseRepositoryInterface
{
    protected $course_category;
    protected $course;
    protected $course_material;

    function __construct(Course $course, CourseMaterial $material, CourseCategory $courseCategory)
    {
        $this->course = $course;
        $this->course_material = $material;
        $this->course_category = $courseCategory;
    }

    public function store(Course $c)
    {
        $c->save();
        return $c;
    }

    public function storeMaterial($Inputs)
    {
        //dd($Inputs);
        if (!empty($Inputs)) {
            if (isset($Inputs[0]['course_id'])) {
                CourseMaterial::where('type', "2")->where('course_id', $Inputs[0]['course_id'])->delete();
            }
            foreach ($Inputs as $val) {

              CourseMaterial::create($val);
            }
            return true;
        } else {
            return false;
        }
    }

    public function All()
    {
        if (Auth::user()) {
            return $this->course->with('coursematerial')->get();
        }
    }



    public function CourseDetail($courseId)
    {
        return $this->course->with(['coursematerial', 'coursecategory'])->find($courseId);
    }

    public function DeleteCourse($id)
    {
        $course = Course::find($id);
        return $course->delete();
    }

    public function DeleteMaterial($id)
    {
        $material = CourseMaterial::find($id);
        return $material->delete();
    }

    public function GetCategory($term)
    {
        $category = CourseCategory::where('category_name','like',"%$term%")->get()->lists('category_name','category_id');
        return $category;
    }
}