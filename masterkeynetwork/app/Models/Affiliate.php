<?php
namespace App\Models;

/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 5/2/2016
 * Time: 11:14 AM
 */
class Affiliate extends Model
{
    public $timestamps = false;

    protected $table      = 'affiliate';
    protected $primaryKey = 'affiliate_id';
    protected $fillable = ['affiliate_type', 'affiliate_status', 'first_name', 'last_name', 'email', 'user_id', 'ip'];



    public static $fundraiser_rules = ['first_name'           => 'Required',
        'last_name'            => 'Required',
        'email_address'        => 'Required|email',
        'organization_website' => 'Url',
        'phone_number'         => 'Numeric'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function campaign()
    {
        return $this->hasMany('App\Models\Campaign');
    }
}
