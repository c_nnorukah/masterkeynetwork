<?php

namespace App\Models;

class sys_module extends Model
{
	protected $table = 'sys_module';
	protected $primaryKey = 'sys_module_id';
	protected $fillable = ['module_key','module_name'];
}
