<?php


namespace App\Models;

class State extends Model
{
    protected $table = 'state';
    protected $primaryKey = 'state_id';
    protected $fillable = ['state_name', 'state_code', 'zone'];
}
