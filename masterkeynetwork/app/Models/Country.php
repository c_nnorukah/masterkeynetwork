<?php

namespace App\Models;

class Country extends Model
{
    protected $table      = 'country';
    protected $primaryKey = 'country_id';

    public function states()
    {
        return $this->hasMany('App\Models\State', 'country_id')->orderBy('state_name');
    }
    protected $fillable = ['country_name', 'country_iso_code_2', 'country_iso_code_3'];
}
