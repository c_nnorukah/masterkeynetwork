<?php

namespace App\Models;

class PaymentLevelHistory extends Model
{
	protected $table = 'payment_level_history';
	protected $primaryKey = 'payment_level_history_id';
	protected $fillable = ['payment_level_1', 'payment_level_2', 'payment_level_3'];
}
