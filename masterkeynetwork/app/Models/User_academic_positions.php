<?php

namespace App\Models;

class User_academic_positions extends Model
{
	protected $fillable = ['title', 'from_date', 'to_date', 'summary', 'user_id'];

	protected $table = 'user_academic_positions';
	public function users()
	{
		return $this->belongsTo(User::class, "user_id", "user_id");
	}

}
