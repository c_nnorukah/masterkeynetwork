<?php

namespace App\Models;

class User_education_details extends Model
{		protected $fillable = ['degree', 'title', 'university', 'passing_year', 'user_id'];

	protected $table = 'user_education_details';

	public function users()
	{
		return $this->belongsTo(User::class, "user_id", "user_id");
	}

}
