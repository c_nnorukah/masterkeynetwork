<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Course;

class CourseCategory extends Model
{
    protected $table = 'course_category';
    protected $primaryKey = 'category_id';
    protected $fillable = ['category_name'];

    public $timestamps = false;

    public function course()
    {
        return $this->hasMany('App\Models\Course', 'category_id', 'category_id');
    }
    public function totalRow()
    {
        return $this->get()->count();
    }
}
