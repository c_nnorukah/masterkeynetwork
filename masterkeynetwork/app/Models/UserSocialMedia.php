<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocialMedia extends Model
{
	protected $table = "user_social_media";
	public $timestamps = false;
	protected $fillable = [
		'type_id', 'access_token', 'user_id','user_name','user_dp','screen_name'
	];
}
