<?php
namespace App\Models;

/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 5/2/2016
 * Time: 11:15 AM
 */
class Campaign extends Model
{
    protected $table      = 'campaign';
    protected $primaryKey = 'campaign_id';

    public $timestamps = false;

    public function affiliate()
    {
        return $this->hasOne('App\Models\Affiliate', 'affiliate_id', 'affiliate_id');
    }

}
