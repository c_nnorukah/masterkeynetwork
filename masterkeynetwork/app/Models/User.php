<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $fillable = ['first_name', 'last_name', 'user_name', 'email', 'password', 'status', 'phone_number', 'country_id', 'state_id', 'plan_id','usertoken','confirmation_code','country_dial_code','referral_id','referral_code'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $guarded = ['password_confirmation', 'terms_and_cond'];
    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */

    // Auto Hydrate
    public $autoPurgeRedundantAttributes = true;
    public $autoHashPasswordAttributes = true;

    public static $passwordAttributes = ['password'];
    public static $registration_rules = ['first_name' => 'Required',
        'last_name' => 'Required',
        'email' => 'Required|email|unique:customer',
        'password' => 'Required|confirmed',
        'password_confirmation' => 'Required',
        'terms_and_cond' => 'accepted'];

    public static $subscription_rules = ['first_name' => 'Required',
        'last_name' => 'Required',
        'email' => 'Required|Email'];

    public static $change_password_rules = ['old_password' => 'Required|correct_current_password',
        'password' => 'Required|no_same_password:old_password',
        'password_confirmation' => 'Required|same:password'];

    public static $change_password_messages = ['correct_current_password' => 'Please Enter Correct Current Password.',
        'no_same_password' => 'Current Password and New Password cannot be same'];

    public static $manage_account_rules = ['first_name' => 'Required',
        'last_name' => 'Required',
        'email' => 'Required|Email',
        'phone_number' => 'regex:/^[0-9\-+.()\s]+$/i'];

    public static $notify_email_rules = ['notify_email_address' => 'Required|Email'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //used for hashing the password when customer sign up
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function roles()
    {
       return $this->hasOne(AssignedRole::class, 'user_id',"user_id");
    }

    public function plan()
    {
        return $this->hasOne('App\Models\UserPlan', 'plan_id','plan_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'country_id');
    }

    public function user_portfolios()
    {
        return $this->hasMany(User_Portfolio::class, "user_id", "user_id");
    }
    public function user_medias()
    {
        return $this->belongsTo(User_media::class, "image_id", "id");
    }
    public function User_academic_positions()
    {
        return $this->hasMany(User_academic_positions::class, "user_id", "user_id")->orderBy('to_date', 'DESC');
    }
    public function User_education_details()
    {
        return $this->hasMany(User_education_details::class, "user_id", "user_id")->orderBy('passing_year', 'DESC');
    }
    public function User_honors()
    {
        return $this->hasMany(User_honors::class, "user_id", "user_id")->orderBy('date', 'DESC');
    }
    public function user_projects()
    {
        return $this->hasMany(User_project::class, "user_id", "user_id");
    }
    public function image(){

        return $this->hasOne('App\Models\Image', 'id');

    }
    public function user_media(){

        return $this->hasOne('App\Models\User_media', 'id');

    }
    public function landing_page_response(){

        return $this->hasMany(Landing_page_response::class, "user_id", "user_id");

    }
    public function assignedRoles(){

        return $this->hasOne(AssignedRole::class, "user_id","user_id");
    }
    public function paymentHistory(){

        return $this->hasMany(PaymentHistory::class, "user_id", "user_id");

    }
    public function affiliates()
    {
        return $this->hasMany(\Affiliate::class);
    }
    public function subcategories() {
        return $this->hasMany(User::class,'referral_id');
    }
}
