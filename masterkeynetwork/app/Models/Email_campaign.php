<?php

namespace App\Models;

class Email_campaign extends Model
{
	protected $table = 'email_campaign';
	protected $primaryKey = 'id';
	public function template()
	{
		return $this->belongsTo(EmailTemplate::class, 'template_id',"email_template_id");
	}
}
