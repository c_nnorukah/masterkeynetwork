<?php

namespace App\Models;

class ContactApi extends Model
{
    protected $table = 'contacts_api';
    protected $guarded = ['contacts_api'];
    protected $primaryKey = 'contacts_api_id';
    protected $fillable = ['user_id', 'mail_chimp', 'get_response', 'aweber','constant_contact'];
}
