<?php

namespace App\Models;

class Role extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'slug'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'assigned_roles');
    }
}
