<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Image extends Model
{
    protected $table = 'images';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'image_name','image_path'];

    public $timestamps = false;

    public function user()
    {
    }
}
