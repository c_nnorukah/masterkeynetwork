<?php

namespace App\Models;

class UserPlan extends Model
{
    protected $table = 'user_plans';
    protected $primaryKey = 'plan_id';
    protected $fillable = ['plan_name','price','days','description','interval'];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'plan_id');
    }

}
