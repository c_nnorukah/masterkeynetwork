<?php

namespace App\Models;

class User_honors extends Model
{
	protected $fillable = ['title', 'details', 'date', 'image_id', 'user_id'];

	protected $table = 'user_honors';

	public function users()
	{
		return $this->belongsTo(User::class, "user_id", "user_id");
	}
	public function user_medias()
	{
		return $this->belongsTo(User_media::class, "image_id", "id");
	}
}
