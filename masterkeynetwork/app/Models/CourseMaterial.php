<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Course;

class CourseMaterial extends Model
{
    protected $table = 'course_material';
    protected $primaryKey = 'course_material_id';
    protected $foreign_key='course_id';
    protected $fillable = ['course_material_id','course_id', 'material_name', 'material_url', 'type', 'is_default','material_key','material_video_src'];
    public $timestamps = false;

    public function course()
    {
        return $this->belongsTo(' App\Models\Course','course_id','course_id');
    }



}
