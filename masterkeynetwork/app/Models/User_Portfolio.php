<?php

namespace App\Models;

class User_Portfolio extends Model
{
	protected $table = 'user_portfolios';

	protected $fillable = ['title', 'summary', 'user_id', 'image_id'];


	public function users()
	{
		return $this->belongsTo(User::class, "user_id", "user_id");
	}



	public function user_medias()
	{
		return $this->belongsTo(User_media::class, "image_id", "id");
	}
}
