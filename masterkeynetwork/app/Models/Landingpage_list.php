<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class Landingpage_list extends Model
{
	protected $table = 'landingpage_list';
	protected $primaryKey = 'list_id';
	protected $fillable = ["name"];
	public function project()
	{
		return $this->belongsTo(User_project::class, 'project_id',"project_id")->where("user_id",Auth::user()->user_id);
	}
}
