<?php

/**
 *  Main model which is extend by every model file
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel
{
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}
