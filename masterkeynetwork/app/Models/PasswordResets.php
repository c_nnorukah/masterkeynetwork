<?php
/**
 * Created by PhpStorm.
 * User: murtaza.vhora
 * Date: 4/5/2016
 * Time: 3:02 PM
 */
namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class PasswordResets extends Authenticatable
{
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
         protected $table = 'password_resets';
         protected $fillable = ['email', 'token','created_at'];

}