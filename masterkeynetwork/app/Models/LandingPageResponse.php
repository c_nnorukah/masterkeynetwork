<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageResponse extends Model
{
    protected $table = 'landing_page_response';
    protected $primaryKey = 'landing_page_response_id';
    protected $fillable = ['landing_page_response_id', 'landingpage_id','response'];
    public $timestamps = false;

}
