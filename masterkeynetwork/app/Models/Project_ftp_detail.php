<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project_ftp_detail extends Model
{
	protected $table = 'project_ftp_detail';
	public function projects()
	{
		return $this->belongsTo(Project::class, "project_id", "id");
	}
}
