<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageTemplate extends Model
{
    protected $table = 'landing_page_templates';
    protected $primaryKey = 'landing_page_id';
    protected $fillable = ['title', 'content'];
    public $timestamps = false;


}
