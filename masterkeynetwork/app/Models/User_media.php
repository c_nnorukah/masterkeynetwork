<?php

namespace App\Models;

class User_media extends Model
{
	protected $table = 'user_medias';
	protected $primaryKey = 'id';
	protected $fillable = ['id', 'image_name','image_path'];

	public $timestamps = false;
	public function user_portfolios()
	{
		return $this->belongsTo(User_Portfolio::class, "image_id", "id");
	}
}
