<?php

namespace App\Models;

class Contacts extends Model
{
	protected $table = 'contacts';
	protected $primaryKey = 'contacts_id';
	protected $fillable = ['first_name', 'last_name', 'email', 'user_id'];
	public function user()
	{
		return $this->hasOne(User::class, 'user_id',"user_id");
	}
	public function contact_list()
	{
		return $this->hasOne(Landingpage_list::class, 'list_id',"list_id");
	}
}
