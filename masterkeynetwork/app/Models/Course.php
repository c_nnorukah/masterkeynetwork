<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CourseMaterial;
use App\Models\CourseCategory;

class Course extends Model
{
    protected $table = 'course';
    protected $primaryKey = 'course_id';
    protected $fillable = ['course_id', 'course_title', 'course_description', 'created_by','category_id'];

    public function coursematerial()
    {
        return $this->hasMany('App\Models\CourseMaterial', 'course_id', 'course_id');
    }
    public function coursecategory()
    {
        return $this->belongsTo('App\Models\CourseCategory','category_id','category_id');
    }
}
