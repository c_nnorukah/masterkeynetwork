<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $table = 'projects';
	protected $primaryKey='id';
	public function user_projects()
	{
		return $this->hasOne(User_project::class, "project_id", "id");

	}
	public function landing_page_response()
	{
		return $this->hasMany(Landing_page_response::class, "id", "landingpage_id");
	}

}
