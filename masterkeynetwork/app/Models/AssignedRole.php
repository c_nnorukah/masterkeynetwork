<?php

namespace App\Models;

class AssignedRole extends Model
{
    protected $table = 'assigned_roles';
    protected $primaryKey = 'assigned_role_id';
    protected $fillable = ['user_id', 'role_id'];
    public $timestamps = false;
}
