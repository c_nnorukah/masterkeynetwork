<?php

namespace App\Models;

class PaypalPaymentSettings extends Model
{
	protected $table = 'admin_paypal_setting';
	protected $primaryKey = 'paypal_payment_setting_id';
	protected $fillable = ['paypal_email', 'payment_currency', 'message', 'paypal_IPN_URL', 'paypal_API_username', 'paypal_API_signature', 'paypal_API_password', 'paypal_API_URL', 'payment_date'];

}
