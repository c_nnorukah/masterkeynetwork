<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Landing_page_response extends Model
{
    protected $table = 'landing_page_response';
    protected $primaryKey = 'landingpage_id';

    public function projects()
    {
        return $this->belongsTo(Project::class, "landingpage_id", "id");

    }
   /* public function user()
    {
        return $this->belongsTo(' App\Models\User','user_id','user_id');
    }*/
}
