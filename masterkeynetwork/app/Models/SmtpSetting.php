<?php

namespace App\Models;

class SmtpSetting extends Model
{

	protected $table = 'smtp_setting';
	protected $primaryKey = 'smtp_setting_id';
	public $timestamps = false;
	protected $fillable = ['email_for_smtp', 'password', 'port', 'domain','user_name','user_id','title'];
}
