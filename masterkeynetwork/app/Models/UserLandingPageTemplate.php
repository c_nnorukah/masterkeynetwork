<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLandingPageTemplate extends Model
{
    protected $table = 'user_landing_page_templates';
    protected $primaryKey = 'user_landing_page_id';
    protected $fillable = ['title', 'content', 'created_by', 'preview_image'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

}
