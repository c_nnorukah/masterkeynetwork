<?php

namespace App\Models;

use App\Models\SmtpSetting;
use Illuminate\Database\Eloquent\Model;

class AutoResponder extends Model
{
    protected $table = 'auto_responders';
    protected $primaryKey = 'auto_responders_id';
    protected $fillable = ['landingpage_id', 'from_name','from_email','subject','email_body'];

    public function smtp_setting()
    {
        return $this->hasOne(SmtpSetting::class, 'smtp_setting_id', 'smtp_setting_id');
    }
}
