<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;

class User_project extends Model
{
	protected $table = 'users_projects';
	public function users()
	{
		return $this->belongsTo(User::class, "user_id", "user_id");
	}
	public function projects()
	{
		return $this->belongsTo(Project::class, "project_id", "id");
	}
}
