<?php

namespace App\Models;

class PaymentHistory extends Model
{
	protected $table = 'payment_history';

	protected $fillable = ['profile_id','user_id', 'plan_id', 'amount', 'transaction_response'];
	public function plan()
	{
		return $this->hasOne(UserPlan::class, 'plan_id','plan_id');
	}
}
