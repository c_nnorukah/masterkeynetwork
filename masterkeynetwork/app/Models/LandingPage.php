<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    protected $table = 'landing_pages';
    protected $primaryKey = 'landing_page_id';
    protected $fillable = ['title', 'content', 'custom_css', 'custom_js'];


}
