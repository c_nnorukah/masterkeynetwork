<?php

namespace App\Models;

class EmailTemplate extends Model
{
	protected $table = 'email_templates';
    protected $primaryKey = 'email_template_id';
	protected $fillable = ['content','subject','created_by'];

	public $timestamps = false;

	public function getVariables()
	{
		return $this->hasMany('App\Models\EmailTemplateVariable')->whereNotNull('email_template_id');
	}

	public function getAuthor()
	{
		return $this->belongsToMany('App\Models\User')->whereNotNull('id');
	}
}