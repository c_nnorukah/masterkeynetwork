<?php

namespace App\Models;

class EmailTemplateVariable extends Model
{
	protected $table = 'email_templates_variables';

	public function emailtemplate()
	{
		return $this->belongsToMany('App\Models\EmailTemplate')->whereNotNull('email_template_id');
	}
}