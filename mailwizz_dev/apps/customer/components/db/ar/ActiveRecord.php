<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * ActiveRecord
 * 
 * @package MailWizz EMA
 * @author Serban George Cristian <cristian.serban@mailwizz.com> 
 * @link http://www.mailwizz.com/
 * @copyright 2013-2015 MailWizz EMA (http://www.mailwizz.com)
 * @license http://www.mailwizz.com/license/
 * @since 1.0
 */
 
class ActiveRecord extends BaseActiveRecord
{
    protected function beforeSave()
    {
        Yii::app()->db->createCommand('set foreign_key_checks=0')->execute();
        return parent::beforeSave();
    }

    protected function afterSave()
    {
        Yii::app()->db->createCommand('set foreign_key_checks=1')->execute();
        return parent::afterSave();
    }

}